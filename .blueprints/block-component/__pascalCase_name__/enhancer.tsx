import { State } from "@src/store";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import {
  Component,
  FunctionProps,
  ParentProps,
  StoreProps,
  LocalProps
} from "./{{pascalCase name}}";

function getStoreProps(state: State, props: ParentProps): StoreProps {
  return {};
}

function getFunctionProps(
  dispatch: Dispatch,
  props: ParentProps
): FunctionProps {
  return {};
}

function mergeProps(
  storeProps: StoreProps,
  functionProps: FunctionProps,
  parentProps: ParentProps
): StoreProps & FunctionProps & ParentProps {
  return {};
}

export const Container = connect(
  getStoreProps,
  getFunctionProps,
  mergeProps
)(Component);
