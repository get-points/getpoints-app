import {Component}  from "./component";
import {Container} from "./enhancer";

export const {{pascalCase name}} = Container;
