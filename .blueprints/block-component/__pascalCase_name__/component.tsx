import React from "react";
import { Text, View } from "react-native";
import glamorous from "glamorous-native";
import { StyleProp, ViewStyle } from "react-native";

/**
 * Component Properties
 */

export interface FunctionProps {}

export interface StoreProps {}

export interface ParentProps {
  style?: StyleProp<ViewStyle>
}

export interface LocalProps {}

type ComponentProps = FunctionProps & StoreProps & ParentProps & LocalProps;



/**
 * Styled Components
 */

const {{pascalCase name}}Container = glamorous(View)((props: {}) => ({
  display: "flex",
  backgroundColor: "lightgrey",
  padding: 10
}));




/**
 * Component Definition
 */

export class Component extends React.Component<ComponentProps> {
  render() {
    return <{{pascalCase name}}Container style={this.props.style}>
        <Text>{{pascalCase name}}</Text>
      </{{pascalCase name}}Container>
  }
}
