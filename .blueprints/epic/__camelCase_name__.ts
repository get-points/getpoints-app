import { actions } from "@src/actions";
import { filter, mergeMap, map, catchError } from "rxjs/operators";
import { isActionOf } from "typesafe-actions";
import { Epic } from "@src/types/Epic";

export const get{{pascalCase name}}: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.get{{pascalCase name}}.request)),
    mergeMap(async a => {
      try {
        const b = await api.{{camelCase name}}.getOne(a.payload.id);
        return actions.get{{pascalCase name}}.success({ [b.id]: b }, a.meta);
      } catch (e) {
        let message = "An error occurred while retrieving the {{camelCase name}}.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.get{{pascalCase name}}.failure(
          { request: a.payload, message },
          a.meta
        );
      }
    })
  );

export const get{{pascalCase name}}List: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.get{{pascalCase name}}List.request)),
    mergeMap(async a => {
      try {
        const b = await api.{{camelCase name}}.getMany(a.payload.ids);
        return actions.get{{pascalCase name}}List.success(
          b.reduce((map, x) => ({ ...map, [x.id]: x }), {}),
          a.meta
        );
      } catch (e) {
        let message = "An error occurred while retrieving the {{camelCase name}} list.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.get{{pascalCase name}}List.failure(
          { request: a.payload, message },
          a.meta
        );
      }
    })
  );
