import { action } from "@tyro";
import { {{pascalCase name}} } from "@src/types/Models";

export const get{{pascalCase name}}List = {
  request: action("GET_{{upperSnakeCase name}}_LIST/REQUEST")<{ ids: string[] }>(),
  success: action("GET_{{upperSnakeCase name}}_LIST/SUCCESS")<{ [id: string]: {{pascalCase name}} }>(),
  failure: action("GET_{{upperSnakeCase name}}_LIST/FAILURE")<{
    request: { ids: string[] };
    message: string;
  }>()
};

// single {{camelCase name}} action
export const get{{pascalCase name}} = {
  request: action("GET_{{upperSnakeCase name}}/REQUEST")<{ id: string }>(),
  success: action("GET_{{upperSnakeCase name}}/SUCCESS")<{ [id: string]: {{pascalCase name}} }>(),
  failure: action("GET_{{upperSnakeCase name}}/FAILURE")<{
    request: { id: string };
    message: string;
  }>()
};

export const clearAll{{pascalCase name}}Errors = action("CLEAR_ALL_{{upperSnakeCase name}}_ERRORS")<{}>();

export const clear{{pascalCase name}}Error = action("CLEAR_{{upperSnakeCase name}}_ERROR")<{
  requestID: number;
}>();
