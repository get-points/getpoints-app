import { {{pascalCase name}} } from "@src/types/Models";

export interface API {
  getOne: (id: string) => Promise<{{pascalCase name}}>;
  getMany: (ids: string[]) => Promise<{{pascalCase name}}[]>;
}
