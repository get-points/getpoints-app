import { Action, actions } from "@src/actions";
import { getType } from "typesafe-actions";
import {
  LoadableFactory,
  LoadableContainer,
  makeLoadableContainer
} from "@tyro";
import { {{pascalCase name}} } from "@src/types/Models";

export type {{pascalCase name}}State = LoadableContainer<{{pascalCase name}}>;

export const {{camelCase name}}Reducer = function(
  state: {{pascalCase name}}State = makeLoadableContainer<{{pascalCase name}}>(),
  action: Action
): {{pascalCase name}}State {
  switch (action.type) {
    case getType(actions.get{{pascalCase name}}List.request):
      return state.request(action);

    case getType(actions.get{{pascalCase name}}.request):
      return state.requestOne(action);

    case getType(actions.get{{pascalCase name}}List.success):
    case getType(actions.get{{pascalCase name}}.success):
      return state.receive(action);

    case getType(actions.get{{pascalCase name}}List.failure):
    case getType(actions.get{{pascalCase name}}.failure):
      return state.fail(action);

    case getType(actions.clearAll{{pascalCase name}}Errors):
      return state.clearErrors();

    case getType(actions.clear{{pascalCase name}}Error):
      return state.clearError(action.payload.requestID);
  }
  return state;
};
