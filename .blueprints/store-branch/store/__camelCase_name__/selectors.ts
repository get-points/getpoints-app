import { {{pascalCase name}}State } from "./index";
import { {{pascalCase name}} } from "@src/types/Models";
import { Loadable, LoadableFactory, LoadedLoadable } from "@tyro";
import { State } from "@src/store";

export const get{{pascalCase name}}State = (state: State) => state.{{camelCase name}};

export const get{{pascalCase name}}IDs = (state: State) => {
  const {{camelCase name}}State = get{{pascalCase name}}State(state);
  return Object.keys({{camelCase name}}State.items);
};

export const getOnly{{pascalCase name}} = (state: State): Loadable<{{pascalCase name}}> => {
  const {{camelCase name}}State = get{{pascalCase name}}State(state);
  const ids = get{{pascalCase name}}IDs(state);

  if ({{camelCase name}}State.pendingRequests.length > 0) return LoadableFactory.pending();

  if (ids.length < 1) return LoadableFactory.failed("No {{camelCase name}}s found");
  if (ids.length > 1)
    return LoadableFactory.failed("More than one {{camelCase name}} loaded");
  return {{camelCase name}}State.items[ids[0]];
};

export const getLoaded{{pascalCase name}}s = (state: State): {{pascalCase name}}[] => {
  const {{camelCase name}}State = get{{pascalCase name}}State(state);
  return get{{pascalCase name}}IDs(state)
    .map(id => {{camelCase name}}State.items[id])
    .filter((u): u is LoadedLoadable<{{pascalCase name}}> => u.state === "LOADED")
    .map(u => u.item);
};

export const get{{pascalCase name}}ByID = (state: State, id: string): Loadable<{{pascalCase name}}> => {
  const {{camelCase name}}State = get{{pascalCase name}}State(state);
  return {{camelCase name}}State.items[id];
};
