import React from "react";
import { Text, View } from "react-native";
import glamorous from "glamorous-native";
import { StyleProp, ViewStyle } from "react-native";
import { ReactNode } from "react-redux";

/**
 * Styled Components
 */

export const Component = glamorous(Text)((props: {}) => ({
  display: "flex",
  backgroundColor: "lightgrey",
  padding: 10
}));
