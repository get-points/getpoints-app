import {Component}  from "./component";

export const {{pascalCase name}} = Component;
