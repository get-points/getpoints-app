import { {{pascalCase name}} } from "@src/types/Models";

export interface API {
  getOne: (id: string) => Promise<{{pascalCase name}}>;
  getAll: () => Promise<{{pascalCase name}}[]>;
}
