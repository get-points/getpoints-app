// import * as Expo from "expo";
import { registerRootComponent } from "expo";
import { App } from "./src/pages/App";
// console.log({ Expo }, App);
// import StoryBook from "./storybook";

// TODO: conditionally apply this intelligently - __DEV__ doesn't seem to work.
// const main = false ? StoryBook : App;
const main = App;

registerRootComponent(main);
