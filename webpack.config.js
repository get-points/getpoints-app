const path = require("path");
const webpack = require("webpack");

const appDirectory = path.resolve(__dirname, "./");

// Many OSS React Native packages are not compiled to ES5 before being
// published. If you depend on uncompiled packages they may cause webpack build
// errors. To fix this webpack can be configured to compile to the necessary
// 'node_module'.
const babelLoaderConfiguration = {
  test: /\.js$/,
  // Add every directory that needs to be compiled by Babel during the build.
  include: [
    path.resolve(appDirectory, "src"),
    path.resolve(appDirectory, "node_modules/react-router-native"),
    path.resolve(appDirectory, "node_modules/glamorous-native"),
    path.resolve(appDirectory, "node_modules/react-native-"),
    path.resolve(appDirectory, "node_modules/@expo/samples"),
    path.resolve(appDirectory, "node_modules/@expo/vector-icons")
  ],
  use: {
    loader: "babel-loader",
    options: {
      // cacheDirectory: false,
      babelrc: false,
      // Babel configuration (or use .babelrc)
      plugins: [
        "expo-web",
        "react-native-web",
        "transform-decorators-legacy",
        // ["@babel/plugin-proposal-decorators", { legacy: true }],
        // "transform-decorators-legacy",
        [
          "transform-runtime",
          { helpers: false, polyfill: false, regenerator: true }
        ]
      ],
      // The 'react-native' preset is recommended to match React Native's packager
      presets: ["react-native"]
    }
  }
};

// This is needed for loading css
const cssLoaderConfiguration = {
  test: /\.css$/,
  use: ["style-loader", "css-loader"]
};

const imageLoaderConfiguration = {
  test: /\.(gif|jpe?g|png|svg)$/,
  use: {
    loader: "file-loader",
    options: {
      name: "[name].[ext]"
    }
  }
};

const ttfLoaderConfiguration = {
  test: /\.ttf$/,
  use: [
    {
      loader: "file-loader",
      options: {
        name: "./fonts/[hash].[ext]"
      }
    }
  ],
  include: [
    path.resolve(appDirectory, "./src/assets/fonts"),
    path.resolve(appDirectory, "node_modules/react-native-vector-icons"),
    path.resolve(appDirectory, "node_modules/@expo/vector-icons/fonts"),
    path.resolve(
      appDirectory,
      "node_modules/expo-web/node_modules/react-native-vector-icons/Fonts"
    ),
    path.resolve(
      appDirectory,
      "node_modules/expo/node_modules/@expo/vector-icons/fonts"
    )
  ]
};

const typescriptConfiguration = {
  test: /\.(ts|tsx)$/,
  loader: require.resolve("ts-loader"),
  options: {
    reportFiles: ["src/**/*.{ts,tsx}"]
  }
};

module.exports = {
  // your web-specific entry file
  entry: path.resolve(appDirectory, "main.web.tsx"),
  devtool: "cheap-module-source-map",

  // configures where the build ends up
  output: {
    filename: "bundle.js",
    publicPath: "/",
    path: path.resolve(appDirectory, "./")
  },

  devServer: {
    historyApiFallback: true
  },

  module: {
    rules: [
      babelLoaderConfiguration,
      cssLoaderConfiguration,
      imageLoaderConfiguration,
      ttfLoaderConfiguration,
      typescriptConfiguration
    ]
  },

  plugins: [
    // process.env.NODE_ENV === 'production' must be true for production
    // builds to eliminate development checks and reduce build size. You may
    // wish to include additional optimizations.
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify(
        process.env.NODE_ENV || "development"
      ),
      __DEV__: process.env.NODE_ENV === "production" || true
    })
  ],

  resolve: {
    // If you're working on a multi-platform React Native app, web-specific
    // module implementations should be written in files using the extension
    // '.web.js'.
    symlinks: false,
    extensions: [".web.js", ".js", ".ts", ".web.ts", ".tsx", ".web.tsx"],
    alias: {
      "@expo/vector-icons": "expo-web",
      expo: "expo-web",
      "glamorous-native": "glamorous",
      "react-native": "react-native-web",
      "react-native-svg": "react-native-svg-web",
      "@src": path.resolve(__dirname, "./src/"),
      "@tyro": path.resolve(__dirname, "./src/tyro"),
      "react-native-maps": path.resolve(__dirname, "./src/tyro/native-maps")
    }
  }
};
