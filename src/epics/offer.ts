import { actions } from "@src/actions";
import { filter, mergeMap, map, catchError } from "rxjs/operators";
import { isActionOf } from "typesafe-actions";
import { Epic } from "@src/types/Epic";

export const getOffer: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.getOffer.request)),
    mergeMap(async a => {
      try {
        const b = await api.offer.getOne(a.payload.id);
        return actions.getOffer.success({ [b.id]: b }, a.meta);
      } catch (e) {
        let message = "An error occurred while retrieving the offer.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.getOffer.failure(
          { request: a.payload, message },
          a.meta
        );
      }
    })
  );

export const getOfferList: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.getOfferList.request)),
    mergeMap(async a => {
      try {
        const b = await api.offer.getMany(a.payload.ids);
        return actions.getOfferList.success(
          b.reduce((map, x) => ({ ...map, [x.id]: x }), {}),
          a.meta
        );
      } catch (e) {
        let message = "An error occurred while retrieving the offer list.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.getOfferList.failure(
          { request: a.payload, message },
          a.meta
        );
      }
    })
  );
