import { actions } from "@src/actions";
import { filter, mergeMap, map, catchError } from "rxjs/operators";
import { isActionOf } from "typesafe-actions";
import { Epic } from "@src/types/Epic";

export const getUser: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.getUser.request)),
    mergeMap(async a => {
      try {
        const b = await api.user.getOne(a.payload.id);
        return actions.getUser.success({ [b.id]: b }, a.meta);
      } catch (e) {
        let message = "An error occurred while retrieving the user.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.getUser.failure({ request: a.payload, message }, a.meta);
      }
    })
  );

export const getUserList: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.getUserList.request)),
    mergeMap(async a => {
      try {
        const b = await api.user.getMany(a.payload.ids);
        return actions.getUserList.success(
          b.reduce((map, x) => ({ ...map, [x.id]: x }), {}),
          a.meta
        );
      } catch (e) {
        let message = "An error occurred while retrieving the user list.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.getUserList.failure(
          { request: a.payload, message },
          a.meta
        );
      }
    })
  );
