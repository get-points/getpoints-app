import { actions, Action } from "@src/actions";
import {
  filter,
  switchMap,
  map,
  catchError,
  ignoreElements,
  tap,
  combineLatest,
  withLatestFrom,
  flatMap
} from "rxjs/operators";
import { isActionOf } from "typesafe-actions";
import { Epic } from "@src/types/Epic";
import { push } from "connected-react-router";
import { PopupType } from "@src/actions/ui";
import { getUserIDs } from "@src/store/user/selectors";

export const signInEpic: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.signIn.request)),
    switchMap(
      async (a): Promise<Action[]> => {
        try {
          if (a.payload.username.length < 1)
            throw new Error("Username cannot be blank");
          if (a.payload.password.length < 1)
            throw new Error("Password cannot be blank");
          const { token, id } = await api.account.signIn(
            a.payload.username,
            a.payload.password
          );
          return [
            actions.signIn.success({ token }, a.meta),
            actions.getUser.request({ id })
            // push("/")
          ];
        } catch (e) {
          let message = "An error occurred while logging in...";
          if (e instanceof Error) {
            message = e.message;
          }
          return [actions.signIn.failure({ message }, a.meta)];
        }
      }
    ),
    switchMap(a => a)
  );

export const signOutConfirmEpic: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.signOut.confirm)),
    withLatestFrom(state$),
    switchMap(
      async ([a, state]): Promise<Action[]> => {
        const ids = getUserIDs(state);
        await api.account.signOut();
        const result = [
          actions.hidePopup() as Action,
          push("/") as Action
        ].concat(ids.map(id => actions.removeUser.request({ id })));
        return result;
      }
    ),
    switchMap(a => a)
  );

export const signOutEpic: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.signOut.request)),
    flatMap(a => [
      actions.showPopup({ popup: { type: PopupType.CONFIRM_SIGNOUT } }),
      actions.closeMenu()
    ])
  );

export const sendSupportRequestEpic: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.sendSupportRequest.request)),
    switchMap(
      async (a): Promise<Action[]> => {
        try {
          await api.account.sendSupportRequest(
            a.payload.message,
            a.payload.email,
            a.payload.name
          );
          return [
            actions.sendSupportRequest.success(undefined, a.meta),
            push("/about/support/success")
          ];
        } catch (e) {
          let message =
            "An error occurred while submitting your support request.";
          if (e instanceof Error) {
            message = e.message;
          }
          return [actions.sendSupportRequest.failure({ message }, a.meta)];
        }
      }
    ),
    switchMap(a => a)
  );

export const signUp1Epic: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.signUp1.request)),
    switchMap(
      async (a): Promise<Action[]> => {
        if (a.payload.firstName.trim() === "") {
          return [
            actions.signUp1.failure({
              message: "First name must not be blank"
            })
          ];
        }
        if (a.payload.lastName.trim() === "") {
          return [
            actions.signUp1.failure({
              message: "Last name must not be blank"
            })
          ];
        }
        if (a.payload.password.trim() === "") {
          return [
            actions.signUp1.failure({
              message: "Password must not be blank"
            })
          ];
        }
        if (a.payload.confirmPassword !== a.payload.password) {
          return [
            actions.signUp1.failure({ message: "Passwords do not match" })
          ];
        }
        return [actions.signUp1.success(a.payload), push("/sign-up/2")];
      }
    ),
    switchMap(a => a)
  );

export const signUp2Epic: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.signUp1.success)),
    combineLatest(action$.pipe(filter(isActionOf(actions.signUp2.request)))),
    switchMap(
      async ([a1, a2]): Promise<Action[]> => {
        try {
          const user = await api.account.signUp({
            email: a2.payload.email,
            countryCode: a2.payload.countryCode,
            telephone: a2.payload.telephone,
            firstName: a1.payload.firstName,
            lastName: a1.payload.lastName,
            password: a1.payload.password,
            confirmPassword: a1.payload.confirmPassword
          });
          return [actions.signUp2.success({ id: user.id }, a2.meta), push("/")];
        } catch (e) {
          let message = "An error occurred while signing up";
          if (e instanceof Error) {
            message = e.message;
          }
          return [
            actions.signUp2.failure({ message }, a2.meta),
            push("/sign-up/1")
          ];
        }
      }
    ),
    switchMap(a => a)
  );
