import { actions } from "@src/actions";
import { Epic } from "@src/types/Epic";
import { filter, mergeMap } from "rxjs/operators";
import { isActionOf } from "typesafe-actions";

export const getReward: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.getReward.request)),
    mergeMap(async a => {
      try {
        const b = await api.reward.getOne(a.payload.id);
        return actions.getReward.success({ [b.id]: b }, a.meta);
      } catch (e) {
        let message = "An error occurred while retrieving the reward.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.getReward.failure(
          { request: a.payload, message },
          a.meta
        );
      }
    })
  );

export const getRewardList: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.getRewardList.request)),
    mergeMap(async a => {
      try {
        const b = await api.reward.getMany(a.payload.ids);
        return actions.getRewardList.success(
          b.reduce((map, x) => ({ ...map, [x.id]: x }), {}),
          a.meta
        );
      } catch (e) {
        let message = "An error occurred while retrieving the reward list.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.getRewardList.failure(
          { request: a.payload, message },
          a.meta
        );
      }
    })
  );
