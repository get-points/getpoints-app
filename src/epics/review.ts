import { actions } from "@src/actions";
import { filter, mergeMap, map, catchError } from "rxjs/operators";
import { isActionOf } from "typesafe-actions";
import { Epic } from "@src/types/Epic";

export const getReview: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.getReview.request)),
    mergeMap(async a => {
      try {
        const b = await api.review.getOne(a.payload.id);
        return actions.getReview.success({ [b.id]: b }, a.meta);
      } catch (e) {
        let message = "An error occurred while retrieving the review.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.getReview.failure(
          { request: a.payload, message },
          a.meta
        );
      }
    })
  );

export const getReviewList: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.getReviewList.request)),
    mergeMap(async a => {
      try {
        const b = await api.review.getMany(a.payload.ids);
        return actions.getReviewList.success(
          b.reduce((map, x) => ({ ...map, [x.id]: x }), {}),
          a.meta
        );
      } catch (e) {
        let message = "An error occurred while retrieving the review list.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.getReviewList.failure(
          { request: a.payload, message },
          a.meta
        );
      }
    })
  );
