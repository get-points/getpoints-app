import { actions } from "@src/actions";
import { Epic } from "@src/types/Epic";
import { filter, mergeMap } from "rxjs/operators";
import { isActionOf } from "typesafe-actions";

export const getFeedback: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.getFeedback.request)),
    mergeMap(async a => {
      try {
        const b = await api.feedback.getOne(a.payload.id);
        return actions.getFeedback.success({ [b.id]: b }, a.meta);
      } catch (e) {
        let message = "An error occurred while retrieving the feedback.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.getFeedback.failure(
          { request: a.payload, message },
          a.meta
        );
      }
    })
  );

export const getFeedbackList: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.getFeedbackList.request)),
    mergeMap(async a => {
      try {
        const b = await api.feedback.getMany(a.payload.ids);
        return actions.getFeedbackList.success(
          b.reduce((map, x) => ({ ...map, [x.id]: x }), {}),
          a.meta
        );
      } catch (e) {
        let message = "An error occurred while retrieving the feedback list.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.getFeedbackList.failure(
          { request: a.payload, message },
          a.meta
        );
      }
    })
  );
