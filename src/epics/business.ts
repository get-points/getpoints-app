import { actions, Action } from "@src/actions";
import {
  filter,
  switchMap,
  mergeMap,
  map,
  catchError,
  ignoreElements,
  tap
} from "rxjs/operators";
import { isActionOf } from "typesafe-actions";
import { Epic } from "@src/types/Epic";

export const getBusiness: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.getBusiness.request)),
    mergeMap(async a => {
      try {
        const b = await api.business.getOne(a.payload.id);
        return actions.getBusiness.success({ [b.id]: b }, a.meta);
      } catch (e) {
        let message = "An error occurred while retrieving the business.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.getBusiness.failure(
          { request: a.payload, message },
          a.meta
        );
      }
    })
  );

export const getBusinesses: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.getBusinessList.request)),
    mergeMap(async a => {
      try {
        const b = await api.business.getMany(a.payload.ids);
        return actions.getBusinessList.success(
          b.reduce((map, x) => ({ ...map, [x.id]: x }), {}),
          a.meta
        );
      } catch (e) {
        let message = "An error occurred while retrieving the business list.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.getBusinessList.failure(
          { request: a.payload, message },
          a.meta
        );
      }
    })
  );

export const getNearbyBusinesses: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.getNearbyBusinesses.request)),
    switchMap(
      async (a): Promise<Action[]> => {
        try {
          const location = await new Promise<Position>((y, n) =>
            navigator.geolocation.getCurrentPosition(y, n)
          );
          let { type, city, country, text } = a.payload;
          if (type && type.trim().length < 1) type = undefined;
          if (city && city.trim().length < 1) city = undefined;
          if (country && country.trim().length < 1) country = undefined;
          if (text && text.trim().length < 1) text = undefined;
          const ids = await api.business.getNearby({
            latitude: location.coords.latitude,
            longitude: location.coords.longitude,
            type,
            city,
            country,
            text
          });
          return [actions.getNearbyBusinesses.success({ ids }, a.meta)];
        } catch (e) {
          let message = "An error occurred while retrieving nearby businesses.";
          if (e instanceof Error) {
            message = e.message;
          }
          return [actions.getNearbyBusinesses.failure({ message }, a.meta)];
        }
      }
    ),
    switchMap(a => a)
  );
