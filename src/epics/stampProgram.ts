import { actions } from "@src/actions";
import { filter, mergeMap, map, catchError } from "rxjs/operators";
import { isActionOf } from "typesafe-actions";
import { Epic } from "@src/types/Epic";

export const getStampProgram: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.getStampProgram.request)),
    mergeMap(async a => {
      try {
        const b = await api.stampProgram.getOne(a.payload.id);
        return actions.getStampProgram.success({ [b.id]: b }, a.meta);
      } catch (e) {
        let message = "An error occurred while retrieving the stampProgram.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.getStampProgram.failure(
          { request: a.payload, message },
          a.meta
        );
      }
    })
  );

export const getStampProgramList: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.getStampProgramList.request)),
    mergeMap(async a => {
      try {
        const b = await api.stampProgram.getMany(a.payload.ids);
        return actions.getStampProgramList.success(
          b.reduce((map, x) => ({ ...map, [x.id]: x }), {}),
          a.meta
        );
      } catch (e) {
        let message =
          "An error occurred while retrieving the stampProgram list.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.getStampProgramList.failure(
          { request: a.payload, message },
          a.meta
        );
      }
    })
  );
