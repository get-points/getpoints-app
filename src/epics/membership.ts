import { actions } from "@src/actions";
import { filter, mergeMap, map, catchError } from "rxjs/operators";
import { isActionOf } from "typesafe-actions";
import { Epic } from "@src/types/Epic";

export const getMembership: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.getMembership.request)),
    mergeMap(async a => {
      try {
        const b = await api.membership.getOne(a.payload.id);
        return actions.getMembership.success({ [b.id]: b }, a.meta);
      } catch (e) {
        let message = "An error occurred while retrieving the membership.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.getMembership.failure(
          { request: a.payload, message },
          a.meta
        );
      }
    })
  );

export const getMembershipList: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(isActionOf(actions.getMembershipList.request)),
    mergeMap(async a => {
      try {
        const b = await api.membership.getMany(a.payload.ids);
        return actions.getMembershipList.success(
          b.reduce((map, x) => ({ ...map, [x.id]: x }), {}),
          a.meta
        );
      } catch (e) {
        let message = "An error occurred while retrieving the membership list.";
        if (e instanceof Error) {
          message = e.message;
        }
        return actions.getMembershipList.failure(
          { request: a.payload, message },
          a.meta
        );
      }
    })
  );
