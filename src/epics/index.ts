import { Epic } from "@src/types/Epic";
import { combineEpics } from "redux-observable";
import * as account from "./account";
import * as business from "./business";
import * as feedback from "./feedback";
import * as membership from "./membership";
import * as offer from "./offer";
import * as review from "./review";
import * as reward from "./reward";
import * as stampProgram from "./stampProgram";
import * as user from "./user";
import * as ui from "./ui";

export const rootEpic: Epic = combineEpics(
  account.sendSupportRequestEpic,
  account.signInEpic,
  account.signOutConfirmEpic,
  account.signOutEpic,
  account.signUp1Epic,
  account.signUp2Epic,
  business.getBusiness,
  business.getBusinesses,
  business.getNearbyBusinesses,
  feedback.getFeedback,
  feedback.getFeedbackList,
  membership.getMembership,
  membership.getMembershipList,
  offer.getOffer,
  offer.getOfferList,
  review.getReview,
  review.getReviewList,
  reward.getReward,
  reward.getRewardList,
  stampProgram.getStampProgram,
  stampProgram.getStampProgramList,
  ui.hideGlobalError,
  ui.showGlobalError,
  user.getUser,
  user.getUserList
);
