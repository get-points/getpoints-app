import { Action, actions } from "@src/actions";
import { Epic } from "@src/types/Epic";
import { filter, mergeMap } from "rxjs/operators";
import { getType } from "typesafe-actions";

export const showGlobalError: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(a => {
      switch (a.type) {
        case getType(actions.signUp1.failure):
        case getType(actions.signUp2.failure):
        case getType(actions.signIn.failure):
          return true;
      }
      return false;
    }),
    mergeMap(
      async (a): Promise<Action> => {
        const payload = a.payload as { message: string };
        return actions.showError(payload);
      }
    )
  );

export const hideGlobalError: Epic = (action$, state$, { api }) =>
  action$.pipe(
    filter(a => {
      switch (a.type) {
        case getType(actions.signUp1.request):
        case getType(actions.signUp2.request):
        case getType(actions.signIn.request):
          return true;
      }
      return false;
    }),
    mergeMap(
      async (a): Promise<Action> => {
        const payload = a.payload as { message: string };
        return actions.hideError();
      }
    )
  );
