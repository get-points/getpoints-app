type OpeningHours = {
  start: string;
  end: string;
};

type Date = {
  day: number;
  month: number;
  year: number;
};

type Location = {
  country: string;
  city: string;
  description: string;
  lat: number;
  lng: number;
  distance?: number;
};

export interface User {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  telephone: {
    countryCode: string;
    telNumber: string;
  };
  profilePicture: string;
  qrCode: string;
  dateOfBirth: {
    day: string;
    month: string;
    year: string;
  };
  gender?: "MALE" | "FEMALE";
  memberships: string[];
  address: {
    country: string;
    state: string;
    city: string;
  };
  notificationSettings: {
    email: {
      pointsEarned: boolean;
      purchaseMade: boolean;
    };
    call: {
      pointsEarned: boolean;
      purchaseMade: boolean;
    };
  };
}

export interface Business {
  id: string;
  name: string;
  type: string;
  description: string;
  coverPhoto: string;
  profilePhoto: string;
  offerText: string;
  locations: Location[];
  contacts: {
    text: string;
    type: "ADDRESS" | "PHONE" | "EMAIL" | "WEBSITE";
  }[];
  hours: {
    monday: OpeningHours;
    tuesday: OpeningHours;
    wednesday: OpeningHours;
    thursday: OpeningHours;
    friday: OpeningHours;
    saturday: OpeningHours;
    sunday: OpeningHours;
  };
  rating: {
    average: number;
    count: number;
  };
  membershipRewards: string[];
  offers: string[];
  stampPrograms: string[];
  images: string[];
  reviews: string[];
}

export interface Review {
  id: string;
  date: Date;
  businessID: string;
  userID: string;
  rating: number;
  title: string;
  comments: string;
}

export interface Membership {
  id: string;
  businessID: string;
  userID: string;
  visits: number;
  points: number;
  history: {
    date: string;
    time: string;
    pointDifference: number;
  }[];
  unlockedRewardIDs: string[];
  redeemedRewardIDs: string[];
  redeemedOfferIDs: string[];
  claimedOfferIDs: string[];
  stamps: {
    stampProgramID: string;
    stampCount: number;
  }[];
  notificationSettings: {
    email: boolean;
    mobile: boolean;
  };
}

export interface Offer {
  id: string;
  businessID: string;
  headline: string;
  requirement: string;
  description: string;
  redemptionLimit: number;
  expiry: string;
}

export interface StampProgram {
  id: string;
  businessID: string;
  headline: string;
  description: string;
  stampsRequired: number;
}

export interface Reward {
  id: string;
  businessID: string;
  headline: string;
  pointsRequired: number;
}

export interface Feedback {
  id: string;
  emoji: "HAPPY" | "SAD";
  userID: string;
  businessID: string;
  message: string;
}
