import { Action } from "@src/actions";
import { API } from "@src/api/interface";
import { State } from "@src/store";
import {
  Epic as RXEpic,
  EpicMiddleware as RXEpicMiddleware
} from "redux-observable";

export interface Deps {
  api: API;
}

export type Epic<T extends Action = Action> = RXEpic<Action, T, State, Deps>;

export type EpicMiddleware = RXEpicMiddleware<Action, Action, State, Deps>;
