import { Platform } from "react-native";

export const TYRO_GREEN = "#8CC541";

export const GETPOINTS_GREEN = "#8CC541";
export const GETPOINTS_LIGHT_GREEN = "#B3D982";
export const GETPOINTS_DARK_GREEN = "#68952D";

export const GETPOINTS_TEAL = "#00A79D";
export const GETPOINTS_LIGHT_TEAL = "#02CDC1";
export const GETPOINTS_DARK_TEAL = "#00A79D";

export const GETPOINTS_BLACK = "#000000";
export const GETPOINTS_WHITE = "#FFFFFF";
export const GETPOINTS_DARK_GREY = "#333333";
export const GETPOINTS_GREY = "#6D6E71";
export const GETPOINTS_PLACEHOLDER_GREY = "#B8B9BA";

export const GETPOINTS_YELLOW = "#FEE400";
export const GETPOINTS_RED = "#ED1C24";

export const GETPOINTS_BLUE = "#0E73BA";
export const GETPOINTS_LIGHT_BLUE = "#199DF9";
export const GETPOINTS_DARK_BLUE = "#0E73BA";

export const GETPOINTS_BACKGROUND_GREY = "#F2F2F2";

export const GETPOINTS_FONT_PRIMARY =
  Platform.OS === "android"
    ? "Roboto"
    : Platform.OS === "ios"
    ? "Helvetica"
    : "Arial";
