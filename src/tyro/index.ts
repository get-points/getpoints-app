import { createStandardAction, createAction } from "typesafe-actions";
const { deepmerge: deepmergeJS } = require("./deepmerge");

export const action = <T extends string>(type: T) => <P>() =>
  createAction(type, resolve => {
    return (payload: P, meta?: { requestId?: number }) =>
      resolve(payload, {
        requestId: Math.ceil(Math.random() * 1000000),
        ...meta
      });
  });

export type LoadableState<T> =
  | { loading: true; error?: undefined }
  | { error: string; loading?: false }
  | ({ loading?: false; error?: undefined } & T);

export interface AbsentLoadable {
  state: "ABSENT";
  ids: string[];
}
export interface LoadedLoadable<T> {
  state: "LOADED";
  more?: FailedLoadable | PendingLoadable;
  item: T;
}
export interface FailedLoadable {
  state: "FAILED";
  message: string;
}
export interface PendingLoadable {
  state: "PENDING";
}

export type Loadable<T> =
  | LoadedLoadable<T>
  | FailedLoadable
  | PendingLoadable
  | AbsentLoadable;

// export type LoadableContainer<T> = LoadedLoadable<{
//   [id: string]: Loadable<T>;
// }>;

export const LoadableFactory = {
  pending: () => ({ state: "PENDING" } as PendingLoadable),
  absent: (ids: string[]) => ({ state: "ABSENT", ids } as AbsentLoadable),
  failed: (message: string) => ({ state: "FAILED", message } as FailedLoadable),
  loaded: <T>(item: T) => ({ state: "LOADED", item } as LoadedLoadable<T>)
};

export interface LoadableContainer<T extends { id: string }> {
  pendingRequests: number[];
  errors: { [requestId: number]: string };

  items: {
    [k: string]: Loadable<T>;
  };

  clearError(
    this: LoadableContainer<T>,
    requestId: number
  ): LoadableContainer<T>;
  clearErrors(this: LoadableContainer<T>): LoadableContainer<T>;

  request(
    this: LoadableContainer<T>,
    action: {
      meta: { requestId: number };
      payload: { id?: string; ids?: string[] };
    }
  ): LoadableContainer<T>;
  requestOne(
    this: LoadableContainer<T>,
    action: { meta: { requestId: number }; payload: { id: string } }
  ): LoadableContainer<T>;

  removeOne(
    this: LoadableContainer<T>,
    action: { payload: { id: string } }
  ): LoadableContainer<T>;

  receive(
    this: LoadableContainer<T>,
    action: { meta: { requestId: number }; payload: { [id: string]: T } }
  ): LoadableContainer<T>;

  fail(
    this: LoadableContainer<T>,
    action: {
      meta: { requestId: number };
      payload: { request?: { id?: string; ids?: string[] }; message: string };
    }
  ): LoadableContainer<T>;
}

export const makeLoadableContainer = <
  T extends { id: string }
>(): LoadableContainer<T> => ({
  pendingRequests: [],
  errors: {},
  items: {},
  clearError(requestId) {
    const { [requestId]: removed, ...errors } = this.errors;
    return {
      ...this,
      errors
    };
  },
  clearErrors() {
    return { ...this, errors: {} };
  },

  fail(action) {
    let items = this.items;
    if (
      action.payload.request &&
      typeof action.payload.request.id === "string"
    ) {
      items = {
        ...items,
        [action.payload.request.id]: LoadableFactory.failed(
          action.payload.message
        )
      };
    }
    if (action.payload.request && action.payload.request.ids) {
      items = {
        ...items,
        ...action.payload.request.ids.reduce(
          (map, id) => ({
            ...map,
            [id]: LoadableFactory.failed(action.payload.message)
          }),
          {}
        )
      };
    }
    return {
      ...this,
      pendingRequests: this.pendingRequests.filter(
        x => x !== action.meta.requestId
      ),
      errors: {
        ...this.errors,
        [action.meta.requestId]: action.payload.message
      },
      items
    };
  },

  request({ meta: { requestId }, payload: { id, ids } }) {
    const pendings: { [id: string]: Loadable<T> } = {};
    if (id) {
      pendings[id] = LoadableFactory.pending();
    }
    if (ids) {
      ids.forEach(id => (pendings[id] = LoadableFactory.pending()));
    }
    return {
      ...this,
      pendingRequests: this.pendingRequests.concat([requestId]),
      items: {
        ...this.items,
        ...pendings
      }
    };
  },
  requestOne(action) {
    return {
      ...this.request(action),
      items: {
        ...this.items,
        [action.payload.id]: LoadableFactory.pending()
      }
    };
  },

  removeOne({ payload }) {
    const { [payload.id]: removed, ...rest } = this.items;

    return {
      ...this,
      items: {
        ...rest
      }
    };
  },

  receive({ meta: { requestId }, payload }) {
    const items: { [k: string]: Loadable<T> } = {};
    for (const id in payload) {
      items[id] = LoadableFactory.loaded(payload[id]);
    }

    return {
      ...this,
      pendingRequests: this.pendingRequests.filter(x => x !== requestId),
      items: {
        ...this.items,
        ...items
      }
    };
  }
});

export const deepmerge = deepmergeJS as <T extends object, V extends object>(
  t: Partial<T>,
  v: Partial<V>
) => T & V;
