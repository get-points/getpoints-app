import React, { Component } from "react";
import { Marker, InfoWindow } from "react-google-maps";
import InfoBox from "react-google-maps/lib/components/addons/InfoBox";

export class MapViewMarker extends Component<{
  description?: string;
  title?: string;
  coordinate: { latitude: number; longitude: number };
  onPress?: () => any;
}> {
  render() {
    return (
      <Marker
        onClick={this.props.onPress}
        title={
          this.props.description
            ? `${this.props.title}\n${this.props.description}`
            : this.props.title
        }
        position={{
          lat: this.props.coordinate.latitude,
          lng: this.props.coordinate.longitude
        }}
      >
        {this.props.children}
      </Marker>
    );
  }
}
