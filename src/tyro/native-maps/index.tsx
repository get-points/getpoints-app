import React, { Component } from "react";
import { View, ActivityIndicator, StyleSheet } from "react-native";
import { GoogleMap, withGoogleMap } from "react-google-maps";

const GoogleMapContainer = withGoogleMap((props: GoogleMap["props"]) => (
  <GoogleMap {...props} />
));

export default class MapView extends Component<{
  region: { latitude: number; longitude: number };
  onPress?: () => any;
}> {
  render() {
    return (
      <GoogleMapContainer
        containerElement={
          <div
            style={{
              flex: 1,
              display: "flex",
              flexDirection: "column"
            }}
          />
        }
        onClick={this.props.onPress}
        mapElement={<div style={{ flex: 1 }} />}
        center={{
          lat: this.props.region.latitude,
          lng: this.props.region.longitude
        }}
        defaultZoom={13}
        options={{
          clickableIcons: true
        }}
      >
        {this.props.children}
      </GoogleMapContainer>
    );
  }
}

export { MapViewMarker as Marker } from "./Marker";
export { MapViewCallout as Callout } from "./Callout";
