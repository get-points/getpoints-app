import React, { Component } from "react";
import { Marker, InfoWindow } from "react-google-maps";
import InfoBox from "react-google-maps/lib/components/addons/InfoBox";
import { ReactNode } from "react-redux";

export class MapViewCallout extends Component<{
  children: ReactNode;
  onPress?: () => any;
  tooltip?: boolean;
  style?: any;
}> {
  render() {
    return (
      <InfoBox
        onCloseClick={() => {}}
        options={{
          closeBoxURL: "",
          alignBottom: true,

          maxWidth: 150,
          pixelOffset: new google.maps.Size(0, -30),

          infoBoxClearance: new google.maps.Size(1, 1)
        }}
      >
        {this.props.children}
      </InfoBox>
    );
  }
}
