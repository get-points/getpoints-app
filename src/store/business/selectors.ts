import { BusinessState } from "./index";
import { Business } from "@src/types/Models";
import {
  Loadable,
  LoadableFactory,
  LoadedLoadable,
  FailedLoadable,
  PendingLoadable,
  AbsentLoadable
} from "@tyro";
import { State } from "@src/store";

export const getBusinessState = (state: State) => state.business;

export const getBusinessIDs = (state: State) => {
  const businessState = getBusinessState(state);
  return Object.keys(businessState.items);
};

export const getBusinessListByIDs = (state: State, ids: string[]) => {
  const businessState = getBusinessState(state);
  const loadables = ids.map(
    id => businessState.items[id] || LoadableFactory.absent([id])
  );
  const absent = loadables
    .filter((l): l is AbsentLoadable => l.state === "ABSENT")
    .map(loadable => loadable.ids);
  if (absent.length > 0)
    return LoadableFactory.absent(([] as string[]).concat(...absent));

  const failures = loadables
    .filter((l): l is FailedLoadable => l.state === "FAILED")
    .map(loadable => loadable.message);
  if (failures.length > 0) return LoadableFactory.failed(failures.join(" "));

  const pendings = loadables.filter(
    (l): l is PendingLoadable => l.state === "PENDING"
  );
  if (pendings.length > 0) return LoadableFactory.pending();

  const loaded = loadables
    .filter((l): l is LoadedLoadable<Business> => l.state === "LOADED")
    .map(l => l.item);

  return LoadableFactory.loaded(loaded);
};

export const getBusinessMapByIDs = (state: State, ids: string[]) => {
  const loadableList = getBusinessListByIDs(state, ids);
  if (loadableList.state !== "LOADED") return loadableList;
  const map = loadableList.item.reduce(
    (map, i) => ({
      ...map,
      [i.id]: i
    }),
    {} as { [id: string]: Business }
  );
  return LoadableFactory.loaded(map);
};

export const getOnlyBusiness = (state: State): Loadable<Business> => {
  const businessState = getBusinessState(state);
  const ids = getBusinessIDs(state);

  if (businessState.pendingRequests.length > 0)
    return LoadableFactory.pending();

  if (ids.length < 1) return LoadableFactory.failed("No businesss found");
  if (ids.length > 1)
    return LoadableFactory.failed("More than one business loaded");
  return businessState.items[ids[0]];
};

export const getLoadedBusinessList = (state: State): Business[] => {
  const businessState = getBusinessState(state);
  return getBusinessIDs(state)
    .map(id => businessState.items[id])
    .filter((u): u is LoadedLoadable<Business> => u.state === "LOADED")
    .map(u => u.item);
};

export const getBusinessByID = (
  state: State,
  id: string
): Loadable<Business> => {
  const businessState = getBusinessState(state);
  return businessState.items[id] || LoadableFactory.absent([id]);
};
