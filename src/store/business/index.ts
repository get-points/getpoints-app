import { Action, actions } from "@src/actions";
import { getType } from "typesafe-actions";
import {
  LoadableFactory,
  LoadableContainer,
  makeLoadableContainer
} from "@tyro";
import { Business } from "@src/types/Models";

export type BusinessState = LoadableContainer<Business>;

export const businessReducer = function(
  state: BusinessState = makeLoadableContainer<Business>(),
  action: Action
): BusinessState {
  switch (action.type) {
    case getType(actions.getBusinessList.request):
      return state.request(action);

    case getType(actions.getBusiness.request):
      return state.requestOne(action);

    case getType(actions.getBusinessList.success):
    case getType(actions.getBusiness.success):
      return state.receive(action);

    case getType(actions.getBusinessList.failure):
    case getType(actions.getBusiness.failure):
      return state.fail(action);

    case getType(actions.clearAllBusinessErrors):
      return state.clearErrors();

    case getType(actions.clearBusinessError):
      return state.clearError(action.payload.requestID);
  }
  return state;
};
