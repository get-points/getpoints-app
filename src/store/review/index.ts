import { Action, actions } from "@src/actions";
import { getType } from "typesafe-actions";
import {
  LoadableFactory,
  LoadableContainer,
  makeLoadableContainer
} from "@tyro";
import { Review } from "@src/types/Models";

export type ReviewState = LoadableContainer<Review>;

export const reviewReducer = function(
  state: ReviewState = makeLoadableContainer<Review>(),
  action: Action
): ReviewState {
  switch (action.type) {
    case getType(actions.getReviewList.request):
      return state.request(action);

    case getType(actions.getReview.request):
      return state.requestOne(action);

    case getType(actions.getReviewList.success):
    case getType(actions.getReview.success):
      return state.receive(action);

    case getType(actions.getReviewList.failure):
    case getType(actions.getReview.failure):
      return state.fail(action);

    case getType(actions.clearAllReviewErrors):
      return state.clearErrors();

    case getType(actions.clearReviewError):
      return state.clearError(action.payload.requestID);
  }
  return state;
};
