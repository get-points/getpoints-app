import { ReviewState } from "./index";
import { Review } from "@src/types/Models";
import { Loadable, LoadableFactory, LoadedLoadable } from "@tyro";
import { State } from "@src/store";

export const getReviewState = (state: State) => state.review;

export const getReviewIDs = (state: State) => {
  const reviewState = getReviewState(state);
  return Object.keys(reviewState.items);
};

export const getOnlyReview = (state: State): Loadable<Review> => {
  const reviewState = getReviewState(state);
  const ids = getReviewIDs(state);

  if (reviewState.pendingRequests.length > 0) return LoadableFactory.pending();

  if (ids.length < 1) return LoadableFactory.failed("No reviews found");
  if (ids.length > 1)
    return LoadableFactory.failed("More than one review loaded");
  return reviewState.items[ids[0]];
};

export const getLoadedReviews = (state: State): Review[] => {
  const reviewState = getReviewState(state);
  return getReviewIDs(state)
    .map(id => reviewState.items[id])
    .filter((u): u is LoadedLoadable<Review> => u.state === "LOADED")
    .map(u => u.item);
};

export const getReviewByID = (state: State, id: string): Loadable<Review> => {
  const reviewState = getReviewState(state);
  return reviewState.items[id];
};
