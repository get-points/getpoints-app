import { UserState } from "@src/store/user";
import { User } from "@src/types/Models";
import { Loadable, LoadableFactory, LoadedLoadable } from "@tyro";
import { State } from "@src/store";

export const getUserState = (state: State) => state.user;

export const getUserIDs = (state: State) => {
  const userState = getUserState(state);
  return Object.keys(userState.items);
};

export const getOnlyUser = (state: State): Loadable<User> => {
  const userState = getUserState(state);
  const ids = getUserIDs(state);

  if (userState.pendingRequests.length > 0) return LoadableFactory.pending();

  if (ids.length < 1) return LoadableFactory.failed("No users found");
  if (ids.length > 1)
    return LoadableFactory.failed("More than one user loaded");
  return userState.items[ids[0]];
};

export const getLoadedUsers = (state: State): User[] => {
  const userState = getUserState(state);
  return getUserIDs(state)
    .map(id => userState.items[id])
    .filter((u): u is LoadedLoadable<User> => u.state === "LOADED")
    .map(u => u.item);
};

export const getUserByID = (state: State, id: string): Loadable<User> => {
  const userState = getUserState(state);
  return userState.items[id];
};
