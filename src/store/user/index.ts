import { Action, actions } from "@src/actions";
import { getType } from "typesafe-actions";
import {
  LoadableFactory,
  LoadableContainer,
  makeLoadableContainer
} from "@tyro";
import { User } from "@src/types/Models";

export type UserState = LoadableContainer<User>;

export const userReducer = function(
  state: UserState = makeLoadableContainer<User>(),
  action: Action
): UserState {
  switch (action.type) {
    case getType(actions.getUserList.request):
      return state.request(action);

    case getType(actions.getUser.request):
      return state.requestOne(action);

    case getType(actions.getUserList.success):
    case getType(actions.getUser.success):
      return state.receive(action);

    case getType(actions.getUserList.failure):
    case getType(actions.getUser.failure):
      return state.fail(action);

    case getType(actions.clearAllUserErrors):
      return state.clearErrors();

    case getType(actions.clearUserError):
      return state.clearError(action.payload.requestID);

    case getType(actions.removeUser.request):
      return state.removeOne(action);
  }
  return state;
};
