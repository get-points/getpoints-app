export function listToMap<T extends { id: string }>(
  list: T[],
  k: keyof T = "id"
): { [id: string]: T } {
  return list.reduce(
    (map, i) => {
      const key = (i[k] as unknown) as string;
      return {
        ...map,
        [key]: i
      };
    },
    {} as { [id: string]: T }
  );
}
