import { NearbyBusinessState } from "./index";
import { Business } from "@src/types/Models";
import { Loadable, LoadableFactory, LoadedLoadable } from "@tyro";
import { State } from "@src/store";

export const getNearbyBusinesses = (state: State) =>
  state.search.nearbyBusinesses;
