import { Action, actions } from "@src/actions";
import { getType } from "typesafe-actions";
import {
  LoadableFactory,
  LoadableContainer,
  makeLoadableContainer,
  Loadable
} from "@tyro";

export type NearbyBusinessState = Loadable<{ id: string; location: number }[]>;

export const nearbyBusinessesReducer = function(
  state: NearbyBusinessState = LoadableFactory.absent([]),
  action: Action
): NearbyBusinessState {
  switch (action.type) {
    case getType(actions.getNearbyBusinesses.request):
      return LoadableFactory.pending();
    case getType(actions.getNearbyBusinesses.success):
      return LoadableFactory.loaded<{ id: string; location: number }[]>(
        action.payload.ids
      );
    case getType(actions.getNearbyBusinesses.failure):
      return LoadableFactory.failed(action.payload.message);
  }
  return state;
};
