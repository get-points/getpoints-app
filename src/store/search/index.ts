import { createBrowserHistory } from "history";
import { combineReducers } from "redux";
import {
  nearbyBusinessesReducer,
  NearbyBusinessState
} from "./nearbyBusinesses";

export interface SearchState {
  readonly nearbyBusinesses: NearbyBusinessState;
}

export const searchReducer = combineReducers<SearchState>({
  nearbyBusinesses: nearbyBusinessesReducer
});
