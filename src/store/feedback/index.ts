import { Action, actions } from "@src/actions";
import { getType } from "typesafe-actions";
import {
  LoadableFactory,
  LoadableContainer,
  makeLoadableContainer
} from "@tyro";
import { Feedback } from "@src/types/Models";

export type FeedbackState = LoadableContainer<Feedback>;

export const feedbackReducer = function(
  state: FeedbackState = makeLoadableContainer<Feedback>(),
  action: Action
): FeedbackState {
  switch (action.type) {
    case getType(actions.getFeedbackList.request):
      return state.request(action);

    case getType(actions.getFeedback.request):
      return state.requestOne(action);

    case getType(actions.getFeedbackList.success):
    case getType(actions.getFeedback.success):
      return state.receive(action);

    case getType(actions.getFeedbackList.failure):
    case getType(actions.getFeedback.failure):
      return state.fail(action);

    case getType(actions.clearAllFeedbackErrors):
      return state.clearErrors();

    case getType(actions.clearFeedbackError):
      return state.clearError(action.payload.requestID);
  }
  return state;
};
