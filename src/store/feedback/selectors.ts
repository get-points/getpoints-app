import { FeedbackState } from "./index";
import { Feedback } from "@src/types/Models";
import { Loadable, LoadableFactory, LoadedLoadable } from "@tyro";
import { State } from "@src/store";

export const getFeedbackState = (state: State) => state.feedback;

export const getFeedbackIDs = (state: State) => {
  const feedbackState = getFeedbackState(state);
  return Object.keys(feedbackState.items);
};

export const getOnlyFeedback = (state: State): Loadable<Feedback> => {
  const feedbackState = getFeedbackState(state);
  const ids = getFeedbackIDs(state);

  if (feedbackState.pendingRequests.length > 0) return LoadableFactory.pending();

  if (ids.length < 1) return LoadableFactory.failed("No feedbacks found");
  if (ids.length > 1)
    return LoadableFactory.failed("More than one feedback loaded");
  return feedbackState.items[ids[0]];
};

export const getLoadedFeedbacks = (state: State): Feedback[] => {
  const feedbackState = getFeedbackState(state);
  return getFeedbackIDs(state)
    .map(id => feedbackState.items[id])
    .filter((u): u is LoadedLoadable<Feedback> => u.state === "LOADED")
    .map(u => u.item);
};

export const getFeedbackByID = (state: State, id: string): Loadable<Feedback> => {
  const feedbackState = getFeedbackState(state);
  return feedbackState.items[id];
};
