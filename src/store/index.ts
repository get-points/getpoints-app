// TODO: replace this with live API
import { api } from "@src/api/mock";
import { actions } from "@src/actions";
import { rootEpic } from "@src/epics";
import { EpicMiddleware } from "@src/types/Epic";
import { composeWithDevTools } from "remote-redux-devtools";
import {
  connectRouter,
  routerMiddleware,
  RouterState
} from "connected-react-router";
import { createMemoryHistory as createHistory } from "history";
import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { createEpicMiddleware } from "redux-observable";
import { businessReducer, BusinessState } from "./business";
import { feedbackReducer, FeedbackState } from "./feedback";
import { membershipReducer, MembershipState } from "./membership";
import { offerReducer, OfferState } from "./offer";
import { reviewReducer, ReviewState } from "./review";
import { rewardReducer, RewardState } from "./reward";
import { stampProgramReducer, StampProgramState } from "./stampProgram";
import { userReducer, UserState } from "./user";
import { uiReducer, UIState } from "./ui";
import { SearchState, searchReducer } from "./search";

export const history = createHistory();

export interface State {
  router: RouterState;
  readonly ui: UIState;
  readonly search: SearchState;
  readonly business: BusinessState;
  readonly feedback: FeedbackState;
  readonly membership: MembershipState;
  readonly offer: OfferState;
  readonly review: ReviewState;
  readonly reward: RewardState;
  readonly stampProgram: StampProgramState;
  readonly user: UserState;
}

export const reducer = combineReducers<State>({
  router: connectRouter(history),
  ui: uiReducer,
  search: searchReducer,
  business: businessReducer,
  feedback: feedbackReducer,
  membership: membershipReducer,
  offer: offerReducer,
  review: reviewReducer,
  reward: rewardReducer,
  stampProgram: stampProgramReducer,
  user: userReducer
});

let composeEnhancers;
if (
  typeof window === "object" &&
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
) {
  composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    name: "GetPoints Window Action Debugger",
    actionCreators: actions
  });
} else {
  composeEnhancers = composeWithDevTools;
}

const epicMiddleware: EpicMiddleware = createEpicMiddleware({
  dependencies: {
    api
  }
});

export const store = createStore(
  reducer,
  composeEnhancers(applyMiddleware(routerMiddleware(history), epicMiddleware))
);

epicMiddleware.run(rootEpic);
