import { Action, actions } from "@src/actions";
import { getType } from "typesafe-actions";
import {
  LoadableFactory,
  LoadableContainer,
  makeLoadableContainer
} from "@tyro";
import { Business } from "@src/types/Models";
import { PopupState } from "@src/actions/ui";

export type UIState = {
  menuOpen: boolean;
  error: string | undefined;
  popup: PopupState | undefined;
};

export const menuReducer = function(
  state: boolean = false,
  action: Action
): boolean {
  switch (action.type) {
    case getType(actions.openMenu):
      return true;
    case getType(actions.closeMenu):
      return false;
    case getType(actions.toggleMenu):
      return !state;
  }
  return state;
};

export const uiReducer = function(
  state: UIState = { menuOpen: false, error: undefined, popup: undefined },
  action: Action
): UIState {
  switch (action.type) {
    case getType(actions.openMenu):
      return { ...state, menuOpen: true };
    case getType(actions.closeMenu):
      return { ...state, menuOpen: false };
    case getType(actions.toggleMenu):
      return { ...state, menuOpen: !state.menuOpen };

    case getType(actions.showError):
      return { ...state, error: action.payload.message };
    case getType(actions.hideError):
      return { ...state, error: undefined };

    case getType(actions.showPopup):
      return { ...state, popup: action.payload.popup };
    case getType(actions.hidePopup):
      return { ...state, popup: undefined };
  }
  return state;
};
