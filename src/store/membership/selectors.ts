import { MembershipState } from "./index";
import { Membership } from "@src/types/Models";
import {
  Loadable,
  LoadableFactory,
  LoadedLoadable,
  AbsentLoadable,
  FailedLoadable,
  PendingLoadable
} from "@tyro";
import { State } from "@src/store";

export const getMembershipState = (state: State) => state.membership;

export const getMembershipIDs = (state: State) => {
  const membershipState = getMembershipState(state);
  return Object.keys(membershipState.items);
};

export const getOnlyMembership = (state: State): Loadable<Membership> => {
  const membershipState = getMembershipState(state);
  const ids = getMembershipIDs(state);

  if (membershipState.pendingRequests.length > 0)
    return LoadableFactory.pending();

  if (ids.length < 1) return LoadableFactory.failed("No memberships found");
  if (ids.length > 1)
    return LoadableFactory.failed("More than one membership loaded");
  return membershipState.items[ids[0]];
};

export const getLoadedMemberships = (state: State): Membership[] => {
  const membershipState = getMembershipState(state);
  return getMembershipIDs(state)
    .map(id => membershipState.items[id])
    .filter((u): u is LoadedLoadable<Membership> => u.state === "LOADED")
    .map(u => u.item);
};

export const getMembershipByID = (
  state: State,
  id: string
): Loadable<Membership> => {
  const membershipState = getMembershipState(state);
  return membershipState.items[id] || LoadableFactory.absent([id]);
};

export const getMembershipListByIDs = (state: State, ids: string[]) => {
  const membershipState = getMembershipState(state);
  const loadables = ids.map(
    id => membershipState.items[id] || LoadableFactory.absent([id])
  );
  const absent = loadables
    .filter((l): l is AbsentLoadable => l.state === "ABSENT")
    .map(loadable => loadable.ids);
  if (absent.length > 0)
    return LoadableFactory.absent(([] as string[]).concat(...absent));

  const failures = loadables
    .filter((l): l is FailedLoadable => l.state === "FAILED")
    .map(loadable => loadable.message);
  if (failures.length > 0) return LoadableFactory.failed(failures.join(" "));

  const pendings = loadables.filter(
    (l): l is PendingLoadable => l.state === "PENDING"
  );
  if (pendings.length > 0) return LoadableFactory.pending();

  const loaded = loadables
    .filter((l): l is LoadedLoadable<Membership> => l.state === "LOADED")
    .map(l => l.item);

  return LoadableFactory.loaded(loaded);
};
