import { Action, actions } from "@src/actions";
import { getType } from "typesafe-actions";
import {
  LoadableFactory,
  LoadableContainer,
  makeLoadableContainer
} from "@tyro";
import { Membership } from "@src/types/Models";

export type MembershipState = LoadableContainer<Membership>;

export const membershipReducer = function(
  state: MembershipState = makeLoadableContainer<Membership>(),
  action: Action
): MembershipState {
  switch (action.type) {
    case getType(actions.getMembershipList.request):
      return state.request(action);

    case getType(actions.getMembership.request):
      return state.requestOne(action);

    case getType(actions.getMembershipList.success):
    case getType(actions.getMembership.success):
      return state.receive(action);

    case getType(actions.getMembershipList.failure):
    case getType(actions.getMembership.failure):
      return state.fail(action);

    case getType(actions.clearAllMembershipErrors):
      return state.clearErrors();

    case getType(actions.clearMembershipError):
      return state.clearError(action.payload.requestID);
  }
  return state;
};
