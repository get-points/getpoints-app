import { StampProgramState } from "./index";
import { StampProgram } from "@src/types/Models";
import {
  Loadable,
  LoadableFactory,
  LoadedLoadable,
  PendingLoadable,
  FailedLoadable,
  AbsentLoadable
} from "@tyro";
import { State } from "@src/store";

export const getStampProgramState = (state: State) => state.stampProgram;

export const getStampProgramIDs = (state: State) => {
  const stampProgramState = getStampProgramState(state);
  return Object.keys(stampProgramState.items);
};

export const getOnlyStampProgram = (state: State): Loadable<StampProgram> => {
  const stampProgramState = getStampProgramState(state);
  const ids = getStampProgramIDs(state);

  if (stampProgramState.pendingRequests.length > 0)
    return LoadableFactory.pending();

  if (ids.length < 1) return LoadableFactory.failed("No stampPrograms found");
  if (ids.length > 1)
    return LoadableFactory.failed("More than one stampProgram loaded");
  return stampProgramState.items[ids[0]];
};

export const getLoadedStampPrograms = (state: State): StampProgram[] => {
  const stampProgramState = getStampProgramState(state);
  return getStampProgramIDs(state)
    .map(id => stampProgramState.items[id])
    .filter((u): u is LoadedLoadable<StampProgram> => u.state === "LOADED")
    .map(u => u.item);
};

export const getStampProgramByID = (
  state: State,
  id: string
): Loadable<StampProgram> => {
  const stampProgramState = getStampProgramState(state);
  return stampProgramState.items[id] || LoadableFactory.absent([id]);
};

export const getStampProgramListByIDs = (state: State, ids: string[]) => {
  const stampProgramState = getStampProgramState(state);
  const loadables = ids.map(
    id => stampProgramState.items[id] || LoadableFactory.absent([id])
  );
  const absent = loadables
    .filter((l): l is AbsentLoadable => l.state === "ABSENT")
    .map(loadable => loadable.ids);
  if (absent.length > 0)
    return LoadableFactory.absent(([] as string[]).concat(...absent));

  const failures = loadables
    .filter((l): l is FailedLoadable => l.state === "FAILED")
    .map(loadable => loadable.message);
  if (failures.length > 0) return LoadableFactory.failed(failures.join(" "));

  const pendings = loadables.filter(
    (l): l is PendingLoadable => l.state === "PENDING"
  );
  if (pendings.length > 0) return LoadableFactory.pending();

  const loaded = loadables
    .filter((l): l is LoadedLoadable<StampProgram> => l.state === "LOADED")
    .map(l => l.item);

  return LoadableFactory.loaded(loaded);
};
