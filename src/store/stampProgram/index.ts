import { Action, actions } from "@src/actions";
import { getType } from "typesafe-actions";
import {
  LoadableFactory,
  LoadableContainer,
  makeLoadableContainer
} from "@tyro";
import { StampProgram } from "@src/types/Models";

export type StampProgramState = LoadableContainer<StampProgram>;

export const stampProgramReducer = function(
  state: StampProgramState = makeLoadableContainer<StampProgram>(),
  action: Action
): StampProgramState {
  switch (action.type) {
    case getType(actions.getStampProgramList.request):
      return state.request(action);

    case getType(actions.getStampProgram.request):
      return state.requestOne(action);

    case getType(actions.getStampProgramList.success):
    case getType(actions.getStampProgram.success):
      return state.receive(action);

    case getType(actions.getStampProgramList.failure):
    case getType(actions.getStampProgram.failure):
      return state.fail(action);

    case getType(actions.clearAllStampProgramErrors):
      return state.clearErrors();

    case getType(actions.clearStampProgramError):
      return state.clearError(action.payload.requestID);
  }
  return state;
};
