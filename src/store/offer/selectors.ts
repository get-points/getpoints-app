import { OfferState } from "./index";
import { Offer } from "@src/types/Models";
import {
  Loadable,
  LoadableFactory,
  LoadedLoadable,
  FailedLoadable,
  PendingLoadable,
  AbsentLoadable
} from "@tyro";
import { State } from "@src/store";

export const getOfferState = (state: State) => state.offer;

export const getOfferIDs = (state: State) => {
  const offerState = getOfferState(state);
  return Object.keys(offerState.items);
};

export const getOnlyOffer = (state: State): Loadable<Offer> => {
  const offerState = getOfferState(state);
  const ids = getOfferIDs(state);

  if (offerState.pendingRequests.length > 0) return LoadableFactory.pending();

  if (ids.length < 1) return LoadableFactory.failed("No offers found");
  if (ids.length > 1)
    return LoadableFactory.failed("More than one offer loaded");
  return offerState.items[ids[0]];
};

export const getLoadedOffers = (state: State): Offer[] => {
  const offerState = getOfferState(state);
  return getOfferIDs(state)
    .map(id => offerState.items[id])
    .filter((u): u is LoadedLoadable<Offer> => u.state === "LOADED")
    .map(u => u.item);
};

export const getOfferByID = (state: State, id: string): Loadable<Offer> => {
  const offerState = getOfferState(state);
  return offerState.items[id] || LoadableFactory.absent([id]);
};

export const getOfferListByIDs = (state: State, ids: string[]) => {
  const offerState = getOfferState(state);
  const loadables = ids.map(
    id => offerState.items[id] || LoadableFactory.absent([id])
  );
  const absent = loadables
    .filter((l): l is AbsentLoadable => l.state === "ABSENT")
    .map(loadable => loadable.ids);
  if (absent.length > 0)
    return LoadableFactory.absent(([] as string[]).concat(...absent));

  const failures = loadables
    .filter((l): l is FailedLoadable => l.state === "FAILED")
    .map(loadable => loadable.message);
  if (failures.length > 0) return LoadableFactory.failed(failures.join(" "));

  const pendings = loadables.filter(
    (l): l is PendingLoadable => l.state === "PENDING"
  );
  if (pendings.length > 0) return LoadableFactory.pending();

  const loaded = loadables
    .filter((l): l is LoadedLoadable<Offer> => l.state === "LOADED")
    .map(l => l.item);

  return LoadableFactory.loaded(loaded);
};
