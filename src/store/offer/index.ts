import { Action, actions } from "@src/actions";
import { getType } from "typesafe-actions";
import {
  LoadableFactory,
  LoadableContainer,
  makeLoadableContainer
} from "@tyro";
import { Offer } from "@src/types/Models";

export type OfferState = LoadableContainer<Offer>;

export const offerReducer = function(
  state: OfferState = makeLoadableContainer<Offer>(),
  action: Action
): OfferState {
  switch (action.type) {
    case getType(actions.getOfferList.request):
      return state.request(action);

    case getType(actions.getOffer.request):
      return state.requestOne(action);

    case getType(actions.getOfferList.success):
    case getType(actions.getOffer.success):
      return state.receive(action);

    case getType(actions.getOfferList.failure):
    case getType(actions.getOffer.failure):
      return state.fail(action);

    case getType(actions.clearAllOfferErrors):
      return state.clearErrors();

    case getType(actions.clearOfferError):
      return state.clearError(action.payload.requestID);
  }
  return state;
};
