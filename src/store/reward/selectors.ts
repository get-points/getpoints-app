import { RewardState } from "./index";
import { Reward } from "@src/types/Models";
import {
  Loadable,
  LoadableFactory,
  LoadedLoadable,
  AbsentLoadable,
  FailedLoadable,
  PendingLoadable
} from "@tyro";
import { State } from "@src/store";

export const getRewardState = (state: State) => state.reward;

export const getRewardIDs = (state: State) => {
  const rewardState = getRewardState(state);
  return Object.keys(rewardState.items);
};

export const getOnlyReward = (state: State): Loadable<Reward> => {
  const rewardState = getRewardState(state);
  const ids = getRewardIDs(state);

  if (rewardState.pendingRequests.length > 0) return LoadableFactory.pending();

  if (ids.length < 1) return LoadableFactory.failed("No rewards found");
  if (ids.length > 1)
    return LoadableFactory.failed("More than one reward loaded");
  return rewardState.items[ids[0]];
};

export const getLoadedRewards = (state: State): Reward[] => {
  const rewardState = getRewardState(state);
  return getRewardIDs(state)
    .map(id => rewardState.items[id])
    .filter((u): u is LoadedLoadable<Reward> => u.state === "LOADED")
    .map(u => u.item);
};

export const getRewardByID = (state: State, id: string): Loadable<Reward> => {
  const rewardState = getRewardState(state);
  return rewardState.items[id] || LoadableFactory.absent([id]);
};

export const getRewardListByIDs = (state: State, ids: string[]) => {
  const rewardState = getRewardState(state);
  const loadables = ids.map(
    id => rewardState.items[id] || LoadableFactory.absent([id])
  );
  const absent = loadables
    .filter((l): l is AbsentLoadable => l.state === "ABSENT")
    .map(loadable => loadable.ids);
  if (absent.length > 0)
    return LoadableFactory.absent(([] as string[]).concat(...absent));

  const failures = loadables
    .filter((l): l is FailedLoadable => l.state === "FAILED")
    .map(loadable => loadable.message);
  if (failures.length > 0) return LoadableFactory.failed(failures.join(" "));

  const pendings = loadables.filter(
    (l): l is PendingLoadable => l.state === "PENDING"
  );
  if (pendings.length > 0) return LoadableFactory.pending();

  const loaded = loadables
    .filter((l): l is LoadedLoadable<Reward> => l.state === "LOADED")
    .map(l => l.item);

  return LoadableFactory.loaded(loaded);
};
