import { Action, actions } from "@src/actions";
import { getType } from "typesafe-actions";
import {
  LoadableFactory,
  LoadableContainer,
  makeLoadableContainer
} from "@tyro";
import { Reward } from "@src/types/Models";

export type RewardState = LoadableContainer<Reward>;

export const rewardReducer = function(
  state: RewardState = makeLoadableContainer<Reward>(),
  action: Action
): RewardState {
  switch (action.type) {
    case getType(actions.getRewardList.request):
      return state.request(action);

    case getType(actions.getReward.request):
      return state.requestOne(action);

    case getType(actions.getRewardList.success):
    case getType(actions.getReward.success):
      return state.receive(action);

    case getType(actions.getRewardList.failure):
    case getType(actions.getReward.failure):
      return state.fail(action);

    case getType(actions.clearAllRewardErrors):
      return state.clearErrors();

    case getType(actions.clearRewardError):
      return state.clearError(action.payload.requestID);
  }
  return state;
};
