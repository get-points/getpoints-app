import React from "react";
import { Text, View } from "react-native";
import glamorous from "glamorous-native";
import { RewardTileHeader } from "../RewardTileHeader";
import { Icon } from "../Icon";
import { RectangularButton, FixedWidthButton } from "../RectangularButton";
import {
  GETPOINTS_WHITE,
  GETPOINTS_LIGHT_GREEN,
  GETPOINTS_DARK_TEAL,
  GETPOINTS_TEAL,
  GETPOINTS_LIGHT_BLUE,
  GETPOINTS_BLUE
} from "@src/const";
import { Reward } from "@src/types/Models";
import { Loading } from "../Loading";
import { ErrorMessage } from "../ErrorMessage";
import { PointStatus } from "../PointStatus";
import { Action } from "@src/actions";

const RadioBox = glamorous(Icon)((props: {}) => ({
  margin: 2
}));

const WhiteContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1,
  backgroundColor: GETPOINTS_WHITE,
  borderWidth: 1,
  borderTopWidth: 0,
  borderStyle: "solid",
  borderColor: GETPOINTS_DARK_TEAL,
  borderBottomLeftRadius: 2,
  borderBottomRightRadius: 2,
  padding: 5
}));

const Button = glamorous(FixedWidthButton)((props: {}) => ({
  alignSelf: "center" as "center",
  flexGrow: 0,
  margin: 5
}));

const StampsTileContainer = glamorous(View)((props: {}) => ({
  // marginTop: 5, marginBottom: 5
}));

const ButtonContainer = glamorous(View)((props: {}) => ({
  flexDirection: "row",
  justifyContent: "center"
}));

const StatusContainer = glamorous(View)((props: {}) => ({
  flexDirection: "row"
}));

export class StampsTile extends React.Component<
  {
    totalStamps: number;
    filledStamps: number;
    stampsNeeded: number;
    status: string;
    offerHeadline: string;
    requirement: string;
    tapGetStampedButton: () => void;
    tapRedeemButton: () => void;
    loadContent?: (x?: Action) => any;

    loaded?: boolean;
    error?: string;
  },
  {
    isOpen: boolean;
  }
> {
  state = {
    isOpen: false
  };
  componentDidMount() {
    this.update(this.props);
  }
  componentWillReceiveProps(nextProps: StampsTile["props"]) {
    this.update(nextProps);
  }
  update = (props: StampsTile["props"]) =>
    props.loadContent && props.loadContent();

  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }

    let redeemButton;
    if (this.props.status === "Unlocked") {
      redeemButton = (
        <Button topRightSquare onTap={this.props.tapRedeemButton}>
          Redeem
        </Button>
      );
    }

    let stampTileLower;
    if (this.state.isOpen === true) {
      stampTileLower = (
        <WhiteContainer>
          <StatusContainer>
            <PointStatus points={this.props.totalStamps}>
              Stamps Required
            </PointStatus>
            <PointStatus points={this.props.filledStamps}>
              Stamps Earned
            </PointStatus>
            <PointStatus end points={this.props.stampsNeeded}>
              Stamps Needed
            </PointStatus>
          </StatusContainer>
          <ButtonContainer>
            <Button
              color={GETPOINTS_TEAL}
              topLeftSquare
              onTap={this.props.tapGetStampedButton}
            >
              Get Stamped
            </Button>
            {redeemButton}
          </ButtonContainer>
        </WhiteContainer>
      );
    } else {
      stampTileLower = null;
    }

    return (
      <StampsTileContainer>
        <RewardTileHeader
          isOpen={this.state.isOpen}
          offerHeadline={this.props.offerHeadline}
          requirement={this.props.requirement}
          status={this.props.status}
          tapDown={() => this.setState({ isOpen: !this.state.isOpen })}
          color={
            this.props.status === "Locked"
              ? GETPOINTS_LIGHT_BLUE
              : GETPOINTS_BLUE
          }
        />
        {stampTileLower}
      </StampsTileContainer>
    );
  }
}
