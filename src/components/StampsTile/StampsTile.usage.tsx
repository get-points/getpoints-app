import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { StampsTile } from "./StampsTile";

export const basic = () => (
  <StampsTile
    offerHeadline={text("offerHeadline", "Offer Headline")}
    requirement={text("requirement", "Offer Requirement")}
    status={text("status", "Locked")}
    filledStamps={number("filledStamps", 5)}
    stampsNeeded={number("filledStamps", 6)}
    totalStamps={number("totalStamps", 11)}
    tapGetStampedButton={action("Get stamped")}
    tapRedeemButton={action("Redeem")}
  />
);
