import { StampsTile as Component } from "./StampsTile";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State } from "@src/store";
import { actions, Action } from "@src/actions";
import {} from "@src/store/selectors";
import { getStampProgramByID } from "@src/store/stampProgram/selectors";
import { PopupType } from "@src/actions/ui";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]> & {
    programID: string;
    filledStamps: number;
  }
): Partial<Component["props"] & { loadAction: Action }> {
  try {
    // prepare data here
    const programID = props.programID;
    const loadableProgram = getStampProgramByID(store, programID);
    if (loadableProgram.state === "ABSENT")
      return {
        loaded: false,
        loadAction: actions.getStampProgram.request({ id: programID })
      };
    if (loadableProgram.state === "PENDING") return { loaded: false };
    if (loadableProgram.state === "FAILED")
      return { error: loadableProgram.message };
    const program = loadableProgram.item;

    return {
      // pass data props from here
      totalStamps: program.stampsRequired,
      offerHeadline: program.headline,
      requirement: program.stampsRequired + " Stamps",
      filledStamps: props.filledStamps,
      stampsNeeded: Math.max(program.stampsRequired - props.filledStamps, 0),
      status:
        props.filledStamps >= program.stampsRequired ? "Unlocked" : "Locked"
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]> & {
    programID: string;
    filledStamps: number;
  }
): Partial<Component["props"]> {
  return {
    tapGetStampedButton: () =>
      dispatch(
        actions.showPopup({
          popup: {
            type: PopupType.STAMPS_EARN,
            data: {
              stampProgramID: props.programID,
              stampsAvailable: props.filledStamps
            }
          }
        })
      ),
    tapRedeemButton: () =>
      dispatch(
        actions.showPopup({
          popup: {
            type: PopupType.STAMPS_REDEMPTION,
            data: {
              stampProgramID: props.programID,
              stampsAvailable: props.filledStamps
            }
          }
        })
      ),
    loadContent: action => dispatch(action!)
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps,
  (storeProps, functionProps, parentProps) => {
    return {
      ...storeProps,
      ...functionProps,
      ...parentProps,
      loadContent: storeProps.loadAction
        ? () => functionProps.loadContent!(storeProps.loadAction)
        : undefined
    };
  }
)(Component as any);
