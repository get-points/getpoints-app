import { GETPOINTS_WHITE } from "@src/const";
import React from "react";
import { View, ViewStyle } from "react-native";
import glamorous from "glamorous-native";

import { WhiteButton } from "../../components/WhiteButton";

export interface FunctionProps {
  onTapFAQs: () => any;
  // onTapTour: () => any;
  onTapSupport: () => any;
  onTapAbout: () => any;
  onTapTerms: () => any;
}

export interface StateProps {}

export interface ParentProps {
  style?: ViewStyle;
  selected?: "faq" | "about" | "terms" | "support";
}

export interface LocalProps {}

const InPageMenuBarContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  // justifyContent: "space-between",
  flexShrink: 1,
  flexGrow: 1,
  backgroundColor: GETPOINTS_WHITE,
  padding: 5
}));

export class Component extends React.Component<
  FunctionProps & StateProps & ParentProps & LocalProps
> {
  render() {
    return (
      <InPageMenuBarContainer style={this.props.style}>
        <WhiteButton
          selected={this.props.selected === "faq"}
          onTap={this.props.onTapFAQs}
        >
          FAQs
        </WhiteButton>
        <WhiteButton
          selected={this.props.selected === "support"}
          onTap={this.props.onTapSupport}
        >
          Support
        </WhiteButton>
        <WhiteButton
          selected={this.props.selected === "about"}
          onTap={this.props.onTapAbout}
        >
          About
        </WhiteButton>
        <WhiteButton
          selected={this.props.selected === "terms"}
          onTap={this.props.onTapTerms}
        >
          T&Cs
        </WhiteButton>
      </InPageMenuBarContainer>
    );
  }
}
