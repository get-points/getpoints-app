import React from "react";
import { boolean, text, number, select } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { InPageMenuBar } from "./index";

export const basic = () => (
  <InPageMenuBar
    selected={select("selected", ["faq", "support", "about"], "faq")}
  />
);
