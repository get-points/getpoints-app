import { State } from "@src/store";
import { push } from "connected-react-router";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import {
  Component,
  FunctionProps,
  ParentProps,
  StateProps
} from "./InPageMenuBar";

function mapStateToProps(state: State, props: ParentProps): StateProps {
  return {};
}

function mapDispatchToProps(
  dispatch: Dispatch,
  props: ParentProps
): FunctionProps {
  return {
    onTapAbout: () => dispatch(push("/about/us")),
    onTapSupport: () => dispatch(push("/about/support")),
    onTapFAQs: () => dispatch(push("/about/faqs")),
    onTapTerms: () => dispatch(push("/about/terms"))
    // onTapTour: () => dispatch(push("/about/how-it-works"))
  };
}

function mergeProps(
  stateProps: ReturnType<typeof mapStateToProps>,
  dispatchProps: ReturnType<typeof mapDispatchToProps>,
  ownProps: ParentProps
): StateProps & FunctionProps & ParentProps {
  return {
    onTapAbout: dispatchProps.onTapAbout,
    onTapSupport: dispatchProps.onTapSupport,
    onTapFAQs: dispatchProps.onTapFAQs,
    onTapTerms: dispatchProps.onTapTerms,
    // onTapTour: dispatchProps.onTapTour,
    selected: ownProps.selected
  };
}

export const ConnectedComponent = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Component);
