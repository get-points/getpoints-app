import { GETPOINTS_GREEN, GETPOINTS_WHITE } from "@src/const";
import React from "react";
import { View, ViewStyle } from "react-native";
import glamorous from "glamorous-native";

import { WhiteButton, WhiteButtonGreen } from "../../components/WhiteButton";

const InPageMenuBarContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flex: 0,
  backgroundColor: GETPOINTS_WHITE,
  padding: 5
}));
const MenuWhiteButton = glamorous(WhiteButtonGreen)({
  padding: 5
});

export interface FunctionProps {
  // onTapAbout: () => any;
  // onTapHours: () => any;
  // onTapGallery: () => any;
  // onTapMap: () => any;
}
export interface StateProps {}
export interface ParentProps {
  style?: ViewStyle;
  businessID: string;
  setMode: (x: "hours" | "about" | "gallery" | "map") => any;
  selected?: "hours" | "about" | "gallery" | "map";
}
export interface LocalProps {}

export class InPageMenuBarGreen extends React.Component<
  FunctionProps & StateProps & ParentProps & LocalProps
> {
  render() {
    return (
      <InPageMenuBarContainer style={this.props.style}>
        <MenuWhiteButton
          onTap={() => this.props.setMode("about")}
          selected={this.props.selected === "about"}
        >
          About
        </MenuWhiteButton>
        <MenuWhiteButton
          onTap={() => this.props.setMode("hours")}
          selected={this.props.selected === "hours"}
        >
          Hours
        </MenuWhiteButton>
        <MenuWhiteButton
          onTap={() => this.props.setMode("gallery")}
          selected={this.props.selected === "gallery"}
        >
          Gallery
        </MenuWhiteButton>
        <MenuWhiteButton
          onTap={() => this.props.setMode("map")}
          selected={this.props.selected === "map"}
        >
          Map
        </MenuWhiteButton>
      </InPageMenuBarContainer>
    );
  }
}
