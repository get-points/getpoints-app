import { State } from "@src/store";
import { goBack, push } from "connected-react-router";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { Dispatch } from "redux";

import {
  InPageMenuBarGreen,
  FunctionProps,
  StateProps,
  ParentProps
} from "./InPageMenuBarGreen";

function mapStateToProps(state: State, props: ParentProps): StateProps {
  return {};
}

function mapDispatchToProps(
  dispatch: Dispatch,
  props: ParentProps
): FunctionProps {
  return {
    // onTapAbout: () => dispatch(push(`/business/${props.businessID}/about`)),
    // onTapHours: () => dispatch(push(`/business/${props.businessID}/hours`)),
    // onTapGallery: () => dispatch(push(`/business/${props.businessID}/gallery`)),
    // onTapMap: () => dispatch(push(`/business/${props.businessID}/map`))
  };
}

function mergeProps(
  stateProps: ReturnType<typeof mapStateToProps>,
  dispatchProps: ReturnType<typeof mapDispatchToProps>,
  ownProps: ParentProps
): InPageMenuBarGreen["props"] {
  return {
    ...ownProps
    // onTapAbout: dispatchProps.onTapAbout,
    // onTapHours: dispatchProps.onTapHours,
    // onTapGallery: dispatchProps.onTapGallery,
    // onTapMap: dispatchProps.onTapMap
  };
}

export const ConnectedInPageMenuBarGreen = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(InPageMenuBarGreen);
