import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { InPageMenuBarGreen } from "./index";

export const basic = () => (
  <InPageMenuBarGreen selected="reviews" businessID="42" />
);
