import { GETPOINTS_WHITE, GETPOINTS_GREEN } from "@src/const";
import React, { ReactNode } from "react";
import { View, Image } from "react-native";
import glamorous from "glamorous-native";
import { Icon } from "../Icon";
import { Constants } from "expo";

export interface FunctionProps {
  tapMenuButton: () => any;
}

export interface StateProps {}

export interface ParentProps {
  hideMenu?: boolean;
}

export interface LocalProps {}

const HeaderBarContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "center",
  backgroundColor: GETPOINTS_WHITE,
  paddingTop: 0,
  paddingBottom: 0,
  paddingLeft: 16,
  paddingRight: 16,
  height: 70,
  width: `100%`
}));

const IconWrapper = glamorous(View)((props: {}) => ({
  position: "absolute",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  top: 0,
  bottom: 0,
  left: 15,
  zIndex: 1
}));

const GetPointsLogo = glamorous(Image)((props: {}) => ({
  flexGrow: 1,
  height: 26
}));

export class HeaderBar extends React.Component<
  FunctionProps & StateProps & LocalProps & ParentProps
> {
  render() {
    return (
      <HeaderBarContainer>
        {this.props.hideMenu ? null : (
          <IconWrapper>
            <Icon
              onPress={this.props.tapMenuButton}
              name="md-menu"
              size={40}
              color={GETPOINTS_GREEN}
            />
          </IconWrapper>
        )}

        <GetPointsLogo
          resizeMode="contain"
          source={require("../../../assets/splash.png")}
        />
      </HeaderBarContainer>
    );
  }
}
