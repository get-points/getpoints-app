import { State } from "@src/store";
import { connect } from "react-redux";
import { Dispatch } from "redux";

import { HeaderBar, StateProps, FunctionProps, ParentProps } from "./HeaderBar";
import { actions } from "@src/actions";

function mapStateToProps(state: State, props: ParentProps): StateProps {
  return {};
}

function mapDispatchToProps(
  dispatch: Dispatch,
  props: ParentProps
): FunctionProps {
  return {
    tapMenuButton: () => {
      let action = actions.openMenu();
      dispatch(action);
    }
  };
}

export const ConnectedHeaderBar = connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderBar);
