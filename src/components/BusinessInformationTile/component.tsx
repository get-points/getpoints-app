import React from "react";
import { Text, View } from "react-native";
import glamorous from "glamorous-native";
import { StyleProp, ViewStyle } from "react-native";
import { InformationTile } from "../InformationTile";
import { GETPOINTS_TEAL, GETPOINTS_YELLOW } from "@src/const";
import { ExplainerText } from "../Typography/ExplainerText";
import { Icon } from "../Icon";
import { Business } from "@src/types/Models";

/**
 * Component Properties
 */

export interface FunctionProps {}

export interface StoreProps {}

export interface ParentProps {
  style?: StyleProp<ViewStyle>;
  contacts: Business["contacts"];
  locations: Business["locations"];
}

export interface LocalProps {}

type ComponentProps = FunctionProps & StoreProps & ParentProps & LocalProps;

/**
 * Styled Components
 */

const LabelText = glamorous(ExplainerText)((props: {}) => ({
  fontWeight: "bold",
  textAlign: "left",
  fontSize: 13,
  marginTop: 15,
  marginBottom: 5
}));

const LeftAlignedExplainerText = glamorous(ExplainerText)((props: {}) => ({
  textAlign: "left",
  marginTop: 0,
  marginBottom: 0
}));

/**
 * Component Definition
 */

export class Component extends React.Component<ComponentProps> {
  getLocations() {
    return this.props.locations.map((l, i) => (
      <View key={i}>
        <LeftAlignedExplainerText>{l.description}</LeftAlignedExplainerText>
      </View>
    ));
  }

  getContacts() {
    return this.props.contacts.map(c => {
      let label;
      switch (c.type) {
        case "ADDRESS":
          return null;
        case "EMAIL":
          label = "Email";
          break;
        case "PHONE":
          label = "Phone";
          break;
        case "WEBSITE":
          label = "Website";
          break;
      }

      return (
        <View key={label}>
          <LabelText>{label}</LabelText>
          <LeftAlignedExplainerText>{c.text}</LeftAlignedExplainerText>
        </View>
      );
    });
  }

  render() {
    return (
      <InformationTile title="Business Information" color={"#FFAA06"}>
        <LabelText>Locations</LabelText>
        {this.getLocations()}

        {this.getContacts()}
      </InformationTile>
    );
  }
}
