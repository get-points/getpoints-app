import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { BusinessInformationTile } from "./index";

export const basic = () => (
  <BusinessInformationTile
    businessLocation={text("businessLocation", "31 Chwa II Road, Mbuya")}
    businessPhoneNumber={text("businessPhoneNumber", "+256 712345678")}
    businessEmail={text("businessEmail", "manager@myrestaurant.com")}
    businessWebsite={text("businessWebsite", "www.myrestaurant.com")}
  />
);
