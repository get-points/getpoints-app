import React from "react";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { Text, View, ActivityIndicator } from "react-native";
import glamorous from "glamorous-native";
import { StyleProp, ViewStyle } from "react-native";
import { GETPOINTS_GREEN } from "@src/const";

/**
 * Component Properties
 */

export interface FunctionProps {}

export interface StoreProps {}

export interface ParentProps {
  style?: StyleProp<ViewStyle>;
}

export interface LocalProps {}

type ComponentProps = FunctionProps & StoreProps & ParentProps;

/**
 * Styled Components
 */

const LoadingContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  margin: 20
}));

/**
 * Component Definition
 */

export class Component extends React.Component<ComponentProps, LocalProps> {
  state: LocalProps = {};

  render() {
    return (
      <LoadingContainer style={this.props.style}>
        <ActivityIndicator size="large" color={GETPOINTS_GREEN} />
      </LoadingContainer>
    );
  }
}
