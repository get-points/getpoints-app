import React from "react";
import { Text, View } from "react-native";
import glamorous from "glamorous-native";
import {
  GETPOINTS_GREEN,
  GETPOINTS_WHITE,
  GETPOINTS_FONT_PRIMARY
} from "@src/const";
import { TableRowText } from "../Typography/TableRowText";

const TableRowContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 1,
  paddingTop: 4,
  paddingBottom: 4,
  paddingLeft: 6,
  paddingRight: 6
}));

const Cell = glamorous(Text)((props: {}) => ({
  fontFamily: GETPOINTS_FONT_PRIMARY,
  flexGrow: 1,
  marginLeft: 1,
  marginRight: 1,
  backgroundColor: GETPOINTS_GREEN,
  borderWidth: 1,
  borderStyle: "solid",
  borderColor: GETPOINTS_GREEN,
  borderRadius: 2,
  color: "white",
  fontSize: 12,
  textAlign: "center" as "center",
  paddingTop: 10,
  paddingBottom: 10,
  paddingLeft: 3,
  paddingRight: 3
}));

const Date = glamorous(Cell)((props: {}) => ({
  maxWidth: `25%`,
  marginLeft: 0
}));

const Time = glamorous(Cell)((props: {}) => ({
  maxWidth: `20%`
}));

const Business = glamorous(Cell)((props: {}) => ({
  maxWidth: `40%`
}));

const PointsChange = glamorous(Cell)((props: {}) => ({
  maxWidth: `15%`,
  marginRight: 0
}));
export class TableHeaderRow extends React.Component<{}> {
  render() {
    return (
      <TableRowContainer>
        <Date>Date</Date>
        <Time>Time</Time>
        <Business>Business Name</Business>
        <PointsChange>+1</PointsChange>
      </TableRowContainer>
    );
  }
}
