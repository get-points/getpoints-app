import React, { Children, ReactNode } from "react";
import {
  Text,
  View,
  Button,
  TouchableHighlight,
  ViewStyle,
  TouchableOpacity,
  ViewProps,
  StyleProp
} from "react-native";
import glamorous from "glamorous-native";
import {
  GETPOINTS_GREEN,
  GETPOINTS_WHITE,
  GETPOINTS_FONT_PRIMARY
} from "@src/const";

const MyText = glamorous(Text)((props: {}) => ({
  color: GETPOINTS_WHITE,
  fontSize: 12,
  fontFamily: GETPOINTS_FONT_PRIMARY,
  fontStyle: "normal",
  fontWeight: "normal",
  textAlign: "center"
}));

class MyView extends React.PureComponent<
  {
    color?: string;
    small?: boolean;
    topLeftSquare?: boolean;
    topRightSquare?: boolean;
    bottomRightSquare?: boolean;
    bottomLeftSquare?: boolean;
  } & View["props"]
> {
  render() {
    const {
      color,
      topLeftSquare,
      topRightSquare,
      bottomRightSquare,
      bottomLeftSquare,
      small,
      children,
      ...props
    } = this.props;
    return <View {...props}>{children}</View>;
  }
}

const RectangularButtonContainer = glamorous(MyView)(
  (props: {
    color?: string;
    topLeftSquare?: boolean;
    topRightSquare?: boolean;
    bottomRightSquare?: boolean;
    bottomLeftSquare?: boolean;
  }) => ({
    // display: "inline-block",
    // flex: 1,
    flexGrow: 1,
    padding: 10,
    borderTopLeftRadius: props.topLeftSquare ? undefined : 10,
    borderTopRightRadius: props.topRightSquare ? undefined : 10,
    borderBottomLeftRadius: props.bottomLeftSquare ? undefined : 10,
    borderBottomRightRadius: props.bottomRightSquare ? undefined : 10,

    backgroundColor: props.color || GETPOINTS_GREEN,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 3,
    marginRight: 3,
    alignItems: "stretch"
  })
);

const SmallRectangularButtonContainer = glamorous(RectangularButtonContainer)(
  (props: {}) => ({
    padding: 8,
    fontSize: 12
  })
);

export class RectangularButton extends React.Component<{
  style?: StyleProp<ViewStyle>;
  className?: string;
  small?: boolean;
  color?: string;
  topLeftSquare?: boolean;
  topRightSquare?: boolean;
  bottomRightSquare?: boolean;
  bottomLeftSquare?: boolean;
  children: string;
  onTap?: () => any;
}> {
  render() {
    // alert(JSON.stringify(this.props));
    const {
      small,
      color,
      topLeftSquare,
      bottomLeftSquare,
      bottomRightSquare,
      topRightSquare,
      className,
      style,
      ...props
    } = this.props;
    let button;
    if (this.props.small) {
      button = (
        <SmallRectangularButtonContainer
          {...{
            small,
            color,
            topLeftSquare,
            bottomLeftSquare,
            bottomRightSquare,
            topRightSquare,
            className,
            style
          }}
        >
          <MyText>{this.props.children.toUpperCase()}</MyText>
        </SmallRectangularButtonContainer>
      );
    } else {
      button = (
        <RectangularButtonContainer
          {...{
            small,
            color,
            topLeftSquare,
            bottomLeftSquare,
            bottomRightSquare,
            topRightSquare,
            className,
            style
          }}
        >
          <MyText>{this.props.children.toUpperCase()}</MyText>
        </RectangularButtonContainer>
      );
    }
    if (this.props.onTap) {
      return (
        <TouchableOpacity onPress={this.props.onTap}>{button}</TouchableOpacity>
      );
    }

    return button;
  }
}

export const FixedWidthButton = glamorous(RectangularButton)((props: {}) => ({
  width: 100,
  alignSelf: "center" as "center",
  flexGrow: 0
}));

export const ChangePasswordButton = glamorous(FixedWidthButton)(
  (props: {}) => ({
    width: 150
  })
);
