import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { RectangularButton } from "./";

export const basic = () => (
  <RectangularButton
    onTap={action("Tapped button")}
    bottomLeftSquare={boolean("bottomLeftSquare", false)}
    bottomRightSquare={boolean("bottomRightSquare", false)}
    topRightSquare={boolean("topRightSquare", false)}
    topLeftSquare={boolean("topLeftSquare", false)}
    small={boolean("small", false)}
  >
    {text("label", "Click me")}
  </RectangularButton>
);
