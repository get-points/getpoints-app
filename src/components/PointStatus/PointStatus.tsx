import React from "react";
import { Text, View } from "react-native";
import glamorous from "glamorous-native";
import {} from "../Typography/ExplainerText";
import { GETPOINTS_GREY, GETPOINTS_WHITE } from "@src/const";

const PointStatusContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flex: 1,
  marginTop: 10,
  marginBottom: 10
}));
const PointsStatus = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  textAlign: "center",
  flex: 1
}));
const PointsText = glamorous(Text)((props: { end?: boolean }) => ({
  backgroundColor: GETPOINTS_WHITE,
  color: GETPOINTS_GREY,
  padding: 8,
  flex: 1,
  fontSize: 12,
  borderWidth: 1,
  borderStyle: "solid",
  borderRightWidth: props.end ? 1 : 0,
  borderColor: GETPOINTS_GREY
}));
const PointsNumber = glamorous(Text)((props: { end?: boolean }) => ({
  backgroundColor: GETPOINTS_WHITE,
  borderWidth: 1,
  borderTopWidth: 0,
  borderStyle: "solid",
  borderColor: GETPOINTS_GREY,
  color: GETPOINTS_GREY,
  borderRightWidth: props.end ? 1 : 0,
  padding: 8,
  flex: 1
}));

export class PointStatus extends React.Component<{
  points: number;
  end?: boolean;
}> {
  render() {
    return (
      <PointStatusContainer>
        <PointsStatus>
          <PointsText end={this.props.end}>{this.props.children}</PointsText>
          <PointsNumber end={this.props.end}>{this.props.points}</PointsNumber>
        </PointsStatus>
      </PointStatusContainer>
    );
  }
}
