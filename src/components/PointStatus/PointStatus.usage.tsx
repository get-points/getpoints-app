import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { PointStatus } from "./index";

export const basic = () => (
  <PointStatus points={number("points", 5)} end>
    {text("label", "My Visits")}
  </PointStatus>
);
