import React, { ReactNode } from "react";
import {
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
  KeyboardAvoidingView
} from "react-native";
import glamorous from "glamorous-native";
import { StyleProp, ViewStyle } from "react-native";
import { GETPOINTS_TEAL, GETPOINTS_WHITE } from "@src/const";
import { HeaderText } from "../Typography/HeaderText";
import { WhiteButton } from "../WhiteButton";

/**
 * Component Properties
 */

export interface FunctionProps {
  clearPopup: () => any;
}

export interface StoreProps {}

export interface ParentProps {
  style?: StyleProp<ViewStyle>;
  hideFrame?: boolean;
  title?: string;
  children: ReactNode;
}

export interface LocalProps {}

type ComponentProps = FunctionProps & StoreProps & ParentProps & LocalProps;

/**
 * Styled Components
 */

const fullScreen = {
  position: "absolute",
  top: 0,
  left: 0,
  right: 0,
  bottom: 0
};

const PopupContainerContainer = glamorous(KeyboardAvoidingView)(
  (props: {}) => ({
    display: "flex",
    ...fullScreen,
    flexGrow: 0,
    flexShrink: 1,
    zIndex: 20,
    alignItems: "center",
    justifyContent: "center"
  })
);

const Contents = glamorous(View)({
  maxWidth: "90%"
});
const ClickZone = glamorous(TouchableWithoutFeedback)({
  ...fullScreen
});
const ClickView = glamorous(View)({
  backgroundColor: "rgba(0,0,0,0.5)",
  ...fullScreen
});

const HeaderBarContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "center",
  backgroundColor: GETPOINTS_TEAL,
  paddingTop: 0,
  paddingBottom: 0,
  paddingLeft: 16,
  paddingRight: 16,
  height: 70,
  width: `100%`
}));
const GetPointsLogo = glamorous(Image)((props: {}) => ({
  flexGrow: 1,
  height: 26
}));

const PageContent = glamorous(View)({
  backgroundColor: "white",
  borderWidth: 2,
  borderRadius: 20,
  overflow: "hidden",
  borderColor: GETPOINTS_TEAL
});
const TitleText = glamorous(HeaderText)({
  color: GETPOINTS_WHITE,
  fontSize: 25
});

/**
 * Component Definition
 */

export class Component extends React.Component<ComponentProps> {
  getTitle = () => {
    if (this.props.title) return <TitleText>{this.props.title}</TitleText>;
    return (
      <GetPointsLogo
        resizeMode="contain"
        source={require("../../../assets/getpoints-light.png")}
      />
    );
  };

  getContents = () => {
    if (this.props.hideFrame) return this.props.children;

    return (
      <PageContent>
        <HeaderBarContainer>{this.getTitle()}</HeaderBarContainer>
        <View style={{ padding: 10 }}>{this.props.children}</View>
      </PageContent>
    );
  };

  render() {
    return (
      <PopupContainerContainer style={this.props.style} behavior="padding">
        <ClickZone onPress={this.props.clearPopup}>
          <ClickView />
        </ClickZone>
        <Contents>{this.getContents()}</Contents>
      </PopupContainerContainer>
    );
  }
}
