import { State } from "@src/store";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import {
  WhiteButtonComponent,
  WhiteButtonFunctionProps,
  WhiteButtonParentProps,
  WhiteButtonProps,
  WhiteButtonStateProps
} from "./WhiteButton";

function mapStateToProps(
  state: State,
  props: WhiteButtonParentProps
): WhiteButtonStateProps {
  return {};
}

function mapDispatchToProps(
  dispatch: Dispatch,
  props: WhiteButtonParentProps
): WhiteButtonFunctionProps {
  return {};
}

function mergeProps(
  stateProps: WhiteButtonStateProps,
  functionProps: WhiteButtonFunctionProps,
  parentProps: WhiteButtonParentProps
): WhiteButtonProps {
  return {
    ...parentProps
  };
}

export const enhance = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
);
