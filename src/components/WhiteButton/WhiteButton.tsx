import {
  GETPOINTS_BACKGROUND_GREY,
  GETPOINTS_FONT_PRIMARY,
  GETPOINTS_GREEN,
  GETPOINTS_GREY,
  GETPOINTS_LIGHT_GREEN,
  GETPOINTS_WHITE
} from "@src/const";
import glamorous from "glamorous-native";
import React, { ReactNode } from "react";
import {
  StyleProp,
  Text,
  TouchableHighlight,
  ViewStyle,
  TextStyle,
  View,
  TouchableOpacity
} from "react-native";

// TYPE DEFINITIONS
export interface WhiteButtonLocalProps {}
export interface WhiteButtonParentProps {
  style?: StyleProp<ViewStyle>;
  className?: string;
  textStyle?: StyleProp<TextStyle>;
  children: ReactNode;
  onTap?: () => any;
  selected?: boolean;
}
export interface WhiteButtonStateProps {}
export interface WhiteButtonFunctionProps {}
export type WhiteButtonProps = WhiteButtonLocalProps &
  WhiteButtonParentProps &
  WhiteButtonStateProps &
  WhiteButtonFunctionProps;

// STYLED COMPONENTS

interface WhiteButtonContainerProps {
  selected?: boolean;
}
const WhiteButtonContainer = glamorous(TouchableOpacity)(
  (props: WhiteButtonContainerProps) => ({
    display: "flex",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 1,
    minHeight: 40,
    backgroundColor: props.selected ? GETPOINTS_GREEN : GETPOINTS_WHITE,
    borderColor: props.selected ? GETPOINTS_WHITE : GETPOINTS_GREY,
    borderWidth: 1,
    borderStyle: "solid",
    borderRadius: 6,
    padding: 3,
    alignItems: "center",
    justifyContent: "center",
    marginLeft: 2,
    marginRight: 2
  })
) as any;

interface WhiteButtonTextProps {
  selected?: boolean;
}
const WhiteButtonText = glamorous(Text)((props: WhiteButtonTextProps) => ({
  color: props.selected ? GETPOINTS_WHITE : GETPOINTS_GREY,
  fontFamily: GETPOINTS_FONT_PRIMARY,
  fontSize: 12,
  textAlign: "center",
  // lineHeight: 12,
  // marginTop: 10,
  // marginBottom: 10,
  marginLeft: 3,
  marginRight: 3
})) as any;

// PRIMARY COMPONENT

export class WhiteButtonComponent extends React.Component<WhiteButtonProps> {
  render() {
    const button = (
      <WhiteButtonContainer
        style={this.props.style}
        selected={this.props.selected}
        className={this.props.className}
        onPress={this.props.onTap}
      >
        <WhiteButtonText
          style={this.props.textStyle}
          selected={this.props.selected}
        >
          {this.props.children}
        </WhiteButtonText>
      </WhiteButtonContainer>
    );

    // if (this.props.onTap) {
    //   return (
    //     <TouchableOpacity onPress={this.props.onTap}>{button}</TouchableOpacity>
    //   );
    // }
    return button;
  }
}
