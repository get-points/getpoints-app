import { GETPOINTS_GREEN } from "@src/const";
import glamorous from "glamorous-native";
import { enhance } from "./enhancer";
import { WhiteButtonComponent } from "./WhiteButton";

export const WhiteButton = enhance(WhiteButtonComponent);

// VARIANTS
export const WhiteButtonGreen = enhance(
  glamorous(WhiteButtonComponent)({
    borderColor: GETPOINTS_GREEN
  })
);
