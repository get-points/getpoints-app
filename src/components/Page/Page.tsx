import React from "react";
import { Text, View, ViewStyle } from "react-native";
import glamorous from "glamorous-native";

const PageContainer = glamorous(View)((props: {}) => ({
  backgroundColor: "lightgrey",
  padding: 10
}));

export class Page extends React.Component<{
  style?: ViewStyle;
}> {
  static defaultProps: Page["props"];
  render() {
    return (
      <PageContainer style={this.props.style}>
        {this.props.children}
      </PageContainer>
    );
  }
}
