import React from "react";
import { Text, View } from "react-native";
import glamorous from "glamorous-native";
import { StyleProp, ViewStyle } from "react-native";
import { GETPOINTS_RED } from "@src/const";

/**
 * Component Properties
 */

export interface FunctionProps {}

export interface StoreProps {}

export interface ParentProps {
  style?: StyleProp<ViewStyle>;
  message?: string;
}

export interface LocalProps {}

type ComponentProps = FunctionProps & StoreProps & ParentProps;

/**
 * Styled Components
 */

const ErrorMessageContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  borderColor: GETPOINTS_RED,
  borderWidth: 3,
  borderRadius: 5,
  margin: 10,
  padding: 20,
  alignSelf: "center"
}));

const RedText = glamorous(Text)((props: {}) => ({
  color: GETPOINTS_RED,
  textAlign: "center"
}));

/**
 * Component Definition
 */

export class Component extends React.Component<ComponentProps, LocalProps> {
  state: LocalProps = {};

  render() {
    return (
      <ErrorMessageContainer style={this.props.style}>
        <RedText>{this.props.message}</RedText>
      </ErrorMessageContainer>
    );
  }
}
