import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { ErrorMessage } from "./index";

export const basic = () => (
  <ErrorMessage message="Houston, we have a problem." />
);
