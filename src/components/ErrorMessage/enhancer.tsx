import { State } from "@src/store";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import {
  Component,
  FunctionProps,
  ParentProps,
  StoreProps,
  LocalProps
} from "./component";

function getStoreProps(store: State, props: ParentProps): StoreProps {
  return {
    numberOfStamps: 6,
    rewardName: "Free Pizza",
    stampRules: "Stamp Rules"
  };
}

function getFunctionProps(
  dispatch: Dispatch,
  props: ParentProps
): FunctionProps {
  return {};
}

function mergeProps(
  storeProps: StoreProps,
  functionProps: FunctionProps,
  parentProps: ParentProps
): StoreProps & FunctionProps & ParentProps {
  return {
    ...storeProps,
    ...functionProps,
    ...parentProps
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps,
  mergeProps
)(Component as any);
