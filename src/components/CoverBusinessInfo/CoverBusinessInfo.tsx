import { GETPOINTS_DARK_GREY, GETPOINTS_BLACK } from "@src/const";
import React from "react";
import { View, StyleProp, ViewStyle } from "react-native";

import { SmallButton } from "../SmallButton";
import { StarRating } from "../StarRating";
import { H4 } from "../Typography/H4";
import { HeaderText } from "../Typography/HeaderText";
import glamorous from "glamorous-native";

export interface CoverBusinessInfoLocalProps {}
export interface CoverBusinessInfoParentProps {
  style?: StyleProp<ViewStyle>;
  id: string;
}
export interface CoverBusinessInfoStateProps {
  businessName: string;
  location: string;
  // rating: number;
  businessType: string;
}
export interface CoverBusinessInfoFunctionProps {
  // tapAddReviewButton: () => any;
}
export type CoverBusinessInfoProps = CoverBusinessInfoLocalProps &
  CoverBusinessInfoParentProps &
  CoverBusinessInfoStateProps &
  CoverBusinessInfoFunctionProps;

interface CoverBusinessInfoContainerProps {}
const CoverBusinessInfoContainer = glamorous(View)({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1,
  // justifyContent: "center",
  alignItems: "flex-start",
  justifyContent: "space-between",
  backgroundColor: "rgba(255,255,255,0.8)",
  paddingHorizontal: 5,
  paddingVertical: 15,
  paddingLeft: 10,
  paddingRight: 10
});

const HeaderGreyText = glamorous(HeaderText)((props: {}) => ({
  color: GETPOINTS_BLACK,
  marginTop: 0,
  marginBottom: 0
}));
const H4GreyText = glamorous(H4)((props: {}) => ({
  color: GETPOINTS_BLACK,
  marginTop: 0,
  marginBottom: 0
}));

export class CoverBusinessInfoComponent extends React.Component<
  CoverBusinessInfoProps
> {
  render() {
    return (
      <CoverBusinessInfoContainer>
        {/* <ContentRow> */}
        <HeaderGreyText>{this.props.businessName}</HeaderGreyText>
        <H4GreyText>
          {this.props.businessType} | {this.props.location}
        </H4GreyText>
        {/* </ContentRow> */}
      </CoverBusinessInfoContainer>
    );
  }
}
