import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { CoverBusinessInfo } from "./index";

export const basic = () => <CoverBusinessInfo id="42" />;
