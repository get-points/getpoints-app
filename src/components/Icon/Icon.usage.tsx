import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { Icon } from "./";
import { GETPOINTS_GREEN } from "@src/const";

export const basic = () => (
  <Icon name="md-menu" color={GETPOINTS_GREEN} size={32} />
);
