import { Ionicons } from "@expo/vector-icons";
import React from "react";
import { View } from "react-native";
import glamorous from "glamorous-native";

export const Icon = Ionicons;

// export class Icon extends React.Component<Ionicons["props"]> {
//   static defaultProps: Icon["props"] = {
//     name: "md-menu"
//   };
//   render() {
//     return (
//       <IconContainer>
//         <Ionicons {...this.props} />
//       </IconContainer>
//     );
//   }
// }
