import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { RewardTile } from "./RewardTile";

export const basic = () => (
  <RewardTile
    offerHeadline={text("offerHeadline", "Offer Headline")}
    status={text("status", "Locked")}
    pointsEarned={4}
    pointsRequired={4}
    pointsNeeded={4}
    tapRedeemButton={action("Tap get stamped")}
  />
);
