import React from "react";
import { Text, View, Image } from "react-native";
import glamorous from "glamorous-native";
import { RewardTileHeader } from "../RewardTileHeader";
import { SmallPointStatusBox } from "../SmallPointStatusBox";
import {
  GETPOINTS_WHITE,
  GETPOINTS_TEAL,
  GETPOINTS_GREEN,
  GETPOINTS_LIGHT_TEAL
} from "@src/const";
import { PointStatus } from "../PointStatus";
import { Action } from "@src/actions";
import { RectangularButton } from "../RectangularButton";
import giftBox from "../../../assets/giftbox-circle-large.png";
import giftBoxSm from "../../../assets/giftbox-circle-small.png";
import filledPoint from "../../../assets/point-stamped.png";
import emptyPoint from "../../../assets/point-empty.png";

const RectangleWhite = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "center",
  padding: 5,
  borderWidth: 1,
  borderTopWidth: 0,
  borderStyle: "solid",
  borderColor: GETPOINTS_TEAL,
  borderBottomLeftRadius: 2,
  borderBottomRightRadius: 2,
  backgroundColor: "white"
}));

const RewardTileContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column"
}));

export class RewardTile extends React.Component<
  {
    pointsRequired: number;
    pointsEarned: number;
    pointsNeeded: number;
    status: string;
    offerHeadline: string;
    tapRedeemButton: () => void;
    loaded?: boolean;
    error?: string;
    loadContent?: (x?: Action) => any;
  },
  {
    isOpen: boolean;
  }
> {
  state = {
    isOpen: false
  };
  componentDidMount() {
    this.update(this.props);
  }
  componentWillReceiveProps(nextProps: RewardTile["props"]) {
    this.update(nextProps);
  }
  update = (props: RewardTile["props"]) =>
    props.loadContent && props.loadContent();

  render() {
    let rewardTileLower;

    if (this.state.isOpen === true) {
      if (this.props.status === "Unlocked") {
        rewardTileLower = (
          <RectangleWhite>
            <Image
              source={giftBox}
              style={{ height: 40, width: 40 }}
              resizeMode="contain"
            />
            <RectangularButton
              topRightSquare
              color={GETPOINTS_GREEN}
              onTap={this.props.tapRedeemButton}
              style={{ flexGrow: 0, minWidth: 190 }}
            >
              REDEEM REWARD
            </RectangularButton>
          </RectangleWhite>
        );
      } else {
        const filledStamps = [...Array(this.props.pointsEarned).keys()].map(
          i => (
            <Image
              source={filledPoint}
              resizeMode="contain"
              key={i}
              style={{ margin: 3, height: 20, width: 20 }}
            />
          )
        );
        const emptyStamps = [...Array(this.props.pointsNeeded - 1).keys()].map(
          i => (
            <Image
              source={emptyPoint}
              resizeMode="contain"
              key={i}
              style={{ margin: 3, height: 20, width: 20 }}
            />
          )
        );
        rewardTileLower = (
          <RectangleWhite style={{ flexDirection: "column" }}>
            <View style={{ flexDirection: "row" }}>
              <PointStatus points={this.props.pointsRequired}>
                Required
              </PointStatus>
              <PointStatus points={this.props.pointsEarned}>Earned</PointStatus>
              <PointStatus end points={this.props.pointsNeeded}>
                Needed
              </PointStatus>
            </View>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              {filledStamps}
              {emptyStamps}
              <Image
                source={giftBoxSm}
                resizeMode="contain"
                style={{ margin: 3, height: 20, width: 20 }}
              />
            </View>
          </RectangleWhite>
        );
      }
    } else {
      rewardTileLower = null;
    }

    return (
      <RewardTileContainer>
        <RewardTileHeader
          isOpen={this.state.isOpen}
          offerHeadline={this.props.offerHeadline}
          requirement={this.props.pointsRequired + " points"}
          status={this.props.status}
          tapDown={() => this.setState({ isOpen: !this.state.isOpen })}
          color={
            this.props.status === "Locked"
              ? GETPOINTS_LIGHT_TEAL
              : GETPOINTS_TEAL
          }
        />
        {rewardTileLower}
      </RewardTileContainer>
    );
  }
}
