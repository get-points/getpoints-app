import { RewardTile as Component } from "./RewardTile";

import { connect } from "react-redux";
import { Dispatch, DeepPartial } from "redux";
import { State } from "@src/store";
import { actions, Action } from "@src/actions";
import {} from "@src/store/selectors";
import { deepmerge } from "@tyro";
import { getRewardByID } from "@src/store/reward/selectors";
import { PopupType } from "@src/actions/ui";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]> & {
    rewardID: string;
    pointsEarned: number;
  }
): Partial<Component["props"] & { loadAction: Action }> {
  try {
    // prepare data here
    const rewardID = props.rewardID;
    const loadableReward = getRewardByID(store, props.rewardID);
    if (loadableReward.state === "ABSENT")
      return {
        loaded: false,
        loadAction: actions.getReward.request({ id: rewardID })
      };
    if (loadableReward.state === "PENDING") return { loaded: false };
    if (loadableReward.state === "FAILED")
      return { error: loadableReward.message };
    const reward = loadableReward.item;

    return {
      pointsRequired: reward.pointsRequired,
      pointsEarned: props.pointsEarned,
      pointsNeeded: Math.max(0, reward.pointsRequired - props.pointsEarned),
      status:
        reward.pointsRequired <= props.pointsEarned ? "Unlocked" : "Locked",
      offerHeadline: reward.headline
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]> & { rewardID: string }
): Partial<Component["props"]> {
  return {
    tapRedeemButton: () =>
      dispatch(
        actions.showPopup({
          popup: {
            type: PopupType.REWARD_REDEMPTION,
            data: { rewardID: props.rewardID }
          }
        })
      ),
    loadContent: action => dispatch(action!)
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps,
  (storeProps, functionProps, parentProps) => {
    return {
      ...storeProps,
      ...functionProps,
      ...parentProps,
      loadContent: storeProps.loadAction
        ? () => functionProps.loadContent!(storeProps.loadAction)
        : undefined
    };
  }
)(Component as any);
