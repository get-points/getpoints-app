import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { RewardTileHeader } from "./";

export const basic = () => (
  <RewardTileHeader
    isOpen={boolean("isOpen", true)}
    offerHeadline={text("offerHeadline", "Offer Headline")}
    requirement={text("requirement", "Requirement")}
    status={text("status", "Locked")}
    tapDown={action("Tapped arrow")}
  />
);
