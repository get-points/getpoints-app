import React from "react";
import { Text, View, TouchableOpacity } from "react-native";
import glamorous from "glamorous-native";
import { H4 } from "../Typography/H4";
import { Icon } from "../Icon";
import { H6 } from "../Typography/H6";
import { H5 } from "../Typography/H5";
import { GETPOINTS_TEAL, GETPOINTS_WHITE, GETPOINTS_GREY } from "@src/const";
import { HeaderText } from "../Typography/HeaderText";

const RewardTileHeaderContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  width: `100%`,
  alignItems: "stretch",
  height: 61,
  marginTop: 10,
  flexGrow: 1
}));

const RightContainer = glamorous(View)((props: { color: string }) => ({
  display: "flex",
  flexDirection: "row",
  backgroundColor: props.color,
  padding: 20,
  flexGrow: 1,
  alignItems: "center",
  height: 61,
  borderRadius: 0,
  borderTopRightRadius: 25
}));

const RectangleLeftAlignedItems = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  height: `100%`,
  flex: 1,
  justifyContent: "center"
}));

const ArrowIcon = glamorous(Icon)((props: {}) => ({
  flex: 0,
  marginLeft: 15
}));

const TightH6 = glamorous(H6)((props: {}) => ({
  margin: 0,
  marginTop: 3
}));
const TightH5 = glamorous(H5)((props: {}) => ({
  margin: 0,
  color: GETPOINTS_GREY,
  padding: 3
}));

const TightH4 = glamorous(HeaderText)((props: {}) => ({
  margin: 0,
  color: GETPOINTS_WHITE,
  textAlign: "left"
}));

const WhiteBox = glamorous(View)((props: { color: string }) => ({
  margin: 0,
  backgroundColor: GETPOINTS_WHITE,
  borderWidth: 2,
  justifyContent: "center",
  borderColor: props.color,
  // borderBottomLeftRadius: 2, // There's a bug which makes all borders rounded annoyingly
  padding: 5
}));

export class RewardTileHeader extends React.Component<{
  offerHeadline: string;
  requirement: string;
  status: string;
  tapDown: () => any;
  isOpen: boolean;
  color: string;
}> {
  static defaultProps = {
    color: GETPOINTS_TEAL
  };
  render() {
    let arrow;

    if (this.props.isOpen) {
      arrow = (
        <ArrowIcon name="ios-arrow-forward" color={GETPOINTS_WHITE} size={20} />
      );
    } else {
      arrow = (
        <ArrowIcon name="ios-arrow-down" color={GETPOINTS_WHITE} size={20} />
      );
    }

    return (
      <TouchableOpacity onPress={this.props.tapDown}>
        <RewardTileHeaderContainer>
          <WhiteBox color={this.props.color}>
            <TightH5>{this.props.requirement}</TightH5>
          </WhiteBox>
          <RightContainer color={this.props.color}>
            <RectangleLeftAlignedItems>
              <TightH4>{this.props.offerHeadline}</TightH4>
            </RectangleLeftAlignedItems>
            <H4>{this.props.status}</H4>
            {arrow}
          </RightContainer>
        </RewardTileHeaderContainer>
      </TouchableOpacity>
    );
  }
}
