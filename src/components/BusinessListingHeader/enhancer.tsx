import { BusinessListingHeader as Component } from "./BusinessListingHeader";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State } from "@src/store";
import { actions } from "@src/actions";
import { getBusinessByID } from "@src/store/business/selectors";
import { push } from "connected-react-router";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  return {};
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  return {
    // pass functional props from here
    onTap: () => dispatch(push(`/business/${props.businessID!}`))
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
