import {
  GETPOINTS_GREEN,
  GETPOINTS_TEAL,
  GETPOINTS_WHITE,
  GETPOINTS_YELLOW
} from "@src/const";
import React from "react";
import { Text, View, TouchableOpacity } from "react-native";
import glamorous from "glamorous-native";

import { BusinessLogo } from "../BusinessLogo";
import { CornerBanner } from "../CornerBanner";
import { Icon } from "../Icon";
import { H4 } from "../Typography/H4";
import { H5 } from "../Typography/H5";
import { H6 } from "../Typography/H6";
import { Loading } from "../Loading";
import { ErrorMessage } from "../ErrorMessage";

const BusinessListingHeaderContainer = glamorous(TouchableOpacity)(
  (props: {}) => ({
    display: "flex",
    flexDirection: "row",
    backgroundColor: GETPOINTS_TEAL,
    alignItems: "center",
    borderBottomRightRadius: 50,
    borderTopRightRadius: 50,

    marginTop: 5,
    marginBottom: 5
  })
);

const RightGreenContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 1,
  flexShrink: 1,
  backgroundColor: GETPOINTS_TEAL,
  padding: 10,
  alignItems: "center",
  borderRadius: 30
}));

const LeftAlignedItemsContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  // height: `100%`,
  alignSelf: "stretch",
  justifyContent: "space-between",
  flexGrow: 1,
  flexShrink: 1
}));

const Arrow = glamorous(Icon)((props: {}) => ({
  marginTop: 0,
  marginBottom: 0,
  marginLeft: 10,
  marginRight: 10
}));

const TightH4 = glamorous(H4)((props: {}) => ({
  marginTop: 3,
  marginBottom: 3,
  marginLeft: 0,
  marginRight: 0,
  flexShrink: 0
}));
const TightH6 = glamorous(H6)((props: {}) => ({
  marginTop: 0,
  marginBottom: 0,
  marginLeft: 0,
  marginRight: 0
}));
const TightH5 = glamorous(H5)((props: {}) => ({
  marginTop: 2,
  marginBottom: 2,
  marginLeft: 0,
  marginRight: 0
}));
const YellowText = glamorous(Text)((props: {}) => ({
  color: GETPOINTS_YELLOW
}));

export class BusinessListingHeader extends React.Component<{
  onTap: () => any;
  loaded?: boolean;
  error?: string;
  businessID: string;
  businessName: string;
  businessLogo: string;
  businessType: string;
  businessLocation?: string;
  businessDistance?: number;
  isMember?: boolean;
  offerText: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }

    return (
      <BusinessListingHeaderContainer onPress={this.props.onTap}>
        {this.props.isMember ? null : <CornerBanner>Join</CornerBanner>}
        <BusinessLogo image={this.props.businessLogo} />
        <RightGreenContainer>
          <LeftAlignedItemsContainer>
            <TightH4>{this.props.businessName}</TightH4>
            <TightH5>{this.props.businessType}</TightH5>
            {this.props.businessLocation ? (
              <TightH6>
                {this.props.businessLocation}{" "}
                {this.props.businessDistance ? (
                  <YellowText>{this.props.businessDistance}km</YellowText>
                ) : null}
              </TightH6>
            ) : null}
          </LeftAlignedItemsContainer>
          <TightH4>{this.props.offerText}</TightH4>
          <Arrow name="ios-arrow-forward" size={30} color={GETPOINTS_WHITE} />
        </RightGreenContainer>
      </BusinessListingHeaderContainer>
    );
  }
}
