import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { BusinessListingHeader } from "./BusinessListingHeader";

export const basic = () => (
  <BusinessListingHeader
    businessLogo={"https://via.placeholder.com/100?text=Logo"}
    businessDistance={4}
    businessID="42"
    businessName="The Pantry"
    businessLocation="Kampala"
    businessType="Restaurant"
    offerText="20% off"
    onTap={action("Tapped")}
  />
);
