import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { InformationTile } from "./index";
import { Text } from "react-native";

export const basic = () => (
  <InformationTile title="Hello" color="red">
    <Text>Content</Text>
  </InformationTile>
);
