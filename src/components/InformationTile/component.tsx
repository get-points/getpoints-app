import React, { ReactNode } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import glamorous from "glamorous-native";
import { StyleProp, ViewStyle } from "react-native";
import {
  GETPOINTS_WHITE,
  GETPOINTS_FONT_PRIMARY,
  GETPOINTS_GREEN
} from "@src/const";
import { Icon } from "../Icon";

/**
 * Component Properties
 */

export interface FunctionProps {}

export interface StoreProps {}

export interface ParentProps {
  style?: StyleProp<ViewStyle>;
  children?: ReactNode;
  title: string;
  color: string;
}

export interface LocalProps {}

type ComponentProps = FunctionProps & StoreProps & ParentProps & LocalProps;

/**
 * Styled Components
 */

const InformationTileContainer = glamorous(View)(
  (props: { color: string; open: boolean }) => ({
    display: "flex",
    marginTop: 0,
    marginBottom: props.open ? 15 : 5,
    borderRadius: 2,
    borderColor: props.color,
    borderWidth: 2
  })
);

const TileHeader = glamorous(View)((props: { color: string }) => ({
  display: "flex",
  backgroundColor: props.color,
  padding: 10,
  flexDirection: "row",
  flexGrow: 1
}));

const TileBody = glamorous(View)((props: { color: string }) => ({
  display: "flex",
  backgroundColor: GETPOINTS_WHITE,
  padding: 10,
  minHeight: 50
}));

const TileHeaderText = glamorous(Text)((props: {}) => ({
  fontSize: 12,
  color: GETPOINTS_WHITE,
  fontFamily: GETPOINTS_FONT_PRIMARY,
  fontWeight: "bold",
  textAlign: "center",
  marginRight: 20,
  justifyContent: "center",
  flexGrow: 1,
  marginLeft: 25
}));

const WhiteIcon = glamorous(Icon)((props: {}) => ({
  color: GETPOINTS_WHITE,
  marginRight: 10
}));

/**
 * Component Definition
 */

export class Component extends React.Component<
  ComponentProps,
  { open: boolean }
> {
  state = {
    open: false
  };
  getTileLower() {
    if (this.state.open) {
      return (
        <TileBody color={this.props.color}>{this.props.children}</TileBody>
      );
    }
    return null;
  }

  toggle = () => this.setState({ open: !this.state.open });

  render() {
    return (
      <InformationTileContainer
        style={this.props.style}
        open={this.state.open}
        color={this.props.color}
      >
        <TouchableOpacity onPress={this.toggle}>
          <TileHeader color={this.props.color}>
            <TileHeaderText>{this.props.title}</TileHeaderText>
            <WhiteIcon
              name={this.state.open ? "ios-arrow-up" : "ios-arrow-down"}
            />
          </TileHeader>
        </TouchableOpacity>
        {this.getTileLower()}
      </InformationTileContainer>
    );
  }
}
