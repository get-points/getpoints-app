import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { SettingsTile } from "./index";

export const basic = () => (
  <SettingsTile
    businessName={text("businessName", "acChi Restaurant")}
    emotion={"HAPPY"}
    message={text("message", "great meal!")}
    tapSendButton={action("buttonTapped")}
  />
);
