import { State } from "@src/store";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import {
  Component,
  FunctionProps,
  ParentProps,
  StoreProps,
  LocalProps
} from "./component";
import { getOnlyUser } from "@src/store/user/selectors";
import { getMembershipListByIDs } from "@src/store/membership/selectors";
import { actions, Action } from "@src/actions";

function getStoreProps(
  store: State,
  props: ParentProps & { businessID: string }
): StoreProps & { loadAction?: Action } {
  const loadableUser = getOnlyUser(store);
  if (loadableUser.state === "ABSENT" || loadableUser.state === "PENDING")
    return { state: "LOADING" };
  if (loadableUser.state === "FAILED")
    return { state: "ERROR", error: loadableUser.message };
  const user = loadableUser.item;

  const loadableMemberships = getMembershipListByIDs(store, user.memberships);
  if (loadableMemberships.state === "ABSENT")
    return {
      state: "LOADING",
      loadAction: actions.getMembershipList.request({
        ids: loadableMemberships.ids
      })
    };
  if (loadableMemberships.state === "PENDING") return { state: "LOADING" };
  if (loadableMemberships.state === "FAILED")
    return { state: "ERROR", error: loadableMemberships.message };

  const membership = loadableMemberships.item.find(
    m => m.businessID === props.businessID
  );

  return {
    state: "LOADED",
    notifications: membership && membership.notificationSettings
  };
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  return {
    // pass functional props from here
    loadContent: action => dispatch(action!),
    toggleEmail: v => console.log("Enabling email"),
    toggleSMS: v => console.log("Enabling sms ")
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps,
  (storeProps, functionProps, parentProps) => {
    return {
      ...storeProps,
      ...functionProps,
      ...parentProps,
      loadContent: storeProps.loadAction
        ? () =>
            storeProps.state === "LOADING" &&
            storeProps.loadContent &&
            storeProps.loadContent(storeProps.loadAction)
        : undefined
    };
  }
)(Component as any);
