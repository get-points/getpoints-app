import { Action } from "@src/actions";
import { GETPOINTS_BLUE } from "@src/const";
import { Membership } from "@src/types/Models";
import glamorous from "glamorous-native";
import React from "react";
import { StyleProp, Switch, View, ViewStyle } from "react-native";
import { ErrorMessage } from "../ErrorMessage";
import { InformationTile } from "../InformationTile";
import { Loading } from "../Loading";
import { ExplainerText } from "../Typography/ExplainerText";

/**
 * Component Properties
 */

export interface FunctionProps {
  toggleEmail: (x: boolean) => any;
  toggleSMS: (x: boolean) => any;
}

export type StoreProps =
  | {
      state: "LOADING";
      loadAction?: Action;
      loadContent?: (a?: Action) => any;
    }
  | { state: "ERROR"; error: string }
  | ({
      state: "LOADED";
      notifications?: Membership["notificationSettings"];
    });

export interface ParentProps {
  style?: StyleProp<ViewStyle>;
}

export interface LocalProps {}

type ComponentProps = FunctionProps & StoreProps & ParentProps & LocalProps;

/**
 * Styled Components
 */

const LabelText = glamorous(ExplainerText)((props: {}) => ({
  fontWeight: "bold",
  textAlign: "left",
  fontSize: 13
}));

const LeftAlignedExplainerText = glamorous(ExplainerText)((props: {}) => ({
  textAlign: "left"
}));

const SmileysGroup = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 0,
  justifyContent: "center"
}));

const SettingsRow = glamorous(View)((props: {}) => ({
  margin: 10,
  flexDirection: "row",
  flexGrow: 1,
  justifyContent: "space-between",
  alignItems: "center"
}));

/**
 * Component Definition
 */

export class Component extends React.Component<ComponentProps> {
  componentDidMount() {
    this.update(this.props);
  }
  componentWillReceiveProps(nextProps: Component["props"]) {
    this.update(nextProps);
  }
  update = (props: Component["props"]) =>
    this.props.state === "LOADING" &&
    this.props.loadContent &&
    this.props.loadContent();

  render() {
    if (this.props.state === "LOADING") {
      return <Loading />;
    }
    if (this.props.state === "ERROR") {
      return <ErrorMessage message={this.props.error} />;
    }

    if (this.props.notifications === undefined) {
      return (
        <InformationTile title="Notifications" color={GETPOINTS_BLUE}>
          <ExplainerText>
            Notification settings are only available to members.
          </ExplainerText>
        </InformationTile>
      );
    }

    return (
      <InformationTile title="Notifications" color={GETPOINTS_BLUE}>
        <SettingsRow>
          <LabelText>Email Notification</LabelText>
          <Switch
            value={this.props.notifications.email}
            onValueChange={this.props.toggleEmail}
          />
        </SettingsRow>
        <SettingsRow>
          <LabelText>Text Notification</LabelText>
          <Switch
            value={this.props.notifications.mobile}
            onValueChange={this.props.toggleSMS}
          />
        </SettingsRow>
      </InformationTile>
    );
  }
}
