import { State } from "@src/store";
import { connect } from "react-redux";
import { Dispatch } from "redux";

import {
  PageHeader,
  StateProps,
  FunctionProps,
  ParentProps
} from "./PageHeader";
import { goBack } from "connected-react-router";

function mapStateToProps(state: State, props: ParentProps): StateProps {
  return {};
}

function mapDispatchToProps(
  dispatch: Dispatch,
  props: ParentProps
): FunctionProps {
  return {
    tapBackButton: () => dispatch(goBack())
  };
}

export const ConnectedPageHeader = connect(
  mapStateToProps,
  mapDispatchToProps
)(PageHeader);
