import { GETPOINTS_TEAL, GETPOINTS_WHITE } from "@src/const";
import React, { ReactNode } from "react";
import { View } from "react-native";
import glamorous from "glamorous-native";
import { Icon } from "../Icon";
import { HeaderText } from "../Typography/HeaderText";
import { Constants } from "expo";

export interface FunctionProps {
  tapBackButton: () => any;
}

export interface StateProps {}

export interface ParentProps {
  hideBack?: boolean;
  children: ReactNode;
}

export interface LocalProps {}

const PageHeaderContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 0,
  backgroundColor: GETPOINTS_TEAL,
  paddingTop: 13,
  paddingBottom: 13,
  paddingLeft: 20,
  paddingRight: 20,

  alignItems: "center",
  height: 55
}));
const IconContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 0,
  paddingRight: 20
}));
const HeaderTextContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1
}));

const PageHeaderText = glamorous(HeaderText)((props: {}) => ({
  color: GETPOINTS_WHITE,
  marginRight: 35
}));

export class PageHeader extends React.Component<
  FunctionProps & StateProps & ParentProps & LocalProps
> {
  render() {
    return (
      <PageHeaderContainer>
        {this.props.hideBack ? null : (
          <IconContainer>
            <Icon
              onPress={this.props.tapBackButton}
              name="ios-arrow-back"
              size={40}
              color={GETPOINTS_WHITE}
            />
          </IconContainer>
        )}
        <HeaderTextContainer>
          <PageHeaderText>{this.props.children}</PageHeaderText>
        </HeaderTextContainer>
      </PageHeaderContainer>
    );
  }
}
