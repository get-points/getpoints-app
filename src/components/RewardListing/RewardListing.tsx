import React from "react";
import { Text, View, ViewStyle, TouchableOpacity } from "react-native";
import glamorous from "glamorous-native";
import { Placeholder } from "../Placeholder";
import { H4 } from "../Typography/H4";
import { H5 } from "../Typography/H5";
import { H6 } from "../Typography/H6";
import { BusinessLogo } from "../BusinessLogo";
import { Loading } from "../Loading";
import { ErrorMessage } from "../ErrorMessage";
import { CornerBanner } from "../CornerBanner";
import { GETPOINTS_TEAL, GETPOINTS_WHITE } from "@src/const";
import { Icon } from "../Icon";

const RewardListingContainer = glamorous(TouchableOpacity)(
  (props: { color: string }) => ({
    display: "flex",
    flexDirection: "row",
    backgroundColor: props.color,
    alignItems: "stretch",
    borderBottomRightRadius: 50,
    borderTopRightRadius: 50,
    marginTop: 5,
    marginBottom: 5
  })
);

const RightGreenContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 1,
  flexShrink: 1,
  padding: 10,
  alignItems: "center",
  borderRadius: 30
}));

const Arrow = glamorous(Icon)((props: {}) => ({
  marginTop: 0,
  marginBottom: 0,
  marginLeft: 10,
  marginRight: 10
}));

const LeftAlignedItemsContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  // height: `100%`,
  alignSelf: "stretch",
  justifyContent: "space-between",
  flexGrow: 1,
  flexShrink: 1
  // lineHeight: 0
}));
const TightH4 = glamorous(H4)((props: {}) => ({
  marginTop: 3,
  marginBottom: 3,
  marginLeft: 0,
  marginRight: 0,
  flexShrink: 0
}));
const TightH6 = glamorous(H6)((props: {}) => ({
  marginTop: 0,
  marginBottom: 0,
  marginLeft: 0,
  marginRight: 0
}));
const TightH5 = glamorous(H5)((props: {}) => ({
  marginTop: 2,
  marginBottom: 2,
  marginLeft: 0,
  marginRight: 0
}));
export class RewardListing extends React.Component<{
  style?: ViewStyle;
  businessLogo: string;
  rewardHeadline: string;
  businessName: string;
  city: string;
  status: string;
  loaded?: boolean;
  error?: string;
  businessID: string;
  color?: string;
  onTap: () => any;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <RewardListingContainer
        color={this.props.color || GETPOINTS_TEAL}
        style={this.props.style}
        onPress={this.props.onTap}
      >
        <BusinessLogo image={this.props.businessLogo} />
        <RightGreenContainer>
          <LeftAlignedItemsContainer>
            <TightH4>{this.props.rewardHeadline}</TightH4>
            <TightH5>{this.props.businessName}</TightH5>
            <TightH6>{this.props.city}</TightH6>
          </LeftAlignedItemsContainer>
          <TightH5>{this.props.status}</TightH5>
          <Arrow name="ios-arrow-forward" size={30} color={GETPOINTS_WHITE} />
        </RightGreenContainer>
      </RewardListingContainer>
    );
  }
}
