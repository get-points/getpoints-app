import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { RewardListing } from "./index";

export const basic = () => (
  <RewardListing businessID={text("businessID", "14")} />
);
