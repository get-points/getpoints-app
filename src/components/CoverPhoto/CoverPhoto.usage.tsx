import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { CoverPhoto } from "./";

export const basic = () => (
  <CoverPhoto image="http://placekitten.com/500/100" />
);
