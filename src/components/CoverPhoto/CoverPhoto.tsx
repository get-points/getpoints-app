import { GETPOINTS_WHITE } from "@src/const";
import React from "react";
import { Image } from "react-native";
import glamorous from "glamorous-native";

const CoverPhotoContainer = glamorous(Image)((props: {}) => ({
  position: "absolute",
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
  zIndex: -1,
  flexGrow: 1,
  minHeight: 139
}));

export class CoverPhoto extends React.Component<{
  image: string;
}> {
  render() {
    return (
      <CoverPhotoContainer
        source={{ uri: this.props.image }}
        resizeMode="cover"
      />
    );
  }
}
