import glamorous from "glamorous-native";
import React from "react";
import { View } from "react-native";
import { ProfilePicture } from "../BusinessLogo";
import { CoverBusinessInfo } from "../CoverBusinessInfo";
import { CoverPhoto } from "../CoverPhoto";
import { Loading } from "../Loading";
import { ErrorMessage } from "../ErrorMessage";

const BusinessHeadingContainer = glamorous(View)({
  display: "flex",
  justifyContent: "flex-end",
  height: 139,
  padding: 10
});

const Row = glamorous(View)({
  display: "flex",
  flexDirection: "row",
  alignItems: "stretch"
});

export class BusinessHeading extends React.Component<{
  id: string;
  coverPhoto: string;
  profilePhoto: string;
  businessName: string;
  businessLocation: string;
  businessType: string;
  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <BusinessHeadingContainer>
        <Row>
          <ProfilePicture image={this.props.profilePhoto} />
          <CoverBusinessInfo
            id={this.props.id}
            businessName={this.props.businessName}
            location={this.props.businessLocation}
            businessType={this.props.businessType}
          />
        </Row>
        <CoverPhoto image={this.props.coverPhoto} />
      </BusinessHeadingContainer>
    );
  }
}
