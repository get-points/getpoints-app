import { GETPOINTS_DARK_GREEN, GETPOINTS_WHITE } from "@src/const";
import glamorous from "glamorous-native";
import React from "react";
import { TouchableOpacity, View } from "react-native";
import { BusinessLogo } from "../BusinessLogo";
import { CornerBanner } from "../CornerBanner";
import { Icon } from "../Icon";
import { RectangularButton } from "../RectangularButton";
import { H4 } from "../Typography/H4";
import { H5 } from "../Typography/H5";
import { H6 } from "../Typography/H6";

const OfferListingContainer = glamorous(TouchableOpacity)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  backgroundColor: GETPOINTS_DARK_GREEN,
  borderBottomRightRadius: 50,
  borderTopRightRadius: 50,
  alignItems: "stretch",
  marginTop: 5,
  marginBottom: 5
}));

const RightGreenContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 1,
  flexShrink: 1,
  padding: 10,
  alignItems: "center"

  // borderRadius: 50
}));

const OfferDetailsText = glamorous(View)((props: {}) => ({
  alignSelf: "stretch",
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-between",
  flexGrow: 1,
  flexShrink: 1,
  fontSize: 12,
  lineHeight: 0
}));

const RightIcon = glamorous(Icon)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 0,
  padding: 10
}));

const TightH4 = glamorous(H4)((props: {}) => ({
  marginTop: 3,
  marginBottom: 3,
  marginLeft: 0,
  marginRight: 0
}));
const TightH6 = glamorous(H6)((props: {}) => ({
  padding: 0,
  margin: 0,
  marginTop: 0,
  marginBottom: 0
}));
const TightH5 = glamorous(H5)((props: {}) => ({
  marginTop: 2,
  marginBottom: 2,
  marginLeft: 0,
  marginRight: 0
}));

const ClaimItButton = glamorous(RectangularButton)((props: {}) => ({
  flex: 0,
  flexGrow: 0,
  fontSize: 12,
  lineHeight: 20,
  paddingLeft: 6,
  paddingRight: 6,
  fontWeight: "bold"
}));

export class OfferListing extends React.Component<{
  businessName: string;
  offerHeadline: string;
  expiryDate: string;
  tapClaimItButton: () => any;
  businessID: string;
  businessLogo: string;

  onTap: () => any;
  isMember: boolean;
  isClaimed: boolean;
}> {
  render() {
    let claimItButton = null;
    let cornerBanner = null;

    if (this.props.isClaimed !== true)
      claimItButton = (
        <ClaimItButton topRightSquare small onTap={this.props.tapClaimItButton}>
          Claim&nbsp;It
        </ClaimItButton>
      );
    if (this.props.isMember !== true) cornerBanner = <CornerBanner />;

    return (
      <OfferListingContainer onPress={this.props.onTap}>
        {cornerBanner}
        <BusinessLogo image={this.props.businessLogo} />
        <RightGreenContainer>
          <OfferDetailsText>
            <TightH4>{this.props.businessName}</TightH4>
            <TightH5>{this.props.offerHeadline}</TightH5>
            <TightH6>Expires on {this.props.expiryDate}</TightH6>
          </OfferDetailsText>
          {claimItButton}
          <RightIcon
            name="ios-arrow-forward"
            size={30}
            color={GETPOINTS_WHITE}
          />
        </RightGreenContainer>
      </OfferListingContainer>
    );
  }
}
