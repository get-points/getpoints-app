import { OfferListing as Component } from "./OfferListing";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State } from "@src/store";
import { actions } from "@src/actions";
import {} from "@src/store/selectors";
import { action } from "@storybook/addon-actions";
import { push } from "connected-react-router";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  return {};
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  return {
    onTap: () => dispatch(push("/business/" + props.businessID)),
    tapClaimItButton: () => action("Claim offer")
    // pass functional props from here
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
