import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { OfferListing } from "./index";

export const basic = () => (
  <OfferListing
    businessID="42"
    businessLogo={text(
      "businessLogo",
      "https://via.placeholder.com/100?text=Logo"
    )}
    isClaimed={boolean("isClaimed", false)}
    businessName={text("businessName", "The Pantry")}
    expiryDate={text("expiryDate", "11/03/2019")}
    isMember={boolean("isMember", false)}
    offerHeadline={text("offerHeadline", "Offer Hedaline")}
    tapClaimItButton={action("Tap Claimit")}
  />
);
