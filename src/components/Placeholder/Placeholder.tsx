import React from "react";
import { Text, View } from "react-native";
import glamorous from "glamorous-native";

const PlaceholderContainer = glamorous(View)(
  (props: { orientation?: "row" | "column" }) => ({
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "grey",
    padding: 10,
    display: "flex",
    flexDirection: props.orientation,
    alignItems: "center" as "center",
    justifyContent: "center" as "center",
    flex: 1
  })
);

function wrapString(c: React.ReactNode, i?: number): React.ReactNode {
  if (typeof c === "string") return <Text key={i}>{c}</Text>;

  if (React.isValidElement(c)) return React.cloneElement(c, { key: i });
  return c;
}

export class Placeholder extends React.Component<{
  children?: React.ReactNode;
  orientation?: "row" | "column";
}> {
  static defaultProps: Placeholder["props"] = {
    orientation: "row"
  };

  render() {
    const children = Array.isArray(this.props.children)
      ? this.props.children.map(wrapString)
      : wrapString(this.props.children);
    return (
      <PlaceholderContainer orientation={this.props.orientation}>
        {children}
      </PlaceholderContainer>
    );
  }
}
