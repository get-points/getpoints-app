import { GETPOINTS_GREEN } from "@src/const";
import React, { ReactNode } from "react";
import { View } from "react-native";
import glamorous from "glamorous-native";

import { H5 } from "../Typography/H5";

const RibbonContainer = glamorous(View)((props: {}) => ({
  position: "absolute",
  left: 0,
  top: 0,
  zIndex: 1,
  overflow: "hidden",
  width: 75,
  height: 75
}));

const RibbonInner = glamorous(View)((props: {}) => ({
  transform: [{ translateX: -75 }, { translateY: -32 }, { rotate: "-45deg" }],
  position: "absolute",
  top: 28,
  left: 12,
  // top: -32,
  // left: -75,
  // marginTop: 10,
  // marginLeft: 10,
  width: 150,
  backgroundColor: GETPOINTS_GREEN,
  alignItems: "stretch",
  // height: 20,
  // boxShadow: `0 3px 10px -5px rgba(0, 0, 0, 1)`
  elevation: 2
}));
const Ribbon = glamorous(H5)((props: {}) => ({
  fontWeight: "bold",
  fontSize: 14,
  textAlign: "center",
  textAlignVertical: "bottom"
  // lineHeight: 30,
  // paddingTop: 40,
  // color: "white"
}));

export class CornerBanner extends React.Component<{
  children: string;
}> {
  static defaultProps: CornerBanner["props"] = {
    children: "JOIN"
  };
  render() {
    return (
      <RibbonContainer>
        <RibbonInner>
          <Ribbon>{this.props.children.toUpperCase()}</Ribbon>
        </RibbonInner>
      </RibbonContainer>
    );
  }
}
