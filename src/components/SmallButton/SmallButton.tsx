import React, { ReactNode } from "react";
import { Text, View, TouchableHighlight } from "react-native";
import glamorous from "glamorous-native";
import {
  GETPOINTS_WHITE,
  GETPOINTS_FONT_PRIMARY,
  GETPOINTS_GREEN,
  GETPOINTS_LIGHT_GREEN
} from "@src/const";

export interface SmallButtonParentProps {
  onTap: () => any;
  children: ReactNode;
}
export interface SmallButtonFunctionProps {}
export interface SmallButtonStateProps {}
export interface SmallButtonLocalProps {}
export type SmallButtonProps = SmallButtonParentProps &
  SmallButtonFunctionProps &
  SmallButtonStateProps &
  SmallButtonLocalProps;

const SmallButtonContainer = glamorous(Text)({
  display: "inline-flex",
  flexDirection: "row",
  flexGrow: 0,
  backgroundColor: GETPOINTS_GREEN,
  borderWidth: 1,
  borderStyle: "solid",
  borderColor: GETPOINTS_GREEN,
  borderRadius: 9,
  paddingHorizontal: 3,
  paddingVertical: 5
});

const SmallButtonText = glamorous(Text)((props: {}) => ({
  color: GETPOINTS_WHITE,
  fontFamily: GETPOINTS_FONT_PRIMARY,
  fontWeight: "bold",
  fontSize: 11,
  textAlign: "center"
}));

export class SmallButton extends React.Component<SmallButtonProps> {
  render() {
    return (
      <SmallButtonContainer>
        <TouchableHighlight
          onPress={this.props.onTap}
          underlayColor={GETPOINTS_LIGHT_GREEN}
        >
          <View>
            <SmallButtonText>{this.props.children}</SmallButtonText>
          </View>
        </TouchableHighlight>
      </SmallButtonContainer>
    );
  }
}
