import React, { ReactNode } from "react";
import { Text, View, ViewStyle } from "react-native";
import glamorous from "glamorous-native";
import { H5 } from "../Typography/H5";
import { GETPOINTS_GREEN } from "@src/const";

const RectangleGreen = glamorous(View)((props: {}) => ({
  textAlign: "center",
  backgroundColor: GETPOINTS_GREEN,
  borderColor: GETPOINTS_GREEN,
  borderRadius: 2,
  padding: 10,
  flexGrow: 1
}));

const TableHeaderContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  marginTop: 10,
  marginBottom: 10,
  marginLeft: 3,
  marginRight: 3
}));

export class TableHeader extends React.Component<{
  style?: ViewStyle;
  children: ReactNode;
}> {
  render() {
    return (
      <TableHeaderContainer style={this.props.style}>
        <RectangleGreen>
          <H5>{this.props.children}</H5>
        </RectangleGreen>
      </TableHeaderContainer>
    );
  }
}
