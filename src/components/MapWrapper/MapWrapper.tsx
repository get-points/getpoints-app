import React, { ReactNode } from "react";
import { Text, View } from "react-native";
import glamorous from "glamorous-native";
import MapView, { Marker, Callout } from "react-native-maps";

const MapWrapperContainer = glamorous(View)((props: {}) => ({
  backgroundColor: "lightgrey",
  minHeight: 240,
  flex: 1,
  width: "100%",
  marginTop: 10,
  marginBottom: 10
}));

type MarkerType = {
  latitude: number;
  longitude: number;
  title?: string;
  description?: string;
  view?: string;
};

export class MapWrapper extends React.Component<
  {
    latitude?: number;
    longitude?: number;
    children?: ReactNode;
    markers: MarkerType[];
  },
  {
    activeMarker?: number;
  }
> {
  static defaultProps = {
    markers: []
  };

  render() {
    const avg = { lat: 0, lng: 0 };
    const mins: { lat?: number; lng?: number } = {
      lat: undefined,
      lng: undefined
    };
    const maxs: { lat?: number; lng?: number } = {
      lat: undefined,
      lng: undefined
    };
    const children = this.props.markers.map((p, i) => {
      avg.lat += p.latitude;
      avg.lng += p.longitude;
      maxs.lat = maxs.lat ? Math.max(maxs.lat, p.latitude) : p.latitude;
      mins.lat = mins.lat ? Math.min(mins.lat, p.latitude) : p.latitude;
      maxs.lng = maxs.lng ? Math.max(maxs.lng, p.longitude) : p.longitude;
      mins.lng = mins.lng ? Math.min(mins.lng, p.longitude) : p.longitude;
      let view = null;
      return (
        <Marker
          key={i}
          title={p.title}
          coordinate={p}
          description={p.description}
        >
          {view}
        </Marker>
      );
    });
    return (
      <MapWrapperContainer>
        <MapView
          style={{ flex: 1 }}
          onPress={() => this.setState({ activeMarker: undefined })}
          region={{
            latitude:
              this.props.latitude || avg.lat / this.props.markers.length,
            longitude:
              this.props.longitude || avg.lng / this.props.markers.length,
            latitudeDelta:
              (maxs.lat && mins.lat ? maxs.lat - mins.lat : 0.1) + 0.01,
            longitudeDelta:
              (maxs.lng && mins.lng ? maxs.lng - mins.lng : 0.1) + 0.01
          }}
        >
          {children}
        </MapView>
      </MapWrapperContainer>
    );
  }
}
