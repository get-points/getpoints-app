import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { MapWrapper } from "./index";

export const basic = () => (
  <MapWrapper
    {...{
      latitude: number("latitude", 0),
      longitude: number("longitude", 30),
      markers: []
    }}
  />
);
