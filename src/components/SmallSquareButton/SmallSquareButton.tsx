import React from "react";
import { Text, View, ViewStyle } from "react-native";
import glamorous from "glamorous-native";
import { ExplainerText } from "../Typography/ExplainerText";

const SmallSquareButtonContainer = glamorous(View)((props: {}) => ({
  backgroundColor: "lightgrey",
  padding: 10
}));

export class SmallSquareButton extends React.Component<{
  style?: ViewStyle;
  children: string;
  onTap: () => any;
}> {
  render() {
    return (
      <SmallSquareButtonContainer style={this.props.style}>
        <ExplainerText>{this.props.children}</ExplainerText>
      </SmallSquareButtonContainer>
    );
  }
}
