import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { SmallSquareButton } from "./index";

export const basic = () => (
  <SmallSquareButton onTap={action("Tapped")}>
    {text("label", "Click Me")}
  </SmallSquareButton>
);
