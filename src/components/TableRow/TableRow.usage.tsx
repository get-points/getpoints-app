import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { TableRow } from "./index";

export const basic = () => (
  <TableRow
    businessName={text("businessName", "The Pantry")}
    date={text("date", "09/10/2019")}
    pointReward={number("pointReward", 1)}
    time={text("time", "08:56am")}
  />
);
