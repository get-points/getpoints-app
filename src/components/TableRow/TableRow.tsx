import React from "react";
import { Text, View } from "react-native";
import glamorous from "glamorous-native";
import { GETPOINTS_GREEN, GETPOINTS_WHITE } from "@src/const";
import { TableRowText } from "../Typography/TableRowText";

const TableRowContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 1,
  background: GETPOINTS_WHITE,
  borderWidth: 1,
  borderStyle: "solid",
  borderColor: GETPOINTS_GREEN,
  borderRadius: 2,
  marginTop: 2,
  marginBottom: 2,
  marginLeft: 6,
  marginRight: 6,
  paddingTop: 6,
  paddingBottom: 6,
  paddingLeft: 0,
  paddingRight: 0
}));

const Cell = glamorous(View)((props: {}) => ({
  flexGrow: 1,
  textAlign: "center"
}));

const Date = glamorous(Cell)((props: {}) => ({
  maxWidth: `25%`,
  marginLeft: 0
}));

const Time = glamorous(Cell)((props: {}) => ({
  maxWidth: `20%`
}));

const Business = glamorous(Cell)((props: {}) => ({
  maxWidth: `40%`
}));

const PointsChange = glamorous(Cell)((props: {}) => ({
  maxWidth: `15%`,
  marginRight: 0
}));
export class TableRow extends React.Component<{
  date: string;
  time: string;
  businessName: string;
  pointReward: number;
}> {
  render() {
    return (
      <TableRowContainer>
        <Date>
          <TableRowText>{this.props.date}</TableRowText>
        </Date>
        <Time>
          <TableRowText>{this.props.time}</TableRowText>
        </Time>
        <Business>
          <TableRowText>{this.props.businessName}</TableRowText>
        </Business>
        <PointsChange>
          <TableRowText>{this.props.pointReward}</TableRowText>
        </PointsChange>
      </TableRowContainer>
    );
  }
}
