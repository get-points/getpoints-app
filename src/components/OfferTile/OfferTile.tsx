import React from "react";
import { Text, View, TouchableOpacity } from "react-native";
import glamorous from "glamorous-native";
import { RewardTileHeader } from "../RewardTileHeader";
import { RectangularButton } from "../RectangularButton";
import { HeaderText } from "../Typography/HeaderText";
import {
  GETPOINTS_WHITE,
  GETPOINTS_DARK_TEAL,
  GETPOINTS_GREEN,
  GETPOINTS_TEAL,
  GETPOINTS_FONT_PRIMARY
} from "@src/const";
import { Loading } from "../Loading";
import { ErrorMessage } from "../ErrorMessage";
import { Action } from "@src/actions";

const RedeemButton = glamorous(TouchableOpacity)((props: {}) => ({
  padding: 10,
  flexGrow: 1,
  flexShrink: 0,
  fontWeight: "bold",
  flexDirection: "row",
  backgroundColor: GETPOINTS_TEAL,
  justifyContent: "center",
  height: 50,
  alignItems: "center"
}));

const RectangleWhite = glamorous(View)((props: {}) => ({
  backgroundColor: GETPOINTS_WHITE,
  maxWidth: 100,
  height: 100,
  flexShrink: 0,
  flexGrow: 1,
  padding: 6,
  justifyContent: "center"
}));
const RectangleGreen = glamorous(View)((props: {}) => ({
  backgroundColor: GETPOINTS_GREEN,
  flexGrow: 1,
  padding: 10,
  justifyContent: "center",
  alignItems: "center"
}));

const TopContainer = glamorous(View)((props: {}) => ({
  backgroundColor: GETPOINTS_GREEN,
  flexDirection: "row",
  borderWidth: 2,
  borderBottomWidth: 0,
  borderColor: GETPOINTS_GREEN
}));
const FixedText = glamorous(Text)((props: {}) => ({
  textAlign: "center"
}));

const WhiteText = glamorous(Text)((props: {}) => ({
  textAlign: "center",
  color: GETPOINTS_WHITE,
  fontSize: 16
}));
const WhiteTextSmall = glamorous(Text)((props: {}) => ({
  textAlign: "center",
  color: GETPOINTS_WHITE
}));

const OfferHeaderText = glamorous(HeaderText)((props: {}) => ({
  textAlign: "center",
  color: GETPOINTS_WHITE,
  margin: 2
}));
export class OfferTile extends React.Component<
  {
    offerHeadline: string;
    number: number;
    expiry: string;
    tapRedeemButton: () => any;
    loadContent?: (action?: Action) => any;
    loaded?: boolean;
    error?: string;
  },
  { isOpen: boolean }
> {
  componentDidMount() {
    this.update(this.props);
  }
  componentWillReceiveProps(nextProps: OfferTile["props"]) {
    this.update(nextProps);
  }
  update = (props: OfferTile["props"]) =>
    props.loadContent && props.loadContent();

  render() {
    if (this.props.loaded === false) return <Loading />;
    if (this.props.error !== undefined)
      return <ErrorMessage message={this.props.error} />;

    return (
      <View style={{ marginBottom: 15 }}>
        <TopContainer>
          <RectangleWhite>
            <FixedText> Limited Time & Quantity Offer</FixedText>
          </RectangleWhite>
          <RectangleGreen>
            <OfferHeaderText>{this.props.offerHeadline} </OfferHeaderText>

            <WhiteTextSmall>
              Available to the first {this.props.number} people
            </WhiteTextSmall>
            <WhiteTextSmall>Expires on: {this.props.expiry}</WhiteTextSmall>
          </RectangleGreen>
        </TopContainer>

        <RedeemButton onPress={this.props.tapRedeemButton}>
          <WhiteText>View and Claim Offer</WhiteText>
        </RedeemButton>
      </View>
    );
  }
}
