import { Action, actions } from "@src/actions";
import { State } from "@src/store";
import { getOfferByID } from "@src/store/offer/selectors";
import {} from "@src/store/selectors";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { OfferTile as Component } from "./OfferTile";
import { PopupType } from "@src/actions/ui";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]> & { offerID: string }
): Partial<Component["props"] & { loadAction: Action }> {
  try {
    // prepare data here
    const offerID = props.offerID;
    const loadableOffer = getOfferByID(store, props.offerID);
    if (loadableOffer.state === "ABSENT")
      return {
        loaded: false,
        loadAction: actions.getOffer.request({ id: offerID })
      };
    if (loadableOffer.state === "PENDING") return { loaded: false };
    if (loadableOffer.state === "FAILED")
      return { error: loadableOffer.message };
    const offer = loadableOffer.item;

    return {
      offerHeadline: offer.headline,
      expiry: offer.expiry,
      number: offer.redemptionLimit
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]> & { offerID: string }
): Partial<Component["props"]> {
  return {
    tapRedeemButton: () =>
      dispatch(
        actions.showPopup({
          popup: {
            type: PopupType.OFFER_CLAIM,
            data: { offerID: props.offerID }
          }
        })
      ),
    loadContent: action => dispatch(action!)
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps,
  (storeProps, functionProps, parentProps) => {
    return {
      ...storeProps,
      ...functionProps,
      ...parentProps,
      loadContent: storeProps.loadAction
        ? () => functionProps.loadContent!(storeProps.loadAction)
        : undefined
    };
  }
)(Component as any);
