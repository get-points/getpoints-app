import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { OfferTile } from "./OfferTile";
import { string } from "prop-types";

export const basic = () => (
  <OfferTile
    offerHeadline={text("offerHeadline", "Free Pizza")}
    tapRedeemButton={action("Redeem")}
    number={number("number", 5)}
    expiry={text("expiry", "05/05/19")}
  />
);
