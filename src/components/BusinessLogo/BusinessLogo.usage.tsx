import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { BusinessLogo } from "./index";

export const basic = () => (
  <BusinessLogo image="http://via.placeholder.com/100?text=Logo" />
);
