import glamorous from "glamorous-native";
import React from "react";
import { StyleProp, ImageStyle, Image, ViewStyle } from "react-native";

// TYPE DEFINITIONS
export interface BusinessLogoLocalProps {}
export interface BusinessLogoParentProps {
  style?: StyleProp<ImageStyle>;
  className?: string;
  image: string;
}
export interface BusinessLogoStateProps {}
export interface BusinessLogoFunctionProps {}
export type BusinessLogoProps = BusinessLogoLocalProps &
  BusinessLogoParentProps &
  BusinessLogoStateProps &
  BusinessLogoFunctionProps;

// PRIMARY COMPONENT
const StyledImage = glamorous(Image)({
  height: 75,
  width: 75,
  alignSelf: "flex-start"
}) as any;

export class BusinessLogoComponent extends React.Component<BusinessLogoProps> {
  render() {
    return (
      <StyledImage
        style={[this.props.style]}
        resizeMode="cover"
        source={{ uri: this.props.image }}
        className={this.props.className}
      />
    );
  }
}
