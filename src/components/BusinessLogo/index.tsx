import { BusinessLogoComponent } from "./BusinessLogo";
import glamorous from "glamorous-native";

export const BusinessLogo = BusinessLogoComponent;

// VARIANTS
export const ProfilePicture = glamorous(BusinessLogoComponent)({
  minHeight: 61,
  minWidth: 61,
  marginRight: 10
});
