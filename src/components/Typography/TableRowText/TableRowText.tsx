import React, { ReactNode } from "react";
import { Text, View, TextStyle } from "react-native";
import glamorous from "glamorous-native";
import { GETPOINTS_BLACK, GETPOINTS_FONT_PRIMARY } from "@src/const";

const TableRowTextStyle = glamorous(Text)((props: {}) => ({
  color: GETPOINTS_BLACK,
  fontFamily: GETPOINTS_FONT_PRIMARY,
  fontStyle: "normal",
  fontWeight: "normal",

  fontSize: 12,
  textAlign: "center",
  marginTop: 10,
  marginBottom: 10,
  marginLeft: 3,
  marginRight: 3
})) as any;

export class TableRowText extends React.Component<{
  style?: TextStyle;
  className?: string;
  children: ReactNode;
}> {
  static defaultProps: TableRowText["props"] = {
    children: "Hello World"
  };
  render() {
    return (
      <TableRowTextStyle
        style={this.props.style}
        className={this.props.className}
      >
        {this.props.children}
      </TableRowTextStyle>
    );
  }
}
