import {
  GETPOINTS_FONT_PRIMARY,
  GETPOINTS_GREY,
  GETPOINTS_PLACEHOLDER_GREY,
  GETPOINTS_WHITE,
  GETPOINTS_DARK_GREY
} from "@src/const";
import React, { ReactNode } from "react";
import { TextInput, View, ViewStyle } from "react-native";
import glamorous from "glamorous-native";

const InputPlaceholderContainer = glamorous(TextInput)((props: {}) => ({
  fontFamily: GETPOINTS_FONT_PRIMARY,
  fontStyle: "normal",
  fontWeight: "normal",

  fontSize: 12,
  textAlign: "center",
  backgroundColor: GETPOINTS_WHITE,
  color: GETPOINTS_DARK_GREY,
  borderWidth: 1,
  borderStyle: "solid",
  borderColor: GETPOINTS_GREY,
  borderRadius: 2,
  marginTop: 10,
  marginBottom: 10,
  marginLeft: 3,
  marginRight: 3,
  padding: 8,
  flexGrow: 1,
  flexShrink: 1,
  minWidth: 0
})) as any;

export class InputPlaceholder extends React.Component<{
  style?: ViewStyle;
  numberOfLines?: number;
  onChange?: (text: string) => any;
  maxLength?: number;
  children?: string;
  className?: string;
  placeholder?: string;
  secureTextEntry?: boolean;
  keyboardType?: TextInput["props"]["keyboardType"];
}> {
  render() {
    return (
      <InputPlaceholderContainer
        onChangeText={this.props.onChange}
        style={this.props.style}
        placeholder={this.props.placeholder}
        placeholderTextColor={GETPOINTS_PLACEHOLDER_GREY}
        multiline={
          this.props.numberOfLines !== undefined && this.props.numberOfLines > 1
        }
        className={this.props.className}
        secureTextEntry={this.props.secureTextEntry}
        numberOfLines={this.props.numberOfLines}
        maxLength={this.props.maxLength}
        keyboardType={this.props.keyboardType}
        value={this.props.children}
      />
    );
  }
}

export const ChangePasswordInputField = glamorous(InputPlaceholder)(
  (props: {}) => ({
    width: 300,
    flexShrink: 1
  })
);

export const InputGroup = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 1,
  width: `100%`,
  alignItems: "center",
  justifyContent: "flex-start"
}));
