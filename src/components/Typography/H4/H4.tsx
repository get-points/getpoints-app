import React, { ReactNode } from "react";
import { Text, View, TextStyle } from "react-native";
import glamorous from "glamorous-native";
import { GETPOINTS_FONT_PRIMARY, GETPOINTS_WHITE } from "@src/const";

const H4Container = glamorous(Text)((props: {}) => ({
  fontFamily: GETPOINTS_FONT_PRIMARY,
  fontStyle: "normal",
  fontWeight: "bold",

  fontSize: 12,
  color: GETPOINTS_WHITE,
  marginTop: 10,
  marginBottom: 10,
  marginLeft: 3,
  marginRight: 3
})) as any;

export class H4 extends React.Component<{
  style?: TextStyle;
  className?: string;
  children?: ReactNode;
}> {
  static defaultProps: H4["props"] = {
    children: "Hello World"
  };
  render() {
    return (
      <H4Container style={this.props.style} className={this.props.className}>
        {this.props.children}
      </H4Container>
    );
  }
}
