import {
  GETPOINTS_FONT_PRIMARY,
  GETPOINTS_GREEN,
  GETPOINTS_GREY,
  GETPOINTS_WHITE
} from "@src/const";
import React, { ReactNode } from "react";
import { Text, TextStyle, StyleProp } from "react-native";
import glamorous from "glamorous-native";

const HeaderTextContainer = glamorous(Text)((props: {}) => ({
  fontFamily: GETPOINTS_FONT_PRIMARY,
  fontStyle: "normal",
  fontWeight: "bold",

  fontSize: 18,
  lineHeight: 25,
  minHeight: 25,
  textAlign: "center",
  color: GETPOINTS_GREY,
  marginTop: 10,
  marginBottom: 10,
  marginLeft: 3,
  marginRight: 3
})) as any;

export class HeaderText extends React.Component<{
  style?: StyleProp<TextStyle>;
  className?: string;
  children?: ReactNode;
}> {
  static defaultProps: HeaderText["props"] = {
    children: "Hello World"
  };
  render() {
    return (
      <HeaderTextContainer
        style={this.props.style}
        className={this.props.className}
      >
        {this.props.children}
      </HeaderTextContainer>
    );
  }
}

export const GreenHeaderText = glamorous(HeaderText)((props: {}) => ({
  color: GETPOINTS_GREEN
}));

export const WhiteHeaderText = glamorous(HeaderText)((props: {}) => ({
  color: GETPOINTS_WHITE
}));

export const LeftAlignedHeaderText = glamorous(HeaderText)((props: {}) => ({
  textAlign: "left" as "left"
}));
