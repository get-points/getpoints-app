import React, { ReactNode } from "react";
import { Text, View, TextStyle, StyleProp } from "react-native";
import glamorous from "glamorous-native";
import { GETPOINTS_WHITE, GETPOINTS_FONT_PRIMARY } from "@src/const";

const H5Container = glamorous(Text)((props: {}) => ({
  fontFamily: GETPOINTS_FONT_PRIMARY,
  fontStyle: "normal",
  fontWeight: "bold",

  fontSize: 12,
  color: GETPOINTS_WHITE,
  marginTop: 10,
  marginBottom: 10,
  marginLeft: 3,
  marginRight: 3
})) as any;

export class H5 extends React.Component<{
  style?: StyleProp<TextStyle>;
  children: ReactNode;
  className?: string;
}> {
  static defaultProps: H5["props"] = {
    children: "Hello World"
  };
  render() {
    return (
      <H5Container style={this.props.style} className={this.props.className}>
        {this.props.children}
      </H5Container>
    );
  }
}
