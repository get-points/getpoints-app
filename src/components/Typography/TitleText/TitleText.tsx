import React, { ReactNode } from "react";
import { Text, View, TextStyle } from "react-native";
import glamorous from "glamorous-native";
import { GETPOINTS_FONT_PRIMARY, GETPOINTS_WHITE } from "@src/const";

const TitleTextContainer = glamorous(Text)((props: {}) => ({
  fontFamily: GETPOINTS_FONT_PRIMARY,
  fontStyle: "normal",
  fontWeight: "bold",

  fontSize: 24,
  textAlign: "center",
  color: GETPOINTS_WHITE,
  marginTop: 10,
  marginBottom: 10,
  marginLeft: 3,
  marginRight: 3
})) as any;

export class TitleText extends React.Component<{
  style?: TextStyle;
  className?: string;
  children: ReactNode;
}> {
  static defaultProps: TitleText["props"] = {
    children: "Hello World"
  };
  render() {
    return (
      <TitleTextContainer className={this.props.className}>
        {this.props.children}
      </TitleTextContainer>
    );
  }
}
