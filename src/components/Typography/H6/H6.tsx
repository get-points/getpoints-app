import React, { ReactNode } from "react";
import { Text, TextStyle } from "react-native";
import glamorous from "glamorous-native";
import { GETPOINTS_FONT_PRIMARY, GETPOINTS_WHITE } from "@src/const";

const H6Container = glamorous(Text)((props: {}) => ({
  fontFamily: GETPOINTS_FONT_PRIMARY,
  fontStyle: "normal",
  fontWeight: "normal",

  fontSize: 12,
  color: GETPOINTS_WHITE,
  marginTop: 10,
  marginBottom: 10
  // marginLeft: 3,
  // marginRight: 3
})) as any;

export class H6 extends React.Component<{
  style?: TextStyle;
  className?: string;
  children: ReactNode;
}> {
  static defaultProps: H6["props"] = {
    children: "Hello World"
  };
  render() {
    return (
      <H6Container style={this.props.style} className={this.props.className}>
        {this.props.children}
      </H6Container>
    );
  }
}
