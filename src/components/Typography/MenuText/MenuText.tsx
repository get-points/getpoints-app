import React from "react";
import { Text, View, TextStyle } from "react-native";
import glamorous from "glamorous-native";
import { GETPOINTS_GREY, GETPOINTS_FONT_PRIMARY } from "@src/const";

export const MenuText = glamorous(Text)((props: {}) => ({
  color: GETPOINTS_GREY,
  fontWeight: "bold",
  fontFamily: GETPOINTS_FONT_PRIMARY,
  marginLeft: 20
}));
