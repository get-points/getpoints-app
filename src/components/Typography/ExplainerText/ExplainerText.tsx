import React, { ReactNode } from "react";
import { Text, View, TextStyle, StyleProp } from "react-native";
import glamorous from "glamorous-native";
import { GETPOINTS_FONT_PRIMARY, GETPOINTS_GREY } from "@src/const";

const ExplainerTextContainer = glamorous(Text)((props: {}) => ({
  fontFamily: GETPOINTS_FONT_PRIMARY,
  fontStyle: "normal",
  fontWeight: "normal",

  fontSize: 13,
  textAlign: "center",
  color: GETPOINTS_GREY,
  marginTop: 10,
  marginBottom: 10,
  marginLeft: 3,
  marginRight: 3
})) as any;

export class ExplainerText extends React.Component<{
  style?: StyleProp<TextStyle>;
  className?: string;
  children: ReactNode;
}> {
  static defaultProps: ExplainerText["props"] = {
    children: "Hello World"
  };
  render() {
    return (
      <ExplainerTextContainer
        style={this.props.style}
        className={this.props.className}
      >
        {this.props.children}
      </ExplainerTextContainer>
    );
  }
}

export const InputExplainerText = glamorous(ExplainerText)((props: {}) => ({
  width: 150,
  flexShrink: 1,
  textAlign: "left" as "left",
  marginLeft: 10
}));
