import React from "react";
import { Text, View, ViewStyle, TouchableOpacity } from "react-native";
import glamorous from "glamorous-native";
import { Placeholder } from "../Placeholder";
import {
  GETPOINTS_WHITE,
  GETPOINTS_GREEN,
  GETPOINTS_FONT_PRIMARY,
  GETPOINTS_GREY
} from "@src/const";

const WhiteButtonText = glamorous(Text)((props: {}) => ({
  fontFamily: GETPOINTS_FONT_PRIMARY,
  fontSize: 14,
  lineHeight: 11,
  textAlign: "center",
  color: GETPOINTS_GREY
}));

const WhiteButtonSmallContainer = glamorous(View)((props: {}) => ({
  flex: 1,
  backgroundColor: GETPOINTS_WHITE,
  borderWidth: 2,
  borderStyle: "solid",
  borderColor: GETPOINTS_GREEN,
  borderRadius: 6,
  paddingTop: 9,
  paddingBottom: 9,
  paddingLeft: 10,
  paddingRight: 10,
  marginLeft: 2,
  marginRight: 2,
  alignItems: "center",
  justifyContent: "center"
}));

export class WhiteButtonSmall extends React.Component<{
  style?: ViewStyle;
  children: string;
  tapWhiteButtonSmall: () => any;
}> {
  render() {
    return (
      <TouchableOpacity onPress={this.props.tapWhiteButtonSmall}>
        <WhiteButtonSmallContainer style={this.props.style}>
          <WhiteButtonText>{this.props.children}</WhiteButtonText>
        </WhiteButtonSmallContainer>
      </TouchableOpacity>
    );
  }
}
