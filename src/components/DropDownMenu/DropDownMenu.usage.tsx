import React from "react";
import { DropDownMenu } from "./";

export const basic = () => (
  <DropDownMenu options={["Option One", "Option Two", "Option Three"]}>
    Option Three
  </DropDownMenu>
);
