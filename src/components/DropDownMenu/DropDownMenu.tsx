import {
  GETPOINTS_DARK_GREY,
  GETPOINTS_GREY,
  GETPOINTS_WHITE,
  GETPOINTS_FONT_PRIMARY
} from "@src/const";
import React, { Ref } from "react";
import {
  Picker,
  View,
  StyleProp,
  ViewStyle,
  Text,
  TouchableOpacity,
  TextStyle
} from "react-native";
import glamorous from "glamorous-native";
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from "react-native-popup-menu";

const triggerStyles: StyleProp<ViewStyle> = {
  borderWidth: 1,
  borderStyle: "solid",
  borderColor: GETPOINTS_GREY,
  margin: 4,
  flexGrow: 1,
  backgroundColor: GETPOINTS_WHITE,
  borderRadius: 2,
  padding: 8
};
const triggerInner: StyleProp<TextStyle> = {
  fontFamily: GETPOINTS_FONT_PRIMARY,
  fontSize: 12,
  textAlign: "center",
  color: GETPOINTS_DARK_GREY
};

export class DropDownMenu extends React.Component<{
  style?: StyleProp<ViewStyle>;
  options: string[];
  onChange?: (x: string) => any;
  children: string;
  placeholder?: string;
}> {
  onChange = (v: string) => {
    const onChange = this.props.onChange || (c => {});
    onChange(v);
  };

  renderOptions() {
    return this.props.options
      .filter(x => x.trim().length > 0)
      .map((item, i) => (
        <MenuOption key={i} onSelect={() => this.onChange(item)} text={item} />
      ));
  }

  render() {
    return (
      <View style={{ flexGrow: 1 }}>
        <Menu>
          <MenuTrigger
            text={this.props.children || this.props.placeholder || "Select..."}
            customStyles={{
              triggerOuterWrapper: triggerStyles,
              triggerText: triggerInner
            }}
          />
          <MenuOptions>{this.renderOptions()}</MenuOptions>
        </Menu>
      </View>
    );
  }
}
