import React from "react";
import { Text, View, ViewStyle } from "react-native";
import glamorous from "glamorous-native";
import { ExplainerText } from "../Typography/ExplainerText";
import {
  GETPOINTS_GREY,
  GETPOINTS_WHITE,
  GETPOINTS_FONT_PRIMARY
} from "@src/const";

const LeftRectangle = glamorous(ExplainerText)((props: {}) => ({
  display: "flex" as "flex",
  flexDirection: "row" as "row",
  flexGrow: 1,
  flexShrink: 1,
  backgroundColor: GETPOINTS_WHITE,
  padding: 9,
  borderTopLeftRadius: 2,
  borderBottomLeftRadius: 2,
  borderWidth: 1,
  borderStyle: "solid",
  borderColor: GETPOINTS_GREY,
  margin: 0
}));

const RightRectangle = glamorous(ExplainerText)((props: {}) => ({
  display: "flex" as "flex",
  flexDirection: "row" as "row",
  flexGrow: 0,
  backgroundColor: GETPOINTS_GREY,
  padding: 9,
  borderTopRightRadius: 2,
  borderBottomRightRadius: 2,
  color: GETPOINTS_WHITE,
  textAlign: "center" as "center",
  flexShrink: 0,
  margin: 0
}));

const SmallPointStatusBoxContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexShrink: 1,
  flexGrow: 1,
  alignItems: "stretch",
  marginTop: 10,
  marginBottom: 10,
  marginLeft: 2,
  marginRight: 2
}));

export class SmallPointStatusBox extends React.Component<{
  style?: ViewStyle;
  points: number;
}> {
  render() {
    return (
      <SmallPointStatusBoxContainer style={this.props.style}>
        <LeftRectangle>{this.props.children}</LeftRectangle>
        <RightRectangle>{this.props.points}</RightRectangle>
      </SmallPointStatusBoxContainer>
    );
  }
}
