import { GETPOINTS_LIGHT_GREEN, GETPOINTS_WHITE } from "@src/const";
import React from "react";
import { View } from "react-native";
import glamorous from "glamorous-native";

import { Icon } from "../Icon";
import { ExplainerText } from "../Typography/ExplainerText";
import { TableRowText } from "../Typography/TableRowText";
import { StarRating } from "../StarRating";

const ReviewTileContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  marginTop: 5,
  marginBottom: 5
}));

const LeftColumn = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  width: `40%`
}));

const Box = glamorous(View)((props: {}) => ({
  display: "flex",
  flexGrow: 1,
  flexShrink: 1,
  flexDirection: "row",
  backgroundColor: GETPOINTS_WHITE,
  borderWidth: 1,
  borderStyle: "solid",
  borderColor: GETPOINTS_LIGHT_GREEN,
  borderRadius: 2,
  padding: 7,
  margin: 2,
  alignItems: "center",
  justifyContent: "center"
}));

const BoldTableRowText = glamorous(TableRowText)((props: {}) => ({
  fontWeight: "bold" as "bold"
}));

export class ReviewTile extends React.Component<{
  date: string;
  businessName: string;
  comments: string;
  rating: number;
}> {
  render() {
    return (
      <ReviewTileContainer>
        <LeftColumn>
          <Box>
            <TableRowText>{this.props.date}</TableRowText>
          </Box>
          <Box>
            <BoldTableRowText>{this.props.businessName}</BoldTableRowText>
          </Box>
          <Box>
            <StarRating size={32} rating={this.props.rating} />
          </Box>
        </LeftColumn>
        <Box>
          <ExplainerText>{this.props.comments}</ExplainerText>
        </Box>
      </ReviewTileContainer>
    );
  }
}
