import React, { ReactNode } from "react";
import { Text, View, ViewStyle } from "react-native";
import glamorous from "glamorous-native";
import { RectangularButton } from "../RectangularButton";
import { InputPlaceholder } from "../Typography/InputPlaceholder";
import { DropDownMenu } from "../DropDownMenu";
import { WhiteButton } from "../WhiteButton";
import { Icon } from "../Icon";
import { GETPOINTS_TEAL } from "@src/const";

const ListingSearchBarContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column"
  // marginLeft: -5,
  // marginRight: -5
}));
const TopRowDropDown = glamorous(View)((props: {}) => ({
  justifyContent: "space-between",
  display: "flex",
  flexDirection: "row",
  flexGrow: 1
}));

const SecondRow = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row"
}));

const ColumnOne = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1
}));

const FilterRow = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row"
}));

const TallRectangularButton = glamorous(RectangularButton)((props: {}) => ({
  marginTop: 4,
  marginBottom: 6,
  maxWidth: 100,
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center"
}));

const SearchInputPlaceholder = glamorous(InputPlaceholder)((props: {}) => ({
  height: 30,
  flexGrow: 1,
  marginTop: 2,
  marginBottom: 6
}));
const Line = glamorous(View)((props: {}) => ({
  height: 2,
  backgroundColor: GETPOINTS_TEAL,
  marginTop: 10
}));

export class ListingSearchBar extends React.Component<
  {
    style?: ViewStyle;
    countries: string[];
    cities: string[];
    categories: string[];
    city?: string;
    country?: string;
    type?: string;
    text?: string;
    onTapSearch: (x: {
      city: string;
      country: string;
      type: string;
      text: string;
    }) => any;
    onTapMemberships: () => any;
    onTapRewards: () => any;
    onTapMap: () => any;
    buttons: {
      label: ReactNode;
      handler: () => any;
    }[];
    selectedButton?: number;
  },
  { city: string; country: string; type: string; text: string }
> {
  state = {
    city: this.props.city || "",
    country: this.props.country || "",
    type: this.props.type || "",
    text: this.props.text || ""
  };

  getButtons() {
    return this.props.buttons.map((b, i) => (
      <WhiteButton
        key={i}
        onTap={b.handler}
        selected={i === this.props.selectedButton}
      >
        {b.label}
      </WhiteButton>
    ));
  }

  render() {
    return (
      <ListingSearchBarContainer style={this.props.style}>
        <TopRowDropDown>
          <DropDownMenu
            options={this.props.countries}
            onChange={country => this.setState({ country })}
          >
            {this.state.country}
          </DropDownMenu>
          <DropDownMenu
            options={this.props.cities}
            onChange={city => this.setState({ city })}
          >
            {this.state.city}
          </DropDownMenu>
        </TopRowDropDown>

        <SecondRow>
          <ColumnOne>
            <DropDownMenu
              options={this.props.categories}
              onChange={type => this.setState({ type })}
            >
              {this.state.type}
            </DropDownMenu>
            <SearchInputPlaceholder
              placeholder="Search"
              onChange={text => this.setState({ text: text })}
            >
              {this.state.text}
            </SearchInputPlaceholder>
          </ColumnOne>
          <TallRectangularButton
            onTap={() => this.props.onTapSearch(this.state)}
            topLeftSquare
          >
            ENTER
          </TallRectangularButton>
        </SecondRow>

        <FilterRow>{this.getButtons()}</FilterRow>
        <Line />
      </ListingSearchBarContainer>
    );
  }
}
