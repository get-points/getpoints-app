import { ListingSearchBar as Component } from "./ListingSearchBar";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State } from "@src/store";
import { actions } from "@src/actions";
import {} from "@src/store/selectors";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  return {
    // pass data props from here
    categories: ["", "Restaurant", "Spa"],
    cities: ["", "Kampala", "Nairobi"],
    countries: ["", "Uganda", "Kenya"]
  };
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  return {
    // pass functional props from here
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
