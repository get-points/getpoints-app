import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { ListingSearchBar } from "./index";

export const basic = () => (
  <ListingSearchBar
    categories={["Restaurant", "Cafe", "Hotel"]}
    cities={["Kampala", "Nairobi"]}
    countries={["Uganda", "Kenya"]}
    onTapMap={action("map")}
    onTapMemberships={action("memberships")}
    onTapRewards={action("Rewards")}
    onTapSearch={action("Search")}
  />
);
