import React from "react";
import { Text, View } from "react-native";
import glamorous from "glamorous-native";
import { Placeholder } from "../Placeholder";
import { Icon } from "../Icon";
import {
  GETPOINTS_GREEN,
  GETPOINTS_LIGHT_GREEN,
  GETPOINTS_WHITE
} from "@src/const";
import { string } from "prop-types";

export interface StarRatingParentProps {
  onTapStar?: (star: number) => any;
  rating: number;
  size: number;
}
export interface StarRatingFunctionProps {}
export interface StarRatingStateProps {}
export interface StarRatingLocalProps {}
export type StarRatingProps = StarRatingParentProps &
  StarRatingFunctionProps &
  StarRatingStateProps &
  StarRatingLocalProps;

interface StarRatingContainerProps {
  flex: string;
}
const StarRatingContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 0,
  alignItems: "center"
}));

export class StarRatingComponent extends React.Component<StarRatingProps> {
  render() {
    const filled = Array.from(Array(Math.floor(this.props.rating)).keys()).map(
      i => (
        <Icon
          onPress={() => (this.props.onTapStar ? this.props.onTapStar(i) : {})}
          key={i}
          name="ios-star"
          size={this.props.size}
          color={GETPOINTS_GREEN}
        />
      )
    );
    const empty = Array.from(
      Array(5 - Math.floor(this.props.rating)).keys()
    ).map(i => (
      <Icon
        key={i}
        name="ios-star"
        size={this.props.size}
        color={GETPOINTS_WHITE}
      />
    ));

    return (
      <StarRatingContainer>
        {filled}
        {empty}
      </StarRatingContainer>
    );
  }
}
