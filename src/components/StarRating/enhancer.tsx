import { State } from "@src/store";
import { goBack, push } from "connected-react-router";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { Dispatch } from "redux";

import {
  StarRatingFunctionProps,
  StarRatingStateProps,
  StarRatingParentProps,
  StarRatingProps
} from "./StarRating";

function mapStateToProps(
  state: State,
  props: StarRatingParentProps
): StarRatingStateProps {
  return {};
}

function mapDispatchToProps(
  dispatch: Dispatch,
  props: StarRatingParentProps
): StarRatingFunctionProps {
  return {};
}

function mergeProps(
  stateProps: StarRatingStateProps,
  functionProps: StarRatingFunctionProps,
  parentProps: StarRatingParentProps
): StarRatingProps {
  return {
    ...parentProps
  };
}

export const enhance = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
);
