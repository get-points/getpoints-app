import { StarRatingComponent } from "./StarRating";
import { enhance } from "./enhancer";

export const StarRating = enhance(StarRatingComponent);
