import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { StarRating } from "./index";

export const basic = () => (
  <StarRating rating={number("rating", 3)} size={number("size", 16)} />
);
