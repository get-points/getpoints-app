import { State } from "@src/store";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import {
  Component,
  FunctionProps,
  ParentProps,
  StoreProps,
  LocalProps
} from "./component";
import { actions } from "@src/actions";
import { PopupType } from "@src/actions/ui";

function getStoreProps(state: State, props: ParentProps): StoreProps {
  return {};
}

function getFunctionProps(
  dispatch: Dispatch,
  props: ParentProps
): FunctionProps {
  return {
    tapSendButton: () =>
      dispatch(
        actions.showPopup({
          popup: {
            type: PopupType.FEEDBACK_CONFIRMATION,
            data: { businessID: props.businessID }
          }
        })
      )
  };
}

function mergeProps(
  storeProps: StoreProps,
  functionProps: FunctionProps,
  parentProps: ParentProps
): StoreProps & FunctionProps & ParentProps {
  return {
    ...storeProps,
    ...functionProps,
    ...parentProps
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps,
  mergeProps
)(Component);
