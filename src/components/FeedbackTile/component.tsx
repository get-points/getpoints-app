import React from "react";
import { Text, View, TouchableOpacity } from "react-native";
import glamorous from "glamorous-native";
import { StyleProp, ViewStyle, Image } from "react-native";
import { InformationTile } from "../InformationTile";
import {
  GETPOINTS_TEAL,
  GETPOINTS_GREEN,
  GETPOINTS_YELLOW,
  GETPOINTS_PLACEHOLDER_GREY
} from "@src/const";
import { ExplainerText } from "../Typography/ExplainerText";
import { Icon } from "../Icon";
import { GreenHeaderText } from "../Typography/HeaderText";
import { InputPlaceholder } from "../Typography/InputPlaceholder";
import { FixedWidthButton } from "../RectangularButton";
import feedbackHappyInactive from "../../../assets/feedback-positive-inactive.png";
import feedbackSadInactive from "../../../assets/feedback-negative-inactive.png";
import feedbackHappy from "../../../assets/feedback-positive.png";
import feedbackSad from "../../../assets/feedback-negative.png";

/**
 * Component Properties
 */

export interface FunctionProps {
  tapSendButton: () => any;
}

export interface StoreProps {}

export interface ParentProps {
  style?: StyleProp<ViewStyle>;
  businessID: string;
  businessName: string;
  feedbackSent?: boolean;
}

export interface LocalProps {}

type ComponentProps = FunctionProps & StoreProps & ParentProps & LocalProps;

/**
 * Styled Components
 */

const SmileysGroup = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 0,
  justifyContent: "center"
}));
const PaddingToSmileyIcons = glamorous(View)((props: {}) => ({
  padding: 20
}));

/**
 * Component Definition
 */

export class Component extends React.Component<
  ComponentProps,
  {
    emotion?: "HAPPY" | "SAD";
    message: string;
  }
> {
  state = {
    message: "",
    emotion: undefined
  };

  getSad() {
    if (this.state.emotion === "SAD") {
      return (
        <Image
          source={feedbackSad}
          resizeMode="cover"
          style={{ height: 81, width: 81 }}
        />
      );
    }
    return (
      <Image
        source={feedbackSadInactive}
        resizeMode="cover"
        style={{ height: 81, width: 81 }}
      />
    );
  }
  getHappy() {
    if (this.state.emotion === "HAPPY") {
      return (
        <Image
          source={feedbackHappy}
          resizeMode="cover"
          style={{ height: 81, width: 81 }}
        />
      );
    }
    return (
      <Image
        source={feedbackHappyInactive}
        resizeMode="cover"
        style={{ height: 81, width: 81 }}
      />
    );
  }

  updateMessage = (message: string) => this.setState({ message });
  setHappy = () => this.setState({ emotion: "HAPPY" });
  setSad = () => this.setState({ emotion: "SAD" });

  render() {
    return (
      <InformationTile title="Send Us Feedback" color={GETPOINTS_GREEN}>
        <GreenHeaderText>How was your visit?</GreenHeaderText>
        <ExplainerText>Let {this.props.businessName} Know </ExplainerText>

        <SmileysGroup>
          <PaddingToSmileyIcons>
            <TouchableOpacity onPress={this.setHappy}>
              {this.getHappy()}
            </TouchableOpacity>
          </PaddingToSmileyIcons>
          <PaddingToSmileyIcons>
            <TouchableOpacity onPress={this.setSad}>
              {this.getSad()}
            </TouchableOpacity>
          </PaddingToSmileyIcons>
        </SmileysGroup>
        <InputPlaceholder numberOfLines={5} onChange={this.updateMessage}>
          {this.state.message}
        </InputPlaceholder>

        <FixedWidthButton topRightSquare onTap={this.props.tapSendButton}>
          Send
        </FixedWidthButton>
      </InformationTile>
    );
  }
}
