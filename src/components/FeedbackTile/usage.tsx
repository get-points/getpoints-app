import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { FeedbackTile } from "./index";

export const basic = () => (
  <FeedbackTile
    businessName={text("businessName", "acChi Restaurant")}
    emotion={"HAPPY"}
    message={text("message", "great meal!")}
    tapSendButton={action("buttonTapped")}
  />
);
