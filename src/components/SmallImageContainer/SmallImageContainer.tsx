import React from "react";
import { Image } from "react-native";
import glamorous from "glamorous-native";

const SmallImageContainerContainer = glamorous(Image)((props: {}) => ({
  height: 80,
  width: 80,
  margin: 4
}));

export class SmallImageContainer extends React.Component<{
  photo: string;
}> {
  render() {
    return <SmallImageContainerContainer source={{ uri: this.props.photo }} />;
  }
}
