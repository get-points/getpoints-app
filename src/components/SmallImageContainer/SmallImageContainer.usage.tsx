import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { SmallImageContainer } from "./index";
import { View } from "react-native";

export const single = () => (
  <SmallImageContainer photo="http://placekitten.com/100/100" />
);

export const multi = () => (
  <View
    style={{
      display: "flex",
      flexDirection: "row",
      flexWrap: "wrap",
      alignItems: "center",
      justifyContent: "center"
    }}
  >
    <SmallImageContainer photo="http://placekitten.com/100/100" />
    <SmallImageContainer photo="http://placekitten.com/100/100" />
    <SmallImageContainer photo="http://placekitten.com/100/100" />
    <SmallImageContainer photo="http://placekitten.com/100/100" />
    <SmallImageContainer photo="http://placekitten.com/100/100" />
    <SmallImageContainer photo="http://placekitten.com/100/100" />
    <SmallImageContainer photo="http://placekitten.com/100/100" />
    <SmallImageContainer photo="http://placekitten.com/100/100" />
  </View>
);
