import { action } from "@tyro";
export enum PopupType {
  CONFIRM_SIGNOUT,
  FEEDBACK_CONFIRMATION,
  INVALID_APPROVAL_CODE,
  EARNED_POINTS_CONFIRMATION,

  OFFER_CLAIM,
  OFFER_CLAIM_CONFIRMATION,

  REWARD_REDEMPTION,
  REWARD_REDEMPTION_MANAGER_APPROVAL_CODE,
  REWARD_REDEMPTION_CONFIRMATION,

  STAMPS_REDEMPTION,
  STAMPS_REDEMPTION_MANAGER_APPROVAL_CODE,
  STAMPS_REDEMPTION_CONFIRMATION,

  STAMPS_EARN,
  STAMPS_EARN_MANAGER_APPROVAL_CODE,
  STAMPS_EARN_CONFIRMATION
}

export type PopupState =
  | {
      type: PopupType.CONFIRM_SIGNOUT;
    }
  | {
      type: PopupType.REWARD_REDEMPTION;
      data: {
        rewardID: string;
      };
    }
  | {
      type: PopupType.REWARD_REDEMPTION_MANAGER_APPROVAL_CODE;
      data: {
        rewardID: string;
      };
    }
  | {
      type: PopupType.REWARD_REDEMPTION_CONFIRMATION;
      data: {
        rewardID: string;
      };
    }
  | {
      type: PopupType.STAMPS_EARN;
      data: {
        stampProgramID: string;
        stampsAvailable: number;
      };
    }
  | {
      type: PopupType.STAMPS_EARN_MANAGER_APPROVAL_CODE;
      data: {
        stampProgramID: string;
        stampsAvailable: number;
      };
    }
  | {
      type: PopupType.STAMPS_EARN_CONFIRMATION;
      data: {
        stampProgramID: string;
        stampsAvailable: number;
        rewardNumber: number;
      };
    }
  | {
      type: PopupType.STAMPS_REDEMPTION;
      data: {
        stampsAvailable: number;
        stampProgramID: string;
      };
    }
  | {
      type: PopupType.STAMPS_REDEMPTION_CONFIRMATION;
      data: {
        stampProgramID: string;
        rewardNumber: number;
        stampsRedeemed: number;
      };
    }
  | {
      type: PopupType.STAMPS_REDEMPTION_MANAGER_APPROVAL_CODE;
      data: {
        stampProgramID: string;
        rewardNumber: number;
        stampsRedeemed: number;
      };
    }
  | {
      type: PopupType.OFFER_CLAIM;
      data: {
        offerID: string;
      };
    }
  | {
      type: PopupType.OFFER_CLAIM_CONFIRMATION;
      data: {
        offerID: string;
      };
    }
  | {
      type: PopupType.INVALID_APPROVAL_CODE;
      data: {};
    }
  | {
      type: PopupType.EARNED_POINTS_CONFIRMATION;
      data: {
        businessID: string;
      };
    }
  | {
      type: PopupType.FEEDBACK_CONFIRMATION;
      data: {
        businessID: string;
      };
    };

export const openMenu = action("OPEN_MENU")<void>();
export const closeMenu = action("CLOSE_MENU")<void>();
export const toggleMenu = action("TOGGLE_MENU")<void>();

export const showError = action("SHOW_GLOBAL_ERROR")<{ message: string }>();
export const hideError = action("CLEAR_GLOBAL_ERROR")<void>();

export const showPopup = action("SHOW_POPUP")<{ popup: PopupState }>();
export const hidePopup = action("CLEAR_POPUP")<void>();
