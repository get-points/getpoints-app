import { action } from "@tyro";
import { Offer } from "@src/types/Models";

export const getOfferList = {
  request: action("GET_OFFER_LIST/REQUEST")<{ ids: string[] }>(),
  success: action("GET_OFFER_LIST/SUCCESS")<{ [id: string]: Offer }>(),
  failure: action("GET_OFFER_LIST/FAILURE")<{
    request: { ids: string[] };
    message: string;
  }>()
};

// single offer action
export const getOffer = {
  request: action("GET_OFFER/REQUEST")<{ id: string }>(),
  success: action("GET_OFFER/SUCCESS")<{ [id: string]: Offer }>(),
  failure: action("GET_OFFER/FAILURE")<{
    request: { id: string };
    message: string;
  }>()
};

export const clearAllOfferErrors = action("CLEAR_ALL_OFFER_ERRORS")<{}>();

export const clearOfferError = action("CLEAR_OFFER_ERROR")<{
  requestID: number;
}>();
