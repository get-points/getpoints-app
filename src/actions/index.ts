import { ActionType } from "typesafe-actions";
import * as business from "./business";
import * as membership from "./membership";
import * as feedback from "./feedback";
import * as offer from "./offer";
import * as review from "./review";
import * as reward from "./reward";
import * as stampProgram from "./stampProgram";
import * as user from "./user";
import * as ui from "./ui";
import * as account from "./account";
import { RouterAction } from "connected-react-router";
import { createEpicMiddleware, Epic } from "redux-observable";
import { rootEpic } from "@src/epics";
import { State } from "@src/store";
import { Deps } from "@src/types/Epic";
import { api } from "@src/api/mock";

export const actions = {
  ...business,
  ...ui,
  ...feedback,
  ...membership,
  ...offer,
  ...review,
  ...reward,
  ...stampProgram,
  ...user,
  ...account
};

export type Action = ActionType<typeof actions> | RouterAction;
