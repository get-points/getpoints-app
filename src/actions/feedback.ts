import { action } from "@tyro";
import { Feedback } from "@src/types/Models";

export const getFeedbackList = {
  request: action("GET_FEEDBACK_LIST/REQUEST")<{ ids: string[] }>(),
  success: action("GET_FEEDBACK_LIST/SUCCESS")<{ [id: string]: Feedback }>(),
  failure: action("GET_FEEDBACK_LIST/FAILURE")<{
    request: { ids: string[] };
    message: string;
  }>()
};

// single feedback action
export const getFeedback = {
  request: action("GET_FEEDBACK/REQUEST")<{ id: string }>(),
  success: action("GET_FEEDBACK/SUCCESS")<{ [id: string]: Feedback }>(),
  failure: action("GET_FEEDBACK/FAILURE")<{
    request: { id: string };
    message: string;
  }>()
};

export const clearAllFeedbackErrors = action("CLEAR_ALL_FEEDBACK_ERRORS")<{}>();

export const clearFeedbackError = action("CLEAR_FEEDBACK_ERROR")<{
  requestID: number;
}>();
