import { action } from "@tyro";
import { Review } from "@src/types/Models";

export const getReviewList = {
  request: action("GET_REVIEW_LIST/REQUEST")<{ ids: string[] }>(),
  success: action("GET_REVIEW_LIST/SUCCESS")<{ [id: string]: Review }>(),
  failure: action("GET_REVIEW_LIST/FAILURE")<{
    request: { ids: string[] };
    message: string;
  }>()
};

// single review action
export const getReview = {
  request: action("GET_REVIEW/REQUEST")<{ id: string }>(),
  success: action("GET_REVIEW/SUCCESS")<{ [id: string]: Review }>(),
  failure: action("GET_REVIEW/FAILURE")<{
    request: { id: string };
    message: string;
  }>()
};

export const clearAllReviewErrors = action("CLEAR_ALL_REVIEW_ERRORS")<{}>();

export const clearReviewError = action("CLEAR_REVIEW_ERROR")<{
  requestID: number;
}>();
