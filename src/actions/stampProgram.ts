import { action } from "@tyro";
import { StampProgram } from "@src/types/Models";

export const getStampProgramList = {
  request: action("GET_STAMP_PROGRAM_LIST/REQUEST")<{ ids: string[] }>(),
  success: action("GET_STAMP_PROGRAM_LIST/SUCCESS")<{
    [id: string]: StampProgram;
  }>(),
  failure: action("GET_STAMP_PROGRAM_LIST/FAILURE")<{
    request: { ids: string[] };
    message: string;
  }>()
};

// single stampProgram action
export const getStampProgram = {
  request: action("GET_STAMP_PROGRAM/REQUEST")<{ id: string }>(),
  success: action("GET_STAMP_PROGRAM/SUCCESS")<{
    [id: string]: StampProgram;
  }>(),
  failure: action("GET_STAMP_PROGRAM/FAILURE")<{
    request: { id: string };
    message: string;
  }>()
};

export const clearAllStampProgramErrors = action(
  "CLEAR_ALL_STAMP_PROGRAM_ERRORS"
)<{}>();

export const clearStampProgramError = action("CLEAR_STAMP_PROGRAM_ERROR")<{
  requestID: number;
}>();
