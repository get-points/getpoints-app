import { action } from "@tyro";
import { User } from "@src/types/Models";

// TODO: only use EITHER standard action OR async action

export const getUserList = {
  request: action("GET_USER_LIST/REQUEST")<{ ids: string[] }>(),
  success: action("GET_USER_LIST/SUCCESS")<{ [id: string]: User }>(),
  failure: action("GET_USER_LIST/FAILURE")<{
    request: { ids: string[] };
    message: string;
  }>()
};

// single user action
export const getUser = {
  request: action("GET_USER/REQUEST")<{ id: string }>(),
  success: action("GET_USER/SUCCESS")<{ [id: string]: User }>(),
  failure: action("GET_USER/FAILURE")<{
    request: { id: string };
    message: string;
  }>()
};

export const removeUser = {
  request: action("REMOVE_USER")<{ id: string }>()
};

export const clearAllUserErrors = action("CLEAR_ALL_USER_ERRORS")<{}>();

export const clearUserError = action("CLEAR_USER_ERROR")<{
  requestID: number;
}>();
