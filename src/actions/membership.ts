import { action } from "@tyro";
import { Membership } from "@src/types/Models";

export const getMembershipList = {
  request: action("GET_MEMBERSHIP_LIST/REQUEST")<{ ids: string[] }>(),
  success: action("GET_MEMBERSHIP_LIST/SUCCESS")<{
    [id: string]: Membership;
  }>(),
  failure: action("GET_MEMBERSHIP_LIST/FAILURE")<{
    request: { ids: string[] };
    message: string;
  }>()
};

// single membership action
export const getMembership = {
  request: action("GET_MEMBERSHIP/REQUEST")<{ id: string }>(),
  success: action("GET_MEMBERSHIP/SUCCESS")<{ [id: string]: Membership }>(),
  failure: action("GET_MEMBERSHIP/FAILURE")<{
    request: { id: string };
    message: string;
  }>()
};

export const clearAllMembershipErrors = action(
  "CLEAR_ALL_MEMBERSHIP_ERRORS"
)<{}>();

export const clearMembershipError = action("CLEAR_MEMBERSHIP_ERROR")<{
  requestID: number;
}>();
