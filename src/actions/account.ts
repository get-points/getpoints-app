import { action } from "@tyro";

export const sendSupportRequest = {
  request: action("SEND_SUPPORT_REQUEST/REQUEST")<{
    email: string;
    name: string;
    message: string;
  }>(),
  success: action("SEND_SUPPORT_REQUEST/SUCCESS")<void>(),
  failure: action("SEND_SUPPORT_REQUEST/FAILURE")<{ message: string }>()
};

export const signOut = {
  request: action("SIGN_OUT/REQUEST")<{}>(),
  confirm: action("SIGN_OUT/CONFIRM")<{}>()
};

export const signIn = {
  request: action("SIGN_IN/REQUEST")<{
    username: string;
    password: string;
  }>(),
  success: action("SIGN_IN/SUCCESS")<{ token: string }>(),
  failure: action("SIGN_IN/FAILURE")<{ message: string }>()
};

export const signUp1 = {
  request: action("SIGN_UP/1/REQUEST")<{
    firstName: string;
    lastName: string;
    password: string;
    confirmPassword: string;
  }>(),
  success: action("SIGN_UP/1/SUCCESS")<{
    firstName: string;
    lastName: string;
    password: string;
    confirmPassword: string;
  }>(),
  failure: action("SIGN_UP/1/FAILURE")<{
    message: string;
  }>()
};

export const signUp2 = {
  request: action("SIGN_UP/2/REQUEST")<{
    email: string;
    countryCode: string;
    telephone: string;
  }>(),
  success: action("SIGN_UP/2/SUCCESS")<{}>(),
  failure: action("SIGN_UP/2/FAILURE")<{}>()
};
