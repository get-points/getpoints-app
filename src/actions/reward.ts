import { action } from "@tyro";
import { Reward } from "@src/types/Models";

export const getRewardList = {
  request: action("GET_REWARD_LIST/REQUEST")<{ ids: string[] }>(),
  success: action("GET_REWARD_LIST/SUCCESS")<{ [id: string]: Reward }>(),
  failure: action("GET_REWARD_LIST/FAILURE")<{
    request: { ids: string[] };
    message: string;
  }>()
};

// single reward action
export const getReward = {
  request: action("GET_REWARD/REQUEST")<{ id: string }>(),
  success: action("GET_REWARD/SUCCESS")<{ [id: string]: Reward }>(),
  failure: action("GET_REWARD/FAILURE")<{
    request: { id: string };
    message: string;
  }>()
};

export const clearAllRewardErrors = action("CLEAR_ALL_REWARD_ERRORS")<{}>();

export const clearRewardError = action("CLEAR_REWARD_ERROR")<{
  requestID: number;
}>();
