import { action } from "@tyro";
import { Business } from "@src/types/Models";

export const getBusinessList = {
  request: action("GET_BUSINESS_LIST/REQUEST")<{ ids: string[] }>(),
  success: action("GET_BUSINESS_LIST/SUCCESS")<{ [id: string]: Business }>(),
  failure: action("GET_BUSINESS_LIST/FAILURE")<{
    request: { ids: string[] };
    message: string;
  }>()
};

export const getNearbyBusinesses = {
  request: action("GET_NEARBY_BUSINESS_LIST/REQUEST")<{
    city?: string;
    country?: string;
    text?: string;
    type?: string;
  }>(),
  success: action("GET_NEARBY_BUSINESS_LIST/SUCCESS")<{
    ids: { id: string; location: number }[];
  }>(),
  failure: action("GET_NEARBY_BUSINESS_LIST/FAILURE")<{
    message: string;
  }>()
};

// single user action
export const getBusiness = {
  request: action("GET_BUSINESS/REQUEST")<{ id: string }>(),
  success: action("GET_BUSINESS/SUCCESS")<{ [id: string]: Business }>(),
  failure: action("GET_BUSINESS/FAILURE")<{
    request: { id: string };
    message: string;
  }>()
};

export const clearAllBusinessErrors = action("CLEAR_ALL_BUSINESS_ERRORS")<{}>();

export const clearBusinessError = action("CLEAR_BUSINESS_ERROR")<{
  requestID: number;
}>();
