import React from "react";
import { Text, View, ScrollView, KeyboardAvoidingView } from "react-native";
import glamorous from "glamorous-native";
import { Placeholder } from "@src/components/Placeholder";
import { HeaderBar } from "@src/components/HeaderBar";
import {
  RectangularButton,
  FixedWidthButton
} from "@src/components/RectangularButton";
import { InputPlaceholder } from "@src/components/Typography/InputPlaceholder";
import { HeaderText } from "@src/components/Typography/HeaderText";
import { GETPOINTS_BACKGROUND_GREY, GETPOINTS_RED } from "@src/const";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

const SignUpPage1Container = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flex: 1
}));

const Page = glamorous(KeyboardAwareScrollView)((props: {}) => ({
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  padding: 20
}));

const ButtonContainer = glamorous(View)((props: {}) => ({
  flexDirection: "row",
  justifyContent: "center",
  marginBottom: 20
}));

export class SignUpPage1 extends React.Component<
  {
    tapNextButton: () => any;

    signUp: (x: {
      firstName: string;
      lastName: string;
      password: string;
      confirmPassword: string;
    }) => any;
    signIn: () => any;
    loaded?: boolean;
    error?: string;
  },
  {
    firstName: string;
    lastName: string;
    password: string;
    confirmPassword: string;
  }
> {
  state = {
    firstName: "",
    lastName: "",
    password: "",
    confirmPassword: ""
  };
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }

    return (
      <SignUpPage1Container>
        <HeaderBar hideMenu />
        <Page enableOnAndroid>
          {/* <ScrollView> */}
          <HeaderText>Sign Up Now</HeaderText>
          <InputPlaceholder
            placeholder="First Name"
            onChange={firstName => this.setState({ firstName })}
          >
            {this.state.firstName}
          </InputPlaceholder>
          <InputPlaceholder
            placeholder="Last Name"
            onChange={lastName => this.setState({ lastName })}
          >
            {this.state.lastName}
          </InputPlaceholder>
          <InputPlaceholder
            placeholder="Password"
            secureTextEntry
            onChange={password => this.setState({ password })}
          >
            {this.state.password}
          </InputPlaceholder>
          <InputPlaceholder
            placeholder="Password Confirmation"
            secureTextEntry
            onChange={confirmPassword => this.setState({ confirmPassword })}
          >
            {this.state.confirmPassword}
          </InputPlaceholder>
          <ButtonContainer>
            <FixedWidthButton
              topLeftSquare
              color={GETPOINTS_RED}
              onTap={this.props.signIn}
            >
              CANCEL
            </FixedWidthButton>
            <FixedWidthButton
              topRightSquare
              onTap={() => this.props.signUp(this.state)}
            >
              NEXT
            </FixedWidthButton>
          </ButtonContainer>
          {/* </ScrollView> */}
        </Page>
      </SignUpPage1Container>
    );
  }
}
