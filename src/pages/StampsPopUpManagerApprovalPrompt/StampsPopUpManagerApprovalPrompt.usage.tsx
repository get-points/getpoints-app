import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { StampsPopUpManagerApprovalPrompt } from "./index";

export const basic = () => (
  <StampsPopUpManagerApprovalPrompt
    productName={"Pizza"}
    managerName={"Joey"}
    tapCancelButton={action("tapCancelButton")}
    tapApproveButton={action("tapApproveButton")}
  />
);
