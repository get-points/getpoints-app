import { StampsPopUpManagerApprovalPrompt as Component } from "./StampsPopUpManagerApprovalPrompt";

import { connect } from "react-redux";
import { Dispatch, DeepPartial } from "redux";
import { State } from "@src/store";
import { actions, Action } from "@src/actions";
import {} from "@src/store/selectors";
import { getStampProgramByID } from "@src/store/stampProgram/selectors";
import { PopupType } from "@src/actions/ui";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]> & {
    stampProgramID: string;
    stampsAvailable: number;
  }
): Partial<Component["props"] & { loadAction: Action }> {
  try {
    // prepare data here
    const programID = props.stampProgramID;
    const loadableProgram = getStampProgramByID(store, programID);
    if (loadableProgram.state === "ABSENT")
      return {
        loaded: false,
        loadAction: actions.getStampProgram.request({ id: programID })
      };
    if (loadableProgram.state === "PENDING") return { loaded: false };
    if (loadableProgram.state === "FAILED")
      return { error: loadableProgram.message };
    const program = loadableProgram.item;

    return {
      // pass data props from here
      productName: program.headline
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]> & {
    stampProgramID: string;
    stampsAvailable: number;
  }
): DeepPartial<Component["props"]> {
  return {
    // pass functional props from here
    tapApproveButton: () =>
      dispatch(
        actions.showPopup({
          popup: {
            type: PopupType.STAMPS_EARN_MANAGER_APPROVAL_CODE,
            data: {
              stampsAvailable: props.stampsAvailable,
              stampProgramID: props.stampProgramID
            }
          }
        })
      ),
    tapCancelButton: () => dispatch(actions.hidePopup())
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
