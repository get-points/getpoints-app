import { DropDownMenu } from "@src/components/DropDownMenu";
import { PageHeader } from "@src/components/PageHeader";
import {
  GETPOINTS_RED,
  GETPOINTS_WHITE,
  GETPOINTS_BACKGROUND_GREY
} from "@src/const";
import glamorous from "glamorous-native";
import React from "react";
import { View, ViewStyle } from "react-native";
import { RectangularButton } from "../../components/RectangularButton";
import { HeaderText } from "../../components/Typography/HeaderText";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { HeaderBar } from "@src/components/HeaderBar";
import { PopupContainer } from "@src/components/PopupContainer";

const StampsPopUpManagerApprovalPromptContainer = glamorous(View)(
  (props: {}) => ({
    display: "flex",
    flexDirection: "column",
    flexGrow: 1
  })
);
const PopupElements = glamorous(View)((props: {}) => ({
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  textAlign: "center",
  alignItems: "center",
  padding: 10
}));
const RewardRedemptionText = glamorous(HeaderText)((props: {}) => ({
  fontSize: 12
}));
const RewardName = glamorous(HeaderText)((props: {}) => ({
  fontSize: 12,
  color: GETPOINTS_RED
}));
const ButtonsArea = glamorous(View)((props: {}) => ({
  justifyContent: "center",
  display: "flex",
  flexDirection: "row",
  minWidth: 200
}));
const PopUpButton = glamorous(RectangularButton)((props: {}) => ({
  width: 100,
  fontSize: 12
}));

export class StampsPopUpManagerApprovalPrompt extends React.Component<{
  style?: ViewStyle;
  productName: string;
  tapCancelButton: () => void;
  tapApproveButton: () => void;

  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <PopupContainer>
        <RewardRedemptionText>
          Ask the Cashier or the Manager to Approve your Stamp(s) here to redeem
        </RewardRedemptionText>
        <RewardName>{this.props.productName}</RewardName>
        <ButtonsArea>
          <PopUpButton
            topLeftSquare
            color={GETPOINTS_RED}
            onTap={this.props.tapCancelButton}
          >
            CANCEL
          </PopUpButton>
          <PopUpButton topRightSquare onTap={this.props.tapApproveButton}>
            APPROVE
          </PopUpButton>
        </ButtonsArea>
      </PopupContainer>
    );
  }
}
