import React from "react";
import { Text, View, ViewStyle } from "react-native";
import glamorous from "glamorous-native";
import { HeaderBar } from "@src/components/HeaderBar";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { InputPlaceholder } from "@src/components/Typography/InputPlaceholder";
import {
  RectangularButton,
  FixedWidthButton
} from "@src/components/RectangularButton";
import { GETPOINTS_BACKGROUND_GREY, GETPOINTS_RED } from "@src/const";
import { boolean } from "@storybook/addon-knobs";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";

const ForgotPasswordContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1
}));
const Page = glamorous(View)((props: {}) => ({
  paddingTop: 10,
  paddingBottom: 10,
  paddingLeft: 20,
  paddingRight: 20,
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  display: "flex",
  flexDirection: "column",
  alignItems: "stretch"
}));
const ForgotPasswordText = glamorous(ExplainerText)((props: {}) => ({
  fontSize: 15
}));
const ButtonsContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  flexGrow: 0,
  flexShrink: 1
}));

export class ForgotPassword extends React.Component<
  {
    style?: ViewStyle;
    onTapBack: () => any;
    onTapEnter: () => any;
    loaded?: boolean;
    error?: string;
  },
  { email: string }
> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <ForgotPasswordContainer>
        <HeaderBar />
        <Page>
          <ForgotPasswordText>Enter your email address:</ForgotPasswordText>
          <InputPlaceholder>{this.state.email}</InputPlaceholder>
          <ButtonsContainer>
            <FixedWidthButton
              topLeftSquare
              color={GETPOINTS_RED}
              onTap={this.props.onTapBack}
            >
              BACK
            </FixedWidthButton>
            <FixedWidthButton topRightSquare onTap={this.props.onTapEnter}>
              ENTER
            </FixedWidthButton>
          </ButtonsContainer>
        </Page>
      </ForgotPasswordContainer>
    );
  }
}
