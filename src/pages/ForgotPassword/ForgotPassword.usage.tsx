import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { ForgotPassword } from "./index";

export const basic = () => (
  <ForgotPassword
    onTapBack={action("buttonTapped")}
    onTapEnter={action("buttonTapped")}
  />
);
