import { HeaderBar } from "@src/components/HeaderBar";
import { PageHeader } from "@src/components/PageHeader";
import { FixedWidthButton } from "@src/components/RectangularButton";
import {
  GETPOINTS_BACKGROUND_GREY,
  GETPOINTS_GREY,
  GETPOINTS_RED,
  GETPOINTS_WHITE
} from "@src/const";
import React from "react";
import { View, ScrollView } from "react-native";
import glamorous from "glamorous-native";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";

import { BarCodeScanner, Permissions } from "expo";

const QrCodeScannerOpenContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flex: 1
}));

const Page = glamorous(View)((props: {}) => ({
  padding: 10,
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  display: "flex",
  flexDirection: "column",
  alignItems: "center"
}));

const Wrapper = glamorous(ScrollView)((props: {}) => ({
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  padding: 10
}));

const ButtonsContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  flexGrow: 0
}));
const QRCodeScanner = glamorous(View)((props: {}) => ({
  backgroundColor: GETPOINTS_WHITE,
  width: 250,
  height: 250,
  borderWidth: 1,
  borderColor: GETPOINTS_GREY,
  borderStyle: "solid",
  margin: 10,
  marginTop: 20
}));
export class QrCodeScannerOpen extends React.Component<
  {
    tapCancel: () => any;
    tapScan: () => any;

    loaded?: boolean;
    error?: string;
  },
  { hasCameraPermission?: boolean }
> {
  state = {
    // hasCameraPermission: un,
  };

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === "granted" });
  }

  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <QrCodeScannerOpenContainer>
        <HeaderBar />
        <PageHeader>QR Code Scanner</PageHeader>
        <Wrapper>
          <Page>
            <QRCodeScanner />
            <ButtonsContainer>
              <FixedWidthButton
                topLeftSquare
                color={GETPOINTS_RED}
                onTap={this.props.tapCancel}
              >
                CANCEL
              </FixedWidthButton>
            </ButtonsContainer>
          </Page>
        </Wrapper>
      </QrCodeScannerOpenContainer>
    );
  }
}
