import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { QrCodeScannerOpen } from "./index";

export const basic = () => (
  <QrCodeScannerOpen tapCancel={() => {}} tapScan={() => {}} />
);
