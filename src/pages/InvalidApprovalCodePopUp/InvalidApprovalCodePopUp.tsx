import { ErrorMessage } from "@src/components/ErrorMessage";
import { Loading } from "@src/components/Loading";
import { Placeholder } from "@src/components/Placeholder";
import { PopupContainer } from "@src/components/PopupContainer";
import { RectangularButton } from "@src/components/RectangularButton";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import React from "react";
import { ViewStyle } from "react-native";

export class InvalidApprovalCodePopUp extends React.Component<{
  style?: ViewStyle;
  tapCancelButton: () => void;
  tapRetryButton: () => void;
  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <PopupContainer>
        <ExplainerText>Invalid Approval Code</ExplainerText>
        <Placeholder>
          <RectangularButton onTap={this.props.tapCancelButton}>
            CANCEL
          </RectangularButton>
          <RectangularButton onTap={this.props.tapRetryButton}>
            RETRY
          </RectangularButton>
        </Placeholder>
      </PopupContainer>
    );
  }
}
