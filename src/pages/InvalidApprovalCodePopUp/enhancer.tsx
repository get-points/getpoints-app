import { actions } from "@src/actions";
import { State } from "@src/store";
import {} from "@src/store/selectors";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { InvalidApprovalCodePopUp as Component } from "./InvalidApprovalCodePopUp";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  try {
    // prepare data here

    return {
      // pass data props from here
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  return {
    // pass functional props from here
    tapCancelButton: () => dispatch(actions.hidePopup()),
    tapRetryButton: () => dispatch(actions.hidePopup())
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
