import { FeedbackConfirmationPopUp as Component } from "./FeedbackConfirmationPopUp";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State } from "@src/store";
import { actions } from "@src/actions";
import {} from "@src/store/selectors";
import { getBusinessByID } from "@src/store/business/selectors";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]> & { businessID: string }
): Partial<Component["props"]> {
  try {
    // prepare data here
    const loadableBusiness = getBusinessByID(store, props.businessID);
    if (loadableBusiness.state === "ABSENT") return { loaded: false };
    if (loadableBusiness.state === "PENDING") return { loaded: false };
    if (loadableBusiness.state === "FAILED")
      return { error: loadableBusiness.message };
    const business = loadableBusiness.item;

    return {
      // pass data props from here
      businessName: business.name
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  return {
    // pass functional props from here
    tapDoneButton: () => dispatch(actions.hidePopup())
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
