import { ErrorMessage } from "@src/components/ErrorMessage";
import { Loading } from "@src/components/Loading";
import { PopupContainer } from "@src/components/PopupContainer";
import React from "react";
import { FixedWidthButton } from "../../components/RectangularButton";
import { HeaderText } from "../../components/Typography/HeaderText";

export class FeedbackConfirmationPopUp extends React.Component<{
  tapDoneButton: () => void;

  businessName: string;
  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <PopupContainer>
        <HeaderText>Thank you for your feedback and time!</HeaderText>
        <FixedWidthButton topRightSquare onTap={this.props.tapDoneButton}>
          DONE
        </FixedWidthButton>
      </PopupContainer>
    );
  }
}
