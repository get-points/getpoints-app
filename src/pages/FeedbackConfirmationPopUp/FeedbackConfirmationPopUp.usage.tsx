import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { FeedbackConfirmationPopUp } from "./index";

export const basic = () => (
  <FeedbackConfirmationPopUp
    businessID={"1"}
    tapDoneButton={action("tappedButton")}
  />
);
