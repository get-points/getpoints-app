import { HeaderBar } from "@src/components/HeaderBar";
import { PageHeader } from "@src/components/PageHeader";
import { FixedWidthButton } from "@src/components/RectangularButton";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { GETPOINTS_BACKGROUND_GREY, GETPOINTS_RED } from "@src/const";
import React from "react";
import { Image, View, ScrollView } from "react-native";
import glamorous from "glamorous-native";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";

const QrCodeScannerLauncherContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flex: 1,
  flexDirection: "column"
}));

const Wrapper = glamorous(ScrollView)((props: {}) => ({
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  padding: 20
}));

const Page = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  alignItems: "center"
}));

const QRCodeIcon = glamorous(Image)((props: {}) => ({
  width: 250,
  height: 250,
  margin: 10
}));

const ButtonsContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  flexGrow: 0
}));
export class QrCodeScannerLauncher extends React.Component<{
  tapCancelButton: () => any;
  tapScanNowButton: () => any;

  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <QrCodeScannerLauncherContainer>
        <HeaderBar />
        <PageHeader>QR Code Scanner</PageHeader>
        <Wrapper>
          <Page>
            <ExplainerText>
              Open your QR code scanner in your Get Points app and hold it over
              the merchant’s QR code within the brackets until it registers.
            </ExplainerText>
            <ButtonsContainer>
              <FixedWidthButton
                topLeftSquare
                color={GETPOINTS_RED}
                onTap={this.props.tapCancelButton}
              >
                CANCEL
              </FixedWidthButton>
              <FixedWidthButton
                topRightSquare
                onTap={this.props.tapScanNowButton}
              >
                SCAN NOW
              </FixedWidthButton>
            </ButtonsContainer>
          </Page>
        </Wrapper>
      </QrCodeScannerLauncherContainer>
    );
  }
}
