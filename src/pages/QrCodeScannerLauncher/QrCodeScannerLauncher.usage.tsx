import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { QrCodeScannerLauncher } from "./index";

export const basic = () => (
  <QrCodeScannerLauncher
    tapCancelButton={action("tapButton")}
    tapScanNowButton={action("tapButton")}
  />
);
