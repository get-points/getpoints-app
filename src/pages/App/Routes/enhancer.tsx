import { State, store } from "@src/store";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { Component, FunctionProps, StoreProps } from "./component";
import {
  withRouter,
  RouteProps,
  RouterProps,
  RouteComponentProps
} from "react-router";
import { actions } from "@src/actions";
import { getOnlyUser } from "@src/store/user/selectors";
import { goBack, push } from "connected-react-router";

function getStoreProps(state: State): StoreProps {
  const loadableUser = getOnlyUser(state);
  if (loadableUser.state === "PENDING") return { loading: true };

  return {
    loggedIn: loadableUser.state === "LOADED",
    popup: state.ui.popup,
    globalError: state.ui.error
  };
}

function getFunctionProps(dispatch: Dispatch): FunctionProps {
  return {
    navigateBack: (popupShown?: boolean) => {
      if (popupShown) {
        dispatch(actions.hidePopup());
      } else {
        dispatch(goBack());
      }
    },
    signIn: () => {
      // TODO: Remove dev code console.log
      dispatch(actions.signIn.request({ password: "t", username: "t" }));
      dispatch(push("/my/stamps"));
    }
  };
}

function mergeProps(
  storeProps: StoreProps,
  functionProps: FunctionProps,
  parentProps: RouteComponentProps
): StoreProps & FunctionProps {
  return {
    ...parentProps,
    ...storeProps,
    ...functionProps
  };
}

export const Container = withRouter(
  connect(
    getStoreProps,
    getFunctionProps,
    mergeProps
  )(Component as any)
);
