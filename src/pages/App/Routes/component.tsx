import { PopupType, PopupState } from "@src/actions/ui";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { Loading } from "@src/components/Loading";
import { GETPOINTS_RED, GETPOINTS_WHITE } from "@src/const";
import { MyOffers } from "@src/pages/MyOffers";
import { MyStamps } from "@src/pages/MyStamps";
import { SignIn } from "@src/pages/SignIn";
import { SignOutPopUp } from "@src/pages/SignOutPopUp";
import { LoadableState } from "@tyro";
import glamorous from "glamorous-native";
import React from "react";
import { Text, View } from "react-native";
import { Switch } from "react-router";
// TODO: Abstract and/or choose
// import { NativeRouter as Router, Route, Link } from "react-router-native";
import { Route } from "react-router-dom";
import { ConnectedAboutLaunchPage } from "../../AboutLaunchPage";
import { ConnectedAboutUsPage } from "../../AboutUsPage";
import { ChangePasswordPage } from "../../ChangePasswordPage";
import { DetailedRestaurantView } from "../../DetailedRestaurantView";
// import { DetailedRestaurantViewGallery } from "../../DetailedRestaurantViewGallery";
// import { DetailedRestaurantViewReviews } from "../../DetailedRestaurantViewReviews";
// import { DetailedRestaurantViewMap } from "../../DetailedRestaurantViewMap";
// import { DetailedRestaurantViewRewards } from "../../DetailedRestaurantViewRewards";
import { EditYourProfile } from "../../EditYourProfile";
import { EmailSupport } from "../../EmailSupport";
import { ConnectedFaqs } from "../../Faqs";
import { FeedbackCollection } from "../../FeedbackCollection";
import { HowItWorks } from "../../HowItWorks";
import { MyHistory } from "../../MyHistory";
import { MyMemberships } from "../../MyMemberships";
import { MyRewards } from "../../MyRewards";
import { NearbyOffers } from "../../NearbyOffers";
import { NearbyPlaces } from "../../NearbyPlaces";
import { NotificationSettings } from "../../NotificationSettings";
import { PersonalizeYourAccount } from "../../PersonalizeYourAccount";
import { QrCodeScannerLauncher } from "../../QrCodeScannerLauncher";
import { QrCodeScannerOpen } from "../../QrCodeScannerOpen";
import { ReviewsAndRatingsHistory } from "../../ReviewsAndRatingsHistory";
import { SettingsLaunch } from "../../SettingsLaunch";
import { SignUpPage1 } from "../../SignUpPage1";
import { SignupPage2 } from "../../SignupPage2";
import { TermsAndConditions } from "../../TermsAndConditions";
import { UsersQrCode } from "../../UsersQrCode";
import { WelcomePage } from "../../WelcomePage";
import { Constants } from "expo";
import { BackHandler } from "react-native";
import { FeedbackConfirmationPopUp } from "@src/pages/FeedbackConfirmationPopUp";
import { OfferClaimPopUp } from "@src/pages/OfferClaimPopUp";
import { OfferClaimRedemptionPopUp } from "@src/pages/OfferClaimRedemptionPopUp";
import { RewardRedemptionPopUp } from "@src/pages/RewardRedemptionPopUp";
import { RewardRedemptionCashierCodePopUp } from "@src/pages/RewardRedemptionCashierCodePopUp";
import { RewardRedemptionConfirmationPopUp } from "@src/pages/RewardRedemptionConfirmationPopUp";
import { InvalidApprovalCodePopUp } from "@src/pages/InvalidApprovalCodePopUp";
import { EarnedPointsPopUp } from "@src/pages/EarnedPointsPopUp";
import { StampsPopUpRedemption1 } from "@src/pages/StampsPopUpRedemption1";
import { StampsPopUpRedemption2 } from "@src/pages/StampsPopUpRedemption2";
import { StampsPopUpRedemption3 } from "@src/pages/StampsPopUpRedemption3";
import { StampsPopUpManagerApproval } from "@src/pages/StampsPopUpManagerApproval";
import { StampsPopUpManagerApprovalPrompt } from "@src/pages/StampsPopUpManagerApprovalPrompt";
import { StampsPopUpEarnedStamps } from "@src/pages/StampsPopUpEarnedStamps";
/**
 * Component Properties
 */

export interface FunctionProps {
  navigateBack: (fromPopup?: boolean) => any;
  signIn: () => any;
}

export type StoreProps = LoadableState<{
  loggedIn?: boolean;
  globalError?: string;
  popup?: PopupState;
}>;

type ComponentProps = FunctionProps & StoreProps;

const ErrorContainer = glamorous(Text)({
  color: GETPOINTS_WHITE,
  backgroundColor: GETPOINTS_RED,
  flexGrow: 0,
  minHeight: 50,
  paddingTop: 10,
  paddingBottom: 10,
  textAlign: "center",
  display: "flex",
  alignItems: "center",
  justifyContent: "center"
});

/**
 * Component Definition
 */

export class Component extends React.Component<ComponentProps> {
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
    this.props.signIn();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    if (this.props.loading) return <Loading />;
    if (this.props.error !== undefined)
      return <ErrorMessage message={this.props.error} />;
    this.props.navigateBack(this.props.popup !== undefined);
    return true;
  };

  render() {
    if (this.props.loading) return <Loading />;
    if (this.props.error !== undefined)
      return <ErrorMessage message={this.props.error} />;
    let error;
    if (this.props.globalError !== undefined) {
      error = <ErrorContainer>{this.props.globalError}</ErrorContainer>;
    }

    return (
      <View
        style={{
          flex: 1
        }}
      >
        {error}
        {this.getRoutes()}
        {this.getPopup()}
      </View>
    );
  }

  getPopup() {
    if (this.props.loading) return <Loading />;
    if (this.props.error !== undefined)
      return <ErrorMessage message={this.props.error} />;
    if (this.props.popup === undefined) return null;

    switch (this.props.popup.type) {
      case PopupType.CONFIRM_SIGNOUT:
        return <SignOutPopUp />;

      case PopupType.FEEDBACK_CONFIRMATION:
        return <FeedbackConfirmationPopUp {...this.props.popup.data} />;

      case PopupType.INVALID_APPROVAL_CODE:
        return <InvalidApprovalCodePopUp />;

      case PopupType.EARNED_POINTS_CONFIRMATION:
        return <EarnedPointsPopUp {...this.props.popup.data} />;

      case PopupType.OFFER_CLAIM:
        return <OfferClaimPopUp {...this.props.popup.data} />;

      case PopupType.OFFER_CLAIM_CONFIRMATION:
        return <OfferClaimRedemptionPopUp {...this.props.popup.data} />;

      case PopupType.REWARD_REDEMPTION:
        return <RewardRedemptionPopUp {...this.props.popup.data} />;
      case PopupType.REWARD_REDEMPTION_MANAGER_APPROVAL_CODE:
        return <RewardRedemptionCashierCodePopUp {...this.props.popup.data} />;
      case PopupType.REWARD_REDEMPTION_CONFIRMATION:
        return <RewardRedemptionConfirmationPopUp {...this.props.popup.data} />;

      case PopupType.STAMPS_REDEMPTION:
        return <StampsPopUpRedemption1 {...this.props.popup.data} />;
      case PopupType.STAMPS_REDEMPTION_MANAGER_APPROVAL_CODE:
        return <StampsPopUpRedemption2 {...this.props.popup.data} />;
      case PopupType.STAMPS_REDEMPTION_CONFIRMATION:
        return <StampsPopUpRedemption3 {...this.props.popup.data} />;

      case PopupType.STAMPS_EARN:
        return <StampsPopUpManagerApprovalPrompt {...this.props.popup.data} />;
      case PopupType.STAMPS_EARN_MANAGER_APPROVAL_CODE:
        return <StampsPopUpManagerApproval {...this.props.popup.data} />;
      case PopupType.STAMPS_EARN_CONFIRMATION:
        return <StampsPopUpEarnedStamps {...this.props.popup.data} />;
    }
    return null;
  }

  getRoutes() {
    if (this.props.loading) return <Loading />;
    if (this.props.error !== undefined)
      return <ErrorMessage message={this.props.error} />;
    if (this.props.loggedIn) {
      return (
        <Switch>
          <Route path="/about/us" component={ConnectedAboutUsPage} />
          <Route path="/about/support" component={EmailSupport} />
          <Route path="/about/faqs" component={ConnectedFaqs} />
          <Route path="/about/how-it-works" component={HowItWorks} />
          <Route path="/about/terms" component={TermsAndConditions} />
          <Route path="/about" component={ConnectedAboutLaunchPage} />

          <Route
            path="/settings/change-password"
            component={ChangePasswordPage}
          />
          <Route path="/settings/edit-profile" component={EditYourProfile} />
          <Route
            path="/settings/notifications"
            component={NotificationSettings}
          />
          <Route
            path="/settings/edit-password"
            component={ChangePasswordPage}
          />
          <Route path="/settings" component={SettingsLaunch} />

          <Route path="/qr/scanner" component={QrCodeScannerOpen} />
          <Route path="/qr/code" component={UsersQrCode} />
          <Route path="/qr" component={QrCodeScannerLauncher} />

          <Route path="/history/visits" component={MyHistory} />
          <Route path="/history/reviews" component={ReviewsAndRatingsHistory} />

          <Route path="/my/memberships" component={MyMemberships} />
          <Route path="/my/rewards" component={MyRewards} />
          <Route path="/my/stamps" component={MyStamps} />
          <Route path="/my/offers" component={MyOffers} />

          <Route path="/nearby/offers" component={NearbyOffers} />
          <Route path="/nearby/places" component={NearbyPlaces} />

          <Route
            path="/business/:id/:mode"
            component={DetailedRestaurantView}
          />
          <Route path="/business/:id" component={DetailedRestaurantView} />

          <Route component={WelcomePage} />
        </Switch>
      );
    }

    return (
      <Switch>
        <Route path="/sign-up/1" component={SignUpPage1} />
        <Route path="/sign-up/2" component={SignupPage2} />
        <Route path="/sign-up/personalize" component={PersonalizeYourAccount} />
        <Route component={SignIn} />
      </Switch>
    );
  }
}
