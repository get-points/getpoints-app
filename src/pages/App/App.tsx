import { history, store } from "@src/store";
import { ConnectedRouter } from "connected-react-router";
import React from "react";
import { View } from "react-native";
import { Provider } from "react-redux";
import {} from "redux";
import { Menu } from "../Menu";
import { Routes } from "./Routes";
import { Route } from "react-router";
import { Constants } from "expo";

import { MenuProvider } from "react-native-popup-menu";
// import { ThemeProvider } from "glamorous-native";

export class App extends React.Component<{}> {
  render() {
    return (
      <Provider store={store}>
        <MenuProvider>
          <ConnectedRouter history={history}>
            <View style={{ flex: 1, paddingTop: Constants.statusBarHeight }}>
              <Routes />
              <Menu />
            </View>
          </ConnectedRouter>
        </MenuProvider>
      </Provider>
    );
  }
}
