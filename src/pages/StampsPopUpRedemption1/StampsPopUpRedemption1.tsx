import { DropDownMenu } from "@src/components/DropDownMenu";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { Loading } from "@src/components/Loading";
import { PopupContainer } from "@src/components/PopupContainer";
import { RectangularButton } from "@src/components/RectangularButton";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { HeaderText } from "@src/components/Typography/HeaderText";
import glamorous from "glamorous-native";
import React from "react";
import { View, ViewStyle } from "react-native";
import { GETPOINTS_RED } from "@src/const";

const ButtonsContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  justifyContent: "center"
}));

export class StampsPopUpRedemption1 extends React.Component<
  {
    style?: ViewStyle;
    stampsTotal: number;
    rewardName: string;
    stampRules: string;
    stampsPerReward: number;
    rewardNumber: number[];
    tapCancelButton: () => void;
    tapApproveButton: (x: number, y: number) => void;
    loaded?: boolean;
    error?: string;
  },
  { rewardNumberChosen: number }
> {
  state = {
    rewardNumberChosen: 1
  };
  approve = () => {
    const stampNumber =
      this.state.rewardNumberChosen * this.props.stampsPerReward;
    this.props.tapApproveButton(stampNumber, this.state.rewardNumberChosen);
  };
  setRewardNumber = (rewardNumberChosen: number) => {
    console.log("Picking", rewardNumberChosen, typeof rewardNumberChosen);
    this.setState({ rewardNumberChosen });
  };

  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <PopupContainer>
        <View style={{ flex: 0 }}>
          <HeaderText>My Total Number of Stamps</HeaderText>
          <HeaderText>{this.props.stampsTotal}</HeaderText>
          <HeaderText>
            How many "{this.props.rewardName}" do you want to redeem today ?
          </HeaderText>
          <DropDownMenu
            options={this.props.rewardNumber}
            onChange={this.setRewardNumber}
          >
            {this.state.rewardNumberChosen}
          </DropDownMenu>

          <HeaderText>Stamp Rules</HeaderText>
          <ExplainerText>{this.props.stampRules}</ExplainerText>

          <ButtonsContainer>
            <RectangularButton
              onTap={this.props.tapCancelButton}
              color={GETPOINTS_RED}
              topLeftSquare
            >
              Cancel
            </RectangularButton>
            <RectangularButton onTap={this.approve} topRightSquare>
              Approve
            </RectangularButton>
          </ButtonsContainer>
        </View>
      </PopupContainer>
    );
  }
}
