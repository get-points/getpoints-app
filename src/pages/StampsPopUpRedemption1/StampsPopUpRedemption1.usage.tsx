import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { StampsPopUpRedemption1 } from "./StampsPopUpRedemption1";

export const basic = () => (
  <StampsPopUpRedemption1
    stampsTotal={number("stampsRedeem", 1)}
    stampsPerReward={number("stampsPerReward", 1)}
    rewardName={text("rewardName", "Pizza")}
    stampRules={text("stampRules", "One redemption per user")}
    rewardNumber={[1, 2, 3]}
    tapCancelButton={action("Cancel Button")}
    tapApproveButton={action("Approve Button")}
  />
);
