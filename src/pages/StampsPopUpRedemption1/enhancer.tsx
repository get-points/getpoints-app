import { Action, actions } from "@src/actions";
import { State } from "@src/store";
import { getMembershipByID } from "@src/store/membership/selectors";
import {} from "@src/store/selectors";
import { getStampProgramByID } from "@src/store/stampProgram/selectors";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { StampsPopUpRedemption1 as Component } from "./StampsPopUpRedemption1";
import { PopupType } from "@src/actions/ui";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]> & {
    stampProgramID: string;
    stampsAvailable: number;
  }
): Partial<Component["props"]> & { loadAction?: Action } {
  try {
    // prepare data here
    const stampProgramID = props.stampProgramID;
    const loadableProgram = getStampProgramByID(store, stampProgramID);
    if (loadableProgram.state === "ABSENT")
      return {
        loaded: false,
        loadAction: actions.getStampProgram.request({ id: stampProgramID })
      };
    if (loadableProgram.state === "PENDING") return { loaded: false };
    if (loadableProgram.state === "FAILED")
      return { error: loadableProgram.message };
    const stampProgram = loadableProgram.item;

    const maxRewards = Math.floor(
      props.stampsAvailable / stampProgram.stampsRequired
    );
    const rewardNumber = [...Array(maxRewards).keys()].map(i => i + 1);

    return {
      // pass data props from here
      rewardName: stampProgram.headline,
      rewardNumber,
      stampRules: stampProgram.description,
      stampsPerReward: stampProgram.stampsRequired,
      stampsTotal: props.stampsAvailable
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]> & {
    stampProgramID: string;
    stampsAvailable: number;
  }
): Partial<Component["props"]> {
  return {
    // pass functional props from here
    tapApproveButton: (stampsRedeemed: number, rewardNumber: number) =>
      dispatch(
        actions.showPopup({
          popup: {
            type: PopupType.STAMPS_REDEMPTION_MANAGER_APPROVAL_CODE,
            data: {
              stampProgramID: props.stampProgramID,
              stampsRedeemed,
              rewardNumber
            }
          }
        })
      ),
    tapCancelButton: () => dispatch(actions.hidePopup())
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
