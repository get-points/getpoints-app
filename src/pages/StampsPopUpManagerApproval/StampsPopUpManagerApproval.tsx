import { DropDownMenu } from "@src/components/DropDownMenu";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { Loading } from "@src/components/Loading";
import { PopupContainer } from "@src/components/PopupContainer";
import { RectangularButton } from "@src/components/RectangularButton";
import { HeaderText } from "@src/components/Typography/HeaderText";
import { InputPlaceholder } from "@src/components/Typography/InputPlaceholder";
import { GETPOINTS_RED } from "@src/const";
import glamorous from "glamorous-native";
import React from "react";
import { View, ViewStyle } from "react-native";

const PinCode = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  maxWidth: `100%`
}));

const CashierCodeInput = glamorous(InputPlaceholder)((props: {}) => ({
  width: 54,
  height: 40
}));
const ManagerCode = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  minWidth: `80%`
}));
const ManagerNameInput = glamorous(InputPlaceholder)((props: {}) => ({
  height: 40
}));
const ButtonsArea = glamorous(View)((props: {}) => ({
  display: "flex",
  justifyContent: "center",
  flexDirection: "row",
  minWidth: 200
}));
const PopUpButton = glamorous(RectangularButton)((props: {}) => ({
  width: 100,
  fontSize: 12
}));

export class StampsPopUpManagerApproval extends React.Component<
  {
    style?: ViewStyle;
    productName: string;
    quantityOptions: number[];
    managerName: string;
    tapCancelButton: () => void;
    tapConfirmButton: (x: {
      digitOne: string;
      digitTwo: string;
      digitThree: string;
      digitFour: string;
      managerName: string;
      quantity: number;
    }) => any;

    loaded?: boolean;
    error?: string;
  },
  {
    quantity: number;

    digitOne: string;
    digitTwo: string;
    digitThree: string;
    digitFour: string;
    managerName: string;
  }
> {
  state = {
    quantity: 1,
    digitOne: "",
    digitTwo: "",
    digitThree: "",
    digitFour: "",
    managerName: ""
  };
  setQuantity = (quantity: number) => this.setState({ quantity });
  setDigitOne = (digitOne: string) => this.setState({ digitOne });
  setDigitTwo = (digitTwo: string) => this.setState({ digitTwo });
  setDigitThree = (digitThree: string) => this.setState({ digitThree });
  setDigitFour = (digitFour: string) => this.setState({ digitFour });
  setManagerName = (managerName: string) => this.setState({ managerName });
  approve = () => this.props.tapConfirmButton(this.state);

  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <PopupContainer>
        <HeaderText>Manager Approval Purchase and Quantity</HeaderText>
        <HeaderText> {this.props.productName} </HeaderText>
        <HeaderText>Confirm quantity</HeaderText>
        <DropDownMenu
          options={this.props.quantityOptions}
          onChange={this.setQuantity}
        >
          {this.state.quantity}
        </DropDownMenu>
        <HeaderText>Manager, Enter the 4-digit code</HeaderText>
        <PinCode>
          <CashierCodeInput
            children={this.state.digitOne}
            onChange={this.setDigitOne}
            keyboardType="number-pad"
            maxLength={1}
          />
          <CashierCodeInput
            children={this.state.digitTwo}
            onChange={this.setDigitTwo}
            keyboardType="number-pad"
            maxLength={1}
          />
          <CashierCodeInput
            children={this.state.digitThree}
            onChange={this.setDigitThree}
            keyboardType="number-pad"
            maxLength={1}
          />
          <CashierCodeInput
            children={this.state.digitFour}
            onChange={this.setDigitFour}
            keyboardType="number-pad"
            maxLength={1}
          />
        </PinCode>
        <ManagerCode>
          <ManagerNameInput
            children={this.state.managerName}
            onChange={this.setManagerName}
          />
        </ManagerCode>
        <ButtonsArea>
          <PopUpButton
            topLeftSquare
            color={GETPOINTS_RED}
            onTap={this.props.tapCancelButton}
          >
            CANCEL
          </PopUpButton>
          <PopUpButton topRightSquare onTap={this.approve}>
            CONFIRM
          </PopUpButton>
        </ButtonsArea>
      </PopupContainer>
    );
  }
}
