import { StampsPopUpManagerApproval as Component } from "./StampsPopUpManagerApproval";

import { connect } from "react-redux";
import { Dispatch, DeepPartial } from "redux";
import { State } from "@src/store";
import { actions, Action } from "@src/actions";
import {} from "@src/store/selectors";
import { getStampProgramByID } from "@src/store/stampProgram/selectors";
import { PopupType } from "@src/actions/ui";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]> & {
    stampProgramID: string;
    stampsAvailable: number;
  }
): DeepPartial<Component["props"]> & { loadAction?: Action } {
  // prepare data here
  const stampProgramID = props.stampProgramID;
  const loadableStampProgram = getStampProgramByID(store, stampProgramID);
  if (loadableStampProgram.state === "ABSENT")
    return {
      loaded: false,
      loadAction: actions.getReward.request({ id: stampProgramID })
    };
  if (loadableStampProgram.state === "PENDING") return { loaded: false };
  if (loadableStampProgram.state === "FAILED") {
    return { error: loadableStampProgram.message };
  }
  const stampProgram = loadableStampProgram.item;

  return {
    productName: stampProgram.headline,
    quantityOptions: [...Array(10).keys()].map(i => i + 1)
  };
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]> & {
    stampProgramID: string;
    stampsAvailable: number;
  }
): DeepPartial<Component["props"]> {
  return {
    // pass functional props from here
    tapCancelButton: () => dispatch(actions.hidePopup()),
    tapConfirmButton: (result: { quantity: number }) =>
      dispatch(
        actions.showPopup({
          popup: {
            type: PopupType.STAMPS_EARN_CONFIRMATION,
            data: {
              stampProgramID: props.stampProgramID,
              stampsAvailable: props.stampsAvailable,
              rewardNumber: result.quantity
            }
          }
        })
      )
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
