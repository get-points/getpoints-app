import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { StampsPopUpManagerApproval } from "./index";

export const basic = () => (
  <StampsPopUpManagerApproval
    productName={text("productName", "Pizza")}
    quantityOptions={["1", "2", "3"]}
    managerName={text("managerName", "Joey")}
    tapCancelButton={action("Tap Cancel")}
    tapConfirmButton={action("Tap Confirm")}
  />
);
