import React from "react";
import { RouteComponentProps } from "react-router";
import { SearchPage } from "../SearchPage";
import { Action } from "@src/actions";

export class NearbyOffers extends React.Component<
  {
    listings: React.ReactChild;
    triggerSearch: SearchPage["props"]["triggerSearch"];
    mapPins: SearchPage["props"]["mapPins"];
    buttons: SearchPage["props"]["buttons"];
    loadContent?: (x?: Action) => any;
    loaded?: boolean;
    error?: string;
    membershipCount: number;
  } & RouteComponentProps
> {
  componentDidMount() {
    this.update(this.props);
  }
  componentWillReceiveProps(nextProps: NearbyOffers["props"]) {
    this.update(nextProps);
  }
  update = (props: NearbyOffers["props"]) =>
    props.loadContent && props.loadContent();

  render() {
    return (
      <SearchPage
        baseURL="/nearby/offers"
        pageTitle={"Nearby Offers"}
        {...this.props}
      />
    );
  }
}
