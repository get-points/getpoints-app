import { UsersQrCode as Component } from "./UsersQrCode";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State } from "@src/store";
import { actions } from "@src/actions";
import {} from "@src/store/selectors";
import { getOnlyUser } from "@src/store/user/selectors";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  try {
    // prepare data here
    const loadableUser = getOnlyUser(store);
    if (loadableUser.state === "FAILED") return { error: loadableUser.message };
    if (loadableUser.state === "PENDING" || loadableUser.state === "ABSENT")
      return { loaded: false };

    const user = loadableUser.item;

    return {
      // pass data props from here

      qrImage: user.qrCode
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  return {
    // pass functional props from here
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
