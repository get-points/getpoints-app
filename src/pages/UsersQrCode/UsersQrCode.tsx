import { HeaderBar } from "@src/components/HeaderBar";
import { PageHeader } from "@src/components/PageHeader";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { GETPOINTS_BACKGROUND_GREY, GETPOINTS_GREY } from "@src/const";
import React from "react";
import { Image, ScrollView, View } from "react-native";
import glamorous from "glamorous-native";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";

const UsersQrCodeContainer = glamorous(View)((props: {}) => ({
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  flex: 1
}));

const Page = glamorous(View)((props: {}) => ({
  padding: 20,
  alignItems: "center"
}));

const QRCode = glamorous(Image)((props: {}) => ({
  width: 250,
  height: 250,
  margin: 10,
  marginTop: 20
}));

export class UsersQrCode extends React.Component<{
  qrImage: string;

  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }

    return (
      <UsersQrCodeContainer>
        <HeaderBar />
        <PageHeader>User's QR Code</PageHeader>
        <Page>
          <ExplainerText>
            Hold the QR code below, in front of the tablet’s scanner and wait
            until it registers the image.
          </ExplainerText>
          <QRCode source={{ uri: this.props.qrImage }} />
        </Page>
      </UsersQrCodeContainer>
    );
  }
}
