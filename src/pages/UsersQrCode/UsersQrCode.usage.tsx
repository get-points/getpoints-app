import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { UsersQrCode } from "./index";

export const basic = () => (
  <UsersQrCode
    qrImage={text("qrImage", "www.getpoints.com/images/image006.jpg")}
  />
);
