import { OfferClaimPopUp as Component } from "./OfferClaimPopUp";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State } from "@src/store";
import { actions } from "@src/actions";
import {} from "@src/store/selectors";
import { getOfferByID } from "@src/store/offer/selectors";
import { PopupType } from "@src/actions/ui";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]> & { offerID: string }
): Partial<Component["props"]> {
  try {
    // prepare data here
    const loadableOffer = getOfferByID(store, props.offerID);
    if (loadableOffer.state === "ABSENT") return { loaded: false };
    if (loadableOffer.state === "PENDING") return { loaded: false };
    if (loadableOffer.state === "FAILED")
      return { error: loadableOffer.message };
    const offer = loadableOffer.item;

    return {
      // pass data props from here
      offerHeadline: offer.headline,
      offerDescription: offer.description,
      offerAvailability: `Available for the first ${
        offer.redemptionLimit
      } people only.`,
      offerExpiry: offer.expiry
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]> & { offerID: string }
): Partial<Component["props"]> {
  return {
    // pass functional props from here
    tapGoBackButton: () => dispatch(actions.hidePopup()),
    tapClaimOfferButton: () =>
      dispatch(
        actions.showPopup({
          popup: {
            type: PopupType.OFFER_CLAIM_CONFIRMATION,
            data: { offerID: props.offerID }
          }
        })
      )
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
