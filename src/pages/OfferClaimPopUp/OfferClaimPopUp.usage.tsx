import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { OfferClaimPopUp } from "./index";

export const basic = () => (
  <OfferClaimPopUp
    businessName={text("businessName", "The Pantry")}
    isVisible={boolean("isVisible", true)}
    offerAvailability={text(
      "offerAvailability",
      "available for the first 4 people"
    )}
    offerDescription={text("offerDescription", "free coffee")}
    offerExpiry={text("offerExpiry", "10/10/2018")}
    offerHeadline={text("offerHeadline", "Great Free Coffee")}
    tapClaimOfferButton={action("tapButton")}
    tapGoBackButton={action("tapButton")}
  />
);
