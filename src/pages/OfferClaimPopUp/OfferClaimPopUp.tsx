import {
  GETPOINTS_DARK_TEAL,
  GETPOINTS_GREEN,
  GETPOINTS_RED,
  GETPOINTS_WHITE
} from "@src/const";
import React from "react";
import { View, ScrollView } from "react-native";
import glamorous from "glamorous-native";

import { PageHeader } from "../../components/PageHeader";
import { RectangularButton } from "../../components/RectangularButton";
import { ExplainerText } from "../../components/Typography/ExplainerText";
import { HeaderText } from "../../components/Typography/HeaderText";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { PopupContainer } from "@src/components/PopupContainer";

const OfferClaimPopUpContainer = glamorous(View)((props: {}) => ({
  backgroundColor: GETPOINTS_WHITE
}));

const OfferHeadlineText = glamorous(HeaderText)((props: {}) => ({
  color: GETPOINTS_GREEN,
  fontSize: 25
}));

const PageBody = glamorous(ScrollView)((props: {}) => ({
  padding: 10
}));

const ButtonContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flex: 0,
  justifyContent: "center"
}));

const PopUpButton = glamorous(RectangularButton)((props: {}) => ({
  fontSize: 12,
  flexGrow: 0,
  width: 100
}));

const OfferAvailability = glamorous(ExplainerText)((props: {}) => ({
  color: GETPOINTS_DARK_TEAL,
  fontSize: 15
}));

const OfferExpiryDate = glamorous(ExplainerText)((props: {}) => ({
  fontSize: 15
}));
export class OfferClaimPopUp extends React.Component<{
  offerHeadline: string;
  offerDescription: string;
  offerAvailability: string;
  offerExpiry: string;
  tapGoBackButton: () => any;
  tapClaimOfferButton: () => any;
  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }

    return (
      <PopupContainer>
        <OfferHeadlineText>{this.props.offerHeadline}</OfferHeadlineText>
        <HeaderText>{this.props.offerDescription}</HeaderText>
        <OfferAvailability>{this.props.offerAvailability}</OfferAvailability>
        <OfferExpiryDate>Expires {this.props.offerExpiry}</OfferExpiryDate>
        <ButtonContainer>
          <PopUpButton
            color={GETPOINTS_RED}
            topLeftSquare
            onTap={this.props.tapGoBackButton}
          >
            GO BACK
          </PopUpButton>
          <PopUpButton topRightSquare onTap={this.props.tapClaimOfferButton}>
            CLAIM OFFER
          </PopUpButton>
        </ButtonContainer>
      </PopupContainer>
    );
  }
}
