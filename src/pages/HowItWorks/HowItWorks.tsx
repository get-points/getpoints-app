import { BusinessHeading } from "@src/components/BusinessHeading";
import { HeaderBar } from "@src/components/HeaderBar";
import { Icon } from "@src/components/Icon";
import { InPageMenuBarGreen } from "@src/components/InPageMenuBarGreen";
import { PageHeader } from "@src/components/PageHeader";
import {
  FixedWidthButton,
  RectangularButton
} from "@src/components/RectangularButton";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { GETPOINTS_BACKGROUND_GREY, GETPOINTS_DARK_GREY } from "@src/const";
import React from "react";
import { View, ScrollView } from "react-native";
import glamorous from "glamorous-native";
import { InPageMenuBar } from "@src/components/InPageMenuBar";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";

const HowItWorksContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flex: 1
}));
const Page = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1,
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  padding: 10,
  alignItems: "center"
}));
const ImageWithText = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 1,
  alignItems: "center",
  width: `100%`
}));
const FullText = glamorous(ExplainerText)((props: {}) => ({
  flex: 1,
  textAlign: "left" as "left"
}));
const FullTextRight = glamorous(FullText)((props: {}) => ({
  textAlign: "right" as "right"
}));
const IconWithMargin = glamorous(Icon)((props: {}) => ({
  marginTop: 0,
  marginBottom: 0,
  marginLeft: 20,
  marginRight: 20,
  flex: 0
}));

export class HowItWorks extends React.Component<{
  tapTourButton: () => any;
  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <HowItWorksContainer>
        <HeaderBar />
        <PageHeader>How it Works</PageHeader>
        <ScrollView>
          <InPageMenuBar />
          <Page>
            <ImageWithText>
              <IconWithMargin
                name="ios-download"
                size={100}
                color={GETPOINTS_DARK_GREY}
              />
              <FullText>
                Download the getpoints app and signup to join your favourite and
                nearby places or businesses.
              </FullText>
            </ImageWithText>
            <ImageWithText>
              <FullTextRight>
                Join businesses of your choice, check-in when you visit and get
                loyalty points.
              </FullTextRight>
              <IconWithMargin
                name="ios-phone-portrait"
                size={100}
                color={GETPOINTS_DARK_GREY}
              />
            </ImageWithText>
            <ImageWithText>
              <IconWithMargin
                name="ios-gift"
                size={100}
                color={GETPOINTS_DARK_GREY}
              />
              <FullText>
                When you have enough points you can redeem the rewards on a
                GetPoints tablet or via our app.
              </FullText>
            </ImageWithText>
            <FixedWidthButton topRightSquare onTap={this.props.tapTourButton}>
              TAKE A TOUR
            </FixedWidthButton>
          </Page>
        </ScrollView>
      </HowItWorksContainer>
    );
  }
}
