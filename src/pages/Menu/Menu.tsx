import React from "react";
import {
  Text,
  View,
  ViewStyle,
  TouchableWithoutFeedback,
  TouchableOpacity
} from "react-native";
import glamorous from "glamorous-native";
import { Placeholder } from "@src/components/Placeholder";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { MenuText } from "@src/components/Typography/MenuText";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { Icon } from "@src/components/Icon";
import { GETPOINTS_WHITE, GETPOINTS_TEAL } from "@src/const";
import { PageHeader } from "@src/components/PageHeader";
import { HeaderBar } from "@src/components/HeaderBar";
import { Constants } from "expo";

const MenuCloak = glamorous(View)((props: {}) => ({
  position: "absolute",
  left: 0,
  top: Constants.statusBarHeight,
  right: 0,
  bottom: 0,
  backgroundColor: "rgba(0,0,0,0.5)",
  display: "flex",
  flexDirection: "column",
  alignItems: "stretch"
}));
const MenuContainer = glamorous(View)({
  width: "80%",
  flexGrow: 0,
  backgroundColor: "white"
});
const CancelZone = glamorous(View)({
  flexGrow: 1
});
const MenuRow = glamorous(View)({
  flexDirection: "row",
  alignItems: "center",
  flexShrink: 1,
  flexGrow: 1,
  backgroundColor: GETPOINTS_WHITE,
  borderColor: GETPOINTS_TEAL,

  borderTopWidth: 1
});
const MenuHeader = glamorous(View)({
  flexShrink: 0
});
const ClickZone = glamorous(TouchableOpacity)({
  flexGrow: 1
});
const Page = glamorous(View)({
  flexDirection: "row",
  flexGrow: 1
});
const MenuIcon = glamorous(Icon)({
  marginLeft: 20,
  minWidth: 20
});

export class Menu extends React.Component<{
  style?: ViewStyle;
  profileImage: string;
  email: string;
  closeMenu: () => any;
  isVisible: boolean;
  loaded?: false;
  error?: string;
  handlers: {
    nearbyPlaces: () => any;
    myMemberships: () => any;
    myRewards: () => any;
    nearbyOffers: () => any;
    reviews: () => any;
    myHistory: () => any;
    qrScanner: () => any;
    myQRCode: () => any;
    about: () => any;
    settings: () => any;
    signOut: () => any;
  };
}> {
  render() {
    if (this.props.loaded === false) return <Loading />;
    if (this.props.error !== undefined)
      return <ErrorMessage message={this.props.error} />;
    if (this.props.isVisible !== true) return null;

    return (
      <MenuCloak style={this.props.style}>
        <HeaderBar />
        <Page>
          <MenuContainer>
            <ClickZone onPress={this.props.handlers.myMemberships}>
              <MenuRow>
                <MenuIcon size={20} name="ios-card" color={GETPOINTS_TEAL} />
                <MenuText>My Memberships</MenuText>
              </MenuRow>
            </ClickZone>

            <ClickZone onPress={this.props.handlers.myRewards}>
              <MenuRow>
                <MenuIcon size={20} name="md-ribbon" color={GETPOINTS_TEAL} />
                <MenuText>My Rewards</MenuText>
              </MenuRow>
            </ClickZone>

            <ClickZone onPress={this.props.handlers.nearbyPlaces}>
              <MenuRow>
                <MenuIcon size={20} name="ios-pin" color={GETPOINTS_TEAL} />
                <MenuText>Nearby Places</MenuText>
              </MenuRow>
            </ClickZone>

            <ClickZone onPress={this.props.handlers.nearbyOffers}>
              <MenuRow>
                <MenuIcon size={20} name="ios-pin" color={GETPOINTS_TEAL} />
                <MenuText>Nearby Offers</MenuText>
              </MenuRow>
            </ClickZone>

            <ClickZone onPress={this.props.handlers.qrScanner}>
              <MenuRow>
                <MenuIcon
                  size={20}
                  name="ios-qr-scanner"
                  color={GETPOINTS_TEAL}
                />
                <MenuText>QR Code Scanner</MenuText>
              </MenuRow>
            </ClickZone>

            <ClickZone onPress={this.props.handlers.myQRCode}>
              <MenuRow>
                <MenuIcon
                  size={20}
                  name="ios-qr-scanner"
                  color={GETPOINTS_TEAL}
                />
                <MenuText>My QR Code</MenuText>
              </MenuRow>
            </ClickZone>

            <ClickZone onPress={this.props.handlers.myHistory}>
              <MenuRow>
                <MenuIcon
                  size={20}
                  name="ios-clipboard"
                  color={GETPOINTS_TEAL}
                />
                <MenuText>My History</MenuText>
              </MenuRow>
            </ClickZone>

            <ClickZone onPress={this.props.handlers.about}>
              <MenuRow>
                <MenuIcon
                  size={20}
                  name="ios-chatbubbles"
                  color={GETPOINTS_TEAL}
                />
                <MenuText>Support</MenuText>
              </MenuRow>
            </ClickZone>

            <ClickZone onPress={this.props.handlers.settings}>
              <MenuRow>
                <MenuIcon
                  size={20}
                  name="ios-settings"
                  color={GETPOINTS_TEAL}
                />
                <MenuText>Settings</MenuText>
              </MenuRow>
            </ClickZone>

            <ClickZone onPress={this.props.handlers.signOut}>
              <MenuRow>
                <MenuIcon size={20} name="ios-exit" color={GETPOINTS_TEAL} />
                <MenuText>Signout</MenuText>
              </MenuRow>
            </ClickZone>
          </MenuContainer>

          <TouchableWithoutFeedback onPress={this.props.closeMenu}>
            <CancelZone />
          </TouchableWithoutFeedback>
        </Page>
      </MenuCloak>
    );
  }
}
