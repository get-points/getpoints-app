import { Menu as Component } from "./Menu";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State } from "@src/store";
import { actions } from "@src/actions";
import {} from "@src/store/selectors";
import { push } from "connected-react-router";
import { withRouter } from "react-router";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  try {
    // prepare data here

    return {
      isVisible: store.ui.menuOpen,
      email: "",
      profileImage: ""
      // pass data props from here
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  const closeMenu = () => dispatch(actions.closeMenu());
  return {
    // pass functional props from here
    closeMenu,
    handlers: {
      nearbyPlaces: () => closeMenu() && dispatch(push("/nearby/places")),
      nearbyOffers: () => closeMenu() && dispatch(push("/nearby/offers")),
      myMemberships: () => closeMenu() && dispatch(push("/my/memberships")),
      myRewards: () => closeMenu() && dispatch(push("/my/rewards")),
      reviews: () => closeMenu() && dispatch(push("/history/reviews")),
      myHistory: () => closeMenu() && dispatch(push("/history/visits")),
      qrScanner: () => closeMenu() && dispatch(push("/qr")),
      myQRCode: () => closeMenu() && dispatch(push("/qr/code")),
      about: () => closeMenu() && dispatch(push("/about")),
      settings: () => closeMenu() && dispatch(push("/settings")),
      signOut: () => closeMenu() && dispatch(actions.signOut.request({}))
    }
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
