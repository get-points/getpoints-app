import React from "react";
import { Text, View, ViewStyle } from "react-native";
import glamorous from "glamorous-native";
import { PageHeader } from "@src/components/PageHeader";
import { HeaderText } from "@src/components/Typography/HeaderText";
import {
  RectangularButton,
  FixedWidthButton
} from "@src/components/RectangularButton";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { GETPOINTS_BACKGROUND_GREY, GETPOINTS_RED } from "@src/const";
import { HeaderBar } from "@src/components/HeaderBar";

const StampsPopUpCheckInContainer = glamorous(View)((props: {}) => ({}));

const Page = glamorous(View)((props: {}) => ({
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  padding: 20
}));
const ButtonContainer = glamorous(View)((props: {}) => ({
  flexDirection: "row",
  justifyContent: "center"
}));
export class StampsPopUpCheckIn extends React.Component<{
  style?: ViewStyle;
  tapCancelButton: () => void;
  tapCheckInButton: () => void;
  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <StampsPopUpCheckInContainer style={this.props.style}>
        <HeaderBar hideMenu />
        <Page>
          <HeaderText>You need to check in first to get stamped</HeaderText>
          <ButtonContainer>
            <FixedWidthButton
              onTap={this.props.tapCancelButton}
              color={GETPOINTS_RED}
              topLeftSquare
            >
              Cancel
            </FixedWidthButton>
            <FixedWidthButton
              onTap={this.props.tapCheckInButton}
              topRightSquare
            >
              Check-in
            </FixedWidthButton>
          </ButtonContainer>
        </Page>
      </StampsPopUpCheckInContainer>
    );
  }
}
