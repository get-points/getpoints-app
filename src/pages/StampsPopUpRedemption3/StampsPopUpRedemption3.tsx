import { ErrorMessage } from "@src/components/ErrorMessage";
import { Loading } from "@src/components/Loading";
import { PopupContainer } from "@src/components/PopupContainer";
import { FixedWidthButton } from "@src/components/RectangularButton";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { HeaderText } from "@src/components/Typography/HeaderText";
import { GETPOINTS_BACKGROUND_GREY } from "@src/const";
import glamorous from "glamorous-native";
import React from "react";
import { View, ViewStyle } from "react-native";

export class StampsPopUpRedemption3 extends React.Component<{
  style?: ViewStyle;
  rewardNumber: number;
  rewardName: string;
  stampsRedeeemed: number;
  tapDoneButton: () => void;

  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <PopupContainer>
        <HeaderText>
          Enjoy your {this.props.rewardNumber} {this.props.rewardName}
          {this.props.rewardNumber === 1 ? "" : "s"}
        </HeaderText>
        <ExplainerText>
          {" "}
          You have redeemed {this.props.stampsRedeeemed} Stamps{" "}
        </ExplainerText>
        <FixedWidthButton onTap={this.props.tapDoneButton} topRightSquare>
          DONE
        </FixedWidthButton>
      </PopupContainer>
    );
  }
}
