import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { StampsPopUpRedemption3 } from "./StampsPopUpRedemption3";

export const basic = () => (
  <StampsPopUpRedemption3
    rewardName={text("rewardName", "free coffee")}
    rewardNumber={number("pointsReward", 5)}
    stampsRedeeemed={number("stampsRedeemed", 2)}
    tapDoneButton={action("tapButton")}
  />
);
