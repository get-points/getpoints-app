import { HeaderBar } from "@src/components/HeaderBar";
import { Icon } from "@src/components/Icon";
import { FixedWidthButton } from "@src/components/RectangularButton";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import {
  HeaderText,
  LeftAlignedHeaderText
} from "@src/components/Typography/HeaderText";
import { GETPOINTS_BACKGROUND_GREY, GETPOINTS_GREY } from "@src/const";
import React from "react";
import { ScrollView, Text, View, ViewStyle } from "react-native";
import glamorous from "glamorous-native";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";

const NotificationSettingsContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flex: 1
}));
const Page = glamorous(ScrollView)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flex: 1,
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  padding: 20
}));
const Row = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 1,
  alignItems: "center",
  marginBottom: 10
}));
const IconsGroup = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flex: 1,
  alignItems: "center"
}));
const IconsWithLabel = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flex: 1,
  alignItems: "center",
  justifyContent: "flex-start"
}));
const PaddingExplainerText = glamorous(ExplainerText)((props: {}) => ({
  textAlign: "left",
  paddingLeft: 5,
  flexShrink: 1
}));
const Label = glamorous(ExplainerText)((props: {}) => ({
  width: 130,
  flexGrow: 0,
  flexShrink: 0,
  textAlign: "left"
}));

export class NotificationSettings extends React.Component<
  {
    style?: ViewStyle;
    tapSaveButton: () => any;

    loaded?: boolean;
    error?: string;
    notifications: {
      emailOnEarnedNewPoints: boolean;
      emailOnPurchase: boolean;
      smsOnEarnedNewPoints: boolean;
      smsOnPurchase: boolean;
    };
  },
  {
    emailOnEarnedNewPoints: boolean;
    emailOnPurchase: boolean;
    smsOnEarnedNewPoints: boolean;
    smsOnPurchase: boolean;
  }
> {
  state = this.props.notifications;
  getButtons(selected: boolean, onChange: (x: boolean) => any) {
    return (
      <IconsGroup>
        <IconsWithLabel>
          <Icon
            name={selected ? "ios-radio-button-on" : "ios-radio-button-off"}
            onPress={() => onChange(true)}
            size={20}
            color={GETPOINTS_GREY}
          />
          <PaddingExplainerText>Yes</PaddingExplainerText>
        </IconsWithLabel>
        <IconsWithLabel>
          <Icon
            name={selected ? "ios-radio-button-off" : "ios-radio-button-on"}
            size={20}
            onPress={() => onChange(false)}
            color={GETPOINTS_GREY}
          />
          <PaddingExplainerText>No</PaddingExplainerText>
        </IconsWithLabel>
      </IconsGroup>
    );
  }

  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }

    return (
      <NotificationSettingsContainer style={this.props.style}>
        <HeaderBar />
        <Page>
          <HeaderText>Notification Settings</HeaderText>
          <ExplainerText>
            Edit your notification settings below to determine how and when you
            recieve notifications from your merchants.
          </ExplainerText>
          <LeftAlignedHeaderText>Email me when:</LeftAlignedHeaderText>
          <Row>
            <Label>I earn new points:</Label>
            {this.getButtons(
              this.state.emailOnEarnedNewPoints,
              emailOnEarnedNewPoints =>
                this.setState({ emailOnEarnedNewPoints })
            )}
          </Row>
          <Row>
            <Label>I make a valid purchase:</Label>
            {this.getButtons(this.state.emailOnPurchase, emailOnPurchase =>
              this.setState({ emailOnPurchase })
            )}
          </Row>
          <LeftAlignedHeaderText>Text me when:</LeftAlignedHeaderText>
          <Row>
            <Label>I earn new points:</Label>
            {this.getButtons(
              this.state.smsOnEarnedNewPoints,
              smsOnEarnedNewPoints => this.setState({ smsOnEarnedNewPoints })
            )}
          </Row>
          <Row>
            <Label>I make a valid purchase:</Label>
            {this.getButtons(this.state.smsOnPurchase, smsOnPurchase =>
              this.setState({ smsOnPurchase })
            )}
          </Row>
          <FixedWidthButton topRightSquare onTap={this.props.tapSaveButton}>
            Save
          </FixedWidthButton>
        </Page>
      </NotificationSettingsContainer>
    );
  }
}
