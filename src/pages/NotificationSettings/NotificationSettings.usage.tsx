import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { NotificationSettings } from "./index";

export const basic = () => (
  <NotificationSettings
    emailMeValue={{
      earnedNewPoints: boolean("earnedNewPoints", true),
      madeValidPurchase: boolean("madeValidPurchase", true)
    }}
    textMeValue={{
      earnedNewPoints: boolean("earnedNewPoints", true),
      madeValidPurchase: boolean("madeValidPurchase", true)
    }}
    tapSaveButton={action("tappedButton")}
  />
);
