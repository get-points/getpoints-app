import { HeaderBar } from "@src/components/HeaderBar";
import { Icon } from "@src/components/Icon";
import { PageHeader } from "@src/components/PageHeader";
import { FixedWidthButton } from "@src/components/RectangularButton";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { GreenHeaderText } from "@src/components/Typography/HeaderText";
import { InputPlaceholder } from "@src/components/Typography/InputPlaceholder";
import {
  GETPOINTS_BACKGROUND_GREY,
  GETPOINTS_PLACEHOLDER_GREY,
  GETPOINTS_YELLOW
} from "@src/const";
import React from "react";
import { View, ScrollView } from "react-native";
import glamorous from "glamorous-native";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";

const FeedbackCollectionContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flex: 1
}));
const Page = glamorous(ScrollView)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1,
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  padding: 10
}));
const FormGroup = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 1
}));
const SmileysGroup = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 0,
  justifyContent: "center"
}));
const PaddingToSmileyIcons = glamorous(View)((props: {}) => ({
  padding: 20
}));

export class FeedbackCollection extends React.Component<{
  businessName: string;
  emotion: "HAPPY" | "SAD";
  name: string;
  email: string;
  message: string;
  tapSendButton: () => any;

  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }

    return (
      <FeedbackCollectionContainer>
        <HeaderBar />
        <PageHeader>Feedback</PageHeader>
        <Page>
          <GreenHeaderText>How was your visit?</GreenHeaderText>
          <ExplainerText>Let {this.props.businessName} Know.</ExplainerText>
          <SmileysGroup>
            <PaddingToSmileyIcons>
              <Icon name="ios-happy" size={100} color={GETPOINTS_YELLOW} />
            </PaddingToSmileyIcons>
            <PaddingToSmileyIcons>
              <Icon
                name="ios-sad"
                size={100}
                color={GETPOINTS_PLACEHOLDER_GREY}
              />
            </PaddingToSmileyIcons>
          </SmileysGroup>
          <FormGroup>
            <InputPlaceholder>{this.props.name}</InputPlaceholder>
          </FormGroup>
          <FormGroup>
            <InputPlaceholder>{this.props.email}</InputPlaceholder>
          </FormGroup>
          <FormGroup>
            <InputPlaceholder numberOfLines={5}>
              {this.props.message}
            </InputPlaceholder>
          </FormGroup>
          <FixedWidthButton topRightSquare onTap={this.props.tapSendButton}>
            Send
          </FixedWidthButton>
        </Page>
      </FeedbackCollectionContainer>
    );
  }
}
