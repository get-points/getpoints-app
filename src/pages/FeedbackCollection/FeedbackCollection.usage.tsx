import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { FeedbackCollection } from "./index";

export const basic = () => (
  <FeedbackCollection
    businessName={text("businessName", "The Pantry")}
    email={text("email", "joeblogs@gmail.com")}
    emotion={"HAPPY"}
    message={text("message", "great meal!")}
    name={text("name", "Joe Blogs")}
    tapSendButton={action("buttonTapped")}
  />
);
