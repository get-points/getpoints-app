import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { RewardRedemptionCashierCodePopUp } from "./index";

export const basic = () => (
  <RewardRedemptionCashierCodePopUp
    cashierCodeInput={{
      digitOne: text("digitOne", "1"),
      digitTwo: text("digitTwo", "2"),
      digitThree: text("digitThree", "3"),
      digitFour: text("digitFour", "4")
    }}
    customerFirstName={text("customerFirstName", "Joe")}
    managerName={text("managerName", "Fred")}
    rewardName={text("rewardName", "Free Coffee")}
    rewardValue={number("rewardValue", 5)}
    tapApproveButton={action("tapButton")}
    tapCancelButton={action("tapButton")}
  />
);
