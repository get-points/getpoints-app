import { ErrorMessage } from "@src/components/ErrorMessage";
import { Loading } from "@src/components/Loading";
import { PopupContainer } from "@src/components/PopupContainer";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { GETPOINTS_GREEN, GETPOINTS_RED } from "@src/const";
import glamorous from "glamorous-native";
import React from "react";
import { View } from "react-native";
import { RectangularButton } from "../../components/RectangularButton";
import { HeaderText } from "../../components/Typography/HeaderText";
import { InputPlaceholder } from "../../components/Typography/InputPlaceholder";

const PinCode = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  maxWidth: `100%`
}));

const CashierCodeInput = glamorous(InputPlaceholder)((props: {}) => ({
  width: 54,
  height: 40
}));

const ManagerCode = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  minWidth: `80%`
}));
const ButtonsArea = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  minWidth: 200,
  justifyContent: "center"
}));
const PopUpButton = glamorous(RectangularButton)((props: {}) => ({
  width: 100,
  fontSize: 12
}));

const ManagerNameInput = glamorous(InputPlaceholder)((props: {}) => ({
  height: 40
}));

const EnterCodeText = glamorous(ExplainerText)((props: {}) => ({
  fontSize: 16
}));
const RewardNameText = glamorous(HeaderText)((props: {}) => ({
  fontSize: 25,
  color: GETPOINTS_GREEN
}));

const RewardRedemptionText = glamorous(HeaderText)((props: {}) => ({
  fontSize: 20
}));
export class RewardRedemptionCashierCodePopUp extends React.Component<
  {
    customerFirstName: string;
    rewardValue: number;
    rewardName: string;
    tapCancelButton: () => any;
    tapApproveButton: (x: {
      digitOne: string;
      digitTwo: string;
      digitThree: string;
      digitFour: string;
      managerName: string;
    }) => any;

    loaded?: boolean;
    error?: string;
  },
  {
    digitOne: string;
    digitTwo: string;
    digitThree: string;
    digitFour: string;
    managerName: string;
  }
> {
  state = {
    digitOne: "",
    digitTwo: "",
    digitThree: "",
    digitFour: "",
    managerName: ""
  };

  setManagerName = (managerName: string) => this.setState({ managerName });
  setDigitOne = (digitOne: string) => this.setState({ digitOne });
  setDigitTwo = (digitTwo: string) => this.setState({ digitTwo });
  setDigitThree = (digitThree: string) => this.setState({ digitThree });
  setDigitFour = (digitFour: string) => this.setState({ digitFour });
  approve = () => this.props.tapApproveButton(this.state);

  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <PopupContainer>
        <RewardRedemptionText>
          {this.props.customerFirstName} has redeemed {this.props.rewardValue}{" "}
          points.
        </RewardRedemptionText>
        <RewardNameText>For {this.props.rewardName}</RewardNameText>
        <EnterCodeText>
          Manager, please enter your 4 digit code to approve the reward
        </EnterCodeText>
        <PinCode>
          <CashierCodeInput
            children={this.state.digitOne}
            onChange={this.setDigitOne}
            keyboardType="number-pad"
            maxLength={1}
          />
          <CashierCodeInput
            children={this.state.digitTwo}
            onChange={this.setDigitTwo}
            keyboardType="number-pad"
            maxLength={1}
          />
          <CashierCodeInput
            children={this.state.digitThree}
            onChange={this.setDigitThree}
            keyboardType="number-pad"
            maxLength={1}
          />
          <CashierCodeInput
            children={this.state.digitFour}
            onChange={this.setDigitFour}
            keyboardType="number-pad"
            maxLength={1}
          />
        </PinCode>
        <ManagerCode>
          <ManagerNameInput
            children={this.state.managerName}
            onChange={this.setManagerName}
          />
        </ManagerCode>
        <ButtonsArea>
          <PopUpButton
            topLeftSquare
            color={GETPOINTS_RED}
            onTap={this.props.tapCancelButton}
          >
            CANCEL
          </PopUpButton>
          <PopUpButton topRightSquare onTap={this.approve}>
            APPROVE
          </PopUpButton>
        </ButtonsArea>
      </PopupContainer>
    );
  }
}
