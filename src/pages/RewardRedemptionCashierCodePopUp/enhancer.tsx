import { RewardRedemptionCashierCodePopUp as Component } from "./RewardRedemptionCashierCodePopUp";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State } from "@src/store";
import { actions, Action } from "@src/actions";
import {} from "@src/store/selectors";
import { PopupType } from "@src/actions/ui";
import { getOnlyUser } from "@src/store/user/selectors";
import { getRewardByID } from "@src/store/reward/selectors";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]> & { rewardID: string }
): Partial<Component["props"]> & { loadAction?: Action } {
  try {
    // prepare data here
    const rewardID = props.rewardID;
    const loadableReward = getRewardByID(store, rewardID);
    if (loadableReward.state === "ABSENT")
      return {
        loaded: false,
        loadAction: actions.getReward.request({ id: rewardID })
      };
    if (loadableReward.state === "PENDING") return { loaded: false };
    if (loadableReward.state === "FAILED")
      return { error: loadableReward.message };
    const reward = loadableReward.item;

    const loadableUser = getOnlyUser(store);
    if (loadableUser.state === "ABSENT")
      return {
        loaded: false
      };
    if (loadableUser.state === "PENDING") return { loaded: false };
    if (loadableUser.state === "FAILED") return { error: loadableUser.message };
    const user = loadableUser.item;

    return {
      // pass data props from here
      customerFirstName: user.firstName,
      rewardName: reward.headline
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]> & { rewardID: string }
): Partial<Component["props"]> {
  return {
    // pass functional props from here
    tapApproveButton: () => dispatch(actions.hidePopup()),
    tapCancelButton: () => dispatch(actions.hidePopup())
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
