import React from "react";
import { RouteComponentProps } from "react-router";
import { SearchPage } from "../SearchPage";
import { Action } from "@src/actions";

export class MyOffers extends React.Component<
  {
    listings: React.ReactChild;
    triggerSearch: SearchPage["props"]["triggerSearch"];
    mapPins: SearchPage["props"]["mapPins"];
    buttons: SearchPage["props"]["buttons"];
    loadContent?: (x?: Action) => any;
    loaded?: boolean;
    error?: string;
    membershipCount: number;
  } & RouteComponentProps
> {
  componentDidMount() {
    this.update(this.props);
  }
  componentWillReceiveProps(nextProps: MyOffers["props"]) {
    this.update(nextProps);
  }
  update = (props: MyOffers["props"]) =>
    props.loadContent && props.loadContent();

  render() {
    return (
      <SearchPage
        selectedButton={2}
        baseURL="/my/offers"
        pageTitle={"My Offers"}
        {...this.props}
      />
    );
  }
}
