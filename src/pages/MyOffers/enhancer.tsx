import { actions, Action } from "@src/actions";
import { State } from "@src/store";
import { getBusinessListByIDs } from "@src/store/business/selectors";
import { getNearbyBusinesses } from "@src/store/search/nearbyBusinesses/selectors";
import { listToMap } from "@src/store/selectors";
import { push } from "connected-react-router";
import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { MyOffers as Component } from "./MyOffers";
import { BusinessListingHeader } from "@src/components/BusinessListingHeader";
import { getOfferListByIDs } from "@src/store/offer/selectors";
import { OfferListing } from "@src/components/OfferListing";
import { Icon } from "@src/components/Icon";
import { getOnlyUser } from "@src/store/user/selectors";
import { getMembershipListByIDs } from "@src/store/membership/selectors";
import { getRewardListByIDs } from "@src/store/reward/selectors";
import { RewardListing } from "@src/components/RewardListing";
import { getOffer } from "@src/epics/offer";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]>
): Partial<Component["props"]> & { loadAction?: Action } {
  // get current user
  const loadableUser = getOnlyUser(store);
  if (loadableUser.state === "ABSENT" || loadableUser.state === "PENDING")
    return { loaded: false };
  if (loadableUser.state === "FAILED") return { error: loadableUser.message };
  const user = loadableUser.item;

  // get current user's memberships
  const loadableMemberships = getMembershipListByIDs(store, user.memberships);
  if (loadableMemberships.state === "ABSENT")
    return {
      loaded: false,
      loadAction: actions.getMembershipList.request({
        ids: loadableMemberships.ids
      })
    };
  if (loadableMemberships.state === "PENDING") return { loaded: false };
  if (loadableMemberships.state === "FAILED")
    return { error: loadableMemberships.message };
  const memberships = loadableMemberships.item;
  const membershipMap = listToMap(memberships, "businessID");

  // get offers from memberships
  const offerIDs = ([] as string[]).concat(
    ...loadableMemberships.item.map(m => m.claimedOfferIDs)
  );
  const loadableOffers = getOfferListByIDs(store, offerIDs);
  if (loadableOffers.state === "ABSENT")
    return {
      loaded: false,
      loadAction: actions.getOfferList.request({
        ids: loadableOffers.ids
      })
    };
  if (loadableOffers.state === "PENDING") return { loaded: false };
  if (loadableOffers.state === "FAILED")
    return { error: loadableOffers.message };
  const offers = loadableOffers.item;

  // get businesses from rewards
  const businessIDs = offers.map(o => o.businessID);
  const loadableBusinesses = getBusinessListByIDs(store, businessIDs);
  if (loadableBusinesses.state === "ABSENT")
    return {
      loaded: false,
      loadAction: actions.getBusinessList.request({
        ids: loadableBusinesses.ids
      })
    };
  if (loadableBusinesses.state === "PENDING") return { loaded: false };
  if (loadableBusinesses.state === "FAILED")
    return { error: loadableBusinesses.message };
  const businesses = loadableBusinesses.item;
  const businessMap = listToMap(businesses);

  return {
    // pass data props from here
    listings: (
      <React.Fragment>
        {offers.map((r, i) => (
          <OfferListing
            key={r.id}
            businessID={businessMap[r.businessID].id}
            businessLogo={businessMap[r.businessID].profilePhoto}
            businessName={businessMap[r.businessID].name}
            expiryDate={r.expiry}
            isClaimed={true}
            isMember={membershipMap[r.businessID] !== undefined}
            offerHeadline={r.headline}
          />
        ))}
      </React.Fragment>
    ),
    mapPins: ([] as Component["props"]["mapPins"]).concat(
      ...businesses.map(p =>
        p.locations.map(l => ({
          latitude: l.lat,
          longitude: l.lng,
          description: l.description,
          title: p.name
        }))
      )
    ),
    membershipCount: user.memberships.length
  };
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  let mapButton;
  const pathMatch = props.location!.pathname.match(/(.*)\/map\/?$/);
  if (pathMatch) {
    mapButton = {
      label: "List",
      handler: () => dispatch(push("/my/offers"))
    };
  } else {
    mapButton = {
      label: "Map",
      handler: () => dispatch(push("/my/offers/map"))
    };
  }
  return {
    // pass functional props from here
    triggerSearch: x => dispatch(actions.getNearbyBusinesses.request(x)),

    buttons: [
      {
        label: "My Points",
        handler: () => dispatch(push("/my/rewards"))
      },
      {
        label: "My Stamps",
        handler: () => dispatch(push("/my/stamps"))
      },
      {
        label: "My Offers",
        handler: () => dispatch(push("/my/offers"))
      }
    ],
    loadContent: action => dispatch(action!)
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps,
  (storeProps, functionProps, parentProps) => {
    return {
      ...storeProps,
      ...functionProps,
      ...parentProps,
      loadContent: storeProps.loadAction
        ? () => functionProps.loadContent!(storeProps.loadAction)
        : undefined
    };
  }
)(Component as any);
