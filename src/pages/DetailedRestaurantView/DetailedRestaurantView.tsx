import { InPageMenuBarGreen } from "@src/components/InPageMenuBarGreen";
import {
  GETPOINTS_BACKGROUND_GREY,
  GETPOINTS_DARK_TEAL,
  GETPOINTS_LIGHT_GREEN,
  GETPOINTS_WHITE,
  GETPOINTS_TEAL
} from "@src/const";
import React from "react";
import {
  ScrollView,
  View,
  ViewStyle,
  StyleProp,
  TextStyle
} from "react-native";
import glamorous from "glamorous-native";

import { BusinessHeading } from "../../components/BusinessHeading";
import { HeaderBar } from "../../components/HeaderBar";
import { Icon } from "../../components/Icon";
import { PageHeader } from "../../components/PageHeader";
import { ChangePasswordButton } from "../../components/RectangularButton";
import { ExplainerText } from "../../components/Typography/ExplainerText";
import {
  WhiteHeaderText,
  HeaderText
} from "../../components/Typography/HeaderText";
import { RouteComponentProps } from "react-router";
import { Action } from "@src/actions";
import { Business, Membership } from "@src/types/Models";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { SmallImageContainer } from "../../components/SmallImageContainer";
import { MapWrapper } from "@src/components/MapWrapper";
import { DetailedRestaurantViewRewards } from "../DetailedRestaurantViewRewards";
import { FeedbackTile } from "@src/components/FeedbackTile";
import { BusinessInformationTile } from "@src/components/BusinessInformationTile";
import { InformationTile } from "@src/components/InformationTile";
import { SettingsTile } from "@src/components/SettingsTile";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

const ImageGallery = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  justifyContent: "center",
  flexGrow: 1,
  marginTop: 10
}));

const DetailedRestaurantViewContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flex: 1,
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  height: `100%`
}));
const PageWrapper = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1,
  padding: 10
}));

const TabContentHeading = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1,
  backgroundColor: GETPOINTS_DARK_TEAL,
  alignItems: "center",
  padding: 5,
  marginBottom: 10,
  color: GETPOINTS_WHITE
}));
const ContactDetailsContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  padding: 10,
  alignSelf: "center",
  flexGrow: 0
}));
const ContactDetails = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row"
}));

const IconWithMargin = glamorous(Icon)((props: {}) => ({
  marginRight: 20
}));

const AboutUsText = glamorous(ExplainerText)((props: {}) => ({
  textAlign: "left",
  flexGrow: 1
}));

export class DetailedRestaurantView extends React.Component<
  {
    mode?: "about" | "hours" | "gallery" | "map";
    businessID: string;
    coverPhoto: string;
    profilePhoto: string;
    businessName: string;
    businessDescription: string;
    businessLocation: string;
    address: string;
    images: string[];
    type: string;
    workingHours: {
      [day: string]: {
        start: string;
        end: string;
      };
    };
    locations: Business["locations"];
    contacts: Business["contacts"];
    tapFeedbackButton: () => any;
    loadContent?: (x?: Action) => any;
    loaded?: boolean;
    error?: string;
  } & RouteComponentProps<{
    id: string;
    mode?: "about" | "gallery" | "hours" | "map";
  }>,
  { mode?: "about" | "gallery" | "hours" | "map" }
> {
  state = {
    mode: this.props.mode
  };
  componentDidMount() {
    this.update(this.props);
  }
  componentWillReceiveProps(nextProps: DetailedRestaurantView["props"]) {
    this.update(nextProps);
  }
  update = (props: DetailedRestaurantView["props"]) =>
    props.loadContent && props.loadContent();

  getAboutContent() {
    return <HeaderText>{this.props.businessDescription}</HeaderText>;
  }
  getGalleryContent() {
    const images = this.props.images.map(image => (
      <SmallImageContainer photo={image} key={image} />
    ));
    return <ImageGallery>{images}</ImageGallery>;
  }
  getHoursContent() {
    const rowStyle: StyleProp<ViewStyle> = {
      display: "flex",
      flexDirection: "row"
    };
    const dayStyle: StyleProp<TextStyle> = {
      width: 50,
      textAlign: "right",
      marginTop: 0,
      marginBottom: 0
    };
    const timeStyle: StyleProp<TextStyle> = {
      width: 100,
      textAlign: "center",
      marginTop: 0,
      marginBottom: 0
    };
    const days = Object.keys(this.props.workingHours);
    const rows = days.map(day => {
      const [firstLetter, ...letters] = day;
      return (
        <View style={rowStyle} key={day}>
          <ExplainerText style={dayStyle}>
            {firstLetter.toUpperCase()}
            {letters.join("")}
          </ExplainerText>
          <ExplainerText style={timeStyle}>
            {this.props.workingHours[day].start}
            {" - "}
            {this.props.workingHours[day].end}
          </ExplainerText>
        </View>
      );
    });

    return <View style={{ alignItems: "center" }}>{rows}</View>;
  }
  getMapContent() {
    const markers = this.props.locations.map(l => ({
      title: this.props.businessName,
      latitude: l.lat,
      description: l.description,
      longitude: l.lng
    }));
    return <MapWrapper markers={markers} />;
  }

  getRewardContent() {
    return <DetailedRestaurantViewRewards businessID={this.props.businessID} />;
  }

  getContactDetails() {
    const details = this.props.contacts.map(c => {
      let icon;
      if (c.type === "ADDRESS") return null;
      if (c.type === "EMAIL") icon = "ios-mail";
      else if (c.type === "PHONE") icon = "md-call";
      else if (c.type === "WEBSITE") icon = "ios-globe";
      else icon = "ios-globe";

      return (
        <ContactDetails key={c.text}>
          <IconWithMargin name={icon} size={40} color={GETPOINTS_LIGHT_GREEN} />
          <AboutUsText>{c.text}</AboutUsText>
        </ContactDetails>
      );
    });

    return <ContactDetailsContainer>{details}</ContactDetailsContainer>;
  }

  getTitle() {
    switch (this.state.mode) {
      case "about":
        return (
          <TabContentHeading>
            <WhiteHeaderText>About</WhiteHeaderText>
          </TabContentHeading>
        );
      case "hours":
        return (
          <TabContentHeading>
            <WhiteHeaderText>Opening Hours</WhiteHeaderText>
          </TabContentHeading>
        );
      case "gallery":
        return (
          <TabContentHeading>
            <WhiteHeaderText>Gallery</WhiteHeaderText>
          </TabContentHeading>
        );
      case "map":
        return (
          <TabContentHeading>
            <WhiteHeaderText>Map</WhiteHeaderText>
          </TabContentHeading>
        );
    }
    return null;
  }

  getTopContent() {
    switch (this.state.mode) {
      case "about":
        return this.getAboutContent();
      case "gallery":
        return this.getGalleryContent();
      case "map":
        return this.getMapContent();
      case "hours":
        return this.getHoursContent();
    }
    return null;
  }

  getInfoTiles() {
    return (
      <View style={{ marginTop: 20 }}>
        <BusinessInformationTile
          contacts={this.props.contacts}
          locations={this.props.locations}
        />
        <FeedbackTile
          businessName={this.props.businessName}
          businessID={this.props.businessID}
        />
        <InformationTile title="Reward Rules" color={GETPOINTS_TEAL} />
        <SettingsTile businessID={this.props.businessID} />
      </View>
    );
  }

  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <DetailedRestaurantViewContainer>
        <HeaderBar />
        <PageHeader>{this.props.businessName}</PageHeader>
        <KeyboardAwareScrollView enableOnAndroid={true}>
          <BusinessHeading
            id={this.props.businessID}
            coverPhoto={this.props.coverPhoto}
            profilePhoto={this.props.profilePhoto}
            businessLocation={this.props.businessLocation}
            businessName={this.props.businessName}
            businessType={this.props.type}
          />
          <InPageMenuBarGreen
            businessID={this.props.businessID}
            selected={this.state.mode}
            setMode={mode => this.setState({ mode })}
          />
          {this.getTitle()}
          <PageWrapper>
            {this.getTopContent()}
            {this.getRewardContent()}
            {this.getInfoTiles()}
          </PageWrapper>
        </KeyboardAwareScrollView>
      </DetailedRestaurantViewContainer>
    );
  }
}
