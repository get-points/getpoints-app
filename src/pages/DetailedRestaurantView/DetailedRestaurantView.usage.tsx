import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { DetailedRestaurantView } from "./index";

export const basic = () => (
  <DetailedRestaurantView
    businessID="42"
    businessName={text("businessName", "The Pantry")}
    coverPhoto="http://placekitten.com/100/100"
    profilePhoto="http://placekitten.com"
    workingHours={{
      monday: {
        end: "",
        start: ""
      }
    }}
    businessDescription={text(
      "businessDescription",
      "This is a short description of the business which will hopefully be interesting"
    )}
    tapFeedbackButton={action("tap feedback")}
  />
);
