import { DetailedRestaurantView as Component } from "./DetailedRestaurantView";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State } from "@src/store";
import { actions, Action } from "@src/actions";
import {} from "@src/store/selectors";
import { getBusinessByID } from "@src/store/business/selectors";
import { getOnlyUser } from "@src/store/user/selectors";
import { getMembershipListByIDs } from "@src/store/membership/selectors";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]>
): Partial<Component["props"]> & { loadAction?: Action } {
  try {
    // prepare data here
    const businessID = props.match!.params.id;

    const loadableBusiness = getBusinessByID(store, businessID);
    if (loadableBusiness.state === "ABSENT")
      return {
        loaded: false,
        loadAction: actions.getBusiness.request({
          id: businessID
        })
      };
    if (loadableBusiness.state === "PENDING") return { loaded: false };
    if (loadableBusiness.state === "FAILED")
      return { error: loadableBusiness.message };
    const business = loadableBusiness.item;

    return {
      // pass data props from here
      mode: props.match!.params.mode,
      businessDescription: business.description,
      businessName: business.name,
      businessID: business.id,
      coverPhoto: business.coverPhoto,
      profilePhoto: business.profilePhoto,
      workingHours: business.hours,
      contacts: business.contacts,
      type: business.type,
      businessLocation: business.locations[0].city,
      address: business.contacts.filter(x => x.type === "ADDRESS")[0].text,
      images: business.images,
      locations: business.locations
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  return {
    // pass functional props from here
    tapFeedbackButton: () => {},
    loadContent: action => dispatch(action!)
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps,
  (storeProps, functionProps, parentProps) => {
    return {
      ...storeProps,
      ...functionProps,
      ...parentProps,
      loadContent: storeProps.loadAction
        ? () => functionProps.loadContent!(storeProps.loadAction)
        : undefined
    };
  }
)(Component as any);
