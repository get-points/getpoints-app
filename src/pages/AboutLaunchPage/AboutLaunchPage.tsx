import { GETPOINTS_BACKGROUND_GREY, GETPOINTS_GREEN } from "@src/const";
import React from "react";
import { ScrollView, View } from "react-native";
import glamorous from "glamorous-native";

import { HeaderBar } from "../../components/HeaderBar";
import { PageHeader } from "../../components/PageHeader";
import { WhiteButton, WhiteButtonGreen } from "../../components/WhiteButton";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";

const AboutLaunchPageContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flex: 1,
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  height: `100%`
}));

const ButtonsContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1,
  padding: 30,
  textAlign: "center"
}));

const Button = glamorous(WhiteButtonGreen)({
  padding: 5,
  marginBottom: 10
});

export class AboutLaunchPage extends React.Component<{
  tapFAQsButton: () => any;
  // tapTourButton: () => any;
  tapEmailButton: () => any;
  tapAboutButton: () => any;
  tapTermsButton: () => any;

  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <AboutLaunchPageContainer>
        <HeaderBar />
        <PageHeader>Support</PageHeader>
        <ScrollView>
          <ButtonsContainer>
            <Button onTap={this.props.tapFAQsButton}>FAQs</Button>
            {/* <Button onTap={this.props.tapTourButton}>
              How it works - Take a Tour
            </Button> */}
            <Button onTap={this.props.tapEmailButton}>Email Support</Button>
            <Button onTap={this.props.tapAboutButton}>About Us</Button>
            <Button onTap={this.props.tapTermsButton}>
              Terms & Conditions
            </Button>
          </ButtonsContainer>
        </ScrollView>
      </AboutLaunchPageContainer>
    );
  }
}
