import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { ConnectedAboutLaunchPage as AboutLaunchPage } from "./enhancer";
import { Route } from "react-router";

export const basic = () => <Route component={AboutLaunchPage} />;
