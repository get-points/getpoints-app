import { State } from "@src/store";
import { goBack, push } from "connected-react-router";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { Dispatch } from "redux";

import { AboutLaunchPage } from "./AboutLaunchPage";

interface OwnProps extends RouteComponentProps {}

function mapStateToProps(state: State, props: OwnProps) {
  return {};
}

function mapDispatchToProps(dispatch: Dispatch, props: OwnProps) {
  return {
    tapAboutButton: () => dispatch(push("/about/us")),
    tapEmailButton: () => dispatch(push("/about/support")),
    tapFAQsButton: () => dispatch(push("/about/faqs")),
    tapTermsButton: () => dispatch(push("/about/terms"))
    // tapTourButton: () => dispatch(push("/about/how-it-works"))
  };
}

function mergeProps(
  stateProps: ReturnType<typeof mapStateToProps>,
  dispatchProps: ReturnType<typeof mapDispatchToProps>
): AboutLaunchPage["props"] {
  return {
    tapAboutButton: dispatchProps.tapAboutButton,
    tapEmailButton: dispatchProps.tapEmailButton,
    tapFAQsButton: dispatchProps.tapFAQsButton,
    tapTermsButton: dispatchProps.tapTermsButton
    // tapTourButton: dispatchProps.tapTourButton
  };
}

export const ConnectedAboutLaunchPage = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(AboutLaunchPage);
