import { TermsAndConditions as Component } from "./TermsAndConditions";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State } from "@src/store";
import { actions } from "@src/actions";
import {} from "@src/store/selectors";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  try {
    // prepare data here

    return {
      // pass data props from here
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  return {
    // pass functional props from here
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
