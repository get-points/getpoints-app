import { HeaderBar } from "@src/components/HeaderBar";
import { InPageMenuBar } from "@src/components/InPageMenuBar";
import { PageHeader } from "@src/components/PageHeader";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { HeaderText } from "@src/components/Typography/HeaderText";
import { GETPOINTS_BACKGROUND_GREY } from "@src/const";
import React from "react";
import { View, ScrollView } from "react-native";
import glamorous from "glamorous-native";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";

const TermsAndConditionsContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flex: 1,
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  width: `100%`,
  height: `100%`
}));

const MainPage = glamorous(View)((props: {}) => ({
  padding: 10,
  paddingLeft: 20,
  paddingRight: 20
}));

export class TermsAndConditions extends React.Component<{
  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <TermsAndConditionsContainer>
        <HeaderBar />
        <PageHeader>Support - Terms & Conditions</PageHeader>
        <ScrollView>
          <InPageMenuBar selected="terms" />
          <MainPage>
            <HeaderText>Terms & Conditions</HeaderText>
            <ExplainerText>
              (1) You will be required to accept this Agreement when you enter
              the 'My Account' section of the App and provide your personal
              details for use in the My Account feature within the App ("Account
              Information"). Completing the My Account section allows you to use
              the features described in section 2 below. Should you decline the
              acceptance of this Agreement these aspects of the App will not be
              available to you. (2) By using the App you agree that you have
              read and understood the terms and conditions of this Agreement and
              you acknowledge that these terms and conditions in their entirety
              shall apply to you. (3) We reserve the right to amend, modify,
              update and change any of the terms of this Agreement. We advise
              you to check for updates on a regular basis. Any material changes
              to the Agreement will be notified to you through the messaging
              features of the App. Your continued use of the App will be deemed
              to constitute your acceptance of such changes.
            </ExplainerText>
          </MainPage>
        </ScrollView>
      </TermsAndConditionsContainer>
    );
  }
}
