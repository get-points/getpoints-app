import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { OfferClaimRedemptionPopUp } from "./index";

export const basic = () => (
  <OfferClaimRedemptionPopUp
    businessName={text("businessName", "The Pantry")}
    isVisible={boolean("isVisible", true)}
    tapDone={action("tapButton")}
  />
);
