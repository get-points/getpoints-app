import React from "react";
import { View, ScrollView } from "react-native";
import glamorous from "glamorous-native";

import { PageHeader } from "../../components/PageHeader";
import {
  RectangularButton,
  FixedWidthButton
} from "../../components/RectangularButton";
import { ExplainerText } from "../../components/Typography/ExplainerText";
import { HeaderText } from "../../components/Typography/HeaderText";
import { GETPOINTS_BACKGROUND_GREY } from "@src/const";
import { boolean } from "@storybook/addon-knobs";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { PopupContainer } from "@src/components/PopupContainer";

const OfferClaimRedemptionPopUpContainer = glamorous(View)((props: {}) => ({
  flex: 1
}));
export class OfferClaimRedemptionPopUp extends React.Component<{
  tapDone: () => any;
  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <PopupContainer>
        <HeaderText>Congratulations!</HeaderText>
        <HeaderText>You have now claimed this offer.</HeaderText>
        <ExplainerText>
          Show this reward redemption to a cashier and they will get your
          reward.
        </ExplainerText>
        <FixedWidthButton onTap={this.props.tapDone} topRightSquare>
          DONE
        </FixedWidthButton>
      </PopupContainer>
    );
  }
}
