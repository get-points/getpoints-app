import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { RewardRedemptionPopUp } from "./index";

export const basic = () => (
  <RewardRedemptionPopUp
    rewardName={text("rewardName", "Free Coffee")}
    rewardValue={number("rewardValue", 4)}
    tapConfirmButton={action("tapButton")}
    tapGoBackButton={action("tapButton")}
  />
);
