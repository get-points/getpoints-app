import { GETPOINTS_RED, GETPOINTS_WHITE } from "@src/const";
import React from "react";
import { ScrollView, View } from "react-native";
import glamorous from "glamorous-native";

import { FixedWidthButton } from "../../components/RectangularButton";
import { HeaderText } from "../../components/Typography/HeaderText";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { PopupContainer } from "@src/components/PopupContainer";

const ButtonsContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  justifyContent: "center"
}));

export class RewardRedemptionPopUp extends React.Component<{
  rewardValue: number;
  rewardName: string;
  tapGoBackButton: () => any;
  tapConfirmButton: () => any;
  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <PopupContainer>
        <HeaderText>
          You are redeeming {this.props.rewardValue} points.
        </HeaderText>
        <HeaderText>For {this.props.rewardName}</HeaderText>
        <ButtonsContainer>
          <FixedWidthButton
            color={GETPOINTS_RED}
            topLeftSquare
            onTap={this.props.tapGoBackButton}
          >
            GO BACK
          </FixedWidthButton>
          <FixedWidthButton topRightSquare onTap={this.props.tapConfirmButton}>
            CONFIRM
          </FixedWidthButton>
        </ButtonsContainer>
      </PopupContainer>
    );
  }
}
