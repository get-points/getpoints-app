import { RewardRedemptionPopUp as Component } from "./RewardRedemptionPopUp";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State } from "@src/store";
import { actions, Action } from "@src/actions";
import {} from "@src/store/selectors";
import { PopupType } from "@src/actions/ui";
import { getRewardByID } from "@src/store/reward/selectors";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]> & { rewardID: string }
): Partial<Component["props"]> & { loadAction?: Action } {
  try {
    // prepare data here
    const rewardID = props.rewardID;
    const loadableReward = getRewardByID(store, rewardID);
    if (loadableReward.state === "ABSENT")
      return {
        loaded: false,
        loadAction: actions.getReward.request({ id: rewardID })
      };
    if (loadableReward.state === "PENDING") return { loaded: false };
    if (loadableReward.state === "FAILED")
      return { error: loadableReward.message };
    const reward = loadableReward.item;

    return {
      // pass data props from here
      rewardName: reward.headline,
      rewardValue: reward.pointsRequired
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]> & { rewardID: string }
): Partial<Component["props"]> {
  return {
    // pass functional props from here
    tapConfirmButton: () =>
      dispatch(
        actions.showPopup({
          popup: {
            type: PopupType.REWARD_REDEMPTION_CONFIRMATION,
            data: { rewardID: props.rewardID }
          }
        })
      ),
    tapGoBackButton: () => dispatch(actions.hidePopup())
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
