import React from "react";
import { RouteComponentProps } from "react-router";
import { SearchPage } from "../SearchPage";
import { Action } from "@src/actions";

export class NearbyPlaces extends React.Component<
  {
    listings: React.ReactChild;
    triggerSearch: SearchPage["props"]["triggerSearch"];
    mapPins: SearchPage["props"]["mapPins"];
    buttons: SearchPage["props"]["buttons"];
    loadContent?: (x?: Action) => any;
    loaded?: boolean;
    error?: string;
    membershipCount: number;
  } & RouteComponentProps
> {
  componentDidMount() {
    this.update(this.props);
  }
  componentWillReceiveProps(nextProps: NearbyPlaces["props"]) {
    this.update(nextProps);
  }
  update = (props: NearbyPlaces["props"]) =>
    props.loadContent && props.loadContent();

  render() {
    return (
      <SearchPage
        baseURL="/nearby/places"
        pageTitle={"Nearby Places"}
        buttons={this.props.buttons}
        {...this.props}
      />
    );
  }
}
