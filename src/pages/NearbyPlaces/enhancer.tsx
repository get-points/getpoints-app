import { actions, Action } from "@src/actions";
import { State } from "@src/store";
import { getBusinessListByIDs } from "@src/store/business/selectors";
import { getNearbyBusinesses } from "@src/store/search/nearbyBusinesses/selectors";
import { listToMap } from "@src/store/selectors";
import { push } from "connected-react-router";
import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { NearbyPlaces as Component } from "./NearbyPlaces";
import { BusinessListingHeader } from "@src/components/BusinessListingHeader";
import { Icon } from "@src/components/Icon";
import { getOnlyUser } from "@src/store/user/selectors";
import { getMembershipListByIDs } from "@src/store/membership/selectors";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]>
): Partial<Component["props"]> & { loadAction?: Action } {
  // prepare data here
  const loadableNearbyBusinesses = getNearbyBusinesses(store);
  if (loadableNearbyBusinesses.state === "ABSENT") {
    return {
      loaded: false,
      loadAction: actions.getNearbyBusinesses.request({})
    };
  }
  if (loadableNearbyBusinesses.state === "PENDING") return { loaded: false };
  if (loadableNearbyBusinesses.state === "FAILED")
    return { error: loadableNearbyBusinesses.message };

  const businessIDs = loadableNearbyBusinesses.item;
  const loadableBusinesses = getBusinessListByIDs(
    store,
    businessIDs.map(i => i.id)
  );
  if (loadableBusinesses.state === "ABSENT") {
    return {
      loaded: false,
      loadAction: actions.getBusinessList.request({
        ids: loadableBusinesses.ids
      })
    };
  }
  if (loadableBusinesses.state === "PENDING") return { loaded: false };
  if (loadableBusinesses.state === "FAILED")
    return { error: loadableBusinesses.message };
  const businesses = loadableBusinesses.item;

  const loadableUser = getOnlyUser(store);
  if (loadableUser.state === "PENDING" || loadableUser.state === "ABSENT")
    return { loaded: false };
  if (loadableUser.state === "FAILED") return { error: loadableUser.message };

  const user = loadableUser.item;
  // load memberships
  const loadableMemberships = getMembershipListByIDs(store, user.memberships);
  if (loadableMemberships.state === "ABSENT")
    return {
      loaded: false,
      loadAction: actions.getMembershipList.request({
        ids: loadableMemberships.ids
      })
    };
  if (loadableMemberships.state === "PENDING") return { loaded: false };
  if (loadableMemberships.state === "FAILED")
    return { error: loadableMemberships.message };
  const memberships = loadableMemberships.item;
  const membershipMap = listToMap(memberships, "businessID");

  return {
    // pass data props from here
    listings: (
      <React.Fragment>
        {businesses.map((b, i) => (
          <BusinessListingHeader
            key={b.id}
            businessID={b.id}
            businessDistance={b.locations[businessIDs[i].location].distance!}
            businessLocation={b.locations[businessIDs[i].location].description}
            businessName={b.name}
            businessLogo={b.profilePhoto}
            businessType={b.type}
            offerText={b.offerText}
            isMember={membershipMap[b.id] !== undefined}
          />
        ))}
      </React.Fragment>
    ),
    mapPins: ([] as Component["props"]["mapPins"]).concat(
      ...businesses.map(p =>
        p.locations.map(l => ({
          latitude: l.lat,
          longitude: l.lng,
          description: l.description,
          title: p.name
        }))
      )
    ),
    membershipCount: user.memberships.length
  };
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  let mapButton;
  const pathMatch = props.location!.pathname.match(/(.*)\/map\/?$/);
  if (pathMatch) {
    mapButton = {
      label: "List",
      handler: () => dispatch(push("/nearby/places"))
    };
  } else {
    mapButton = {
      label: "Map",
      handler: () => dispatch(push("/nearby/places/map"))
    };
  }
  return {
    // pass functional props from here
    triggerSearch: x => dispatch(actions.getNearbyBusinesses.request(x)),
    loadContent: action => dispatch(action!),
    buttons: [
      {
        label: "Memberships",
        handler: () => dispatch(push("/my/memberships"))
      },
      {
        label: (
          <React.Fragment>
            <Icon name="md-unlock" /> Rewards
          </React.Fragment>
        ),
        handler: () => dispatch(push("/my/rewards"))
      },
      mapButton
    ]
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps,
  (storeProps, { ...functionProps }, parentProps) => {
    const [membershipButton, ...buttons] = functionProps.buttons!;
    membershipButton.label = `${storeProps.membershipCount || ""} Membership${
      storeProps.membershipCount === 1 ? "" : "s"
    }`;
    return {
      ...storeProps,
      ...functionProps,
      ...parentProps,
      buttons: [membershipButton, ...buttons],
      loadContent: storeProps.loadAction
        ? () => functionProps.loadContent!(storeProps.loadAction)
        : undefined
    };
  }
)(Component as any);
