import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { MyHistory } from "./index";

export const basic = () => (
  <MyHistory
    rewardDetails={{
      currentPoints: number("currentPoints", 5),
      redeemedPoints: number("redeemedPoints", 4),
      visits: number("visits", 3)
    }}
    data={[]}
  />
);
