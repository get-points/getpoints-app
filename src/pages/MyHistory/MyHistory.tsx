import { TableHeaderRow } from "@src/components/TableHeaderRow";
import { GETPOINTS_BACKGROUND_GREY } from "@src/const";
import React from "react";
import { View, ScrollView } from "react-native";
import glamorous from "glamorous-native";

import { HeaderBar } from "../../components/HeaderBar";
import { PageHeader } from "../../components/PageHeader";
import { PointStatus } from "../../components/PointStatus";
import { TableRow } from "../../components/TableRow";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";

const MyHistoryContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flex: 1
}));
const Page = glamorous(ScrollView)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1,
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  padding: 10
}));
const PointsContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 1,
  marginLeft: 10,
  marginRight: 10
}));

export class MyHistory extends React.Component<{
  rewardDetails: {
    visits: number;
    currentPoints: number;
    redeemedPoints: number;
  };
  loaded?: boolean;
  error?: string;
  data: TableRow["props"][];
}> {
  render() {
    console.log(this.props);
    const tableRows = this.props.data.map((data, i) => (
      <TableRow key={i} {...data} />
    ));
    if (this.props.loaded) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <MyHistoryContainer>
        <HeaderBar />
        <PageHeader>My History</PageHeader>
        <Page>
          <PointsContainer>
            <PointStatus points={this.props.rewardDetails.visits}>
              My Visits
            </PointStatus>
            <PointStatus points={this.props.rewardDetails.currentPoints}>
              My Points
            </PointStatus>
            <PointStatus end points={this.props.rewardDetails.redeemedPoints}>
              Redeemed
            </PointStatus>
          </PointsContainer>
          <TableHeaderRow />
          {tableRows}
        </Page>
      </MyHistoryContainer>
    );
  }
}
