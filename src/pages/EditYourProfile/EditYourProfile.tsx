import { DropDownMenu } from "@src/components/DropDownMenu";
import { HeaderBar } from "@src/components/HeaderBar";
import { Icon } from "@src/components/Icon";
import { FixedWidthButton } from "@src/components/RectangularButton";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { HeaderText } from "@src/components/Typography/HeaderText";
import { InputPlaceholder } from "@src/components/Typography/InputPlaceholder";
import { WhiteButton } from "@src/components/WhiteButton";
import {
  GETPOINTS_BACKGROUND_GREY,
  GETPOINTS_GREY,
  GETPOINTS_RED
} from "@src/const";
import React from "react";
import { Image, View, ScrollView } from "react-native";
import glamorous from "glamorous-native";
import { PageHeader } from "@src/components/PageHeader";
import profilePicturePlaceholder from "../../../assets/profile-picture-placeholder.png";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

const WideDropDownMenu = glamorous(DropDownMenu)((props: {}) => ({
  flexGrow: 1
}));
const EditYourProfileContainer = glamorous(View)((props: {}) => ({
  flex: 1,
  display: "flex",
  flexDirection: "column",
  backgroundColor: "lightgrey"
}));
const Page = glamorous(KeyboardAwareScrollView)((props: {}) => ({
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  display: "flex",
  flexDirection: "column",
  flex: 1
}));
const PageContents = glamorous(View)((props: {}) => ({
  padding: 20
}));
const CenterIcon = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  alignItems: "center"
}));
const FormGroup = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  margin: 0,
  padding: 0
}));
const FormLabel = glamorous(ExplainerText)((props: {}) => ({
  width: 60,
  flexGrow: 0,
  flexShrink: 0,
  textAlign: "left",
  minWidth: "25%"
}));
const InputGroup = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 1,
  flexShrink: 1,
  alignItems: "center"
}));
const InputField = glamorous(InputPlaceholder)((props: {}) => ({
  height: 30,
  marginTop: 0,
  marginBottom: 0
  // flex: 1,
  // flexBasis: 1,
  // flexShrink: 1,
  // margin: 5
}));
const LeftText = glamorous(ExplainerText)((props: {}) => ({
  flex: 1,
  marginLeft: 5
}));

const CountryCode = glamorous(InputField)((props: {}) => ({
  flexGrow: 0.5,
  flexBasis: 2
}));

const ButtonGroup = glamorous(View)((props: {}) => ({
  flexDirection: "row",
  justifyContent: "center"
  // marginTop: 20
}));

export class EditYourProfile extends React.Component<
  {
    loaded?: boolean;
    error?: string;
    phone: {
      countryCode: string;
      phone: string;
    };
    countryList: string[];
    dateList: string[];
    monthList: string[];
    yearList: string[];
    tapChooseFileButton: () => any;
    tapSaveButton: () => any;
    user: {
      photo: string;
      firstName: string;
      lastName: string;
      country: string;
      gender: "male" | "female";
      phonePhone: string;
      phoneCountryCode: string;
      date: string;
      month: string;
      year: string;
    };
  },
  {
    photo: string;
    firstName: string;
    lastName: string;
    country: string;
    gender: "male" | "female";
    phonePhone: string;
    phoneCountryCode: string;
    date: string;
    month: string;
    year: string;
  }
> {
  state = this.props.user;

  setPhoto = (photo: string) => this.setState({ photo });
  setFirstName = (firstName: string) => this.setState({ firstName });
  setLastName = (lastName: string) => this.setState({ lastName });
  setCountry = (country: string) => this.setState({ country });
  setMale = () => this.setState({ gender: "male" });
  setFemale = () => this.setState({ gender: "female" });
  setDate = (date: string) => this.setState({ date });
  setMonth = (month: string) => this.setState({ month });
  setYear = (year: string) => this.setState({ year });
  setPhone = (phonePhone: string) => this.setState({ phonePhone });
  setCountryCode = (phoneCountryCode: string) =>
    this.setState({ phoneCountryCode });

  getGenderButtons() {
    if (this.state.gender === "male") {
      return (
        <InputGroup>
          <Icon name="ios-radio-button-on" size={20} color={GETPOINTS_GREY} />
          <LeftText>Male</LeftText>
          <Icon
            name="ios-radio-button-off"
            size={20}
            color={GETPOINTS_GREY}
            onPress={this.setFemale}
          />
          <LeftText>Female</LeftText>
        </InputGroup>
      );
    } else if (this.state.gender === "female") {
      return (
        <InputGroup>
          <Icon
            name="ios-radio-button-off"
            size={20}
            color={GETPOINTS_GREY}
            onPress={this.setMale}
          />
          <LeftText>Male</LeftText>
          <Icon name="ios-radio-button-on" size={20} color={GETPOINTS_GREY} />
          <LeftText>Female</LeftText>
        </InputGroup>
      );
    }
  }

  render() {
    return (
      <EditYourProfileContainer>
        <HeaderBar />
        <PageHeader>Edit Your Profile</PageHeader>
        <Page enableOnAndroid>
          <PageContents>
            <CenterIcon>
              <Image
                source={
                  this.state.photo
                    ? { uri: this.state.photo }
                    : profilePicturePlaceholder
                }
                style={{ width: 80, height: 80 }}
              />
            </CenterIcon>

            <FormGroup>
              <FormLabel>Profile photo</FormLabel>
              <WhiteButton
                style={{
                  height: 30,
                  minHeight: 30,
                  marginTop: 10,
                  marginBottom: 5
                }}
                textStyle={{ lineHeight: 25 }}
                onTap={() => this.setPhoto("https://placekitten.com/200/200")}
              >
                Choose File
              </WhiteButton>
              {/* <FixedWidthButton onTap={this.props.tapSavePhotoButton}>
                Save
              </FixedWidthButton> */}
            </FormGroup>

            <FormGroup>
              <FormLabel>First Name</FormLabel>
              <InputField onChange={this.setFirstName}>
                {this.state.firstName}
              </InputField>
            </FormGroup>

            <FormGroup>
              <FormLabel>Last Name</FormLabel>
              <InputField onChange={this.setLastName}>
                {this.state.lastName}
              </InputField>
            </FormGroup>

            <FormGroup>
              <FormLabel>Phone</FormLabel>
              <InputGroup>
                <CountryCode
                  onChange={this.setPhone}
                  maxLength={3}
                  keyboardType="phone-pad"
                  children={this.state.phoneCountryCode}
                />
                <InputField
                  onChange={this.setCountryCode}
                  keyboardType="phone-pad"
                  maxLength={10}
                  children={this.state.phonePhone}
                />
              </InputGroup>
            </FormGroup>
            <FormGroup>
              <FormLabel>Country</FormLabel>
              <WideDropDownMenu
                options={this.props.countryList}
                onChange={this.setCountry}
              >
                {this.state.country}
              </WideDropDownMenu>
            </FormGroup>
            <FormGroup>
              <FormLabel>Date Of Birth</FormLabel>
              <InputGroup>
                <WideDropDownMenu
                  options={this.props.dateList}
                  onChange={this.setDate}
                >
                  {this.state.date}
                </WideDropDownMenu>
                <WideDropDownMenu
                  options={this.props.monthList}
                  onChange={this.setMonth}
                >
                  {this.state.month}
                </WideDropDownMenu>
                <WideDropDownMenu
                  options={this.props.yearList}
                  onChange={this.setYear}
                >
                  {this.state.year}
                </WideDropDownMenu>
              </InputGroup>
            </FormGroup>

            <FormGroup>
              <FormLabel>Gender</FormLabel>
              {this.getGenderButtons()}
            </FormGroup>

            <ButtonGroup>
              {/* <FixedWidthButton
                onTap={this.props.tapEditButton}
                color={GETPOINTS_RED}
                topLeftSquare
              >
                Edit
              </FixedWidthButton> */}
              <FixedWidthButton onTap={this.props.tapSaveButton} topRightSquare>
                Save
              </FixedWidthButton>
            </ButtonGroup>
          </PageContents>
        </Page>
      </EditYourProfileContainer>
    );
  }
}
