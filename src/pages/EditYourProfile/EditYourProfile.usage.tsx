import React from "react";
import { boolean, text, number, select } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { EditYourProfile } from "./index";

export const basic = () => (
  <EditYourProfile
    photo="http://placekitten.com/100/100"
    firstName={text("firstName", "Joe")}
    lastName={text("firstName", "Johnson")}
    phone={{
      countryCode: text("countryCode", "256"),
      phone: text("countryCode", "7881818188")
    }}
    countryList={["Uganda", "Kenya", "Nairobi"]}
    country="Uganda"
    stateList={[]}
    state=""
    cityList={[]}
    city=""
    dateOfBirth={{
      day: "01",
      month: "01",
      year: "2019"
    }}
    dateList={[]}
    monthList={[]}
    yearList={[]}
    gender={select("gender", ["male", "female"], "male")}
    tapSavePhotoButton={action("tap save photo")}
    tapEditButton={action("tap edit photo")}
    tapSaveButton={action("tap save button")}
    tapChooseFileButton={action("tap choose file")}
  />
);
