import { EditYourProfile as Component } from "./EditYourProfile";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State } from "@src/store";
import { actions } from "@src/actions";
import {} from "@src/store/selectors";
import { getOnlyUser } from "@src/store/user/selectors";
import { push } from "connected-react-router";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  try {
    // prepare data here
    const loadableUser = getOnlyUser(store);

    if (loadableUser.state === "ABSENT")
      return {
        loaded: false
      };
    if (loadableUser.state === "PENDING") return { loaded: false };
    if (loadableUser.state === "FAILED") return { error: loadableUser.message };
    const user = loadableUser.item;

    return {
      // pass data props from here
      countryList: ["Uganda", "Kenya", "South Africa", "Tanzania"],
      dateList: [...Array(31).keys()].map(x => "" + (x + 1)),
      monthList: [...Array(12).keys()].map(x => "" + (x + 1)),
      yearList: [...Array(100).keys()].map(x => "" + (x + 1918)),
      user: {
        firstName: user.firstName,
        lastName: user.lastName,
        gender: user.gender === "MALE" ? "male" : "female",
        phoneCountryCode: user.telephone.countryCode,
        phonePhone: user.telephone.telNumber,
        photo: user.profilePicture,
        country: user.address.country,
        date: user.dateOfBirth.day,
        month: user.dateOfBirth.month,
        year: user.dateOfBirth.year
      }
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  return {
    tapSaveButton: () => dispatch(push("/settings"))
    // pass functional props from here
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
