import React from "react";
import { RouteComponentProps } from "react-router";
import { SearchPage } from "../SearchPage";
import { Action } from "@src/actions";

export class MyMemberships extends React.Component<
  {
    listings: React.ReactChild;
    triggerSearch: SearchPage["props"]["triggerSearch"];
    mapPins: SearchPage["props"]["mapPins"];
    buttons: SearchPage["props"]["buttons"];
    loadContent?: (x?: Action) => any;
    membershipCount: number;
    loaded?: boolean;
    error?: string;
  } & RouteComponentProps
> {
  componentDidMount() {
    this.update(this.props);
  }
  componentWillReceiveProps(nextProps: MyMemberships["props"]) {
    this.update(nextProps);
  }
  update = (props: MyMemberships["props"]) =>
    props.loadContent && props.loadContent();

  render() {
    return (
      <SearchPage
        selectedButton={0}
        baseURL="/my/memberships"
        pageTitle={"My Memberships"}
        {...this.props}
      />
    );
  }
}
