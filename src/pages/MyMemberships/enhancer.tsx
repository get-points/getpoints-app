import { actions, Action } from "@src/actions";
import { State } from "@src/store";
import { getBusinessListByIDs } from "@src/store/business/selectors";
import { getNearbyBusinesses } from "@src/store/search/nearbyBusinesses/selectors";
import { listToMap } from "@src/store/selectors";
import { push } from "connected-react-router";
import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { MyMemberships as Component } from "./MyMemberships";
import { BusinessListingHeader } from "@src/components/BusinessListingHeader";
import { getOfferListByIDs } from "@src/store/offer/selectors";
import { OfferListing } from "@src/components/OfferListing";
import { Icon } from "@src/components/Icon";
import { getOnlyUser } from "@src/store/user/selectors";
import { getMembershipListByIDs } from "@src/store/membership/selectors";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]>
): Partial<Component["props"]> & { loadAction?: Action } {
  // get current user
  const loadableUser = getOnlyUser(store);
  if (loadableUser.state === "ABSENT" || loadableUser.state === "PENDING")
    return { loaded: false };
  if (loadableUser.state === "FAILED") return { error: loadableUser.message };
  const user = loadableUser.item;

  // get current user's memberships
  const loadableMemberships = getMembershipListByIDs(store, user.memberships);
  if (loadableMemberships.state === "ABSENT")
    return {
      loaded: false,
      loadAction: actions.getMembershipList.request({
        ids: loadableMemberships.ids
      })
    };
  if (loadableMemberships.state === "PENDING") return { loaded: false };
  if (loadableMemberships.state === "FAILED")
    return { error: loadableMemberships.message };
  const memberships = loadableMemberships.item;
  const membershipMap = listToMap(memberships);

  // get businesses from memberships
  const businessIDs = loadableMemberships.item.map(m => m.businessID);
  const loadableBusinesses = getBusinessListByIDs(store, businessIDs);
  if (loadableBusinesses.state === "ABSENT")
    return {
      loaded: false,
      loadAction: actions.getBusinessList.request({
        ids: loadableBusinesses.ids
      })
    };
  if (loadableBusinesses.state === "PENDING") return { loaded: false };
  if (loadableBusinesses.state === "FAILED")
    return { error: loadableBusinesses.message };
  const businesses = loadableBusinesses.item;
  const businessMap = listToMap(businesses);

  return {
    // pass data props from here
    listings: (
      <React.Fragment>
        {memberships.map((m, i) => (
          <BusinessListingHeader
            key={m.id}
            businessID={m.businessID}
            businessName={businessMap[m.businessID].name}
            businessLogo={businessMap[m.businessID].profilePhoto}
            businessType={businessMap[m.businessID].type}
            offerText={m.points + " Points"}
            isMember={true}
          />
        ))}
      </React.Fragment>
    ),
    mapPins: ([] as Component["props"]["mapPins"]).concat(
      ...businesses.map(p =>
        p.locations.map(l => ({
          latitude: l.lat,
          longitude: l.lng,
          description: l.description,
          title: p.name
        }))
      )
    ),
    membershipCount: user.memberships.length
  };
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  let mapButton;
  const pathMatch = props.location!.pathname.match(/(.*)\/map\/?$/);
  if (pathMatch) {
    mapButton = {
      label: "List",
      handler: () => dispatch(push("/my/memberships"))
    };
  } else {
    mapButton = {
      label: "Map",
      handler: () => dispatch(push("/my/memberships/map"))
    };
  }
  return {
    // pass functional props from here
    triggerSearch: x => dispatch(actions.getNearbyBusinesses.request(x)),

    buttons: [
      {
        label: "Memberships",
        handler: () => dispatch(push("/my/memberships"))
      },
      {
        label: (
          <React.Fragment>
            <Icon name="md-unlock" /> Rewards
          </React.Fragment>
        ),
        handler: () => dispatch(push("/my/rewards"))
      },
      mapButton
    ],
    loadContent: action => dispatch(action!)
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps,
  (storeProps, functionProps, parentProps) => {
    const [membershipButton, ...buttons] = functionProps.buttons!;
    membershipButton.label = `${storeProps.membershipCount || ""} Membership${
      storeProps.membershipCount === 1 ? "" : "s"
    }`;
    return {
      ...storeProps,
      ...functionProps,
      ...parentProps,
      buttons: [membershipButton, ...buttons],
      loadContent: storeProps.loadAction
        ? () => functionProps.loadContent!(storeProps.loadAction)
        : undefined
    };
  }
)(Component as any);
