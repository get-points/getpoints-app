import { HeaderBar } from "@src/components/HeaderBar";
import { FixedWidthButton } from "@src/components/RectangularButton";
import { WhiteHeaderText } from "@src/components/Typography/HeaderText";
import { TitleText } from "@src/components/Typography/TitleText";
import {
  GETPOINTS_BACKGROUND_GREY,
  GETPOINTS_GREEN,
  GETPOINTS_TEAL,
  GETPOINTS_BLUE
} from "@src/const";
import React from "react";
import { Image, View } from "react-native";
import glamorous from "glamorous-native";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";

const WelcomePageContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flex: 1,
  flexDirection: "column",
  alignItems: "center"
}));

const BackgroundImage = glamorous(Image)((props: {}) => ({
  height: `100%`,
  width: `100%`,
  position: "absolute"
}));

const Content = glamorous(View)((props: {}) => ({
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  width: `100%`,
  minHeight: 490,
  flex: 1,
  display: "flex",
  flexDirection: "column",
  justifyContent: "flex-end",
  alignItems: "center"
}));

const ButtonContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  flexGrow: 0,
  marginBottom: -10
}));

const TealRectangle = glamorous(View)((props: {}) => ({
  backgroundColor: GETPOINTS_TEAL,
  flexGrow: 0,
  width: `100%`,
  padding: 10,
  paddingLeft: 20,
  paddingRight: 20,
  marginBottom: 10
}));
export class WelcomePage extends React.Component<{
  // tapTour: () => any;
  tapCheckIn: () => any;
  tapExplore: () => any;

  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <WelcomePageContainer>
        <HeaderBar />
        <Content>
          <BackgroundImage
            resizeMode="cover"
            source={{ uri: "https://placekitten.com/500/500" }}
          />
          <TealRectangle>
            <TitleText>WELCOME</TitleText>
            <WhiteHeaderText>
              TO THE GETPOINTS LOYALTY APP PROGRAM
            </WhiteHeaderText>
          </TealRectangle>
          <ButtonContainer>
            <FixedWidthButton
              topLeftSquare
              color={GETPOINTS_TEAL}
              onTap={this.props.tapCheckIn}
            >
              CHECK IN
            </FixedWidthButton>
            <FixedWidthButton
              topRightSquare
              color={GETPOINTS_GREEN}
              onTap={this.props.tapExplore}
            >
              EXPLORE
            </FixedWidthButton>
          </ButtonContainer>
          {/* <FixedWidthButton topRightSquare
            color={GETPOINTS_BLUE}
            onTap={this.props.tapTour}
          >
            TOUR
          </FixedWidthButton> */}
        </Content>
      </WelcomePageContainer>
    );
  }
}
