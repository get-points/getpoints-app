import { ErrorMessage } from "@src/components/ErrorMessage";
import { Loading } from "@src/components/Loading";
import { PopupContainer } from "@src/components/PopupContainer";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { GETPOINTS_GREEN } from "@src/const";
import React from "react";
import { ChangePasswordButton as EnterCodeButton } from "../../components/RectangularButton";
import {
  GreenHeaderText,
  HeaderText
} from "../../components/Typography/HeaderText";

export class RewardRedemptionConfirmationPopUp extends React.Component<{
  rewardValue: number;
  rewardName: string;
  tapEnterCashierCodeButton: () => any;

  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <PopupContainer>
        <HeaderText>
          You have redeemed {this.props.rewardValue} points.
        </HeaderText>
        <GreenHeaderText>For {this.props.rewardName}</GreenHeaderText>
        <ExplainerText>
          Show this reward redemption to a cashier and they will get your
          reward.
        </ExplainerText>
        <EnterCodeButton
          color={GETPOINTS_GREEN}
          topRightSquare
          onTap={this.props.tapEnterCashierCodeButton}
        >
          ENTER CASHIER CODE
        </EnterCodeButton>
      </PopupContainer>
    );
  }
}
