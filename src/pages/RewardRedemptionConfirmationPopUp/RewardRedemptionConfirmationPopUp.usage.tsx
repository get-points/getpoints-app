import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { RewardRedemptionConfirmationPopUp } from "./index";

export const basic = () => (
  <RewardRedemptionConfirmationPopUp
    rewardValue={number("rewardValue", 5)}
    rewardName={text("rewardName", "Free Coffee")}
    tapEnterCashierCodeButton={action("tapButton")}
  />
);
