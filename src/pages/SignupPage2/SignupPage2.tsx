import { HeaderBar } from "@src/components/HeaderBar";
import { FixedWidthButton } from "@src/components/RectangularButton";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { HeaderText } from "@src/components/Typography/HeaderText";
import { InputPlaceholder } from "@src/components/Typography/InputPlaceholder";
import { GETPOINTS_BACKGROUND_GREY } from "@src/const";
import React from "react";
import { ScrollView, View } from "react-native";
import glamorous from "glamorous-native";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";

const SignupPage2Container = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flex: 1
}));

const Page = glamorous(ScrollView)((props: {}) => ({
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  padding: 20,
  flexGrow: 1
}));

const PhoneNumberInputContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 1
}));

const PhonenNumberInputSubContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1,
  width: `20%`
}));

const PhonenNumberInputSubContainer2 = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1
}));

const LabelExplainerText = glamorous(ExplainerText)((props: {}) => ({
  marginBottom: 0
}));

export class SignupPage2 extends React.Component<
  {
    tapEnterButton: (email: string, countryCode: string, phone: string) => any;

    loaded?: boolean;
    error?: string;
  },
  {
    email: string;
    countryCode: string;
    phone: string;
  }
> {
  state = {
    email: "",
    countryCode: "",
    phone: ""
  };
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <SignupPage2Container>
        <HeaderBar hideMenu />

        <Page>
          <HeaderText>Account Verification</HeaderText>
          <ExplainerText>
            For password and account security verification
          </ExplainerText>
          <ExplainerText>Email address</ExplainerText>
          <InputPlaceholder
            keyboardType="email-address"
            onChange={email => this.setState({ email })}
          >
            {this.state.email}
          </InputPlaceholder>
          <PhoneNumberInputContainer>
            <PhonenNumberInputSubContainer>
              <LabelExplainerText>Country Code</LabelExplainerText>
              <InputPlaceholder
                keyboardType="phone-pad"
                maxLength={3}
                onChange={countryCode => this.setState({ countryCode })}
              >
                {this.state.countryCode}
              </InputPlaceholder>
            </PhonenNumberInputSubContainer>
            <PhonenNumberInputSubContainer2>
              <LabelExplainerText>Phone Number</LabelExplainerText>
              <InputPlaceholder
                keyboardType="phone-pad"
                maxLength={12}
                onChange={phone => this.setState({ phone })}
              >
                {this.state.phone}
              </InputPlaceholder>
            </PhonenNumberInputSubContainer2>
          </PhoneNumberInputContainer>
          <FixedWidthButton
            topRightSquare
            onTap={() =>
              this.props.tapEnterButton(
                this.state.email,
                this.state.countryCode,
                this.state.phone
              )
            }
          >
            Enter
          </FixedWidthButton>
        </Page>
      </SignupPage2Container>
    );
  }
}
