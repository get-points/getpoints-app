import { HeaderBar } from "@src/components/HeaderBar";
import { InPageMenuBar } from "@src/components/InPageMenuBar";
import { PageHeader } from "@src/components/PageHeader";
import {
  FixedWidthButton,
  RectangularButton
} from "@src/components/RectangularButton";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { InputPlaceholder } from "@src/components/Typography/InputPlaceholder";
import {
  GETPOINTS_BACKGROUND_GREY,
  GETPOINTS_RED,
  GETPOINTS_DARK_TEAL
} from "@src/const";
import React from "react";
import { ScrollView, View, Text } from "react-native";
import glamorous from "glamorous-native";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { HeaderText } from "@src/components/Typography/HeaderText";
import { string } from "prop-types";
import { LoadableState } from "@tyro";
import { Route } from "react-router";

export type StateProps = LoadableState<{
  name: string;
  email: string;
}>;

export interface FunctionProps {
  tapSendButton: (message: string, email: string, name: string) => any;
  // tapTourButton: () => any;
  tapCancelButton: () => any;
}
export interface ParentProps {}
export interface LocalProps {
  message: string;
}

const EmailSupportContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flex: 1,
  backgroundColor: GETPOINTS_BACKGROUND_GREY
}));

const PageWrapper = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1,
  padding: 10
}));

const Row = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  alignItems: "flex-start",
  justifyContent: "center"
}));

const Label = glamorous(ExplainerText)((props: {}) => ({
  marginTop: 18,
  width: 100
}));

const PopUpButton = glamorous(RectangularButton)((props: {}) => ({
  fontSize: 12,
  flexGrow: 0,
  width: 100
}));
// const TourBtn = glamorous(RectangularButton)((props: {}) => ({
//   fontSize: 12,
//   flexGrow: 0,
//   width: 100,
//   backgroundColor: GETPOINTS_DARK_TEAL
// }));

export class EmailSupport extends React.Component<
  FunctionProps & StateProps & ParentProps,
  LocalProps
> {
  state = {
    message: ""
  };

  getConfirmationPopup = () => {
    return <HeaderText>Message sent successfully</HeaderText>;
  };

  render() {
    if (this.props.loading) return <Loading />;
    if (this.props.error !== undefined)
      return <ErrorMessage message={this.props.error} />;

    const { email, name } = this.props;

    return (
      <EmailSupportContainer>
        <HeaderBar />
        <PageHeader>Email Support</PageHeader>
        <ScrollView>
          <InPageMenuBar selected="support" />
          <PageWrapper>
            <Route
              exact
              path="/about/support/success"
              render={this.getConfirmationPopup}
            />
            <Route
              exact
              path="/about/support"
              render={() => {
                return (
                  <React.Fragment>
                    <HeaderText>Enter your message below</HeaderText>
                    <Row>
                      <InputPlaceholder
                        numberOfLines={6}
                        onChange={message => this.setState({ message })}
                      >
                        {this.state.message}
                      </InputPlaceholder>
                    </Row>
                    <Row>
                      <PopUpButton
                        color={GETPOINTS_RED}
                        topLeftSquare
                        onTap={this.props.tapCancelButton}
                      >
                        CANCEL
                      </PopUpButton>
                      <PopUpButton
                        topRightSquare
                        onTap={() =>
                          this.props.tapSendButton(
                            this.state.message,
                            email,
                            name
                          )
                        }
                      >
                        SEND
                      </PopUpButton>
                    </Row>
                  </React.Fragment>
                );
              }}
            />
            {/* <Row> <TourBtn topRightSquare onTap={this.props.tapTourButton}>
                TAKE A TOUR
              </TourBtn>
            </Row> */}
          </PageWrapper>
        </ScrollView>
      </EmailSupportContainer>
    );
  }
}
