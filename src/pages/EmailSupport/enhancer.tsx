import { State } from "@src/store";
import { goBack, push } from "connected-react-router";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { Dispatch } from "redux";

import {
  EmailSupport,
  StateProps,
  FunctionProps,
  ParentProps,
  LocalProps
} from "./EmailSupport";
import { actions } from "@src/actions";
import { getOnlyUser } from "@src/store/user/selectors";

function mapStateToProps(state: State, props: ParentProps): StateProps {
  const loadableUser = getOnlyUser(state);
  if (loadableUser.state === "FAILED") return { error: loadableUser.message };
  if (loadableUser.state === "ABSENT") return { loading: true };
  if (loadableUser.state === "PENDING") return { loading: true };
  const user = loadableUser.item;

  return {
    email: user.email,
    name: user.firstName + " " + user.lastName
  };
}

function mapDispatchToProps(
  dispatch: Dispatch,
  props: ParentProps
): FunctionProps {
  return {
    tapSendButton: (message: string, email: string, name: string) => {
      return dispatch(
        actions.sendSupportRequest.request({ message, email, name })
      );
    },
    // tapTourButton: () => dispatch(push("/tour")),
    tapCancelButton: () => dispatch(push("/about"))
  };
}

function mergeProps(
  stateProps: ReturnType<typeof mapStateToProps>,
  dispatchProps: ReturnType<typeof mapDispatchToProps>,
  ownProps: ParentProps
): EmailSupport["props"] {
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps
  };
}

export const Container = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(EmailSupport as any);
