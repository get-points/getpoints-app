import { State } from "@src/store";
import { goBack, push } from "connected-react-router";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { Dispatch } from "redux";

import { Faqs } from "./Faqs";

interface OwnProps extends RouteComponentProps {}

function mapStateToProps(state: State, props: OwnProps) {
  return {};
}

function mapDispatchToProps(dispatch: Dispatch, props: OwnProps) {
  return {};
}

function mergeProps(
  stateProps: ReturnType<typeof mapStateToProps>,
  dispatchProps: ReturnType<typeof mapDispatchToProps>
): Faqs["props"] {
  return {};
}

export const ConnectedFaqs = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Faqs);
