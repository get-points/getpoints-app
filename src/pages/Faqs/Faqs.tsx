import { HeaderBar } from "@src/components/HeaderBar";
import { InPageMenuBar } from "@src/components/InPageMenuBar";
import { PageHeader } from "@src/components/PageHeader";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { HeaderText } from "@src/components/Typography/HeaderText";
import { GETPOINTS_BACKGROUND_GREY } from "@src/const";
import React from "react";
import { ScrollView, View } from "react-native";
import glamorous from "glamorous-native";

const FaqsContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flex: 1
}));
const Page = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1,
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  padding: 10,
  paddingRight: 20,
  paddingLeft: 15
}));
const LeftAlignedHeaderText = glamorous(HeaderText)((props: {}) => ({
  textAlign: "left" as "left"
}));
const LeftAlignedExplainerText = glamorous(ExplainerText)((props: {}) => ({
  textAlign: "left" as "left"
}));

export class Faqs extends React.Component<{}> {
  render() {
    return (
      <FaqsContainer>
        <HeaderBar />
        <PageHeader>FAQs</PageHeader>
        <ScrollView>
          <InPageMenuBar selected="faq" />
          <Page>
            <LeftAlignedHeaderText>
              <HeaderText>How can I redeem my rewards?</HeaderText>
            </LeftAlignedHeaderText>
            <LeftAlignedExplainerText>
              <ExplainerText>
                With our easy to use tablet and app you can view how many points
                you have from the merchant and decide when you want to redeem
                your points to claim the rewards. Simply click on the "unlocked"
                reward you want and follow the pop up instructions on the tablet
                or app.
              </ExplainerText>
            </LeftAlignedExplainerText>
            <LeftAlignedHeaderText>
              <HeaderText>How do I earn rewards?</HeaderText>
            </LeftAlignedHeaderText>
            <LeftAlignedExplainerText>
              <ExplainerText>
                Simply CHECK-IN through the tablet at the business or check-in
                through our mobile app at indicated restaurants.When you have
                enough check-in's you can redeem the rewards on the tablet or on
                our app.
              </ExplainerText>
            </LeftAlignedExplainerText>
            <LeftAlignedHeaderText>
              <HeaderText>How do I get points?</HeaderText>
            </LeftAlignedHeaderText>
            <LeftAlignedExplainerText>
              <ExplainerText>
                You get points by checking-in on tablets or our mobile app every
                time you visit your favorite places or businesses. The more
                points you accumulate the better the reward.
              </ExplainerText>
            </LeftAlignedExplainerText>
            <LeftAlignedHeaderText>
              <HeaderText>
                How many points do I need to earn a reward?
              </HeaderText>
            </LeftAlignedHeaderText>
            <LeftAlignedExplainerText>
              <ExplainerText>
                You can view all your loyalty points earned and how many more
                you need to start getting a reward on our app or by simply log
                into your account and clicking on the My Reward button.
              </ExplainerText>
            </LeftAlignedExplainerText>
            <LeftAlignedHeaderText>
              <HeaderText>How do I gift a reward?</HeaderText>
            </LeftAlignedHeaderText>
            <LeftAlignedExplainerText>
              <ExplainerText>
                When you get a reward, instead of redeeming it, you can gift it
                to someone of your choice. Go to Gifted Rewards on your account
                or app to learn more.
              </ExplainerText>
            </LeftAlignedExplainerText>
          </Page>
        </ScrollView>
      </FaqsContainer>
    );
  }
}
