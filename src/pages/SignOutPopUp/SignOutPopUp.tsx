import { GETPOINTS_RED, GETPOINTS_TEAL } from "@src/const";
import React from "react";
import { View } from "react-native";
import glamorous from "glamorous-native";

import { FixedWidthButton } from "../../components/RectangularButton";
import { WhiteHeaderText } from "../../components/Typography/HeaderText";
import { HeaderBar } from "@src/components/HeaderBar";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { PopupContainer } from "@src/components/PopupContainer";

const ButtonContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  flexGrow: 0
}));

const Page = glamorous(View)((props: {}) => ({
  backgroundColor: GETPOINTS_TEAL,
  padding: 20,
  flexGrow: 1,
  display: "flex",
  flexDirection: "column",
  alignItems: "center"
}));

export class SignOutPopUp extends React.Component<{
  tapNoButton: () => any;
  tapYesButton: () => any;

  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <PopupContainer hideFrame>
        <HeaderBar hideMenu />
        <Page>
          <WhiteHeaderText>Are you sure you want to sign out?</WhiteHeaderText>
          <ButtonContainer>
            <FixedWidthButton
              topLeftSquare
              color={GETPOINTS_RED}
              onTap={this.props.tapNoButton}
            >
              NO
            </FixedWidthButton>
            <FixedWidthButton topRightSquare onTap={this.props.tapYesButton}>
              YES
            </FixedWidthButton>
          </ButtonContainer>
        </Page>
      </PopupContainer>
    );
  }
}
