import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { SignOutPopUp } from "./index";

export const basic = () => (
  <SignOutPopUp
    tapNoButton={action("tapButton")}
    tapYesButton={action("tapButton")}
  />
);
