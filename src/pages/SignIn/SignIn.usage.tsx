import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { SignIn } from "./index";
import { string } from "prop-types";

export const basic = () => <SignIn />;
