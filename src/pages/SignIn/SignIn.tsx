import React from "react";
import { Text, View, ViewStyle, ScrollView } from "react-native";
import glamorous from "glamorous-native";
import { HeaderBar } from "@src/components/HeaderBar";
import { InputPlaceholder } from "@src/components/Typography/InputPlaceholder";
import {
  RectangularButton,
  FixedWidthButton
} from "@src/components/RectangularButton";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { GETPOINTS_TEAL, GETPOINTS_BACKGROUND_GREY } from "@src/const";
import { HeaderText } from "@src/components/Typography/HeaderText";

const SignInContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flex: 1
}));

const ButtonContainer = glamorous(View)((props: {}) => ({
  flexDirection: "row",
  justifyContent: "center"
}));

const Page = glamorous(ScrollView)((props: {}) => ({
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  padding: 20
}));

export class SignIn extends React.Component<
  {
    signIn: (x: { email: string; password: string }) => any;
    signUp: () => any;

    loaded?: boolean;
    error?: string;
  },
  {
    email: string;
    password: string;
  }
> {
  state = {
    email: "",
    password: ""
  };
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <SignInContainer>
        <HeaderBar hideMenu />
        <Page>
          <HeaderText>Sign In</HeaderText>
          <InputPlaceholder
            placeholder="email"
            keyboardType="email-address"
            onChange={email => this.setState({ email })}
          >
            {this.state.email}
          </InputPlaceholder>
          <InputPlaceholder
            placeholder="password"
            secureTextEntry
            onChange={password => this.setState({ password })}
          >
            {this.state.password}
          </InputPlaceholder>
          <ButtonContainer>
            <FixedWidthButton
              onTap={() => this.props.signIn(this.state)}
              topLeftSquare
            >
              SIGN IN
            </FixedWidthButton>
            <FixedWidthButton
              onTap={() => this.props.signUp()}
              color={GETPOINTS_TEAL}
              topRightSquare
            >
              SIGN UP
            </FixedWidthButton>
          </ButtonContainer>
        </Page>
      </SignInContainer>
    );
  }
}
