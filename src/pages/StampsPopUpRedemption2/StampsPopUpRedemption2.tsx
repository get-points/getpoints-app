import React from "react";
import { Text, View, ViewStyle } from "react-native";
import glamorous from "glamorous-native";
import { PageHeader } from "@src/components/PageHeader";
import { HeaderText } from "@src/components/Typography/HeaderText";
import {
  ExplainerText,
  InputExplainerText
} from "@src/components/Typography/ExplainerText";
import { Placeholder } from "@src/components/Placeholder";
import { InputPlaceholder } from "@src/components/Typography/InputPlaceholder";
import { RectangularButton } from "@src/components/RectangularButton";
import {
  GETPOINTS_WHITE,
  GETPOINTS_RED,
  GETPOINTS_BACKGROUND_GREY
} from "@src/const";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { HeaderBar } from "@src/components/HeaderBar";
import { PopupContainer } from "@src/components/PopupContainer";

const StampsPopUpRedemption2Container = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column"
}));
const PopupElements = glamorous(View)((props: {}) => ({
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  textAlign: "center",
  alignItems: "center",
  padding: 20
}));
const RewardRedemptionText = glamorous(HeaderText)((props: {}) => ({
  fontSize: 16,
  fontWeight: "600"
}));
const RewardRedemptionTextUser = glamorous(HeaderText)((props: {}) => ({
  fontSize: 16
}));
const RuleExplainerText = glamorous(ExplainerText)((props: {}) => ({
  fontSize: 14,
  textAlign: "left"
}));
const Rule = glamorous(ExplainerText)((props: {}) => ({
  fontSize: 14,
  textAlign: "left"
}));
const EnterCodeText = glamorous(ExplainerText)((props: {}) => ({
  fontSize: 16
}));
const PinCode = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  maxWidth: `100%`
}));

const CashierCodeInput = glamorous(InputPlaceholder)((props: {}) => ({
  width: 54,
  height: 40
}));
const ManagerCode = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  minWidth: `80%`
}));
const ManagerNameInput = glamorous(InputPlaceholder)((props: {}) => ({
  height: 40
}));
const ButtonsArea = glamorous(View)((props: {}) => ({
  display: "flex",
  justifyContent: "center",
  flexDirection: "row",
  minWidth: 200
}));
const PopUpButton = glamorous(RectangularButton)((props: {}) => ({
  width: 100,
  fontSize: 12
}));

export class StampsPopUpRedemption2 extends React.Component<
  {
    style?: ViewStyle;
    customerName: string;
    stampsNumber: number;
    rewardName: string;
    rewardNumber: number;
    stampRules: string;
    tapCancelButton: () => void;
    tapConfirmButton: (x: {
      digitOne: string;
      digitTwo: string;
      digitThree: string;
      digitFour: string;
      managerName: string;
    }) => any;

    loaded?: boolean;
    error?: string;
  },
  {
    digitOne: string;
    digitTwo: string;
    digitThree: string;
    digitFour: string;
    managerName: string;
  }
> {
  state = {
    digitOne: "",
    digitTwo: "",
    digitThree: "",
    digitFour: "",
    managerName: ""
  };

  setManagerName = (managerName: string) => this.setState({ managerName });
  setDigitOne = (digitOne: string) => this.setState({ digitOne });
  setDigitTwo = (digitTwo: string) => this.setState({ digitTwo });
  setDigitThree = (digitThree: string) => this.setState({ digitThree });
  setDigitFour = (digitFour: string) => this.setState({ digitFour });
  approve = () => this.props.tapConfirmButton(this.state);

  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }

    return (
      <PopupContainer title="Manager Approval to Redeem Stamps">
        <RewardRedemptionTextUser>
          {this.props.customerName} wants to redeem {this.props.stampsNumber}{" "}
          stamps for {this.props.rewardNumber} {this.props.rewardName}
          {this.props.rewardNumber === 1 ? "" : "s"}
        </RewardRedemptionTextUser>
        <RuleExplainerText>Stamp Rules:</RuleExplainerText>
        <Rule>{this.props.stampRules}</Rule>
        <EnterCodeText>Manager, Enter the 4-digit code</EnterCodeText>
        <PinCode>
          <CashierCodeInput
            children={this.state.digitOne}
            onChange={this.setDigitOne}
            keyboardType="number-pad"
            maxLength={1}
          />
          <CashierCodeInput
            children={this.state.digitTwo}
            onChange={this.setDigitTwo}
            keyboardType="number-pad"
            maxLength={1}
          />
          <CashierCodeInput
            children={this.state.digitThree}
            onChange={this.setDigitThree}
            keyboardType="number-pad"
            maxLength={1}
          />
          <CashierCodeInput
            children={this.state.digitFour}
            onChange={this.setDigitFour}
            keyboardType="number-pad"
            maxLength={1}
          />
        </PinCode>
        <ManagerCode>
          <ManagerNameInput
            children={this.state.managerName}
            onChange={this.setManagerName}
          />
        </ManagerCode>
        <ButtonsArea>
          <PopUpButton
            topLeftSquare
            color={GETPOINTS_RED}
            onTap={this.props.tapCancelButton}
          >
            CANCEL
          </PopUpButton>
          <PopUpButton topRightSquare onTap={this.approve}>
            CONFIRM
          </PopUpButton>
        </ButtonsArea>
      </PopupContainer>
    );
  }
}
