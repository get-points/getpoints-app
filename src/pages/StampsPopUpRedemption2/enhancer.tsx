import { StampsPopUpRedemption2 as Component } from "./StampsPopUpRedemption2";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State } from "@src/store";
import { actions, Action } from "@src/actions";
import {} from "@src/store/selectors";
import { getStampProgramByID } from "@src/store/stampProgram/selectors";
import { getOnlyUser } from "@src/store/user/selectors";
import { PopupType } from "@src/actions/ui";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]> & {
    stampProgramID: string;
    stampsRedeemed: number;
    rewardNumber: number;
  }
): Partial<Component["props"]> & { loadAction?: Action } {
  try {
    // prepare data here
    const stampProgramID = props.stampProgramID;
    const loadableStampProgram = getStampProgramByID(store, stampProgramID);
    if (loadableStampProgram.state === "ABSENT")
      return {
        loaded: false,
        loadAction: actions.getReward.request({ id: stampProgramID })
      };
    if (loadableStampProgram.state === "PENDING") return { loaded: false };
    if (loadableStampProgram.state === "FAILED")
      return { error: loadableStampProgram.message };
    const stampProgram = loadableStampProgram.item;

    const loadableUser = getOnlyUser(store);
    if (loadableUser.state === "ABSENT")
      return {
        loaded: false
      };
    if (loadableUser.state === "PENDING") return { loaded: false };
    if (loadableUser.state === "FAILED") return { error: loadableUser.message };
    const user = loadableUser.item;

    return {
      // pass data props from here
      customerName: user.firstName,
      stampsNumber: props.stampsRedeemed,
      rewardName: stampProgram.headline,
      rewardNumber: props.rewardNumber,
      stampRules: stampProgram.description
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]> & {
    stampProgramID: string;
    rewardNumber: number;
    stampsRedeemed: number;
  }
): Partial<Component["props"]> {
  return {
    // pass functional props from here
    tapCancelButton: () => dispatch(actions.hidePopup()),
    tapConfirmButton: () =>
      dispatch(
        actions.showPopup({
          popup: {
            type: PopupType.STAMPS_REDEMPTION_CONFIRMATION,
            data: {
              stampProgramID: props.stampProgramID,
              stampsRedeemed: props.stampsRedeemed,
              rewardNumber: props.rewardNumber
            }
          }
        })
      )
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
