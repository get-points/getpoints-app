import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { StampsPopUpRedemption2 } from "./StampsPopUpRedemption2";

export const basic = () => (
  <StampsPopUpRedemption2
    customerName={text("customerName", "Joe Blogs")}
    rewardName={text("rewardName", "free coffee")}
    tapCancelButton={action("tapButton")}
    tapConfirmButton={action("tapButton")}
    stampRules={text("stampsRules", "must be a member")}
    stampsNumber={number("stampsNumber", 5)}
    rewardNumber={number("stampsNumber", 1)}
  />
);
