import { SettingsLaunch as Component } from "./SettingsLaunch";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State } from "@src/store";
import { actions } from "@src/actions";
import {} from "@src/store/selectors";
import { push } from "connected-react-router";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  try {
    // prepare data here

    return {
      // pass data props from here
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  return {
    // pass functional props from here
    tapChangeYourPasswordButton: () =>
      dispatch(push("/settings/change-password")),
    tapEditYourNotificationSettingsButton: () =>
      dispatch(push("/settings/notifications")),
    tapEditYourProfileButton: () => dispatch(push("/settings/edit-profile"))
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
