import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { SettingsLaunch } from "./index";

export const basic = () => (
  <SettingsLaunch
    tapChangeYourPasswordButton={action("tap to change")}
    tapEditYourNotificationSettingsButton={action("tap to edit")}
    tapEditYourProfileButton={action("tap to edit")}
  />
);
