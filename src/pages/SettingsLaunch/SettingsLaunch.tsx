import React from "react";
import { Text, View, ScrollView } from "react-native";
import glamorous from "glamorous-native";
import { HeaderBar } from "@src/components/HeaderBar";
import { PageHeader } from "@src/components/PageHeader";
import { WhiteButton, WhiteButtonGreen } from "@src/components/WhiteButton";
import { GETPOINTS_BACKGROUND_GREY } from "@src/const";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";

const SettingsLaunchContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flex: 1,
  flexDirection: "column"
}));

const Page = glamorous(ScrollView)((props: {}) => ({
  padding: 20,
  backgroundColor: GETPOINTS_BACKGROUND_GREY
}));

const SpacedWhiteButtonGreen = glamorous(WhiteButtonGreen)((props: {}) => ({
  marginTop: 5,
  marginBottom: 5
}));
export class SettingsLaunch extends React.Component<{
  tapEditYourProfileButton: () => any;
  tapChangeYourPasswordButton: () => any;
  tapEditYourNotificationSettingsButton: () => any;
  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <SettingsLaunchContainer>
        <HeaderBar />
        <PageHeader>Settings</PageHeader>
        <Page>
          <SpacedWhiteButtonGreen onTap={this.props.tapEditYourProfileButton}>
            Edit Your Profile
          </SpacedWhiteButtonGreen>
          <SpacedWhiteButtonGreen
            onTap={this.props.tapChangeYourPasswordButton}
          >
            Change Your Password
          </SpacedWhiteButtonGreen>
          <SpacedWhiteButtonGreen
            onTap={this.props.tapEditYourNotificationSettingsButton}
          >
            Edit Your Notification Settings
          </SpacedWhiteButtonGreen>
        </Page>
      </SettingsLaunchContainer>
    );
  }
}
