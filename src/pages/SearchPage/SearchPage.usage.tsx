import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { SearchPage } from "./index";
import { Text } from "react-native";
import { Icon } from "@src/components/Icon";

export const basic = () => (
  <SearchPage
    baseURL="/nearby/places"
    triggerSearch={action("Searched")}
    listings={<Text>Listings go here</Text>}
    pageTitle={text("pageTitle", "Nearby Places")}
    mapPins={[
      {
        description: "The first pink",
        latitude: 0,
        longitude: 0,
        title: "Pin 1"
      }
    ]}
    buttons={[
      {
        handler: action("Tapped Memberships"),
        label: "Memberships"
      },
      {
        handler: action("Tapped Rewards"),
        label: (
          <React.Fragment>
            <Icon name="md-unlock" /> Rewards
          </React.Fragment>
        )
      },
      {
        handler: action("Tapped Map"),
        label: "Map"
      }
    ]}
  />
);
