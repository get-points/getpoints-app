import { ErrorMessage } from "@src/components/ErrorMessage";
import { HeaderBar } from "@src/components/HeaderBar";
import { ListingSearchBar } from "@src/components/ListingSearchBar";
import { Loading } from "@src/components/Loading";
import { MapWrapper } from "@src/components/MapWrapper";
import { PageHeader } from "@src/components/PageHeader";
import { GETPOINTS_BACKGROUND_GREY } from "@src/const";
import glamorous from "glamorous-native";
import React, { ReactNode } from "react";
import { ScrollView, View, KeyboardAvoidingView } from "react-native";
import { Route, RouteComponentProps, Switch } from "react-router";

const SearchPageContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flex: 1,
  flexDirection: "column"
}));

const Page = glamorous(KeyboardAvoidingView)((props: {}) => ({
  padding: 10,

  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  display: "flex",
  flexDirection: "column",
  flexGrow: 1
  // alignItems: "stretch"
}));

export class SearchPage extends React.Component<
  {
    triggerSearch: (x: {
      city?: string;
      country?: string;
      type?: string;
      text?: string;
    }) => any;
    pageTitle: string;
    baseURL: string;
    listings: React.ReactChild;
    mapPins: {
      latitude: number;
      longitude: number;
      title: string;
      description: string;
    }[];
    loaded?: boolean;
    error?: string;
    buttons: {
      label: ReactNode;
      handler: () => any;
    }[];
    selectedButton: number;
  },
  {
    country: string;
    city: string;
    type: string;
    text: string;
  }
> {
  search = (state: {
    country: string;
    city: string;
    type: string;
    text: string;
  }) => {
    this.setState(state);
    this.props.triggerSearch(state);
  };

  listings = () => {
    if (this.props.loaded === false) return <Loading />;
    if (this.props.error !== undefined)
      return <ErrorMessage message={this.props.error} />;

    return <ScrollView>{this.props.listings}</ScrollView>;
  };

  map = () => {
    if (this.props.loaded === false) return <Loading />;
    if (this.props.error !== undefined)
      return <ErrorMessage message={this.props.error} />;

    return <MapWrapper markers={this.props.mapPins} />;
  };

  render() {
    return (
      <SearchPageContainer>
        <HeaderBar />
        <PageHeader>{this.props.pageTitle}</PageHeader>
        <Page>
          <ListingSearchBar
            {...this.state}
            onTapSearch={this.search}
            buttons={this.props.buttons}
            selectedButton={this.props.selectedButton}
          />
          <Switch>
            <Route path={this.props.baseURL + "/map"} render={this.map} />
            <Route path={this.props.baseURL} render={this.listings} />
          </Switch>
        </Page>
      </SearchPageContainer>
    );
  }
}
