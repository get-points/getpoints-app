import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { ConfirmationJoiningBusinessPopUp } from "./index";

export const basic = () => (
  <ConfirmationJoiningBusinessPopUp
    businessName={text("businessName", "The Pantry")}
    isVisible={boolean("is pop up visible", true)}
    tapDoneButton={action("tapped button")}
  />
);
