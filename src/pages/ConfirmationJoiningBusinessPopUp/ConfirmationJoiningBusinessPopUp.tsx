import { GETPOINTS_BACKGROUND_GREY } from "@src/const";
import React from "react";
import { View } from "react-native";
import glamorous from "glamorous-native";

import { PageHeader } from "../../components/PageHeader";
import {
  FixedWidthButton,
  RectangularButton
} from "../../components/RectangularButton";
import { HeaderText } from "../../components/Typography/HeaderText";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";

const ConfirmationJoiningBusinessPopUpContainer = glamorous(View)(
  (props: {}) => ({
    display: "flex",
    flex: 1,
    flexDirection: "column"
  })
);

const Page = glamorous(View)((props: {}) => ({
  padding: 20,
  backgroundColor: GETPOINTS_BACKGROUND_GREY
}));
export class ConfirmationJoiningBusinessPopUp extends React.Component<{
  businessName: string;
  tapDoneButton: () => any;

  isVisible: boolean;

  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    if (this.props.isVisible === true) {
      return (
        <ConfirmationJoiningBusinessPopUpContainer>
          <PageHeader>{this.props.businessName}</PageHeader>
          <Page>
            <HeaderText>
              Congratulations you have joined ({this.props.businessName})’s
              Reward Program!
            </HeaderText>
            <FixedWidthButton topRightSquare onTap={this.props.tapDoneButton}>
              DONE
            </FixedWidthButton>
          </Page>
        </ConfirmationJoiningBusinessPopUpContainer>
      );
    } else {
      return null;
    }
  }
}
