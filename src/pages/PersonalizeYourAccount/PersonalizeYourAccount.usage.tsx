import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { PersonalizeYourAccount } from "./";

export const basic = () => (
  <PersonalizeYourAccount
    dateOfBirth={{
      month: text("month", "June"),
      day: text("day", "5th"),
      year: text("year", "2018")
    }}
    gender={text("gender", "female")}
    profilephoto={text("profilePhoto", "https://placekitten.com/100/100")}
    tapEnterButton={action("buttonTap")}
    tapSkipThisButton={action("buttonTap")}
  />
);
