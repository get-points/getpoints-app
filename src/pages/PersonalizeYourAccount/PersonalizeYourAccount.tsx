import React from "react";
import { Text, View, ScrollView } from "react-native";
import glamorous from "glamorous-native";
import { Placeholder } from "@src/components/Placeholder";
import { HeaderBar } from "@src/components/HeaderBar";
import { HeaderText } from "@src/components/Typography/HeaderText";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { Icon } from "@src/components/Icon";
import {
  RectangularButton,
  FixedWidthButton
} from "@src/components/RectangularButton";
import { InputPlaceholder } from "@src/components/Typography/InputPlaceholder";
import { GETPOINTS_BACKGROUND_GREY, GETPOINTS_RED } from "@src/const";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { Loading } from "@src/components/Loading";

const PersonalizeYourAccountContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flex: 1,
  flexDirection: "column"
}));

const Page = glamorous(ScrollView)((props: {}) => ({
  padding: 20,
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  display: "flex",
  flexDirection: "column",
  alignItems: "stretch"
}));

const DateInputContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  maxWidth: `100%`,
  flexGrow: 0
}));
const ButtonsContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  flexGrow: 0,
  flexShrink: 1,
  marginTop: 10
}));

const SelectGenderContainer = glamorous(View)((props: {}) => ({
  alignSelf: "center",
  display: "flex",
  flexDirection: "row"
}));

const ProfilePicture = glamorous(Icon)((props: {}) => ({
  alignSelf: "center"
}));

const MaleContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  marginRight: 20
}));
const FemaleContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  marginLeft: 20
}));

const ExplainerTextWithMargin = glamorous(ExplainerText)((props: {}) => ({
  marginLeft: 10
}));
export class PersonalizeYourAccount extends React.Component<{
  profilephoto: string;
  dateOfBirth: {
    day: string;
    month: string;
    year: string;
  };
  gender: string;
  tapSkipThisButton: () => any;
  tapEnterButton: () => any;
  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <PersonalizeYourAccountContainer>
        <HeaderBar />
        <Page>
          <HeaderText>Personalize Your Account</HeaderText>
          <ExplainerText>
            Create a profile for a customized reward experience, add your photo
            and birthday!
          </ExplainerText>
          <ProfilePicture name="ios-person" size={150} />
          <ExplainerText>
            Please enter your birthdate so your favourite loyalty businesses can
            send you gifts.
          </ExplainerText>
          <DateInputContainer>
            <InputPlaceholder>{this.props.dateOfBirth.day}</InputPlaceholder>
            <InputPlaceholder>{this.props.dateOfBirth.month}</InputPlaceholder>
            <InputPlaceholder>{this.props.dateOfBirth.year}</InputPlaceholder>
          </DateInputContainer>
          <ExplainerText>
            Please share your gender with us so we can better recommend specific
            nearby merchants for you.
          </ExplainerText>
          <SelectGenderContainer>
            <MaleContainer>
              <Icon name="ios-radio-button-off" size={20} />
              <ExplainerTextWithMargin> Male </ExplainerTextWithMargin>
            </MaleContainer>
            <FemaleContainer>
              <Icon name="ios-radio-button-on" size={20} />
              <ExplainerTextWithMargin> Female </ExplainerTextWithMargin>
            </FemaleContainer>
          </SelectGenderContainer>
          <ButtonsContainer>
            {/* <FixedWidthButton
              topLeftSquare
              color={GETPOINTS_RED}
              onTap={this.props.tapSkipThisButton}
            >
              SKIP THIS
            </FixedWidthButton> */}
            <FixedWidthButton topRightSquare onTap={this.props.tapEnterButton}>
              SAVE
            </FixedWidthButton>
          </ButtonsContainer>
        </Page>
      </PersonalizeYourAccountContainer>
    );
  }
}
