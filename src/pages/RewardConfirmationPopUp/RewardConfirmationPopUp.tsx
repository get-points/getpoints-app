import { GETPOINTS_WHITE } from "@src/const";
import React from "react";
import { View, ScrollView } from "react-native";
import glamorous from "glamorous-native";

import { FixedWidthButton } from "../../components/RectangularButton";
import { HeaderText } from "../../components/Typography/HeaderText";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";

const RewardConfirmationPopUpContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 1,
  backgroundColor: GETPOINTS_WHITE,
  textAlign: "center",
  padding: 20
}));

const ElementsContainer = glamorous(ScrollView)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1
}));

export class RewardConfirmationPopUp extends React.Component<{
  tapDoneButton: () => any;

  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <RewardConfirmationPopUpContainer>
        <ElementsContainer>
          <HeaderText>Enjoy your reward</HeaderText>
          <FixedWidthButton topRightSquare onTap={this.props.tapDoneButton}>
            DONE
          </FixedWidthButton>
        </ElementsContainer>
      </RewardConfirmationPopUpContainer>
    );
  }
}
