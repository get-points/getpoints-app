import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { RewardConfirmationPopUp } from "./index";

export const basic = () => (
  <RewardConfirmationPopUp tapDoneButton={action("tapButton")} />
);
