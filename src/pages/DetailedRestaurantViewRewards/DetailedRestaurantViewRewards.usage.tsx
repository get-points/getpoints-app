import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { DetailedRestaurantViewRewards } from "./index";

export const basic = () => (
  <DetailedRestaurantViewRewards
    coverPhoto="http://placekitten.com/500/100"
    profilePicture="http://placekitten.com/100/100"
    businessID="42"
    availablePoints={number("availablePoints", 5)}
    myVisits={number("myVisits", 5)}
    redeemedPoints={number("redeemedPoints", 5)}
    businessName={text("businessName", "The Pantry")}
    isMember={boolean("isMember", true)}
    offers={[]}
    rewards={[]}
    stamps={[]}
    tapJoinButton={action("tapJoinButton")}
  />
);
