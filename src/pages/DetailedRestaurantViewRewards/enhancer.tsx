import { DetailedRestaurantViewRewards as Component } from "./DetailedRestaurantViewRewards";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State } from "@src/store";
import { actions, Action } from "@src/actions";
import {} from "@src/store/selectors";
import { getOnlyUser } from "@src/store/user/selectors";
import { getMembershipListByIDs } from "@src/store/membership/selectors";
import { getBusinessByID } from "@src/store/business/selectors";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]>
): Partial<Component["props"]> & { loadAction?: Action } {
  try {
    // prepare data here
    const loadableUser = getOnlyUser(store);
    if (loadableUser.state === "ABSENT" || loadableUser.state === "PENDING")
      return { loaded: false };
    if (loadableUser.state === "FAILED") return { error: loadableUser.message };
    const user = loadableUser.item;

    const loadableBusiness = getBusinessByID(store, props.businessID!);
    if (loadableBusiness.state === "ABSENT")
      return {
        loaded: false,
        loadAction: actions.getBusiness.request({ id: props.businessID! })
      };
    if (loadableBusiness.state === "PENDING") return { loaded: false };
    if (loadableBusiness.state === "FAILED")
      return { error: loadableBusiness.message };
    const business = loadableBusiness.item;

    const loadableMemberships = getMembershipListByIDs(store, user.memberships);
    if (loadableMemberships.state === "ABSENT")
      return {
        loaded: false,
        loadAction: actions.getMembershipList.request({
          ids: loadableMemberships.ids
        })
      };
    if (loadableMemberships.state === "PENDING") return { loaded: false };
    if (loadableMemberships.state === "FAILED")
      return { error: loadableMemberships.message };
    const membership = loadableMemberships.item.find(
      m => m.businessID === props.businessID
    );

    if (membership) {
      return {
        isMember: true,
        businessID: props.businessID,
        availablePoints: membership.points,
        myVisits: membership.visits,
        redeemedPoints: membership.visits - membership.points,
        businessName: business.name,
        offers: business.offers,
        stamps: business.stampPrograms.map(id => {
          const membershipStamp = membership.stamps.find(
            x => x.stampProgramID === id
          );
          if (membershipStamp) {
            return {
              id,
              points: membershipStamp.stampCount
            };
          } else {
            return {
              id,
              points: 0
            };
          }
        }),
        rewards: business.membershipRewards
      };
    } else {
      return {
        isMember: false,
        businessID: props.businessID,
        availablePoints: 0,
        myVisits: 0,
        redeemedPoints: 0,
        businessName: business.name,
        offers: business.offers,
        stamps: business.stampPrograms.map(id => ({
          id,
          points: 0
        })),
        rewards: business.membershipRewards
      };
    }
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  return {
    // pass functional props from here
    tapJoinButton: () => {},
    loadContent: action => dispatch(action!)
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps,

  (storeProps, functionProps, parentProps) => {
    return {
      ...storeProps,
      ...functionProps,
      ...parentProps,
      loadContent: storeProps.loadAction
        ? () => functionProps.loadContent!(storeProps.loadAction)
        : undefined
    };
  }
)(Component as any);
