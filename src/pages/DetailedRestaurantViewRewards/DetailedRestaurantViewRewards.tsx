import { GETPOINTS_BACKGROUND_GREY } from "@src/const";
import React from "react";
import { View, ScrollView } from "react-native";
import glamorous from "glamorous-native";

import { BusinessHeading } from "../../components/BusinessHeading";
import { HeaderBar } from "../../components/HeaderBar";
import { InPageMenuBarGreen } from "../../components/InPageMenuBarGreen";
import { OfferTile } from "../../components/OfferTile";
import { PageHeader } from "../../components/PageHeader";
import { PointStatus } from "../../components/PointStatus";
import { RewardTile } from "../../components/RewardTile";
import { StampsTile } from "../../components/StampsTile";
import { ExplainerText } from "../../components/Typography/ExplainerText";
import { HeaderText } from "../../components/Typography/HeaderText";
import { RectangularButton } from "@src/components/RectangularButton";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { Action } from "@src/actions";

const DetailedRestaurantViewRewardsContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flex: 1,
  backgroundColor: GETPOINTS_BACKGROUND_GREY
}));
const PageWrapper = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flexGrow: 1,
  padding: 10
}));
const PointStatusContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "row",
  flexGrow: 1
}));
const LeftAlignedHeaderText = glamorous(HeaderText)((props: {}) => ({
  display: "flex" as "flex",
  flexDirection: "row" as "row",
  marginTop: 20,
  flexGrow: 1,
  textAlign: "left" as "left"
}));
const LeftAlignedExplainerText = glamorous(ExplainerText)((props: {}) => ({
  display: "flex" as "flex",
  flexDirection: "row" as "row",
  flexGrow: 1,
  textAlign: "left" as "left"
}));

export class DetailedRestaurantViewRewards extends React.Component<{
  loaded?: boolean;
  error?: string;
  offers: string[];
  rewards: string[];
  stamps: { id: string; points: number }[];
  businessID: string;
  businessName: string;
  availablePoints: number;
  myVisits: number;
  isMember: boolean;
  redeemedPoints: number;
  tapJoinButton: () => any;

  loadContent?: (x?: Action) => any;
}> {
  componentDidMount() {
    this.update(this.props);
  }
  componentWillReceiveProps(nextProps: DetailedRestaurantViewRewards["props"]) {
    this.update(nextProps);
  }
  update = (props: DetailedRestaurantViewRewards["props"]) =>
    props.loadContent && props.loadContent();

  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    const offers = this.props.offers.map(offer => (
      <OfferTile offerID={offer} key={offer} />
    ));

    const stamps = this.props.stamps.map(stamp => (
      <StampsTile
        programID={stamp.id}
        filledStamps={stamp.points}
        key={stamp.id}
      />
    ));

    const rewards = this.props.rewards.map(reward => (
      <RewardTile
        rewardID={reward}
        pointsEarned={this.props.availablePoints}
        key={reward}
      />
    ));

    let offerSection;
    if (this.props.isMember === true) {
      offerSection = (
        <React.Fragment>
          <LeftAlignedHeaderText>
            Limited Time & Quantity Offers
          </LeftAlignedHeaderText>
          <LeftAlignedExplainerText>
            Available for a limited time only, get these excluive offers!
          </LeftAlignedExplainerText>
          {offers}
        </React.Fragment>
      );
    } else {
      offerSection = (
        <React.Fragment>
          <LeftAlignedHeaderText>
            Limited Time & Quantity Offers
          </LeftAlignedHeaderText>
          <LeftAlignedExplainerText>
            Available for a limited time only, get these excluive offers!
          </LeftAlignedExplainerText>
          {offers}
        </React.Fragment>
      );
    }

    let stampSection;
    if (this.props.isMember === true) {
      stampSection = (
        <React.Fragment>
          <LeftAlignedHeaderText>
            {this.props.businessName} Stamp Program
          </LeftAlignedHeaderText>
          <LeftAlignedExplainerText>
            Unlock rewards by every time you make a qualifying purchase
          </LeftAlignedExplainerText>
          {stamps}
        </React.Fragment>
      );
    } else {
      stampSection = (
        <React.Fragment>
          <LeftAlignedHeaderText>
            {this.props.businessName} Stamp program
          </LeftAlignedHeaderText>
          <LeftAlignedExplainerText>
            Members can also unlock rewards every time you make a qualifying
            purchase
          </LeftAlignedExplainerText>
          {stamps}
        </React.Fragment>
      );
    }

    let rewardSection;
    if (this.props.isMember === true) {
      rewardSection = (
        <React.Fragment>
          <LeftAlignedHeaderText>My Getpoints Rewards</LeftAlignedHeaderText>
          <LeftAlignedExplainerText>
            Earn points every time you check-in to unlock great rewards
          </LeftAlignedExplainerText>
          <PointStatusContainer>
            <PointStatus points={this.props.availablePoints}>
              Available
            </PointStatus>
            <PointStatus points={this.props.myVisits}>My Visits</PointStatus>
            <PointStatus end points={this.props.redeemedPoints}>
              Redeemed
            </PointStatus>
          </PointStatusContainer>
          {rewards}
        </React.Fragment>
      );
    } else {
      rewardSection = (
        <React.Fragment>
          <LeftAlignedHeaderText>Member Rewards</LeftAlignedHeaderText>
          <LeftAlignedExplainerText>
            Become a Member of the {this.props.businessName} Reward program and
            earn points every time you check-in to to unlock great rewards.
          </LeftAlignedExplainerText>
          {rewards}
        </React.Fragment>
      );
    }

    if (this.props.isMember === true) {
      return (
        <View>
          {offerSection}
          {rewardSection}
          {stampSection}
        </View>
      );
    } else {
      return (
        <View>
          <RectangularButton onTap={this.props.tapJoinButton}>
            JOIN REWARD PROGRAM
          </RectangularButton>
          {rewardSection}
          {stampSection}
          {offerSection}
        </View>
      );
    }
  }
}
