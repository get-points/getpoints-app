import React from "react";
import { RouteComponentProps } from "react-router";
import { SearchPage } from "../SearchPage";
import { Action } from "@src/actions";

export class MyStamps extends React.Component<
  {
    listings: React.ReactChild;
    triggerSearch: SearchPage["props"]["triggerSearch"];
    mapPins: SearchPage["props"]["mapPins"];
    buttons: SearchPage["props"]["buttons"];
    loadContent?: (x?: Action) => any;
    loaded?: boolean;
    error?: string;
    membershipCount: number;
  } & RouteComponentProps
> {
  componentDidMount() {
    this.update(this.props);
  }
  componentWillReceiveProps(nextProps: MyStamps["props"]) {
    this.update(nextProps);
  }
  update = (props: MyStamps["props"]) =>
    props.loadContent && props.loadContent();

  render() {
    return (
      <SearchPage
        selectedButton={1}
        baseURL="/my/stamps"
        pageTitle={"My Stamps"}
        {...this.props}
      />
    );
  }
}
