import { Action, actions } from "@src/actions";
import { RewardListing } from "@src/components/RewardListing";
import { State } from "@src/store";
import { getBusinessListByIDs } from "@src/store/business/selectors";
import { getMembershipListByIDs } from "@src/store/membership/selectors";
import { getRewardListByIDs } from "@src/store/reward/selectors";
import { listToMap } from "@src/store/selectors";
import { getOnlyUser } from "@src/store/user/selectors";
import { push } from "connected-react-router";
import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { MyStamps as Component } from "./MyStamps";
import { getStampProgramListByIDs } from "@src/store/stampProgram/selectors";
import { GETPOINTS_BLUE } from "@src/const";
import { Membership } from "@src/types/Models";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]>
): Partial<Component["props"]> & { loadAction?: Action } {
  // get current user
  const loadableUser = getOnlyUser(store);
  if (loadableUser.state === "ABSENT" || loadableUser.state === "PENDING")
    return { loaded: false };
  if (loadableUser.state === "FAILED") return { error: loadableUser.message };
  const user = loadableUser.item;

  // get current user's memberships
  const loadableMemberships = getMembershipListByIDs(store, user.memberships);
  if (loadableMemberships.state === "ABSENT")
    return {
      loaded: false,
      loadAction: actions.getMembershipList.request({
        ids: loadableMemberships.ids
      })
    };
  if (loadableMemberships.state === "PENDING") return { loaded: false };
  if (loadableMemberships.state === "FAILED")
    return { error: loadableMemberships.message };
  const memberships = loadableMemberships.item;
  const membershipMap = memberships.reduce(
    (map, m) => {
      return {
        ...map,
        ...m.stamps.reduce(
          (map, s) => ({
            ...map,
            [s.stampProgramID]: {
              membership: m,
              stampCount: s.stampCount
            }
          }),
          {} as { [s: string]: { membership: Membership; stampCount: number } }
        )
      };
    },
    {} as { [s: string]: { membership: Membership; stampCount: number } }
  );

  // get stamps from memberships
  const stampIDs = ([] as string[]).concat(
    ...loadableMemberships.item.map(m => m.stamps.map(s => s.stampProgramID))
  );
  const loadableStamps = getStampProgramListByIDs(store, stampIDs);
  if (loadableStamps.state === "ABSENT")
    return {
      loaded: false,
      loadAction: actions.getStampProgramList.request({
        ids: loadableStamps.ids
      })
    };
  if (loadableStamps.state === "PENDING") return { loaded: false };
  if (loadableStamps.state === "FAILED")
    return { error: loadableStamps.message };
  const stamps = loadableStamps.item;

  // get businesses from stamps
  const businessIDs = stamps.map(s => s.businessID);
  const loadableBusinesses = getBusinessListByIDs(store, businessIDs);
  if (loadableBusinesses.state === "ABSENT")
    return {
      loaded: false,
      loadAction: actions.getBusinessList.request({
        ids: loadableBusinesses.ids
      })
    };
  if (loadableBusinesses.state === "PENDING") return { loaded: false };
  if (loadableBusinesses.state === "FAILED")
    return { error: loadableBusinesses.message };
  const businesses = loadableBusinesses.item;
  const businessMap = listToMap(businesses);

  return {
    // pass data props from here
    listings: (
      <React.Fragment>
        {stamps.map((s, i) => (
          <RewardListing
            key={s.id}
            color={GETPOINTS_BLUE}
            businessID={businessMap[s.businessID].id}
            businessLogo={businessMap[s.businessID].profilePhoto}
            businessName={businessMap[s.businessID].name}
            rewardHeadline={s.headline}
            city={`${membershipMap[s.id].stampCount}/${
              s.stampsRequired
            } stamps earned`}
            status={
              membershipMap[s.id].stampCount >= s.stampsRequired
                ? "Unlocked"
                : "Locked"
            }
          />
        ))}
      </React.Fragment>
    ),
    mapPins: ([] as Component["props"]["mapPins"]).concat(
      ...businesses.map(p =>
        p.locations.map(l => ({
          latitude: l.lat,
          longitude: l.lng,
          description: l.description,
          title: p.name
        }))
      )
    ),
    membershipCount: user.memberships.length
  };
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  let mapButton;
  const pathMatch = props.location!.pathname.match(/(.*)\/map\/?$/);
  if (pathMatch) {
    mapButton = {
      label: "List",
      handler: () => dispatch(push("/my/stamps"))
    };
  } else {
    mapButton = {
      label: "Map",
      handler: () => dispatch(push("/my/stamps/map"))
    };
  }
  return {
    // pass functional props from here
    triggerSearch: x => dispatch(actions.getNearbyBusinesses.request(x)),

    buttons: [
      {
        label: "My Points",
        handler: () => dispatch(push("/my/rewards"))
      },
      {
        label: "My Stamps",
        handler: () => dispatch(push("/my/stamps"))
      },
      {
        label: "My Offers",
        handler: () => dispatch(push("/my/offers"))
      }
    ],
    loadContent: action => dispatch(action!)
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps,
  (storeProps, functionProps, parentProps) => {
    return {
      ...storeProps,
      ...functionProps,
      ...parentProps,
      loadContent: storeProps.loadAction
        ? () => functionProps.loadContent!(storeProps.loadAction)
        : undefined
    };
  }
)(Component as any);
