import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { MyStamps } from "./index";

export const basic = () => <MyStamps />;
