import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { ChangePasswordPage } from "./index";

export const basic = () => (
  <ChangePasswordPage
    confirmNewPassword={text("confirmNewPassword", "password")}
    newPassword={text("newPassword", "xxxxx")}
    oldPassword={text("oldPassword", "XXXXXX")}
    tapChangepasswordButton={action("tappedButton")}
  />
);
