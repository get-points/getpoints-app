import {
  ChangePasswordInputField,
  InputGroup
} from "@src/components/Typography/InputPlaceholder";
import { GETPOINTS_BACKGROUND_GREY } from "@src/const";
import React from "react";
import { View, ScrollView } from "react-native";
import glamorous from "glamorous-native";

import { HeaderBar } from "../../components/HeaderBar";
import { ChangePasswordButton } from "../../components/RectangularButton";
import { InputExplainerText } from "../../components/Typography/ExplainerText";
import { HeaderText } from "../../components/Typography/HeaderText";
import { PageHeader } from "@src/components/PageHeader";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

const ChangePasswordPageContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flex: 1,
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  height: `100%`,
  alignItems: "stretch"
}));

const PageBody = glamorous(KeyboardAwareScrollView)((props: {}) => ({
  flex: 1
}));

const InputFieldContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  alignItems: "flex-start",
  width: `100%`,
  marginTop: 10,
  marginBottom: 10
}));

export class ChangePasswordPage extends React.Component<
  {
    tapChangepasswordButton: () => any;

    loaded?: boolean;
    error?: string;
  },
  {
    oldPassword: string;
    newPassword: string;
    confirmNewPassword: string;
  }
> {
  state = {
    oldPassword: "",
    newPassword: "",
    confirmNewPassword: ""
  };

  changeOldPassword = (oldPassword: string) => this.setState({ oldPassword });
  changeNewPassword = (newPassword: string) => this.setState({ newPassword });
  changeConfirmPassword = (confirmNewPassword: string) =>
    this.setState({ confirmNewPassword });

  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <ChangePasswordPageContainer>
        <HeaderBar />
        <PageHeader>Change Password</PageHeader>
        <PageBody enableOnAndroid>
          <View style={{ padding: 20 }}>
            <InputFieldContainer>
              <InputGroup>
                <InputExplainerText>Old Password</InputExplainerText>
                <ChangePasswordInputField
                  onChange={this.changeOldPassword}
                  secureTextEntry={true}
                  children={this.state.oldPassword}
                />
              </InputGroup>
              <InputGroup>
                <InputExplainerText>New Password</InputExplainerText>
                <ChangePasswordInputField
                  onChange={this.changeNewPassword}
                  secureTextEntry={true}
                  children={this.state.newPassword}
                />
              </InputGroup>
              <InputGroup>
                <InputExplainerText>Confirm New Password</InputExplainerText>
                <ChangePasswordInputField
                  onChange={this.changeConfirmPassword}
                  secureTextEntry={true}
                  children={this.state.confirmNewPassword}
                />
              </InputGroup>
            </InputFieldContainer>
            <ChangePasswordButton
              topRightSquare
              onTap={this.props.tapChangepasswordButton}
            >
              CHANGE PASSWORD
            </ChangePasswordButton>
          </View>
        </PageBody>
      </ChangePasswordPageContainer>
    );
  }
}
