import { GETPOINTS_BACKGROUND_GREY } from "@src/const";
import React from "react";
import { ScrollView, View } from "react-native";
import glamorous from "glamorous-native";

import { HeaderBar } from "../../components/HeaderBar";
import { InPageMenuBar } from "../../components/InPageMenuBar";
import { PageHeader } from "../../components/PageHeader";
import { ExplainerText } from "../../components/Typography/ExplainerText";
import { HeaderText } from "../../components/Typography/HeaderText";
import { Loading } from "@src/components/Loading";
import { ErrorMessage } from "@src/components/ErrorMessage";

const AboutUsPageContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flex: 1,
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  width: `100%`,
  height: `100%`
}));

const MainPage = glamorous(View)((props: {}) => ({
  padding: 10,
  paddingLeft: 20,
  paddingRight: 20
}));

export class AboutUsPage extends React.Component<{
  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <AboutUsPageContainer>
        <HeaderBar />
        <PageHeader>About Us</PageHeader>
        <ScrollView>
          <InPageMenuBar selected="about" />
          <MainPage>
            <HeaderText>About Getpoints</HeaderText>
            <ExplainerText>
              Getpoints is a revolutionary loyalty app powered by mobile
              payments. Customers use the Getpoints app to pay on their
              smartphone, and automatically get a loyalty card just for your
              store. Every time customers pay using the Getpoints app, their
              loyalty points are added instantly and automatically – just with
              one tap on their smartphone!
            </ExplainerText>
          </MainPage>
        </ScrollView>
      </AboutUsPageContainer>
    );
  }
}
