import { State } from "@src/store";
import { goBack, push } from "connected-react-router";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { Dispatch } from "redux";

import { AboutUsPage } from "./AboutUsPage";

interface OwnProps extends RouteComponentProps {}

function mapStateToProps(state: State, props: OwnProps) {
  return {};
}

function mapDispatchToProps(dispatch: Dispatch, props: OwnProps) {
  return {};
}

function mergeProps(
  stateProps: ReturnType<typeof mapStateToProps>,
  dispatchProps: ReturnType<typeof mapDispatchToProps>
): AboutUsPage["props"] {
  return {};
}

export const ConnectedAboutUsPage = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(AboutUsPage);
