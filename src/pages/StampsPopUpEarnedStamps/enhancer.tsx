import { StampsPopUpEarnedStamps as Component } from "./StampsPopUpEarnedStamps";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { State } from "@src/store";
import { actions, Action } from "@src/actions";
import {} from "@src/store/selectors";
import { getStampProgramByID } from "@src/store/stampProgram/selectors";

function getStoreProps(
  store: State,
  props: Partial<Component["props"]> & {
    stampProgramID: string;
    stampsAvailable: number;
    rewardNumber: number;
  }
): Partial<Component["props"]> & { loadAction?: Action } {
  try {
    // prepare data here
    const stampProgramID = props.stampProgramID;
    const loadableStampProgram = getStampProgramByID(store, stampProgramID);
    if (loadableStampProgram.state === "ABSENT")
      return {
        loaded: false,
        loadAction: actions.getReward.request({ id: stampProgramID })
      };
    if (loadableStampProgram.state === "PENDING") return { loaded: false };
    if (loadableStampProgram.state === "FAILED") {
      return { error: loadableStampProgram.message };
    }
    const stampProgram = loadableStampProgram.item;

    return {
      // pass data props from here
      earnedStamps: props.rewardNumber,
      rewardName: stampProgram.headline,
      totalStamps: props.stampsAvailable + props.rewardNumber
    };
  } catch (thrown) {
    if (thrown === "LOADING") {
      return { loaded: false };
    } else if (typeof thrown === "string") {
      return { loaded: false, error: thrown };
    }

    throw thrown;
  }
}

function getFunctionProps(
  dispatch: Dispatch,
  props: Partial<Component["props"]>
): Partial<Component["props"]> {
  return {
    // pass functional props from here
    tapDoneButton: () => dispatch(actions.hidePopup())
  };
}

export const Container = connect(
  getStoreProps,
  getFunctionProps
)(Component as any);
