import { ErrorMessage } from "@src/components/ErrorMessage";
import { Loading } from "@src/components/Loading";
import { PopupContainer } from "@src/components/PopupContainer";
import { FixedWidthButton } from "@src/components/RectangularButton";
import { ExplainerText } from "@src/components/Typography/ExplainerText";
import { GETPOINTS_BACKGROUND_GREY } from "@src/const";
import glamorous from "glamorous-native";
import React from "react";
import { View, ViewStyle } from "react-native";

const StampsPopUpEarnedStampsContainer = glamorous(View)((props: {}) => ({}));
const Page = glamorous(View)((props: {}) => ({
  backgroundColor: GETPOINTS_BACKGROUND_GREY,
  padding: 20
}));

export class StampsPopUpEarnedStamps extends React.Component<{
  style?: ViewStyle;
  earnedStamps: number;
  totalStamps: number;
  rewardName: string;
  tapDoneButton: () => void;
  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <PopupContainer>
        <ExplainerText>
          Congratulations! you have just earned {this.props.earnedStamps} new
          stamps and you now have {this.props.totalStamps} stamps towards a{" "}
          {this.props.rewardName}!
        </ExplainerText>

        <FixedWidthButton onTap={this.props.tapDoneButton} topRightSquare>
          DONE
        </FixedWidthButton>
      </PopupContainer>
    );
  }
}
