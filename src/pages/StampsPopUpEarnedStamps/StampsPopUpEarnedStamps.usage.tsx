import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { StampsPopUpEarnedStamps } from "./index";

export const basic = () => (
  <StampsPopUpEarnedStamps
    rewardName={text("rewardName", "free coffee")}
    tapDoneButton={action("tapButton")}
    earnedStamps={number("earnedStamps", 10)}
    totalStamps={number("totalStamps", 20)}
  />
);
