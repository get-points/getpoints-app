import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { ReviewsAndRatingsHistory } from "./index";

export const basic = () => (
  <ReviewsAndRatingsHistory
    reviewTiles={[
      {
        businessName: text("businessName", "The Pantry"),
        comments: text("comments", "This is an okay place to go"),
        date: text("date", "11/09/2019"),
        rating: number("rating", 3)
      }
    ]}
  />
);
