import React from "react";
import { Text, View, ScrollView } from "react-native";
import glamorous from "glamorous-native";
import { HeaderBar } from "@src/components/HeaderBar";
import { PageHeader } from "@src/components/PageHeader";
import { ReviewTile } from "@src/components/ReviewTile";
import { GETPOINTS_BACKGROUND_GREY } from "@src/const";

const ReviewsAndRatingsHistoryContainer = glamorous(View)((props: {}) => ({
  display: "flex",
  flexDirection: "column",
  flex: 1
}));

const Page = glamorous(ScrollView)((props: {}) => ({
  padding: 10,
  backgroundColor: GETPOINTS_BACKGROUND_GREY
}));

export class ReviewsAndRatingsHistory extends React.Component<{
  reviewTiles: ReviewTile["props"][];
}> {
  render() {
    const reviews = this.props.reviewTiles.map(tile => {
      return (
        <ReviewTile
          businessName={tile.businessName}
          comments={tile.comments}
          date={tile.date}
          rating={tile.rating}
        />
      );
    });
    return (
      <ReviewsAndRatingsHistoryContainer>
        <HeaderBar />
        <PageHeader>My Reviews and Ratings</PageHeader>
        <Page>{reviews}</Page>
      </ReviewsAndRatingsHistoryContainer>
    );
  }
}
