import React from "react";
import { boolean, text, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { EarnedPointsPopUp } from "./index";

export const basic = () => (
  <EarnedPointsPopUp
    isVisible={boolean("isVisible", true)}
    businessName={text("businessName", "The Pantry")}
    earnedPoints={number("earnedPoints", 6)}
    tapDone={action("buttonTapped")}
  />
);
