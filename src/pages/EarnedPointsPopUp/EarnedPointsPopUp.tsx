import { ErrorMessage } from "@src/components/ErrorMessage";
import { Loading } from "@src/components/Loading";
import { PopupContainer } from "@src/components/PopupContainer";
import React from "react";
import { FixedWidthButton } from "../../components/RectangularButton";
import { HeaderText } from "../../components/Typography/HeaderText";

export class EarnedPointsPopUp extends React.Component<{
  businessName: string;
  earnedPoints: number;
  tapDone: () => any;

  loaded?: boolean;
  error?: string;
}> {
  render() {
    if (this.props.loaded === false) {
      return <Loading />;
    }
    if (this.props.error !== undefined) {
      return <ErrorMessage message={this.props.error} />;
    }
    return (
      <PopupContainer title={this.props.businessName}>
        <HeaderText>
          Congratulations you have just earned {this.props.earnedPoints} points!
        </HeaderText>
        <FixedWidthButton onTap={this.props.tapDone} topRightSquare>
          DONE
        </FixedWidthButton>
      </PopupContainer>
    );
  }
}
