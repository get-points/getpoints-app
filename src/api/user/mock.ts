import { API } from "./interface";
import { User } from "@src/types/Models";

const demo: User = {
  id: "1",
  address: {
    city: "Kampala",
    country: "Uganda",
    state: "Kampala"
  },
  qrCode: "http://via.placeholder.com/500?text=QR Code Goes Here",
  dateOfBirth: {
    day: "11",
    month: "3",
    year: "1990"
  },
  email: "joebloggs@getpoints.com",
  firstName: "Joe",
  gender: "MALE",
  lastName: "Bloggs",
  memberships: ["1"],
  notificationSettings: {
    email: {
      pointsEarned: true,
      purchaseMade: false
    },
    call: {
      pointsEarned: false,
      purchaseMade: true
    }
  },
  password: "",
  profilePicture: "https://via.placeholder.com/100x100?text=Joe+Bloggs",
  telephone: {
    countryCode: "+256",
    telNumber: "789 123 456"
  }
};

export const api: API = {
  getMany: async ids =>
    ids.map(id => ({
      ...demo,
      id
    })),
  getOne: async id => {
    return {
      ...demo,
      id
    };
  }
};
