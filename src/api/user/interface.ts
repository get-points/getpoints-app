import { User } from "@src/types/Models";

export interface API {
  getOne: (id: string) => Promise<User>;
  getMany: (ids: string[]) => Promise<User[]>;
}
