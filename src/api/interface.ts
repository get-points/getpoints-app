import { API as Account } from "./account/interface";
import { API as Business } from "./business/interface";
import { API as Feedback } from "./feedback/interface";
import { API as Membership } from "./membership/interface";
import { API as Offer } from "./offer/interface";
import { API as Review } from "./review/interface";
import { API as Reward } from "./reward/interface";
import { API as StampProgram } from "./stampProgram/interface";
import { API as User } from "./user/interface";

export interface API {
  account: Account;
  business: Business;
  feedback: Feedback;
  membership: Membership;
  offer: Offer;
  review: Review;
  reward: Reward;
  stampProgram: StampProgram;
  user: User;
}
