import { API } from "./interface";
import { api as account } from "./account/api";
import { api as business } from "./business/api";
import { api as feedback } from "./feedback/api";
import { api as membership } from "./membership/api";
import { api as offer } from "./offer/api";
import { api as review } from "./review/api";
import { api as reward } from "./reward/api";
import { api as stampProgram } from "./stampProgram/api";
import { api as user } from "./user/api";

export const api: API = {
  account,
  business,
  feedback,
  membership,
  offer,
  review,
  reward,
  stampProgram,
  user
};
