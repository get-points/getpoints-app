import { API } from "./interface";
import { Offer } from "@src/types/Models";

export const api: API = {
  getMany: async ids => Promise.all(ids.map(api.getOne)),
  getOne: async (id): Promise<Offer> => {
    const mocked = offers[id] || {};
    const business = {
      ...mock,
      ...mocked,
      id
    };

    return business;
  }
};

const mock: Offer = {
  id: "1",
  businessID: "2",
  description: "Offer description",
  expiry: "12/12/2012",
  headline: "Offer Headline",
  redemptionLimit: 50,
  requirement: "Offer extra requirement"
};

const offers: { [id: string]: Partial<Offer> } = {
  1: {
    businessID: "1",
    headline: "Free Pizza"
  },
  2: {
    businessID: "1",
    headline: "Reduced-price Pizza"
  },
  3: {
    businessID: "2",
    headline: "5% off washes"
  }
};
