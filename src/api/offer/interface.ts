import { Offer } from "@src/types/Models";

export interface API {
  getOne: (id: string) => Promise<Offer>;
  getMany: (ids: string[]) => Promise<Offer[]>;
}
