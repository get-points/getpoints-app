import { Reward } from "@src/types/Models";

export interface API {
  getOne: (id: string) => Promise<Reward>;
  getMany: (ids: string[]) => Promise<Reward[]>;
}
