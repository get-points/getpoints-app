import { API } from "./interface";
import { Reward } from "@src/types/Models";

const demo: Reward = {
  id: "1",
  businessID: "1",
  headline: "Reward Headline",
  pointsRequired: 5
};

export const api: API = {
  getMany: async ids =>
    ids.map(id => ({
      ...demo,
      id
    })),
  getOne: async id => {
    return {
      ...demo,
      id,
      pointsRequired: demo.pointsRequired + parseInt(id)
    };
  }
};
