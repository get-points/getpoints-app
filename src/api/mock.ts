import { API } from "./interface";
import { api as account } from "./account/mock";
import { api as business } from "./business/mock";
import { api as feedback } from "./feedback/mock";
import { api as membership } from "./membership/mock";
import { api as offer } from "./offer/mock";
import { api as review } from "./review/mock";
import { api as reward } from "./reward/mock";
import { api as stampProgram } from "./stampProgram/mock";
import { api as user } from "./user/mock";

export const api: API = {
  account,
  business,
  feedback,
  membership,
  offer,
  review,
  reward,
  stampProgram,
  user
};
