import { Review } from "@src/types/Models";

export interface API {
  getOne: (id: string) => Promise<Review>;
  getMany: (ids: string[]) => Promise<Review[]>;
}
