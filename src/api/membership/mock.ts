import { API } from "./interface";
import { User, Membership } from "@src/types/Models";

const demo: Membership = {
  id: "1",
  businessID: "1",
  history: [
    {
      date: "19/12/2018",
      pointDifference: -1,
      time: "20:08"
    }
  ],
  points: 6,
  visits: 12,
  claimedOfferIDs: ["2"],
  redeemedOfferIDs: ["1"],
  unlockedRewardIDs: ["2"],
  redeemedRewardIDs: ["2"],
  stamps: [{ stampProgramID: "1", stampCount: 24 }],
  userID: "1",
  notificationSettings: {
    email: true,
    mobile: false
  }
};

export const api: API = {
  getMany: async ids => Promise.all(ids.map(api.getOne)),
  getOne: async id => {
    return {
      ...demo,
      id,
      businessID: id
    };
  }
};
