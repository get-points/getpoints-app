import { Membership } from "@src/types/Models";

export interface API {
  getOne: (id: string) => Promise<Membership>;
  getMany: (ids: string[]) => Promise<Membership[]>;
}
