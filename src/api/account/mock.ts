import { API } from "./interface";

export const api: API = {
  async sendSupportRequest(message, email, user) {
    if (message === "fail") throw new Error("Could not send message");
    // mock an auto success
  },

  async signUp({
    firstName,
    lastName,
    password,
    confirmPassword,
    email,
    countryCode,
    telephone
  }) {
    return Promise.resolve({
      id: "1"
    });
  },

  async signIn(username, password) {
    return Promise.resolve({ token: "", id: "1" });
  },
  async signOut(): Promise<void> {}
};
