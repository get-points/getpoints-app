export interface API {
  sendSupportRequest(
    message: string,
    email: string,
    user: string
  ): Promise<void>;

  signUp(x: {
    firstName: string;
    lastName: string;
    password: string;
    confirmPassword: string;
    email: string;
    countryCode: string;
    telephone: string;
  }): Promise<{ id: string }>;

  signOut(): Promise<void>;
  signIn(
    username: string,
    password: string
  ): Promise<{ token: string; id: string }>;
}
