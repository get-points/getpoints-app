import { API } from "./interface";

class RealAPI implements API {
  token?: string;

  async sendSupportRequest(message: string, email: string, user: string) {
    throw new Error("Not implemented yet");
  }

  async signUp(x: {
    firstName: string;
    lastName: string;
    password: string;
    confirmPassword: string;
    email: string;
    countryCode: string;
    telephone: string;
  }): Promise<{ id: string }> {
    throw new Error("Not implemented yet");
  }

  async signIn(
    username: string,
    password: string
  ): Promise<{ token: string; id: string }> {
    throw new Error("Not implemented yet");
    // TODO: store the token here so we can re-use it in future requests
    this.token = "";
  }
  async signOut(): Promise<void> {}
}

export const api: API = new RealAPI();
