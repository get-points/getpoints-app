import { API } from "./interface";
import { StampProgram } from "@src/types/Models";

const demo: StampProgram = {
  id: "1",
  stampsRequired: 10,
  businessID: "1",
  headline: "Free Drink",
  description: "Get a free drink after 10 stamps"
};
export const api: API = {
  getMany: async ids => Promise.all(ids.map(api.getOne)),
  getOne: async id => {
    return {
      ...demo,
      id,
      businessID: id
    };
  }
};
