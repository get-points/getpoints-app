import { StampProgram } from "@src/types/Models";

export interface API {
  getOne: (id: string) => Promise<StampProgram>;
  getMany: (ids: string[]) => Promise<StampProgram[]>;
}
