import { Feedback } from "@src/types/Models";

export interface API {
  getOne: (id: string) => Promise<Feedback>;
  getMany: (ids: string[]) => Promise<Feedback[]>;
}
