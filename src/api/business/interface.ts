import { Business } from "@src/types/Models";

export interface API {
  getOne: (id: string) => Promise<Business>;
  getMany: (ids: string[]) => Promise<Business[]>;
  getNearby: (
    filters: {
      latitude: number;
      longitude: number;
      country?: string;
      city?: string;
      type?: string;
      text?: string;
    }
  ) => Promise<{ id: string; location: number }[]>;
}
