import { API } from "./interface";
import { Business } from "@src/types/Models";
function distance(
  one: { lat: number; lng: number },
  two: { lat: number; lng: number }
) {
  const toRadians = (x: number) => (x * Math.PI) / 180;
  const earthRadius = 3958.75;
  const latDiff = toRadians(two.lat - one.lat);
  const lngDiff = toRadians(two.lng - one.lng);
  const a =
    Math.sin(latDiff / 2) * Math.sin(latDiff / 2) +
    Math.cos(toRadians(one.lat)) *
      Math.cos(toRadians(two.lat)) *
      Math.sin(lngDiff / 2) *
      Math.sin(lngDiff / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const distance = earthRadius * c;

  const meterConversion = 16.09;

  return Math.round(distance * meterConversion) / 10;
}

export const api: API = {
  getMany: async ids => {
    const i = await new Promise<Position>((y, n) => {
      navigator.geolocation.getCurrentPosition(y, n);
    });
    return Promise.all(
      ids.map(async id => {
        const res = await (api.getOne as any)(id, i);

        return res;
      })
    ).then(x => {
      return x;
    });
  },
  getOne: async (id, i?: Position): Promise<Business> => {
    if (i === undefined) {
      i = await new Promise<Position>((y, n) => {
        navigator.geolocation.getCurrentPosition(y, n);
      });
    }
    const mocked = businesses[id] || {};
    const business = {
      ...demo,
      ...mocked,
      id
    };
    business.locations = business
      .locations!.map(l => ({
        ...l,
        lat: i!.coords.latitude + Math.random() * 0.01 - 0.05,
        lng: i!.coords.longitude + Math.random() * 0.01 - 0.05
      }))
      .map(loc => ({
        ...loc,
        distance: distance(loc, {
          lat: i!.coords.latitude,
          lng: i!.coords.longitude
        })
      }));

    return business;
  },
  getNearby: async filter => {
    return [{ id: "1", location: 0 }, { id: "2", location: 0 }];
  }
};

const demo: Business = {
  id: "1",
  name: "Chwa II Restaurant",
  profilePhoto: "https://via.placeholder.com/100?text=Business+1",
  coverPhoto: "https://placeimg.com/640/480/arch",
  offerText: "5% Off",
  contacts: [
    {
      type: "ADDRESS",
      text: "Plot 31, Chwa II Road, Kampala"
    },
    {
      type: "PHONE",
      text: "+256 778 893 000"
    },
    {
      type: "WEBSITE",
      text: "https://chwa-ii.com"
    },
    {
      type: "EMAIL",
      text: "chwa.II@gmail.com"
    }
  ],
  type: "Restaurant",
  description: "The best resturant this side of the Mbuya Cathedral",
  hours: {
    monday: { start: "08:00", end: "21:00" },
    tuesday: { start: "08:00", end: "21:00" },
    wednesday: { start: "08:00", end: "21:00" },
    thursday: { start: "08:00", end: "21:00" },
    friday: { start: "08:00", end: "21:00" },
    saturday: { start: "08:00", end: "21:00" },
    sunday: { start: "", end: "" }
  },
  images: [
    "http://placekitten.com/100/100",
    "http://placekitten.com/110/110",
    "http://placekitten.com/120/120",
    "http://placekitten.com/130/130",
    "http://placekitten.com/140/140",
    "http://placekitten.com/150/150"
  ],
  locations: [
    {
      country: "Uganda",
      city: "Kampala",
      description: "Chwa II Road, Kampala",
      lat: 0.3275142,
      lng: 32.626405000000005
    },
    {
      country: "Uganda",
      city: "Kampala",
      description: "Kololo, Kampala",
      lat: 0.331251,
      lng: 32.592782
    },
    {
      country: "Uganda",
      city: "Entebbe",
      description: "Kololo, Kampala",
      lat: 0.050478,
      lng: 32.460726
    },
    {
      country: "Kenya",
      city: "Nairobi",
      description: "Kololo, Kampala",
      lat: 1.29054,
      lng: 36.8126703
    }
  ],
  membershipRewards: ["1", "2", "3"],
  offers: ["1"],
  stampPrograms: ["1", "2", "3"],
  reviews: [],
  rating: {
    average: 4.45,
    count: 42
  }
};

let name1 = "Delicious Pizza";
let name2 = "Hot Suds";
const businesses: { [id: string]: Partial<Business> } = {
  1: {
    type: "Restaurant",
    name: name1,
    coverPhoto: "https://placeimg.com/640/480/arch",
    profilePhoto: "https://placeimg.com/200/200/people",
    locations: [
      {
        city: "Kampala",
        country: "Uganda",
        lat: 0.003,
        lng: 0.003,
        description: "Plot 31, Chwa II Road, Kampala"
      },
      {
        city: "Kampala",
        country: "Uganda",
        lat: 0.014,
        lng: 0.002,
        description: "Plot 32, Chwa II Road, Kampala"
      }
    ].map(loc => ({
      ...loc,
      distance: distance(loc, { lat: 0, lng: 0 })
    })),
    offers: ["1", "2"]
  },
  2: {
    type: "Spa",
    name: name2,
    coverPhoto: "https://placeimg.com/640/480/arch",
    profilePhoto: "https://placeimg.com/200/200/people",
    locations: [
      {
        city: "Kampala",
        country: "Uganda",
        lat: 0.006,
        lng: 0.001,
        description: "Location 3"
      },
      {
        city: "Kampala",
        country: "Uganda",
        lat: 0.0002,
        lng: 0.0002,
        description: "Location 3"
      }
    ].map(loc => ({
      ...loc,
      distance: distance(loc, { lat: 0, lng: 0 })
    })),
    offers: ["3"]
  }
};
