
// Auto-generated file created by react-native-storybook-loader
// Do not edit.
//
// https://github.com/elderfo/react-native-storybook-loader.git

function loadStories() {
  require('../src/components/AboutLaunchPage/AboutLaunchPage.story');
  require('../src/components/AboutUsPage/AboutUsPage.story');
  require('../src/components/Banner/Banner.story');
  require('../src/components/BusinessHeading/BusinessHeading.story');
  require('../src/components/BusinessListing/BusinessListing.story');
  require('../src/components/BusinessListingExpanded/BusinessListingExpanded.story');
  require('../src/components/BusinessListingHeader/BusinessListingHeader.story');
  require('../src/components/BusinessLogo/BusinessLogo.story');
  require('../src/components/ChangePasswordPage/ChangePasswordPage.story');
  require('../src/components/CornerBanner/CornerBanner.story');
  require('../src/components/CoverBusinessInfo/CoverBusinessInfo.story');
  require('../src/components/CoverPhoto/CoverPhoto.story');
  require('../src/components/DetailedRestaurantViewRewards/DetailedRestaurantViewRewards.story');
  require('../src/components/DropDownMenu/DropDownMenu.story');
  require('../src/components/EarnedPointsPopUp/EarnedPointsPopUp.story');
  require('../src/components/FeedbackCollectionPopUp/FeedbackCollectionPopUp.story');
  require('../src/components/HeaderBar/HeaderBar.story');
  require('../src/components/Icon/Icon.story');
  require('../src/components/InputPaceholderText/InputPaceholderText.story');
  require('../src/components/InputTextfield/InputTextfield.story');
  require('../src/components/MenuIcon/MenuIcon.story');
  require('../src/components/OfferTile/OfferTile.story');
  require('../src/components/PageHeader/PageHeader.story');
  require('../src/components/Placeholder/Placeholder.story');
  require('../src/components/PointStatusBox/PointStatusBox.story');
  require('../src/components/RatingsBar/RatingsBar.story');
  require('../src/components/RectangularButton/RectangularButton.story');
  require('../src/components/ReviewTile/ReviewTile.story');
  require('../src/components/RewardTile/RewardTile.story');
  require('../src/components/RewardTileHeader/RewardTileHeader.story');
  require('../src/components/SmallButton/SmallButton.story');
  require('../src/components/SmallPointStatusBox/SmallPointStatusBox.story');
  require('../src/components/StampsTile/StampsTile.story');
  require('../src/components/StarRating/StarRating.story');
  require('../src/components/TableHeader/TableHeader.story');
  require('../src/components/TableRow/TableRow.story');
  require('../src/components/TestComponent/TestComponent.story');
  require('../src/components/Typography/ExplainerText/ExplainerText.story');
  require('../src/components/Typography/H4/H4.story');
  require('../src/components/Typography/H5/H5.story');
  require('../src/components/Typography/H6/H6.story');
  require('../src/components/Typography/HeaderText/HeaderText.story');
  require('../src/components/Typography/InputPlaceholder/InputPlaceholder.story');
  require('../src/components/Typography/TitleText/TitleText.story');
  require('../src/components/WhiteButton/WhiteButton.story');
  require('../src/components/WhiteButtonSmall/WhiteButtonSmall.story');
  
}

const stories = [
  '../src/components/AboutLaunchPage/AboutLaunchPage.story',
  '../src/components/AboutUsPage/AboutUsPage.story',
  '../src/components/Banner/Banner.story',
  '../src/components/BusinessHeading/BusinessHeading.story',
  '../src/components/BusinessListing/BusinessListing.story',
  '../src/components/BusinessListingExpanded/BusinessListingExpanded.story',
  '../src/components/BusinessListingHeader/BusinessListingHeader.story',
  '../src/components/BusinessLogo/BusinessLogo.story',
  '../src/components/ChangePasswordPage/ChangePasswordPage.story',
  '../src/components/CornerBanner/CornerBanner.story',
  '../src/components/CoverBusinessInfo/CoverBusinessInfo.story',
  '../src/components/CoverPhoto/CoverPhoto.story',
  '../src/components/DetailedRestaurantViewRewards/DetailedRestaurantViewRewards.story',
  '../src/components/DropDownMenu/DropDownMenu.story',
  '../src/components/EarnedPointsPopUp/EarnedPointsPopUp.story',
  '../src/components/FeedbackCollectionPopUp/FeedbackCollectionPopUp.story',
  '../src/components/HeaderBar/HeaderBar.story',
  '../src/components/Icon/Icon.story',
  '../src/components/InputPaceholderText/InputPaceholderText.story',
  '../src/components/InputTextfield/InputTextfield.story',
  '../src/components/MenuIcon/MenuIcon.story',
  '../src/components/OfferTile/OfferTile.story',
  '../src/components/PageHeader/PageHeader.story',
  '../src/components/Placeholder/Placeholder.story',
  '../src/components/PointStatusBox/PointStatusBox.story',
  '../src/components/RatingsBar/RatingsBar.story',
  '../src/components/RectangularButton/RectangularButton.story',
  '../src/components/ReviewTile/ReviewTile.story',
  '../src/components/RewardTile/RewardTile.story',
  '../src/components/RewardTileHeader/RewardTileHeader.story',
  '../src/components/SmallButton/SmallButton.story',
  '../src/components/SmallPointStatusBox/SmallPointStatusBox.story',
  '../src/components/StampsTile/StampsTile.story',
  '../src/components/StarRating/StarRating.story',
  '../src/components/TableHeader/TableHeader.story',
  '../src/components/TableRow/TableRow.story',
  '../src/components/TestComponent/TestComponent.story',
  '../src/components/Typography/ExplainerText/ExplainerText.story',
  '../src/components/Typography/H4/H4.story',
  '../src/components/Typography/H5/H5.story',
  '../src/components/Typography/H6/H6.story',
  '../src/components/Typography/HeaderText/HeaderText.story',
  '../src/components/Typography/InputPlaceholder/InputPlaceholder.story',
  '../src/components/Typography/TitleText/TitleText.story',
  '../src/components/WhiteButton/WhiteButton.story',
  '../src/components/WhiteButtonSmall/WhiteButtonSmall.story',
  
];

module.exports = {
  loadStories,
  stories,
};
