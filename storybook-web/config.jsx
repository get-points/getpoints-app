import { configure, addDecorator, storiesOf } from "@storybook/react";
import { configureViewport } from "@storybook/addon-viewport";
import { withKnobs } from "@storybook/addon-knobs";
import { withNotes } from "@storybook/addon-notes";
import React from "react";
import { storeComponent } from "./store";

const components = require.context(
  "../src/components/",
  true,
  /.*\.story\.tsx$/
);
const pages = require.context("../src/pages/", true, /.*\.story\.tsx$/);

function loadStories() {
  pages.keys().forEach(pages);
  components.keys().forEach(components);
}

configureViewport({
  defaultViewport: "iphone5"
});

// render every component with the full application store
addDecorator(story => storeComponent(story()));
addDecorator(withNotes);
addDecorator(withKnobs);

configure(loadStories, module);
