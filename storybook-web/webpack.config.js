const path = require("path");
const appDirectory = path.resolve(__dirname, "../");

const babelLoaderConfiguration = {
  test: /\.js$/,
  // Add every directory that needs to be compiled by Babel during the build.
  include: [
    path.resolve(appDirectory, "src"),
    // path.resolve(appDirectory, "node_modules/react-router-native"),
    // path.resolve(appDirectory, "node_modules/glamorous-native"),
    path.resolve(appDirectory, "node_modules/react-native-web-maps")
    // path.resolve(appDirectory, "node_modules/@expo/samples"),
    // path.resolve(appDirectory, "node_modules/@expo/vector-icons")
  ],
  use: {
    loader: "babel-loader",
    options: {
      // cacheDirectory: false,
      babelrc: false,
      // Babel configuration (or use .babelrc)
      plugins: [
        "expo-web",
        "react-native-web",
        "transform-decorators-legacy",
        // ["@babel/plugin-proposal-decorators", { legacy: true }],
        // "transform-decorators-legacy",
        [
          "transform-runtime",
          { helpers: false, polyfill: false, regenerator: true }
        ]
      ],
      // The 'react-native' preset is recommended to match React Native's packager
      presets: ["react-native"]
    }
  }
};

module.exports = (baseConfig, configType, config) => {
  config.resolve = {
    modules: ["node_modules"],
    extensions: [".web.js", ".js", ".json", ".web.jsx", ".jsx", ".tsx", ".ts"],
    alias: {
      "react-native": "react-native-web",
      "glamorous-native": "glamorous",
      "@storybook/react-native": "@storybook/react",
      "@expo/vector-icons": "expo-web",
      "@src": path.resolve(__dirname, "../src/"),
      "@tyro": path.resolve(__dirname, "../src/tyro"),
      "react-native-maps": path.resolve(__dirname, "../src/tyro/native-maps")
    }
  };
  config.module.rules.push(babelLoaderConfiguration);
  config.module.rules.push({
    test: /\.(ts|tsx)$/,
    loader: require.resolve("ts-loader"),
    options: {
      reportFiles: ["src/**/*.{ts,tsx,js}"]
    }
  });
  // config.plugins.push(new TSDocgenPlugin()); // optional
  config.resolve.extensions.push(".ts", ".tsx");
  return config;
};
